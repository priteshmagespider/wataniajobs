package com.fnrco.wataniajobs.di

import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.ApiConstant
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {


    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Singleton
    @Provides
    fun provideOkHttPClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        prefManager: PrefManager
    ): OkHttpClient {
        return OkHttpClient().newBuilder()
            .addNetworkInterceptor(httpLoggingInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor { chain: Interceptor.Chain ->
                val request: Request =
                    chain.request()
                        .newBuilder()
                        .addHeader(
                            "Authorization",
                            "Bearer " + prefManager.getString(PrefConstant.PREF_ACCESS_TOKEN)
                        )
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .build()

                return@addInterceptor chain.proceed(request)
            }
            .build()
    }

    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(ApiConstant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Singleton
    @Provides
    fun provideApiInterface(retrofit: Retrofit.Builder, okHttpClient: OkHttpClient): ApiInterface {
        return retrofit
            .client(okHttpClient)
            .build()
            .create(ApiInterface::class.java)
    }
}