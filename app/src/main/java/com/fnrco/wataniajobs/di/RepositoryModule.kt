package com.fnrco.wataniajobs.di

import com.fnrco.wataniajobs.repository.*
import com.fnrco.wataniajobs.retrofit.ApiInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun homeRepository(
        apiInterface: ApiInterface,
    ): HomeRepository {
        return HomeRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun providesSignUpRepository(
        apiInterface: ApiInterface,
    ): SignUpRepository {
        return SignUpRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun providesLoginRepository(
        apiInterface: ApiInterface,
    ): LoginRepository {
        return LoginRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun providesForgotPasswordRepository(
        apiInterface: ApiInterface,
    ): ForgotPasswordRepository {
        return ForgotPasswordRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun findJobsRepository(
        apiInterface: ApiInterface,
    ): FindJobsRepository {
        return FindJobsRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun providesMoreScreenRepository(
        apiInterface: ApiInterface,
    ): MoreScreenRepository {
        return MoreScreenRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun providesCallUsRepository(
        apiInterface: ApiInterface,
    ): CallUsRepository {
        return CallUsRepository(apiInterface)
    }

    @Singleton
    @Provides
    fun providesNotificationRepository(
        apiInterface: ApiInterface,
    ): NotificationRepository {
        return NotificationRepository(apiInterface)
    }


    @Singleton
    @Provides
    fun editProfileRepository(
        apiInterface: ApiInterface,
    ): EditProfileRepository {
        return EditProfileRepository(apiInterface)
    }

}
