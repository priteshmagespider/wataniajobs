package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.response.DeleteJobPostResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.NotificationListResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.SaveDeletePhoneEmailResponse
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class NotificationRepository
constructor(
    private val apiInterface: ApiInterface
) {
    /** Notification */
    suspend fun notificationListApiCall(
        lang: String
    ): Flow<DataState<NotificationListResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.notificationList(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Notification Delete */
    suspend fun deleteNotificationListApiCall(
        notification_id: String
    ): Flow<DataState<DeleteJobPostResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.deleteNotification(
                notification_id
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }
}