package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.LoginResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.UpdatePlayerResponseBean
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException

class LoginRepository
constructor(private val apiInterface: ApiInterface) {

    /** Login Api Call */

    suspend fun loginApiCall(authRequest: AuthRequest): Flow<DataState<LoginResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.logIn(
                authRequest.grant_type!!,
                authRequest.client_id!!,
                authRequest.client_secret!!,
                authRequest.username!!,
                authRequest.password!!,
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    /** Update Player Api Call */
    suspend fun updatePlayer(player_id_s:String): Flow<DataState<UpdatePlayerResponseBean>> =
        flow {
            emit(DataState.Loading)


            Utils.print("Repository_player_id--login------------------>$player_id_s")

            val player_id: RequestBody =
                player_id_s!!.toRequestBody("multipart/form-data".toMediaTypeOrNull()) as RequestBody


            try {
                val response = apiInterface.updatePlayer(
                    player_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }
}