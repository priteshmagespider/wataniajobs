package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class HomeRepository
constructor(private var apiInterface: ApiInterface) {

    /** Player photo */
    suspend fun cityList(
        language: String,
        city: Int,
        jobTitle: String
    ): Flow<DataState<SearchListResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.cityList(language, city, jobTitle)
            if (response.posts != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    /** Recent Search List */
    suspend fun recentSearchList(
        language: String
    ): Flow<DataState<RecentSearchListResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.recentSearchList(language)
            if (response.posts != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    /** Recent Search List */
    suspend fun autoBiographyList(
        language: String
    ): Flow<DataState<AutoBioGraphyResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.autoBiographyList(language)
            if (response.posts != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun roleList(city: String): Flow<DataState<JobRoleResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.roleList(city)
            if (response.categories != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun categoriesList(city: String): Flow<DataState<SpecializationResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.specializationsList(city)
            if (response.specializations != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun jobPostApplied(post_id: Int): Flow<DataState<JobPostApplyResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.jobPostApplied(post_id)
            if (response.message != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun addFavoriteJob(job_id: Int): Flow<DataState<FavoriteJobResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.addFavoriteJob(job_id)
            if (response.message != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun removeFavoriteJob(job_id: Int): Flow<DataState<RemoveFavoriteJobResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.removeFavoriteJob(job_id)
            if (response.message != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun cityList(language: String): Flow<DataState<CityListResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.cityList(language)
            if (response.cities != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun singlePost(language: String, postId: Int): Flow<DataState<SinglePostResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.singlePost(language, postId)
                if (response.post != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    suspend fun topJobs(language: String, page : Int): Flow<DataState<TopJobsResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.topJobs(language, page)
            if (response.posts != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Home Banner */
    suspend fun homeBannerApiCall(language: String): Flow<DataState<WeAreHiringResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.homeBanner(language)
                emit(DataState.Success(response))
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

}