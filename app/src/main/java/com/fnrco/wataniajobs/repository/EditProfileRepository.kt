package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import java.io.File

class EditProfileRepository
constructor(private var apiInterface: ApiInterface) {

    /** Language List Api Call*/
    suspend fun languageListApiCall(
        lang: String,
    ): Flow<DataState<LanguageListResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.languageList(
                    lang
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Delete Phone/Email API call */
    suspend fun deletePhoneEmailApiCall(
        id: String,
    ): Flow<DataState<SaveDeletePhoneEmailResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deletePhoneEmail(
                    id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Show Phone API call */
    suspend fun saveDeletePhoneEmailApiCall(
        phone: String,
        type: String
    ): Flow<DataState<SaveDeletePhoneEmailResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.phoneEmailSaveDelete(
                    phone, type
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Show Phone API call */
    suspend fun showPhoneListApiCall(): Flow<DataState<PhoneListResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.phoneList("phone")
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Show Phone API call */
    suspend fun showEmailListApiCall(): Flow<DataState<PhoneListResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.emailList("email")
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /** Send Verify OTP  Api Call */
    suspend fun sendVerifyOtpForPhoneApiCall(authRequest: AuthRequest): Flow<DataState<SendVerifyOtpResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.sendVerifyOtpForPhone(
                    authRequest.phone!!,
                    authRequest.code!!,
//                    authRequest.is_admin!!,
                    authRequest.type!!
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Degree Qualification Api call */
    suspend fun qualificationList(lang: String): Flow<DataState<QualificationResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.qualificationList(lang)
            if (response.qualifications != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun basicDataCategoriesList(city: String): Flow<DataState<BasicDataSpecializationResponse>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.basicDataCategoriesList(city)
                if (response.specializations != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    suspend fun jobPostAppliedList(language: String): Flow<DataState<AppliedJobResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.jobPostAppliedList(language)
                if (response.posts != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    suspend fun expiredJobApplicationList(language: String): Flow<DataState<ExpiredJobResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.expiredJobApplicationList(language)
                if (response.posts != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    suspend fun trainingCoursesList(language: String): Flow<DataState<TrainingCoursesMoreResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.trainingCoursesList(language)
                if (response.trainings != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    suspend fun categoriesList(city: String): Flow<DataState<CategoriesListResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.categoriesList(city)
            if (response.categories != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun cityList(language: String): Flow<DataState<CityListResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.cityList(language)
            if (response.cities != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun deleteJobPost(job_post_id: Int): Flow<DataState<DeleteJobPostResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.deleteJobPost(job_post_id)
            if (response != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun deleteTrainingCourses(job_post_id: Int): Flow<DataState<DeleteJobPostResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteTrainingCourses(job_post_id)
                if (response != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    suspend fun cvExcellence(): Flow<DataState<CvExcellenceResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.cvExcellence()
            if (response != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun profileDataList(language: String): Flow<DataState<ProfileResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.profileDataList(language)
            if (response.user != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Personal Info Edit */
    suspend fun personalEditApiCall(
        address: String,
        city: String,
        gender: String,
        birthDate: String,
        Dependents: String,
        marital_status: String,
        experience: String,
        salary: String,
    ): Flow<DataState<EditPersonalInfoResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.personalInfoEdit(
                    address,
                    city,
                    gender,
                    birthDate,
                    Dependents,
                    marital_status,
                    experience,
                    salary,
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /** Communication Data edit Api call */
    suspend fun communicationDataApiCall(
        email: String,
        phone: String

    ): Flow<DataState<EditCommunicationDataResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.communicationDataEdit(
                    email,
                    phone
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Add Education Api call */
    suspend fun addEducationApiCall(
        educational_institution: String,
        general_appreciation: String,
        specialization: String,
        degree: String,
        end_date_educational: String,
        description_education: String,
        id: Int
    ): Flow<DataState<EditEducationResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addEducation(
                    educational_institution,
                    general_appreciation,
                    specialization,
                    degree,
                    end_date_educational,
                    description_education,
                    id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Edit Education Api call */
    suspend fun editEducationApiCall(
        educational_institution: String,
        general_appreciation: String,
        specialization: String,
        degree: String,
        end_date_educational: String,
        description_education: String,
        id: String
    ): Flow<DataState<EditEducationResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.editEducation(
                    educational_institution,
                    general_appreciation,
                    specialization,
                    degree,
                    end_date_educational,
                    description_education,
                    id,
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Delete Education Api call */
    suspend fun deleteEducationApiCall(
        edu_id: String
    ): Flow<DataState<DeleteEducationResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteEducation(
                    edu_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /** Add Experience Api call */
    suspend fun addExperienceApiCall(
        position_in_company: String,
        company_name: String,
        location: String,
        start_jop_M: String,
        start_jop_Y: String,
        end_jop_M: String,
        end_jop_Y: String,
        to_present: String,
        description_job: String
    ): Flow<DataState<AddEditDeleteExperienceResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addExperience(
                    position_in_company,
                    company_name,
                    location,
                    start_jop_M,
                    start_jop_Y,
                    end_jop_M,
                    end_jop_Y,
                    to_present,
                    description_job,
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Delete Experience Api call */
    suspend fun deleteExperienceApiCall(
        exp_id: String
    ): Flow<DataState<AddEditDeleteExperienceResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteExperience(
                    exp_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Edit Experience Api call */
    suspend fun editExperienceApiCall(
        position_in_company: String,
        company_name: String,
        location: String,
        start_jop_M: String,
        start_jop_Y: String,
        end_jop_M: String,
        end_jop_Y: String,
        to_present: String,
        description_job: String,
        id: Int,
    ): Flow<DataState<AddEditDeleteExperienceResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.editExperience(
                    position_in_company,
                    company_name,
                    location,
                    start_jop_M,
                    start_jop_Y,
                    end_jop_M,
                    end_jop_Y,
                    to_present,
                    description_job,
                    id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /** Add Edit Short Description Api call */
    suspend fun addEditExperienceApiCall(
        career_objective: String
    ): Flow<DataState<EditEducationResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addEditShortDescription(
                    career_objective
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Add Delete Skill Api call */
    suspend fun addSkillApiCall(
        skills_name: String
    ): Flow<DataState<AddDeleteSkillResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addSkill(
                    skills_name
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Delete Skill Api call */
    suspend fun deleteSkillApiCall(
        skill_id: String
    ): Flow<DataState<AddDeleteSkillResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteSkill(
                    skill_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /**  Add Interest Api call */
    suspend fun addInterestApiCall(
        interest: String
    ): Flow<DataState<AddDeleteInterestResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addInterest(
                    interest
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Delete Interest Api call */
    suspend fun deleteInterestApiCall(
        interest_id: String
    ): Flow<DataState<AddDeleteInterestResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteInterest(
                    interest_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /**  Add Courses Api call */
    suspend fun addCoursesApiCall(
        address: String,
        center_name: String,
        action_date: String,
        count_times: String,
        description: String,
        logo: String
    ): Flow<DataState<AddEditDeleteCoursesResponseBean>> =
        flow {
            emit(DataState.Loading)
            val file: File = File(logo)

            val requestFile: RequestBody =
                file.asRequestBody("multipart/form-data".toMediaTypeOrNull())

            val rLogo =
                MultipartBody.Part.createFormData("image", file.name, requestFile);

            val rAddress: RequestBody =
                address.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val rCenterName: RequestBody =
                center_name.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val actionDate: RequestBody =
                action_date.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val countTimes: RequestBody =
                count_times.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val rDescription: RequestBody =
                description.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            try {
                val response = apiInterface.addCourses(
                    rAddress,
                    rCenterName,
                    actionDate,
                    countTimes,
                    rDescription,
                    rLogo
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Edit Courses Api call */
    suspend fun editCoursesApiCall(
        address: String,
        center_name: String,
        action_date: String,
        count_times: String,
        description: String,
        course_id: Int,
        logo: String
    ): Flow<DataState<AddEditDeleteCoursesResponseBean>> =
        flow {
            emit(DataState.Loading)
            lateinit var mLogo: MultipartBody.Part

            val file: File = File(logo)
            if (logo.isNotEmpty()) {
                val requestFile: RequestBody =
                    file.asRequestBody("multipart/form-data".toMediaTypeOrNull())

                mLogo =
                    MultipartBody.Part.createFormData("image", file.name, requestFile);
            }


            val rAddress: RequestBody =
                address.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val rCenterName: RequestBody =
                center_name.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val actionDate: RequestBody =
                action_date.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val countTimes: RequestBody =
                count_times.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val rDescription: RequestBody =
                description.toRequestBody("multipart/form-data".toMediaTypeOrNull())
            val courseId: RequestBody =
                course_id.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())

            try {
                if (logo.isNotEmpty()) {
                    val response = apiInterface.editCourses(
                        rAddress,
                        rCenterName,
                        actionDate,
                        countTimes,
                        rDescription,
                        courseId,
                        mLogo
                    )
                    emit(DataState.Success(response))
                } else {
                    val response = apiInterface.editCoursesWithoutLogo(
                        rAddress,
                        rCenterName,
                        actionDate,
                        countTimes,
                        rDescription,
                        courseId,
                    )
                    emit(DataState.Success(response))
                }
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Delete Courses Api call */
    suspend fun deleteCoursesApiCall(
        course_id: Int
    ): Flow<DataState<AddEditDeleteCoursesResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteCourse(
                    course_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Add Certificate Api call */
    suspend fun addCertificateApiCall(
        title: String,
        from_by: String,
        month: String,
        year: String,
        description: String
    ): Flow<DataState<AddEditDeleteCertificateResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addCertificate(
                    title,
                    from_by,
                    month,
                    year,
                    description
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Edit Certificate Api call */
    suspend fun editCertificateApiCall(
        title: String,
        from_by: String,
        month: String,
        year: String,
        description: String,
        certificate_id: Int
    ): Flow<DataState<UpdateCertificateResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.editCertificate(
                    title,
                    from_by,
                    month,
                    year,
                    description,
                    certificate_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /**  Delete Certificate Api call */
    suspend fun deleteCertificateApiCall(

        certificate_id: Int
    ): Flow<DataState<AddEditDeleteCertificateResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteCertificate(
                    certificate_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Add Project Api call */
    suspend fun addProjectApiCall(
        title: String,
        month: String,
        year: String,
        description: String
    ): Flow<DataState<AddEditDeleteProjectResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.addProject(
                    title,
                    month,
                    year,
                    description
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Edit project Api call */
    suspend fun editProjectApiCall(
        title: String,
        month: String,
        year: String,
        description: String,
        project_id: Int
    ): Flow<DataState<AddEditDeleteProjectResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.editProject(
                    title,
                    month,
                    year,
                    description,
                    project_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Delete project Api call */
    suspend fun deleteProjectApiCall(
        project_id: Int
    ): Flow<DataState<AddEditDeleteProjectResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteProject(
                    project_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Computer Level list*/
    suspend fun computerLevelListApiCall(lang: String): Flow<DataState<ComputerLevelResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.computerLevelList(
                    lang
                )
                emit(DataState.Success(response))
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /**  Add Language Api call */
    suspend fun addLanguageApiCall(
        language: String,
        level: String
    ): Flow<DataState<LanguageAddDeleteResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.saveLanguage(
                    language,
                    level
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Delete Language Api call */
    suspend fun deleteLanguageApiCall(
        id: Int
    ): Flow<DataState<LanguageAddDeleteResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.deleteLanguage(
                    id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /** SignUp Api Call */
    suspend fun uploadCV(fileCv: String): Flow<DataState<UploadCvResponseBean>> =
        flow {
            emit(DataState.Loading)
            val file: File = File(fileCv!!)

            val requestFile: RequestBody =
                RequestBody.Companion.create("multipart/form-data".toMediaTypeOrNull(), file)

            val fileCv =
                MultipartBody.Part.createFormData("file", file.name, requestFile);



            try {
                val response = apiInterface.uploadCV(
                    fileCv
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    /**  Social Update Api call */
    suspend fun socialUpdateApiCall(
        fb: String,
        twitter: String,
        linkedIn: String,
        website: String,
        youtube: String,
        instagram: String
    ): Flow<DataState<SocialResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.updateSocial(
                    fb,
                    twitter,
                    linkedIn,
                    website,
                    youtube,
                    instagram
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /**  Basic data Update Api call */
    suspend fun basicDataUpdateApiCall(
        name: String,
        job_title_en: String,
        job_title: String,
        specilization: Int,
        jobRole: Int,
        job_status: Int
    ): Flow<DataState<BasicDataUpdateResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.updateBasicData(
                    name,
                    job_title_en,
                    job_title,
                    specilization,
                    jobRole,
                    job_status
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Update User Profile */
    suspend fun updateUserImageApiCall(
        photo: String
    ): Flow<DataState<BasicDataUpdateResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val file: File = File(photo)

            val requestFile: RequestBody =
                file.asRequestBody("multipart/form-data".toMediaTypeOrNull())

            val photo =
                MultipartBody.Part.createFormData("photo", file.name, requestFile);


            val response = apiInterface.updateUserImage(
                photo
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }
}