package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.ChangePasswordResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.ResetPasswordResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.SendVerifyOtpResponseBean
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class ForgotPasswordRepository
constructor(
    private val apiInterface: ApiInterface
) {

    /** Send Verify OTP  Api Call */
    suspend fun sendVerifyOtpForPhoneApiCall(authRequest: AuthRequest): Flow<DataState<SendVerifyOtpResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.sendVerifyOtpForPhone(
                    authRequest.phone!!,
                    authRequest.code!!,
//                    authRequest.is_admin!!,
                    authRequest.type!!
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }




    /** Send Verify OTP  Api Call */
    suspend fun sendVerifyOtpForEmail(authRequest: AuthRequest): Flow<DataState<SendVerifyOtpResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.sendVerifyOtpForEmail(
                    authRequest.phone!!,
                    authRequest.code!!,
//                    authRequest.is_admin!!,
                    authRequest.type!!
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Reset Password  Api Call */
    suspend fun resetPasswordApiCall(authRequest: AuthRequest): Flow<DataState<ResetPasswordResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.resetPassword(
                    authRequest.password!!,
                    authRequest.email!!,
                    authRequest.is_admin!!
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Change Password */
    suspend fun changePasswordApiCall(
        curPassword: String,
        newPassword: String
    ): Flow<DataState<ChangePasswordResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.changePassword(
                    curPassword, newPassword
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

}