package com.fnrco.wataniajobs.repository

import android.util.Log
import com.fnrco.wataniajobs.mvvm.model.request.SearchRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class FindJobsRepository
constructor(private var apiInterface: ApiInterface) {


    suspend fun cityList(language: String): Flow<DataState<CityListResponse>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.cityList(language)
            if (response.cities != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun singlePost(language: String, postId: Int): Flow<DataState<SinglePostResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.singlePost(language, postId)
                if (response.post != null) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


    suspend fun addFavoriteJob(job_id: Int): Flow<DataState<FavoriteJobResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.addFavoriteJob(job_id)
            if (response.message != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    suspend fun removeFavoriteJob(job_id: Int): Flow<DataState<RemoveFavoriteJobResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.removeFavoriteJob(job_id)
            if (response.message != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }





    suspend fun categoriesList(city: String): Flow<DataState<SpecializationResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.specializationsList(city)
            if (response.specializations != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun favouriteJobsList(city: String): Flow<DataState<FavouriteJobResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.favouriteJobsList(city)
            if (response.posts != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun jobTypeList(city: String): Flow<DataState<JobTypeResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.jobTypeList(city)
            if (response.jobsType != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun roleList(city: String): Flow<DataState<JobRoleResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.roleList(city)
            if (response.categories != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }


    suspend fun qualificationList(city: String): Flow<DataState<QualificationResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.qualificationList(city)
            if (response.qualifications != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Find jobs */
    suspend fun searchList(
        language: String,
        jobTitle: String,
        city: Int,
        jobCat: Int,
        subCat: Int,
        from: Int,
        to: Int,
        jobType: List<Int>,
        gender: Int,
        certificate: Int,
        created_at: Int,
        page : Int

    ): Flow<DataState<SearchListResponseBean>> = flow {
        emit(DataState.Loading)
            val map : HashMap<String, Int> = HashMap()
        for(i in jobType.indices) {
            map["jobType[$i]"] = jobType[i]
        }
        Log.d("TAG", "searchList: $map")

        try {
            val response = apiInterface.searchList(
                language,
                jobTitle,
                city,
                jobCat,
                subCat,
                from,
                to,
                jobType,
                gender,
                certificate,
                created_at,
                page

            )
            if (response.posts != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

}