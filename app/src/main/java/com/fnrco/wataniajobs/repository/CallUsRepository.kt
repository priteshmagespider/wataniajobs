package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.response.CallUsResponseBean
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class CallUsRepository
constructor(
    private val apiInterface: ApiInterface
) {

    /**Call us/Contact us Api call */

    suspend fun callUsApiCall(
        name: String,
        email: String,
        message: String,
        type: Int
    ): Flow<DataState<CallUsResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.callUsApiCall(
                name,
                email,
                message,
                type
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }
}