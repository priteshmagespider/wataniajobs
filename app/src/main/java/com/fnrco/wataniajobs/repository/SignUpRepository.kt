package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import java.io.File

class SignUpRepository
constructor(private val apiInterface: ApiInterface) {

    suspend fun jobTitleList(city: String): Flow<DataState<JobRoleResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.roleList(city)
            if (response.categories != null) {
                emit(DataState.Success(response))
            } else {
                emit(DataState.CustomException("Exception"))
            }

        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }



    /** Send Verify OTP  Api Call */
    suspend fun sendVerifyOtpForPhoneApiCall(authRequest: AuthRequest): Flow<DataState<SendVerifyOtpResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.sendVerifyOtpForPhone(
                    authRequest.phone!!,
                    authRequest.code!!,
//                    authRequest.is_admin!!,
                    authRequest.type!!
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }




    /** Send Verify OTP  Api Call */
    suspend fun sendVerifyOtpForEmail(authRequest: AuthRequest): Flow<DataState<SendVerifyOtpResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.sendVerifyOtpForEmail(
                    authRequest.phone!!,
                    authRequest.code!!,
//                    authRequest.is_admin!!,
                    authRequest.type!!
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }
    /** SignUp Api Call */
    suspend fun signUpApiCall(authRequest: AuthRequest): Flow<DataState<SignUpResponseBean>> =
        flow {
            emit(DataState.Loading)
            val file: File = File(authRequest.fileCv!!)

            val requestFile: RequestBody =
                RequestBody.Companion.create("multipart/form-data".toMediaTypeOrNull(), file)

            val fileCv =
                MultipartBody.Part.createFormData("fileCv", file.name, requestFile);


            val name: RequestBody =
                authRequest.name!!.toRequestBody("multipart/form-data".toMediaTypeOrNull()) as RequestBody

            val email: RequestBody =
                authRequest.email!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val password: RequestBody =
                authRequest.password!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val phone: RequestBody =
                authRequest.phone!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val job_title: RequestBody =
                authRequest.job_title!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val job_title_en: RequestBody =
                authRequest.job_title_en!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())

            val gender: RequestBody =
                authRequest.gender!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
            try {
                val response = apiInterface.signUp(
                    name,
                    email,
                    password,
                    phone,
                    job_title,
                    job_title_en,
                    gender,
                    fileCv
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }




    /** Update Player Api Call */
    suspend fun updatePlayer(player_id_s:String): Flow<DataState<UpdatePlayerResponseBean>> =
        flow {
            emit(DataState.Loading)


            Utils.print("Repository_player_id--signup------------------>$player_id_s")
            val player_id: RequestBody =
                player_id_s!!.toRequestBody("multipart/form-data".toMediaTypeOrNull()) as RequestBody



            try {
                val response = apiInterface.updatePlayer(
                    player_id
                )
                emit(DataState.Success(response))

            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }
}