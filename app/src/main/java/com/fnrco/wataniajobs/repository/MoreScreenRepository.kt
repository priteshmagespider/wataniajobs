package com.fnrco.wataniajobs.repository

import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.retrofit.ApiInterface
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class MoreScreenRepository
constructor(
    private val apiInterface: ApiInterface
) {

    /** About Api Call */
    suspend fun aboutCompanyApiCall(lang: String): Flow<DataState<WhoWeAreResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.aboutCompany(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Speech Api Call */
    suspend fun speechApiCall(lang: String): Flow<DataState<WhoWeAreResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.speech(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** vision goals Api Call */
    suspend fun visionGoalsApiCall(lang: String): Flow<DataState<WhoWeAreResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.visionGoals(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Clients List Api Call */
    suspend fun clientListApiCall(lang: String): Flow<DataState<ClientsListResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.clientList(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** More Screen Service Response */
    suspend fun moreServiceList(lang: String): Flow<DataState<MoreScreenServiceResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.moreServices(lang)
                emit(DataState.Success(response))
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** PrivacyPolicy */
    suspend fun privacyPolicyApiCall(lang: String): Flow<DataState<CMSPageResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.privacyPolicy(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** Terms Condition */
    suspend fun termsConditionApiCall(lang: String): Flow<DataState<CMSPageResponseBean>> = flow {
        emit(DataState.Loading)
        try {
            val response = apiInterface.termsCondition(
                lang
            )
            emit(DataState.Success(response))
        } catch (e: HttpException) {
            emit(DataState.Error(e))
        }
    }

    /** More ISO training Courses */
    suspend fun moreTrainingCoursesApiCall(lang: String): Flow<DataState<TrainingCoursesMoreResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.moreTrainingCourses(
                    lang
                )
                emit(DataState.Success(response))
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }

    /** Register training course */
    suspend fun registerTrainingCourseApiCall(trainingId: Int): Flow<DataState<RegisterTrainingCourse>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.registerCourse(
                    trainingId
                )
                emit(DataState.Success(response))
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }
}