package com.fnrco.wataniajobs.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.ui.activity.SplashActivity
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Utils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import javax.inject.Inject


class MyFirebaseMessagingService : FirebaseMessagingService() {

    var mTAG = "MyFirebaseMessagingService"
    @Inject
    lateinit var prefManager: PrefManager
    override fun onNewToken(token: String) {
        Log.d(mTAG, "onNewToken: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Utils.print("Notification body-> ${remoteMessage.data}")
        sendNotification(remoteMessage)

//        remoteMessage.notification!!.title?.let { sendNotification(it, remoteMessage.data) }
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {

        /*val pendingIntent: PendingIntent
        val intent = Intent(this, SplashActivity::class.java)
        for (key in remoteMessage.keys) {
            val value: Any? = remoteMessage[key]
            intent.putExtra(key, value.toString())
            Utils.print("Notification body value->"+value.toString())
            Log.d(
                "TAG", String.format(
                    "%s %s (%s)", key,
                    value.toString(), value!!.javaClass.name
                )
            )
        }*/

        val intent = Intent(this, SplashActivity::class.java)
        for (key in remoteMessage.data.keys) {
            val value: Any? = remoteMessage.data[key]
            Utils.print("Notification body value->"+value.toString())
            intent.putExtra(key, value.toString())
            Log.d(
                "TAG", String.format(
                    "%s %s (%s)", key,
                    value.toString(), value!!.javaClass.name
                )
            )
        }
//        val title = remoteMessage.notification!!.title
//        intent.putExtra(Constant.INTENT.APPOINTMENT_ID, remoteMessage.data["appointment_id"])
//        intent.putExtra(Constant.INTENT.DOCTOR_NAME, remoteMessage.data["doctor_name"])
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(
            this, 1, intent,
            PendingIntent.FLAG_ONE_SHOT
        )


        /*Utils.print("CUSTOMDATA------>"+ remoteMessage.data["custom"])
        val root = JSONObject(remoteMessage.data["custom"])
        Utils.print("CUSTOMDATA----root-->"+ root.getString("a"))
        val root1 = JSONObject(root.getString("a"))
        Utils.print("CUSTOMDATA----post_id-->"+ root1.getString("post_id"))

        intent.putExtra("NOTIFICATIONS_DATA", root1.getString("type"))*/
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val defaultSoundUri =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        /*pendingIntent = PendingIntent.getService(
            this, 0  *//*Request code *//*, intent,
            PendingIntent.FLAG_ONE_SHOT
        )*/


         /*pendingIntent = PendingIntent.getActivity(
            applicationContext, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )*/
        val channelId = getString(R.string.default_notification_channel_id)

         val notification = NotificationCompat.Builder(this)
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             notification.setSmallIcon(R.drawable.img_job_splash_logo)
             notification.setColor(resources.getColor(R.color.color_primary)).setContentTitle(
                 getString(
                     R.string.app_name
                 )
             )
                 .setAutoCancel(true)
                 .setSound(defaultSoundUri)
                 .setContentIntent(pendingIntent)
         } else {
             notification.setSmallIcon(R.drawable.img_job_splash_logo).setContentTitle(getString(R.string.app_name))
                 .setAutoCancel(true)
                 .setSound(defaultSoundUri)
                 .setContentIntent(pendingIntent)
         }


        /*val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.transperent_logo)
                .setContentTitle(getString(R.string.app_name))
                .setChannelId(channelId) //                        .setColor(ContextCompat.getColor(this, R.color.background_gray))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)*/

        /*val notificationBuilder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.img_logo_smart_health)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)*/
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        assert(notificationManager != null)
        notificationManager.notify(1 /* ID of notification */, notification.build())
    }



}