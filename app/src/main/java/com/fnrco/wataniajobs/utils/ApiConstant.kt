package com.fnrco.wataniajobs.utils

object ApiConstant {

    //    const val BASE_URL = "http://3.131.142.167/api/"
//    const val BASE_URL = "https://demo1.wataniajobs.com/api/"
//    const val BASE_URL = "http://wataniajobs.com/"
    const val BASE_URL = "https://wataniajobs.com/api/"
    const val SEARCH_LIST = "search"


    const val JOB_TYPE_LIST = "job/type"
    const val DELETE_JOB_POSTS = "post/delete"
    const val DELETE_TRAINING_COURSES = "delete/training"
    const val CV_EXCELLENCE = "cv/excellence"
    const val LOG_IN = "oauth/token"
    const val SIGN_UP = "register/jobskeer"
    const val UPDATE_PLAYER = "update/player"
    const val UPLOAD_CV = "upload"
    const val SEND_VERIFY_OTP = "check/mail"
    const val RECENT_SEARCH = "show/recent/searches/belongsTo/user"
    const val RECENT_JOBS = "posts"
    const val AUTOBIOGRAPHY = "get/Jobs/BaseOnCv"

    const val SINGLE_POST = "single/post"
    const val TOP_JOBS = "top/jobs"
    const val CITY_LIST = "cities"
    const val SPECIALIZATIONS = "specializations"
    const val FAVOURITE_JOBS = "show/favourite/belongsTo/user"
    const val CATEGORIES_API = "categories"
//    const val JOB_POST_APPLIED = "jobPost/apply"
    const val JOB_POST_APPLIED = "jop/post/apply"
    const val ADD_FAVORITE_JOB = "add/favourite/job"
    const val REMOVE_FAVORITE_JOB = "remove/favourite/job"
    const val ROLE = "categories"
    const val QUALIFICATION = "qualifications"
    const val FORGOT_PASSWORD = "forget/password"

    const val ABOUT_COMPANY = "about/"

    const val VISION_MISSION = "vision/mission/"

    const val SPEECH = "speech/"

    const val CLIENTS = "Clients/category/"

    const val MORE_SCREEN_SERVICES = "services/"

    const val PRIVACY_POLICY = "privacy/policy/"

    const val TERM_CONDITION = "term/service/"

    const val CONTACT_US = "contact"

    const val NOTIFICATION = "getNotifications/"

    const val CHANGE_PASSWORD = "changePassword"
    const val TRAINING_COURSES = "show/all/training"
    const val TRAINING_COURSES_PROFILE = "training"
    const val USER_TRAINING_COURSE = "apply/training/user/"
    const val EXPIRED_JOB_APPLICATION = "jobPosts/applied/expired"

    const val PROFILE = "profile/"
    const val EDIT_PROFILE_INFO = "/editprofile/info"

    const val EDIT_PERSONAL_INFO = "editprofile/info"

    const val EDIT_COMMUNICATION_DATA = "editprofile/contact"
    const val JOB_POSTS_APPLIED = "jobPosts/applied"

    const val ADD_EDUCATION = "education/add"

    const val EDIT_EDUCATION = "education/edit"

    const val DELETE_EDUCATION = "education/delete"

    const val ADD_EXPERIENCE = "experience/add"

    const val DELETE_EXPERIENCE = "experience/delete"

    const val EDIT_EXPERIENCE = "experience/update"

    const val ADD_EDIT_SHORT_DESCRIPTION = "edit/target"

    const val ADD_SKILL = "skills/add"

    const val DELETE_SKILL = "skill/delete"

    const val ADD_INTEREST = "save/interest"

    const val DELETE_INTEREST = "delete/interest"

    const val ADD_COURSES = "save/course"

    const val EDIT_COURSES = "update/course"

    const val DELETE_COURSE = "delete/course"

    const val ADD_CERTIFICATE = "save/certificate"

    const val EDIT_CERTIFICATE = "update/certificate"

    const val DELETE_CERTIFICATE = "delete/certificate"

    const val ADD_PROJECT = "save/project"

    const val EDIT_PROJECT = "update/project"

    const val DELETE_PROJECT = "delete/project"

    const val COMPUTER_LEVEL = "computer/level/"

    const val SAVE_LANGUAGE = "languages/store"

    const val DELETE_LANGUAGE = "languages/delete"

    const val UPDATE_SOCIAL = "update/social"

    const val UPDATE_BASIC_INFO = "edit/basic/info"

    const val MORE_TRAINING_COURSES = "show/all/training/"

    const val REGISTER_TRAINING_COURSE = "apply/training"

    const val HOME_BANNER = "last/specializations/"

    const val SHOW_PHONES = "show/phones"

    const val SAVE_DELETE_PHONE_EMAIL = "save/phones"

    const val DELETE_PHONE_EMAIL = "delete/phones"

    const val LANGUAGE_LIST = "langs/"

    const val DELETE_NOTIFICATION = "delete/notifications"
}