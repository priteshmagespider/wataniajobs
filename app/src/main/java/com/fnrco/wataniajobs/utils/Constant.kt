package com.fnrco.wataniajobs.utils

object Constant {

    const val APP_NAME = "Wataniajobs"
    const val TITLE = "TITLE"

//    const val IMAGE_URL_WITH_PHOTOS = "https://demo1.wataniajobs.com/photos/"
    const val IMAGE_URL_WITH_PHOTOS = "https://wataniajobs.com/photos/"
//    const val IMAGE_URL_WITHOUT_PHOTOS = "https://demo1.wataniajobs.com/"
    const val IMAGE_URL_WITHOUT_PHOTOS = "https://wataniajobs.com/"
//    const val IMAGE_URL_WITH_LOGOS = "https://demo1.wataniajobs.com/logos/"
    const val IMAGE_URL_WITH_LOGOS = "https://wataniajobs.com/logos/"
//    const val CV_URL = "https://demo1.wataniajobs.com/cvs/"
    const val CV_URL = "https://wataniajobs.com/cvs/"

    var RANDOM_IMAGE_USER = "https://i.picsum.photos/id/1011/5472/3648.jpg"
//    const val IMAGE_URL = "https://demo1.wataniajobs.com/exLogos/"
    const val IMAGE_URL = "https://wataniajobs.com/exLogos/"

    const val GRANT_TYPE = "password"
//    const val CLIENT_ID = "2"
    const val CLIENT_ID = "6"
    const val IS_ADMIN = "2" // For Job Seeker is_admin = 2
//    const val CLIENT_SECRET = "L937c29RKAMNlyX59evzgMpdwVNKSMdVGSZtAejv"
    const val CLIENT_SECRET = "nGQKUnLBw5FyifjDOxwzvO4uusFtnO3f2M20esSL"
    const val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
    const val CITY_ID = "CITY_ID"
    const val USE_TYPE = "user"
//    const val USE_TYPE = "company"
    const val CITY_NAME = "CITY_NAME"
    const val HOME_JOB_TITLE = "JOB_TITLE"
    const val JOB_TITLE_FILTER = "JOB_TITLE_FILTER"
    const val PROFILE_DATA = "PROFILE_DATA"
    const val FILTER_POST_DATA = "POST_DATA"
    const val SPECIALITY_DATA_TITLE = "SPECIALITY_DATA_TITLE"
    const val SPECIALITY_DATA_COUNT = "SPECIALITY_DATA_COUNT"
    const val SPECIALITY_CITY_ID = "SPECIALITY_CITY_ID"
    const val HOME_LIST_POST_DATA = "HOME_LIST_POST_DATA"
    const val POST_ID = "POST_ID"
    const val SEARCH_DATA = "SEARCH_DATA"
    const val HOME_FILTER = "HOME_FILTER"


    interface INTENT {
        companion object {
            const val EMAIL = "EMAIL"
            const val CODE = "CODE"
            const val PHONE_NO = "PHONE_NO"
            const val VERIFICATION_TO_FORGOT_FOR_PHONE = "VERIFICATION_TO_FORGOT_FOR_PHONE"
            const val VERIFICATION_TO_FORGOT_FOR_EMAIL = "VERIFICATION_TO_FORGOT_FOR_EMAIL"
            const val FORGOT_TO_VERIFICATION_FOR_PHONE = "FORGOT_TO_VERIFICATION_FOR_PHONE"
            const val FORGOT_TO_VERIFICATION_FOR_EMAIL = "FORGOT_TO_VERIFICATION_FOR_EMAIL"
            const val SIGN_UP_TO_VERIFICATION_FOR_PHONE = "SIGN_UP_TO_VERIFICATION_FOR_PHONE"
            const val LOGIN_TO_FORGOT_FOR_PHONE = "LOGIN_TO_FORGOT_FOR_PHONE"
            const val INTENT_FROM_SIGN_UP_TO_VERIFY = "INTENT_FROM_SIGN_UP_TO_VERIFY"
            const val INTENT_FROM_FORGOT_PASSWORD_TO_VERIFY =
                "INTENT_FROM_FORGOT_PASSWORD_TO_VERIFY"
            const val INTENT_FROM_VERIFY_TO_RESET_PASSWORD = "INTENT_FROM_VERIFY_TO_RESET_PASSWORD"

            const val SERVICE_TYPE = "SERVICE_TYPE"

            const val ABOUT = "ABOUT"
            const val SPEECH = "SPEECH"
            const val VISION_GOALS = "VISION_GOALS"

            const val PRIVACY_POLICY = "PRIVACY_POLICY"
            const val TERMS_CONDITION = "TERMS_CONDITION"

            const val BOTTOM_FIND_JOB = "BOTTOM_FIND_JOB"
        }

    }

}