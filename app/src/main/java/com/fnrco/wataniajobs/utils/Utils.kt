package com.fnrco.wataniajobs.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.WindowInsets
import android.view.WindowInsetsController
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.ui.activity.SplashActivity
import com.google.android.material.snackbar.Snackbar
import org.jsoup.Jsoup
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object Utils {

    private val mTAG = Utils::class.java.simpleName

    /* fun fullScreen(activity: Activity) {
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
             activity.window.insetsController?.hide(WindowInsets.Type.statusBars())
         } else {
             activity.window.setFlags(
                 WindowManager.LayoutParams.FLAG_FULLSCREEN,
                 WindowManager.LayoutParams.FLAG_FULLSCREEN
             )
         }
     }*/
     fun html2text(html: String?): String? {
        return Jsoup.parse(html).text()
    }

    fun fullScreen(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            activity.window.setDecorFitsSystemWindows(false)
            activity.window.insetsController?.let {
                it.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
                it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        } else {
            @Suppress("DEPRECATION")
            activity.window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION)
        }
    }


    fun setDateTimeField(editText: EditText, context: Context?) {
        val pYear: Int
        val pMonth: Int
        val pDay: Int
        val arrayDate: Array<String>
        if (!editText.text.toString().isEmpty()) {
            val date = editText.text.toString().trim { it <= ' ' }
            arrayDate = date.split("-".toRegex()).toTypedArray()
            pDay = arrayDate[0].trim { it <= ' ' }.toInt()
            pMonth = String.format("%02d", arrayDate[1].trim { it <= ' ' }.toInt()).toInt()
            pYear = String.format("%02d", arrayDate[2].trim { it <= ' ' }.toInt()).toInt()
        } else {
            val newDate = Calendar.getInstance()
            pYear = newDate[Calendar.YEAR]
            pMonth = newDate[Calendar.MONTH] + 1
            pDay = newDate[Calendar.DAY_OF_MONTH]
        }
        val dateDialog = DatePickerDialog(
            context!!,
            { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate[year, monthOfYear] = dayOfMonth
                editText.setText(
                    StringBuilder().append((if (dayOfMonth < 10) "0" else "") + dayOfMonth)
                        .append("-").append(
                            (if (monthOfYear + 1 < 10) "0" else "") + (monthOfYear + 1)
                        ).append("-").append((if (year < 10) "0" else "") + year).append("")
                )
            }, pYear, pMonth - 1, pDay
        )
        dateDialog.datePicker.maxDate = Date().time
        Utils.print("date::" + Date().time)
        dateDialog.show()
    }

    fun padInt(i: Int): String? {
        return if (i < 10) {
            "0$i"
        } else {
            "" + i
        }
    }

    fun checkLanguage(): String {
        val language: String = Locale.getDefault().toLanguageTag();
        return language
    }

    fun getFloatFromDimen(context: Context, dimen: Int): Float {
        val outValue = TypedValue()
        context.resources.getValue(dimen, outValue, true)
        return outValue.float
    }
    fun hideKeyBoardFromView(context: Context) {
        val inputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        // Find the currently focused view, so we can grab the correct window
        // token from it.
        var view = (context as Activity).currentFocus
        // If no view currently has focus, create a new one, just so we can grab
        // a window token from it
        if (view == null) {
            view = View(context)
        }
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
    fun parseDateToddMMyyyy(time: String?, inputPattern: String?, outputPattern: String?): String? {

//        inputPattern = "yyyy-MM-dd HH:mm:ss";
//        outputPattern = "dd-MMM-yyyy h:mm a";
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun print(mesg: String) {
        if (true) {
            println(mesg)
        }
    }

    fun print(title: String, mesg: String) {
        if (true) {
            print("$title :: $mesg")
        }
    }

    fun showAlertDialog(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.app_name)
        builder.setMessage(message)
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    fun hideKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSnackBar(view: View, context: Context) {
        val snackBar = Snackbar.make(
            view, context.getString(R.string.check_internet_connection),
            Snackbar.LENGTH_LONG
        ).setAction("OK", null)
        snackBar.setActionTextColor(Color.WHITE)
//        snackBar.changeFont()
        val snackBarView = snackBar.view
        snackBarView.background = context.getDrawable(R.drawable.snackbar_bg)
        val textView =
            snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        snackBar.show()
    }

//    fun Snackbar.changeFont() {
//        val tv = view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
//        val font = Typeface.create("neo_tech_std_medium.ttf", context.resources.R.style.TextView_style_medium)
//        tv.typeface = font
//    }

    fun isConnected(context: Context): Boolean {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> {
                    false
                }
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> {
                            false
                        }
                    }

                }
            }
        }

        return result
    }

    fun closeKeyBoard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun gmttoLocalDate(date: Date): Date? {
        val timeZone = Calendar.getInstance().timeZone.id
        return Date(date.time + TimeZone.getTimeZone(timeZone).getOffset(date.time))
    }




     fun setLocate(Lang: String, context: Context) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()

        config.locale = locale
        context.resources.updateConfiguration(
            config,
            context.resources.displayMetrics
        )

        val editor = context.getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()
        editor.putString("My_Lang", Lang)
        editor.apply()
    }

    public fun loadLocate(context: Context) {
        val sharedPreferences =
            context.getSharedPreferences("Settings", Activity.MODE_PRIVATE)
        val language = sharedPreferences.getString("My_Lang", "")
        language?.let { setLocate(it, context) }
    }


    fun covertTimeToText(dataDate: String?, context: Context): String? {
        var convTime: String? = null
        val prefix = ""
        val suffix = context.getString(R.string.ago)
        try {
            val dateFormat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val pasTime = dateFormat.parse(dataDate)
            val nowTime = Date()
            val dateDiff = nowTime.time - pasTime.time
            val second: Long = TimeUnit.MILLISECONDS.toSeconds(dateDiff)
            val minute: Long = TimeUnit.MILLISECONDS.toMinutes(dateDiff)
            val hour: Long = TimeUnit.MILLISECONDS.toHours(dateDiff)
            val day: Long = TimeUnit.MILLISECONDS.toDays(dateDiff)
            when {
                second == 0.toLong() -> {
                    convTime = context.getString(R.string.just_now)
                }
                second < 60 -> {
                    var seconds= context.getString(R.string.seconds)
                    convTime = "$second $seconds $suffix"
                }
                minute < 60 -> {
                    var minutes = context.getString(R.string.minutes)
                    convTime = "$minute $minutes $suffix"
                }
                hour < 24 -> {
                    var hours = context.getString(R.string.hours)
                    convTime = "$hour $hours $suffix"
                }
                day >= 7 -> {
                    convTime = if (day > 360) {
                        (day / 360).toString() +" "+ context.getString(R.string.years)+ " " + suffix
                    } else if (day > 30) {
                        (day / 30).toString() + " " +context.getString(R.string.months)+" "+ suffix
                    } else {
                        (day / 7).toString() + " " +context.getString(R.string.weeks)+" " + suffix
                    }
                }
                day < 7 -> {
                    var days = context.getString(R.string.days)
                    convTime = "$day $days $suffix"
                }
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("ConvTimeE", e.message!!)
        }
        return convTime
    }

    fun setLocale(context: Context, localeName: String, currentLanguage: String) {
        if (localeName != currentLanguage) {
            val locale: Locale = Locale(localeName)
            val res = context.resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.locale = locale
            res.updateConfiguration(conf, dm)
            val configuration: Configuration = context.resources.configuration

            configuration.setLayoutDirection(Locale(localeName))
            context.resources.updateConfiguration(
                configuration,
                context.resources.displayMetrics
            )

//            if (localeName == "ar") {
//                configuration.setLayoutDirection(Locale("ar"))
//                context.resources.updateConfiguration(
//                    configuration,
//                    context.resources.displayMetrics
//                )
//            } else {
//                configuration.setLayoutDirection(Locale("en"))
//                context.resources.updateConfiguration(
//                    configuration,
//                    context.resources.displayMetrics
//                )
//            }

            val refresh = Intent(
                context,
                SplashActivity::class.java
            )
            refresh.putExtra("CURRENT_LANG", localeName)
            context.startActivity(refresh)

        } else {
            Toast.makeText(
                context, "Language already selected)!", Toast.LENGTH_SHORT
            ).show();
        }
    }
        fun switchToCzLocale(context: Context, localeName: String, currentLanguage: String) {
        if (localeName != currentLanguage) {
            val locale: Locale = Locale(localeName)
            val res = context.resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.locale = locale
            res.updateConfiguration(conf, dm)
            val configuration: Configuration = context.resources.configuration

            configuration.setLayoutDirection(Locale(localeName))
            context.resources.updateConfiguration(
                configuration,
                context.resources.displayMetrics
            )

        } else {
            Toast.makeText(
                context, "Language already selected)!", Toast.LENGTH_SHORT
            ).show();
        }
    }

    fun getTimeAgo(timeAtMiliseconds: Date, ctx: Context): String? {
        var timeAtMiliseconds = timeAtMiliseconds
        timeAtMiliseconds = Utils.gmttoLocalDate(timeAtMiliseconds)!!
        //API.log("Day Ago "+dayago);
        val result = ctx.getString(R.string.now)
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val todayDate = formatter.format(Date())
        val calendar = Calendar.getInstance()
        val dayagolong = timeAtMiliseconds.time
        calendar.timeInMillis = dayagolong
        val agoformater = formatter.format(timeAtMiliseconds.time)
        var CurrentDate: Date? = null
        var CreateDate: Date? = null
        try {
            CurrentDate = formatter.parse(todayDate)
            CreateDate = formatter.parse(agoformater)
            var different = Math.abs(CurrentDate.time - CreateDate.time)
            val secondsInMilli: Long = 1000
            val minutesInMilli = secondsInMilli * 60
            val hoursInMilli = minutesInMilli * 60
            val daysInMilli = hoursInMilli * 24
            val elapsedDays = different / daysInMilli
            different = different % daysInMilli
            val elapsedHours = different / hoursInMilli
            different = different % hoursInMilli
            val elapsedMinutes = different / minutesInMilli
            different = different % minutesInMilli
            val elapsedSeconds = different / secondsInMilli
            different = different % secondsInMilli
            if (elapsedDays == 0L) {
                if (elapsedHours == 0L) {
                    if (elapsedMinutes == 0L) {
                        if (elapsedSeconds < 0) {
                            return "0" + " s"
                        } else {
                            if (elapsedDays > 0 && elapsedSeconds < 59) {
                                return ctx.getString(R.string.now)
                            }
                        }
                    } else {
                        return elapsedMinutes.toString() + ctx.getString(R.string._m_ago)
                    }
                } else {
                    return elapsedHours.toString() + ctx.getString(R.string._h_ago)
                }
            } else {
                val formatterYear = SimpleDateFormat("dd MMMM yyyy")
                val calendarYear = Calendar.getInstance()
                calendarYear.timeInMillis = dayagolong
                return formatterYear.format(calendarYear.time) + ""
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return result
    }


}