package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.DataTopJobs
import com.fnrco.wataniajobs.mvvm.model.response.PostsTopJobs
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.Utils
import com.github.islamkhsh.CardSliderAdapter
import com.google.android.material.card.MaterialCardView

class TopJobsDetailsAdapter(
    val context: Context,
    val onClickListener: View.OnClickListener,
//    val list: List<DataTopJobs>,
    val prefManager: PrefManager
) : CardSliderAdapter<TopJobsDetailsAdapter.MyViewHolder>() {

    lateinit var listData: ArrayList<DataTopJobs>
//    val listData: MutableList<DataTopJobs> = list as MutableList<DataTopJobs>
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): TopJobsDetailsAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_top_job_item, parent, false)
        )
    }
    fun addData(list: List<DataTopJobs>, isNew: Boolean) {
        if (isNew) {
            listData = ArrayList()
        }
        listData.addAll(list)
    }


    override fun getItemCount(): Int {
        return listData.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMain: MaterialCardView = itemView.findViewById(R.id.clMain)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtJobCity: TextView = itemView.findViewById(R.id.txtJobCity)
        val txtLocation: TextView = itemView.findViewById(R.id.txtLocation)
        val txtJobType: TextView = itemView.findViewById(R.id.txtJobType)
        val txtAgoTime: TextView = itemView.findViewById(R.id.txtAgoTime)
        val imgLogo: ImageView = itemView.findViewById(R.id.imgLogo)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {

        holder.imgLogo.clipToOutline = true
        holder.clMain.tag = position
        holder.clMain.setOnClickListener(onClickListener)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
            holder.txtTitle.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtJobCity.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtJobType.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtAgoTime.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtLocation.textDirection = View.TEXT_DIRECTION_LTR

            holder.txtTitle.text = listData[position].title_en ?: listData[position].title
            holder.txtJobCity.text = if(listData[position].job_city!=null) listData[position].job_city!!.name_en else ""
            holder.txtJobType.text = listData[position].job__type!!.title_en
            holder.txtAgoTime.text =
                Utils.covertTimeToText(listData[position].created_at, context)
        } else {

            holder.txtTitle.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtJobCity.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtJobType.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtAgoTime.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtLocation.textDirection = View.TEXT_DIRECTION_RTL

            holder.txtTitle.text = listData[position].title
            holder.txtJobCity.text = if(listData[position].job_city!=null) listData[position].job_city!!.name_ar else ""
            holder.txtJobType.text = listData[position].job__type!!.title_ar
            holder.txtAgoTime.text =
                Utils.covertTimeToText(listData[position].created_at, context)
        }
        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${listData[position].company_logo}")
            .placeholder(R.drawable.img_home)
            .into(holder.imgLogo)

        holder.txtLocation.text = listData[position].company?.address

    }
}