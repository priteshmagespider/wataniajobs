package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.utils.Constant
import com.github.islamkhsh.CardSliderAdapter

class ShareListAdapter() : CardSliderAdapter<ShareListAdapter.MyViewHolder>() {

    lateinit var context: Context
    lateinit var onClickListener: View.OnClickListener
//    lateinit var list: ArrayList<DataMyCoins>


    constructor(context: Context, onClickListener: View.OnClickListener) : this() {
        this.onClickListener = onClickListener
        this.context = context
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): ShareListAdapter.MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.list_share, parent, false))
    }

    override fun getItemCount(): Int {
        return 5
    }

    /*fun addData(listNew: List<DataMyCoins>, isNew: Boolean) {
        if (isNew) {
            list = ArrayList()
        }
        list.addAll(listNew)
    }

    fun getList(): List<DataMyCoins> {
        return list
    }*/


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMain: ConstraintLayout = itemView.findViewById(R.id.clMain)
        val imgShare: ImageView = itemView.findViewById(R.id.imgShare)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {

        when (position) {
            0 -> {
                Glide.with(context)
                    .load(Constant.RANDOM_IMAGE_USER)
                    .into(holder.imgShare)
            }
            1 -> {
                Glide.with(context)
                    .load(Constant.RANDOM_IMAGE_USER)
                    .into(holder.imgShare)
            }
            2 -> {
                Glide.with(context)
                    .load(Constant.RANDOM_IMAGE_USER)
                    .into(holder.imgShare)
            }
            3 -> {
                Glide.with(context)
                    .load(Constant.RANDOM_IMAGE_USER)
                    .into(holder.imgShare)
            }
            4 -> {
                Glide.with(context)
                    .load(Constant.RANDOM_IMAGE_USER)
                    .into(holder.imgShare)
            }
        }
        holder.clMain.tag = position
        holder.clMain.setOnClickListener(onClickListener)
    }
}