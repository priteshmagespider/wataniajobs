package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.databinding.ListEmailAddItemsBinding
import com.fnrco.wataniajobs.mvvm.model.response.Phone


class EmailListAdapter(
    val context: Context,
    private val emailList: List<Phone>,
    private  val onEditTextChangedEmail: (String) -> Unit,
    private val deleteEmail: (Int) -> Unit

    ) :
    RecyclerView.Adapter<EmailListAdapter.MyViewHolder>() {
    lateinit var listData: ArrayList<Phone>


    inner class MyViewHolder(val binding: ListEmailAddItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(list: Phone, pos : Int) {
            binding.apply {
                if(list.id == -1){
                    cardEmailDelete.visibility = View.GONE
                }
                cardEmailDelete.setOnClickListener {
                    deleteEmail(list.id!!)
                }
                edtEmailAdd.setText(if(list.isEditable) "" else list.phone)
                edtEmailAdd.isEnabled = list.isEditable
                Log.d("TAG", "onBindViewHolder:  SIZE--> ${list.phone}")
                cardEmailDelete.visibility = if(list.isEditable || list.id == -1) View.INVISIBLE else View.VISIBLE

                edtEmailAdd.addTextChangedListener(object : TextWatcher{
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(
                        charSequence: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        onEditTextChangedEmail(charSequence.toString())
                    }

                    override fun afterTextChanged(s: Editable?) {
                        onEditTextChangedEmail(s.toString())
                    }

                })
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MyViewHolder(
            ListEmailAddItemsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(emailList[position], position)
        Log.d("TAG", "onBindViewHolder:  SIZE--> ${emailList.size}")
    }

    override fun getItemCount(): Int = emailList.size
}