package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.SpecializationData
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.github.islamkhsh.CardSliderAdapter
import com.google.android.material.card.MaterialCardView

class HomeListAdapter(
    val context: Context,
    val onClickListener: View.OnClickListener,
    val list: List<SpecializationData>,
    val prefManager: PrefManager
) : CardSliderAdapter<HomeListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HomeListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_home_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return 6
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMainHomeList: ConstraintLayout = itemView.findViewById(R.id.clMainHomeList)

        //        val clMainCard: MaterialCardView = itemView.findViewById(R.id.clMainCard)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtNumber: TextView = itemView.findViewById(R.id.txtNumber)
        val seeAll: TextView = itemView.findViewById(R.id.seeAll)
//        val imgSeeMore: ImageView = itemView.findViewById(R.id.imgSeeMore)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {
        holder.clMainHomeList.tag = position
        holder.clMainHomeList.setOnClickListener(onClickListener)
        holder.seeAll.tag = position
        holder.seeAll.setOnClickListener(onClickListener)
//        holder.imgSeeMore.tag = position
//        holder.imgSeeMore.setOnClickListener(onClickListener)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtTitle.text = list[position].title_en.toString()
        } else {
            holder.txtTitle.text = list[position].title.toString()
        }
        holder.txtNumber.text = list[position].count.toString()

        if (position == 5) {
            holder.seeAll.visibility = View.VISIBLE
//            holder.imgSeeMore.visibility = View.VISIBLE
        } else {
            holder.seeAll.visibility = View.GONE
//            holder.imgSeeMore.visibility = View.GONE
        }
    }
}