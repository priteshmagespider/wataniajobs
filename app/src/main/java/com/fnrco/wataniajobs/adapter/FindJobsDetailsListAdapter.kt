package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.SpecializationData
import com.github.islamkhsh.CardSliderAdapter
import com.google.android.material.card.MaterialCardView

class FindJobsDetailsListAdapter() : CardSliderAdapter<FindJobsDetailsListAdapter.MyViewHolder>() {

    lateinit var context: Context
    lateinit var onClickListener: View.OnClickListener
    lateinit var list: List<SpecializationData>


    constructor(
        context: Context,
        onClickListener: View.OnClickListener,
        dataList: List<SpecializationData>
    ) : this() {
        this.onClickListener = onClickListener
        this.context = context
        this.list = dataList
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FindJobsDetailsListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_find_job_details_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMain: CardView = itemView.findViewById(R.id.clMain)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtNumber: TextView = itemView.findViewById(R.id.txtNumber)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {
        holder.clMain.tag = position
        holder.clMain.setOnClickListener(onClickListener)

        holder.txtTitle.text = list[position].title_en ?: list[position].title
        holder.txtNumber.text = list[position].count.toString()

    }
}