package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.DataTopJobs
import com.fnrco.wataniajobs.mvvm.model.response.PostsTopJobs
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.Utils
import com.github.islamkhsh.CardSliderAdapter
import com.google.android.material.card.MaterialCardView

class TopJobsHomeAdapter(
    val context: Context,
    val onClickListener: View.OnClickListener,
    val list: List<DataTopJobs>,
    val prefManager: PrefManager
) : CardSliderAdapter<TopJobsHomeAdapter.MyViewHolder>() {

    val listData: MutableList<DataTopJobs> = list as MutableList<DataTopJobs>

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): TopJobsHomeAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_top_job_home_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMain: MaterialCardView = itemView.findViewById(R.id.clMain)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtJobCity: TextView = itemView.findViewById(R.id.txtJobCity)
        val txtLocation: TextView = itemView.findViewById(R.id.txtLocation)
        val txtJobType: TextView = itemView.findViewById(R.id.txtJobType)
        val txtAgoTime: TextView = itemView.findViewById(R.id.txtAgoTime)
        val txtTimeAgo: TextView = itemView.findViewById(R.id.txtTimeAgo)
        val imgLogo: ImageView = itemView.findViewById(R.id.imgLogo)
        val imgStarTopjob: ImageView = itemView.findViewById(R.id.imgStarTopjob)
        val imgShareRecentSearch: ImageView = itemView.findViewById(R.id.imgShareRecentSearch)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {
        holder.clMain.tag = position
        holder.clMain.setOnClickListener(onClickListener)
        holder.imgStarTopjob.tag = position
        holder.imgStarTopjob.setOnClickListener(onClickListener)
        holder.imgShareRecentSearch.tag = position
        holder.imgShareRecentSearch.setOnClickListener(onClickListener)
        holder.imgLogo.clipToOutline = true


        if (list[position].is_favourite==1) {
            holder.imgStarTopjob.setImageResource(R.drawable.img_fav)
            holder.imgStarTopjob.setColorFilter(context.getColor(R.color.yellow));
        } else {
            holder.imgStarTopjob.setImageResource(R.drawable.img_un_fav)
        }
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtTitle.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtJobType.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtAgoTime.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtTimeAgo.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtJobCity.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtLocation.textDirection = View.TEXT_DIRECTION_LTR

            holder.txtTitle.text = listData[position].title_en ?: listData[position].title
            holder.txtJobType.text = listData[position].job__type!!.title_en
            holder.txtAgoTime.text =
                Utils.covertTimeToText(listData[position].created_at, context)
            holder.txtTimeAgo.text =
                Utils.covertTimeToText(listData[position].created_at, context)
            if(listData[position].job_city!=null){

                holder.txtJobCity.text = listData[position].job_city!!.name_en
            }
            holder.txtLocation.text = listData[position].company?.address
        } else {
            holder.txtTitle.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtJobType.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtAgoTime.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtTimeAgo.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtJobCity.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtLocation.textDirection = View.TEXT_DIRECTION_RTL

            holder.txtTitle.text = listData[position].title ?: listData[position].title_en
            holder.txtLocation.text = listData[position].company?.address
            holder.txtJobType.text = listData[position].job__type!!.title_ar
            holder.txtAgoTime.text =
                Utils.covertTimeToText(listData[position].created_at, context)
            holder.txtTimeAgo.text =
                Utils.covertTimeToText(listData[position].created_at, context)
            if(listData[position].job_city!=null){

                holder.txtJobCity.text = if(listData[position].job_city!=null) listData[position].job_city!!.name_ar else ""
            }
        }
        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${listData[position].company_logo}")
            .placeholder(R.drawable.img_home)
            .into(holder.imgLogo)
    }
}