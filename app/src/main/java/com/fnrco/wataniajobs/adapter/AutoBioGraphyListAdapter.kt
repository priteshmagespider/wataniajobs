package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.DataAutoBioGraphy
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.Utils
import com.github.islamkhsh.CardSliderAdapter
import com.google.android.material.card.MaterialCardView

class AutoBioGraphyListAdapter(
    val context: Context,
    val onClickListener: View.OnClickListener,
    val list: List<DataAutoBioGraphy>,
    val prefManager: PrefManager
) : CardSliderAdapter<AutoBioGraphyListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AutoBioGraphyListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_auto_biography_jobs_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMainAutoBioGraphyList: MaterialCardView = itemView.findViewById(R.id.clMainAutoBioGraphyList)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtJobCity: TextView = itemView.findViewById(R.id.txtJobCity)
        val txtLocation: TextView = itemView.findViewById(R.id.txtLocation)
        val txtJobType: TextView = itemView.findViewById(R.id.txtJobType)
        val txtAgoTime: TextView = itemView.findViewById(R.id.txtAgoTime)
        val txtTimeAgo: TextView = itemView.findViewById(R.id.txtTimeAgo)
        val imgLogo: ImageView = itemView.findViewById(R.id.imgLogo)
        val imgShareAutoBioGraphy: ImageView = itemView.findViewById(R.id.imgShareAutoBioGraphy)
        val imgStarBioGraphy: ImageView = itemView.findViewById(R.id.imgStarBioGraphy)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {

        holder.imgLogo.clipToOutline = true

        holder.clMainAutoBioGraphyList.tag = position
        holder.clMainAutoBioGraphyList.setOnClickListener(onClickListener)


        holder.imgShareAutoBioGraphy.tag = position
        holder.imgShareAutoBioGraphy.setOnClickListener(onClickListener)



        holder.imgStarBioGraphy.tag = position
        holder.imgStarBioGraphy.setOnClickListener(onClickListener)


        if (list[position].is_favourite==1) {
            holder.imgStarBioGraphy.setImageResource(R.drawable.img_fav)
            holder.imgStarBioGraphy.setColorFilter(context.getColor(R.color.yellow));
        } else {
            holder.imgStarBioGraphy.setImageResource(R.drawable.img_un_fav)
        }

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
            holder.txtTitle.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtJobCity.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtJobType.textDirection = View.TEXT_DIRECTION_LTR
            holder.txtLocation.textDirection = View.TEXT_DIRECTION_LTR

            holder.txtTitle.text = list[position].title_en ?: list[position].title
            holder.txtJobCity.text = list[position].job_city?.name_en
            holder.txtJobType.text = list[position].job__type?.title_en
            holder.txtAgoTime.text = Utils.covertTimeToText(list[position].created_at, context)
            holder.txtTimeAgo.text = Utils.covertTimeToText(list[position].created_at, context)
            if (list[position].company == null) {
                holder.txtLocation.text = ""
            } else {
                holder.txtLocation.text = list[position].company?.address
            }
        } else {
            holder.txtTitle.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtJobCity.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtJobType.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtLocation.textDirection = View.TEXT_DIRECTION_RTL

            holder.txtTitle.text = list[position].title.toString()

            holder.txtJobCity.text = list[position].job_city?.name_ar
            holder.txtJobType.text = list[position].job__type?.title_ar
            holder.txtAgoTime.text = Utils.covertTimeToText(list[position].created_at, context)
            holder.txtTimeAgo.text = Utils.covertTimeToText(list[position].created_at, context)
            if (list[position].company == null) {
                holder.txtLocation.text = ""
            } else {
                holder.txtLocation.text = list[position].company?.address
            }
        }
        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${list[position].company_logo}")
            .placeholder(R.drawable.img_home)
            .into(holder.imgLogo)

    }
}