package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import com.fnrco.wataniajobs.mvvm.model.response.SpecializationData
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class TestListAdapter(
    context: Context,
    @LayoutRes private val layoutResource: Int,
    @IdRes private val textViewResourceId: Int = 0,
    listData: List<SpecializationData>,
    prefManager : PrefManager
) : ArrayAdapter<SpecializationData>(context, layoutResource, listData) {
    private val items: MutableList<SpecializationData> = listData.toMutableList()
    private var allItems: List<SpecializationData> =  listData.toMutableList()
    val prefManager : PrefManager = prefManager
    override fun getCount(): Int {
        return items.size
    }
    override fun getItem(position: Int): SpecializationData {
        return items[position]
    }
    override fun getItemId(position: Int): Long {
        return items[position].id!!.toLong()
    }

    fun getSpecialityList(): List<SpecializationData> {
        return items
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.context).inflate(layoutResource, parent, false)
        }
        try {
            val city: SpecializationData = getItem(position)
            val cityAutoCompleteView = convertView!!.findViewById<View>(textViewResourceId) as TextView
            cityAutoCompleteView.text = if(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") city.title_en else city.title
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return convertView!!
    }




    override fun getFilter(): Filter {
        return object : Filter() {
            override fun convertResultToString(resultValue: Any) :String {
                return (resultValue as SpecializationData).title_en.toString()
            }
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    val citySuggestion: MutableList<SpecializationData> = ArrayList()
                    for (city in allItems) {
                        if (city.title_en?.toLowerCase()!!.startsWith(constraint.toString().toLowerCase())
                        ) {
                            citySuggestion.add(city)
                        }
                    }
                    filterResults.values = citySuggestion
                    filterResults.count = citySuggestion.size
                }
                return filterResults
            }
            override fun publishResults(
                constraint: CharSequence?,
                results: FilterResults
            ) {
                items.clear()
                if (results.count > 0) {
                    for (result in results.values as List<*>) {
                        if (result is SpecializationData) {
                            items.add(result)
                        }
                    }
                    notifyDataSetChanged()
                } else if (constraint == null) {
                    items.addAll(allItems)
                    notifyDataSetInvalidated()
                }
            }
        }
    }

}
