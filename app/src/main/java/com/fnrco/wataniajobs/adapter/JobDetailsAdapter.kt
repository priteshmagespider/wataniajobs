package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.github.islamkhsh.CardSliderAdapter

class JobDetailsAdapter() : CardSliderAdapter<JobDetailsAdapter.MyViewHolder>() {

    lateinit var context: Context
    lateinit var onClickListener: View.OnClickListener
//    lateinit var list: ArrayList<DataMyCoins>


    constructor(context: Context, onClickListener: View.OnClickListener) : this() {
        this.onClickListener = onClickListener
        this.context = context
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): JobDetailsAdapter.MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.list_job_details_item, parent, false))
    }

    override fun getItemCount(): Int {
        return 4
    }

    /*fun addData(listNew: List<DataMyCoins>, isNew: Boolean) {
        if (isNew) {
            list = ArrayList()
        }
        list.addAll(listNew)
    }

    fun getList(): List<DataMyCoins> {
        return list
    }*/


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val clMain: ConstraintLayout = itemView.findViewById(R.id.clMain)
    }

    override fun bindVH(holder: MyViewHolder, position: Int) {


        holder.clMain.tag = position
        holder.clMain.setOnClickListener(onClickListener)
    }
}