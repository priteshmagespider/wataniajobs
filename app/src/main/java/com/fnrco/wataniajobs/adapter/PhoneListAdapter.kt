package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.databinding.ListPhoneAddItemsBinding
import com.fnrco.wataniajobs.mvvm.model.response.Phone
import kotlin.reflect.KFunction1


class PhoneListAdapter(
    val context: Context,
    private val phoneList: List<Phone>,
    private val onEditTextChanged: KFunction1<String, Unit>,
    private val deletePhone: (Int) -> Unit
) :
    RecyclerView.Adapter<PhoneListAdapter.MyViewHolder>() {
    var listData = phoneList

    inner class MyViewHolder(val binding: ListPhoneAddItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(list: Phone, pos : Int) {
            binding.apply {
//                imgDelete.setOnClickListener {
//                        deletePhone(list.id!!)
//                }
                if(list.id == -1){
                    cardPhoneDelete.visibility = View.GONE
                }
                cardPhoneDelete.setOnClickListener {
                        deletePhone(list.id!!)
                }
                edtMobileNumber.setText(if(list.isEditable) "" else list.phone)
                edtMobileNumber.isEnabled = list.isEditable
                Log.d("TAG", "onBindViewHolder:  SIZE--> ${list.phone}")
                cardPhoneDelete.visibility = if(list.isEditable || list.id == -1) View.INVISIBLE else View.VISIBLE

                edtMobileNumber.addTextChangedListener(object : TextWatcher{
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(
                        charSequence: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        onEditTextChanged(charSequence.toString())
                    }

                    override fun afterTextChanged(s: Editable?) {
                        onEditTextChanged(s.toString())
                    }

                })
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MyViewHolder(
            ListPhoneAddItemsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(phoneList[position], position)
        Log.d("TAG", "onBindViewHolder:  SIZE--> ${phoneList.size}")
    }

    override fun getItemCount(): Int = phoneList.size
}