package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.request.WhoWeAreList
import com.google.android.material.card.MaterialCardView

class WhoWeAreAdapter(
    var context: Context,
    var list: ArrayList<WhoWeAreList>,
    val onClickListener: View.OnClickListener
) :
    RecyclerView.Adapter<WhoWeAreAdapter.MyViewHolder>() {
    var pos = 0

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtWhoWerOption: TextView = itemView.findViewById(R.id.txtWhoWerOption)
        var cardWhoWeAre: MaterialCardView = itemView.findViewById(R.id.cardWhoWeAre)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_who_we_are, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtWhoWerOption.tag = position
        holder.txtWhoWerOption.setOnClickListener(onClickListener)
        holder.txtWhoWerOption.text = list[position].name


        if (pos == position) {
            list[position].isSelected = true
            holder.cardWhoWeAre.background =
                context.getDrawable(R.drawable.tab_selected_background)
            holder.txtWhoWerOption.setTextColor(
                context.resources.getColor(R.color.white)
            )
        } else {
            list[position].isSelected = false
            holder.cardWhoWeAre.background =
                context.getDrawable(R.drawable.tab_background_unselected)
            holder.txtWhoWerOption.setTextColor(
                context.resources.getColor(R.color.black)
            )
        }

    }

    fun isSelected(sPos: Int) {
        pos = sPos
        notifyDataSetChanged()
    }

    override fun getItemCount() = list.size
}