package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.compose.ui.text.style.TextDirection
import com.fnrco.wataniajobs.mvvm.model.response.Role
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class JobRoleListAdapter(
    context: Context,
    @LayoutRes private val layoutResource: Int,
    @IdRes private val textViewResourceId: Int = 0,
    private val specialitiList: List<Role>
) : ArrayAdapter<Role>(context, layoutResource, specialitiList) {

    override fun getItem(position: Int): Role = specialitiList[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = createViewFromResource(convertView, parent, layoutResource)

        return bindData(getItem(position), view)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = createViewFromResource(
            convertView,
            parent,
            android.R.layout.simple_spinner_dropdown_item
        )

        return bindData(getItem(position), view)
    }

    fun getSpecialityList(): List<Role> {
        return specialitiList
    }

    private fun createViewFromResource(
        convertView: View?,
        parent: ViewGroup,
        layoutResource: Int
    ): TextView {
        val context = parent.context
        val view =
            convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
        return try {
            if (textViewResourceId == 0) view as TextView
            else {
                view.findViewById(textViewResourceId) ?: throw RuntimeException(
                    "Failed to find view with ID " +
                            "${context.resources.getResourceName(textViewResourceId)} in item layout"
                )
            }
        } catch (ex: ClassCastException) {
            Log.e("CustomArrayAdapter", "You must supply a resource ID for a TextView")
            throw IllegalStateException(
                "ArrayAdapter requires the resource ID to be a TextView", ex
            )
        }
    }

//    override fun areAllItemsEnabled(): Boolean {
//        return super.areAllItemsEnabled()
//    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                filterResults.values = specialitiList
                filterResults.count = specialitiList.size
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                notifyDataSetChanged()
            }

        }
    }

    private fun bindData(specializationData: Role, view: TextView): TextView {
        if (specializationData.title_en == null) {
            view.text = specializationData.title
            view.textDirection = View.TEXT_DIRECTION_RTL
        } else {
            view.text = specializationData.title_en
            view.textDirection = View.TEXT_DIRECTION_LTR
        }
        return view
    }
}


//class JobRoleListAdapter(
//    context: Context,
//    @LayoutRes private val layoutResource: Int,
//    @IdRes private val textViewResourceId: Int = 0,
//    listData: List<Role>,
//    prefManager : PrefManager
//) : ArrayAdapter<Role>(context, layoutResource, listData) {
//
//    private val items: MutableList<Role> = listData.toMutableList()
//    private var allItems: List<Role> = listData.toMutableList()
//    val prefManager: PrefManager = prefManager
//
//    override fun getCount(): Int {
//        return items.size
//    }
//
//    override fun getItem(position: Int): Role {
//        return items[position]
//    }
//
//    override fun getItemId(position: Int): Long {
//        return items[position].id!!.toLong()
//    }
//
//    fun getSpecialityList(): List<Role> {
//        return items
//    }
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//        var convertView = convertView
//        if (convertView == null) {
//            convertView = LayoutInflater.from(parent.context).inflate(layoutResource, parent, false)
//        }
//        try {
//            val city: Role = getItem(position)
//            val cityAutoCompleteView =
//                convertView!!.findViewById<View>(textViewResourceId) as TextView
//            cityAutoCompleteView.text =
//                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") city.title_en else city.title
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        return convertView!!
//    }
//
//
//    override fun getFilter(): Filter {
//        return object : Filter() {
//            override fun convertResultToString(resultValue: Any): String {
//                return (resultValue as Role).title_en.toString()
//            }
//
//            override fun performFiltering(constraint: CharSequence?): FilterResults {
//                val filterResults = FilterResults()
//                if (constraint != null) {
//                    val citySuggestion: MutableList<Role> = ArrayList()
//                    for (city in allItems) {
//                        if (city.title_en?.toLowerCase()!!
//                                .startsWith(constraint.toString().toLowerCase())
//                        ) {
//                            citySuggestion.add(city)
//                        }
//                    }
//                    filterResults.values = citySuggestion
//                    filterResults.count = citySuggestion.size
//                }
//                return filterResults
//            }
//
//            override fun publishResults(
//                constraint: CharSequence?,
//                results: FilterResults
//            ) {
//                items.clear()
//                if (results.count > 0) {
//                    for (result in results.values as List<*>) {
//                        if (result is Role) {
//                            items.add(result)
//                        }
//                    }
//                    notifyDataSetChanged()
//                } else if (constraint == null) {
//                    items.addAll(allItems)
//                    notifyDataSetInvalidated()
//                }
//            }
//        }
//    }
//}



