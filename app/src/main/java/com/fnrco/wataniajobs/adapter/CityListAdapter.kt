package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Post
import java.util.*


class CityListAdapter(mContext: Context) :
    ArrayAdapter<Post>(mContext, R.layout.autocomplete_list_item), Filterable {


    var mListData: ArrayList<Post> = ArrayList<Post>()


    fun setData(list: List<Post>) {
        mListData.clear()
        mListData.addAll(list)
    }


    override fun getCount(): Int {
        return when {
            mListData.isNullOrEmpty() -> 0
            else -> mListData.size
        }
    }

    override fun getItem(position: Int): Post? {
        return when {
            mListData.isNullOrEmpty() -> null
            else -> mListData[position]
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val viewHolder: ViewHolder
        if (view == null) {
            viewHolder = ViewHolder()
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.autocomplete_list_item, parent, false)
            viewHolder.description = view.findViewById(R.id.autocompleteText) as TextView
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as ViewHolder
        }
        val place = mListData[position]
        bindView(viewHolder, place, position)
        return view!!
    }

    private fun bindView(viewHolder: ViewHolder, place: Post, position: Int) {
        if (!mListData.isNullOrEmpty()) {
            if (position != mListData.size - 1) {
                viewHolder.description?.text = place.title
                viewHolder.description?.visibility = View.VISIBLE
            } else {
                viewHolder.description?.visibility = View.GONE
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    /*mListData = placesApi.autocomplete(constraint.toString())
                    mListData?.add(Post("-1", "footer"))*/
                    filterResults.values = mListData
                    filterResults.count = mListData.size
                }
                return filterResults
            }
        }
    }

    internal class ViewHolder {
        var description: TextView? = null
    }
}
