package com.fnrco.wataniajobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.request.WhoWeAreList
import com.fnrco.wataniajobs.mvvm.model.response.MoreScreenService
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.google.android.material.card.MaterialCardView

class ServicesAdapter(
    var context: Context,
    val onClickListener: View.OnClickListener,
    var list: List<MoreScreenService>,
    var prefManager: PrefManager,
) :
    RecyclerView.Adapter<ServicesAdapter.MyViewHolder>() {
    var pos = 0

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtServiceLbl: TextView = itemView.findViewById(R.id.txtServiceLbl)
//        var clServicesList: ConstraintLayout = itemView.findViewById(R.id.clServicesList)
        var cardService: MaterialCardView = itemView.findViewById(R.id.cardService)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_services, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtServiceLbl.tag = position
        holder.txtServiceLbl.setOnClickListener(onClickListener)
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtServiceLbl.text = list[position].name_en
        } else {
            holder.txtServiceLbl.text = list[position].name_ar
        }

        if (pos == position) {
            list[position].isSelected = true
            holder.cardService.background =
                context.getDrawable(R.drawable.tab_selected_background)
            holder.txtServiceLbl.setTextColor(
                context.resources.getColor(R.color.white)
            )
        } else {
            list[position].isSelected = false
            holder.cardService.background =
                context.getDrawable(R.drawable.tab_background_unselected)
            holder.txtServiceLbl.setTextColor(
                context.resources.getColor(R.color.black)
            )
        }

    }

    fun isSelected(sPos: Int) {
        pos = sPos
        notifyDataSetChanged()
    }

    override fun getItemCount() = list.size
}