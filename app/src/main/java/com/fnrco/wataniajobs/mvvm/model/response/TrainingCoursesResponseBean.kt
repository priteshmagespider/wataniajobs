package com.fnrco.wataniajobs.mvvm.model.response

data class TrainingCoursesResponseBean(
    var training: List<TrainingProfile>? = null
)

data class TrainingProfile(
    var id: Int? = null,
    var user_id: Int? = null,
    var currency: Any? = null,
    var job_title: String? = null,
    var job_title_en: String? = null,
    var birthDate: Any? = null,
    var salary: Any? = null,
    var marital_status: Any? = null,
    var military_status: Any? = null,
    var job_status: Any? = null,
    var idCard: Any? = null,
    var phone: String? = null,
    var gender: String? = null,
    var city: Any? = null,
    var carunt_location: Any? = null,
    var aria: Any? = null,
    var address: Any? = null,
    var phone2: Any? = null,
    var certificate: Any? = null,
    var experience: Any? = null,
    var career_objective: Any? = null,
    var special: Int? = null,
    var approve_special: Any? = null,
    var ip: Any? = null,
    var cv: String? = null,
    var photo: Any? = null,
    var photo_approve: Any? = null,
    var cv2: Any? = null,
    var cv2_enter_by: Any? = null,
    var cv2_upload: Any? = null,
    var enter_by: Any? = null,
    var count_view: Int? = null,
    var fb: Any? = null,
    var twitter: Any? = null,
    var linkedIn: Any? = null,
    var website: Any? = null,
    var instagram: Any? = null,
    var youtube: Any? = null,
    var Dependents: Any? = null,
    var mainCat: Any? = null,
    var subCat: Any? = null,
    var phone_is_verified: Int? = null,
    var phone_verification_code: Any? = null,
    var ratio: Int? = null,
    var confirm_training: Int? = null,
    var sms_try: Int? = null,
    var form_uni: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var user: UserProfile? = null,
    var trainings: List<TrainingX>? = null
)

data class UserProfile(
    var id: Int? = null,
    var name: String? = null,
    var name_en: Any? = null,
    var email: String? = null,
    var token: Any? = null,
    var is_admin: Int? = null,
    var provider: Any? = null,
    var provider_id: Any? = null,
    var mail: Any? = null,
    var role: Any? = null,
    var active: Int? = null,
    var code_win: Any? = null,
    var enter_by: Any? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class TrainingX(
    val id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var date: String? = null,
    var duration: String? = null,
    var duration_en: String? = null,
    var type: Int? = null,
    var price: String? = null,
    var status: Int? = null,
    var city: CityProfile? = null,
    var pivot: PivotProfile? = null

)

data class CityProfile(
    var id: Int? = null,
    var name_ar: String? = null,
    var name_en: String? = null
)

data class PivotProfile(
    var seeker_id: Int? = null,
    var training_id: Int? = null
)