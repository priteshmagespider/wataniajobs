package com.fnrco.wataniajobs.mvvm.model.response

class AddEditDeleteExperienceResponseBean : ArrayList<AddExperienceResponseBeanItem>()

data class AddExperienceResponseBeanItem(
    val company_address: String? = null,
    val company_name: String? = null,
    val created_at: String? = null,
    val end_date: String? = null,
    val id: Int? = null,
    val job_description: String? = null,
    val job_title: String? = null,
    val person_informateion_id: Int? = null,
    val start_date: String? = null,
    val total_experience_m: Int? = null,
    val updated_at: String? = null,
    val user_id: Int? = null
)