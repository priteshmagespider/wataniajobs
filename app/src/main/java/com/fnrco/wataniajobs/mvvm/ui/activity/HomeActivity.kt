package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.FindJobsDetailsListAdapter
import com.fnrco.wataniajobs.databinding.ActivityHomeBinding
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.json.JSONObject
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HomeActivity : AppCompatActivity(),
    View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivityHomeBinding
    lateinit var findJobsDetailsListAdapter: FindJobsDetailsListAdapter
    lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var prefManager: PrefManager
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        bottomBarUi()
        initUi()


//        navController.navigate(R.id.findJobsFragment)
//        prefManager.setString(PrefConstant.PREF_CURRENT_LANGUAGE,"ar")
        /*var type=intent.getIntExtra("NOTIFICATIONS_DATA",0)
        Utils.print("HomeStringData---------->"+type)
        if(type==1)
        {
            navController.navigate(R.id.homeDetailFragment)
        }else {
            navController.navigate(R.id.profileFragment)
        }*/


        /*if(intent.hasExtra("NOTIFICATIONS_DATA"))
        {
            var type=intent.getIntExtra("NOTIFICATIONS_DATA",0)
            Utils.print("HomeStringData---------->"+type)
            if(type==1)
            {
                navController.navigate(R.id.homeDetailFragment)
            }else {
                navController.navigate(R.id.profile)
            }
        }*/

    }


    private fun forNotification() {
        val bundle = intent.extras
        Utils.print("HomeNotificationData-bundle------->"+bundle)
        if (bundle != null) {
            for (key in bundle.keySet()) {
                val value = bundle[key]
                Utils.print(
                    "HomeActivityNotification", String.format(
                        "%s %s (%s)", key,
                        value.toString(), value!!.javaClass.name
                    )
                )
                Utils.print("HomeNotificationData-------->"+value.toString())
            }
        }
    }

    private fun bottomBarUi() {
        val radius = resources.getDimension(R.dimen.radius_bottom_bar)
        val bottomNavigationViewBackground =
            binding.bottomBar.background as MaterialShapeDrawable
        bottomNavigationViewBackground.shapeAppearanceModel =
            bottomNavigationViewBackground.shapeAppearanceModel.toBuilder()
                .setTopRightCorner(CornerFamily.ROUNDED, radius)
                .setTopLeftCorner(CornerFamily.ROUNDED, radius)
                .build()
        binding.bottomBar.itemIconTintList = null

    }

    private fun initUi() {
        binding.viewProgress.setOnClickListener(this)
        navController = Navigation.findNavController(this, R.id.mainFragment)
        NavigationUI.setupWithNavController(binding.bottomBar, navController)
        binding.bottomBar.setOnNavigationItemSelectedListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            /*R.id.imgDrawer -> {
                binding.homeDrawerLayout.openDrawer(binding.navView)
            }*/
            R.id.clMain -> {
                /*var intent = Intent(this,HomeDetailActivity::class.java)
                startActivity(intent)*/


            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.homeFragment -> {
                navController.navigate(R.id.homeFragment)
                return true
            }
            R.id.findJobsFragment -> {
                return when {
                    prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN) -> {
                        navController.navigate(R.id.findJobsFragment)
                        true
                    }
                    else -> {
                        loginDialog(this)
                        false
                    }
                }
            }
            R.id.profileFragment -> {
                return when {
                    prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN) -> {
                        navController.navigate(R.id.profileFragment)
                        true
                    }
                    else -> {
                        loginDialog(this)
                        false
                    }
                }
            }
            R.id.moreTrainingCoursesFragment -> {
                return if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    navController.navigate(R.id.moreTrainingCoursesFragment)
                    true
                } else {
                    loginDialog(this)
                    false
                }

            }
            R.id.moreFragment -> {
                navController.navigate(R.id.moreFragment)
                return true
            }
        }
        return false
    }

    private fun loginDialog(context: Context) {
        val intent = Intent(this,LoginPopUpActivity::class.java)
        startActivity(intent)
        /*MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.title_login_dialog)

            .setPositiveButton(R.string.login_in) { dialog, _ ->
                context.startActivity(Intent(context, LoginActivity::class.java))
                dialog.dismiss()
            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()*/
    }

    fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }
}