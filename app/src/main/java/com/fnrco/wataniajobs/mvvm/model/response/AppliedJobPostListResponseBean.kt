package com.fnrco.wataniajobs.mvvm.model.response

data class AppliedJobPostListResponseBean(
    var type: String? = null,
    var posts: List<AppliedJobPost>? = null
)

data class AppliedJobPost(
    var id: Int? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: Any? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: Any? = null,
    var computer_level: AppliedJobPostComputerLevel? = null,
    var MsOffice_level: String? = null,
    var other_condition: Any? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: Any? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: Any? = null,
    var job_id: Any? = null,
    var website: Any? = null,
    var com_email: Any? = null,
    var com_name: Any? = null,
    var com_logo: Any? = null,
    var job_date: Any? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var city: Any? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: Any? = null,
    var job_role: AppliedJobPostJobRole? = null,
    var specialization: Any? = null,
    var job_applicant: List<JobApplicant>? = null,
    var qualification: AppliedJobPostQualificationX? = null,
    var job__type: AppliedJobPostJobType? = null,
    var language_level: AppliedJobPostLanguageLevel? = null,
    var office_level: AppliedJobPostOfficeLevel? = null,
    var job_city: AppliedJobPostJobCity? = null,
    var company: AppliedJobPostCompany? = null
)

data class AppliedJobPostComputerLevel(
    var id: Int? = null,
    var title_ar: String? = null
)

data class AppliedJobPostJobRole(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class JobApplicant(
    var id: Int? = null,
    var user_id: Int? = null,
    var currency: Any? = null,
    var job_title: String? = null,
    var job_title_en: String? = null,
    var birthDate: String? = null,
    var salary: Int? = null,
    var marital_status: Int? = null,
    var military_status: Any? = null,
    var job_status: String? = null,
    var idCard: Any? = null,
    var phone: String? = null,
    var gender: String? = null,
    var city: String? = null,
    var carunt_location: Any? = null,
    var aria: Any? = null,
    var address: String? = null,
    var phone2: Any? = null,
    var certificate: String? = null,
    var experience: Int? = null,
    var career_objective: String? = null,
    var special: Int? = null,
    var approve_special: Any? = null,
    var ip: Any? = null,
    var cv: String? = null,
    var photo: Any? = null,
    var photo_approve: Any? = null,
    var cv2: Any? = null,
    var cv2_enter_by: Any? = null,
    var cv2_upload: Any? = null,
    var enter_by: Any? = null,
    var count_view: Int? = null,
    var fb: String? = null,
    var twitter: String? = null,
    var linkedIn: String? = null,
    var website: Any? = null,
    var instagram: Any? = null,
    var youtube: Any? = null,
    var Dependents: Int? = null,
    var mainCat: Int? = null,
    var subCat: Int? = null,
    var phone_is_verified: Int? = null,
    var phone_verification_code: Any? = null,
    var ratio: Int? = null,
    var confirm_training: Int? = null,
    var sms_try: Int? = null,
    var form_uni: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var pivot: Pivot? = null,
    var user: AppliedJobPostUser? = null,
    var qualificationJobApplicant: AppliedJobPostQualificationJobApplicant? = null
)

data class AppliedJobPostQualificationX(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null
)

data class AppliedJobPostJobType(
    var id: Int? = null,
    var title_ar: String? = null
)

data class AppliedJobPostLanguageLevel(
    var id: Int? = null,
    var title_ar: String? = null
)

data class AppliedJobPostOfficeLevel(
    var id: Int? = null,
    var title_ar: String? = null
)

data class AppliedJobPostJobCity(
    var id: Int? = null,
    var name_ar: String? = null,
    var name_en: String? = null
)

data class AppliedJobPostCompany(
    var id: Int? = null,
    var user_id: Int? = null,
    var comNum: Any? = null,
    var sector: String? = null,
    var mobile: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var city: String? = null,
    var address: String? = null,
    var logo: String? = null,
    var photo_approve: Int? = null,
    var size: String? = null,
    var `field`: String? = null,
    var user_job_title: String? = null,
    var status: Int? = null,
    var phone_is_verified: Int? = null,
    var sms_try: Int? = null,
    var phone_verification_code: Any? = null,
    var career_objective: String? = null,
    var package_id: Any? = null,
    var total_cv: Any? = null,
    var expiration_date: Any? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class Pivot(
    var jobPost_id: Int? = null,
    var jobSeeker_id: Int? = null,
    var status: Int? = null,
    var showed: Int? = null
)

data class AppliedJobPostUser(
    var id: Int? = null,
    var name: String? = null,
    var name_en: Any? = null,
    var email: String? = null,
    var token: Any? = null,
    var is_admin: Int? = null,
    var provider: Any? = null,
    var provider_id: Any? = null,
    var mail: Any? = null,
    var role: Any? = null,
    var active: Int? = null,
    var code_win: Any? = null,
    var enter_by: Any? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class AppliedJobPostQualificationJobApplicant(
    var id: Int? = null,
    var title_ar: String? = null
)