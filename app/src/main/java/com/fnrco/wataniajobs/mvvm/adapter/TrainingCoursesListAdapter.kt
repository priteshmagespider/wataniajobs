package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Training
import com.fnrco.wataniajobs.mvvm.model.response.TrainingProfile
import com.fnrco.wataniajobs.mvvm.model.response.TrainingX
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.google.android.material.card.MaterialCardView

class TrainingCoursesListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    var prefManager: PrefManager
) : RecyclerView.Adapter<TrainingCoursesListAdapter.MyViewHolder>() {
    lateinit var trainings: ArrayList<Training>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardTrainingCourses: MaterialCardView = itemView.findViewById(R.id.cardTrainingCourses)
        val imgDeleteTrainingCourse: Button = itemView.findViewById(R.id.imgDeleteTrainingCourse)
        val txtOrderNumber: TextView = itemView.findViewById(R.id.txtOrderNumber)
        val txtCourseTitle: TextView = itemView.findViewById(R.id.txtCourseTitle)
        val txtCoursesDate: TextView = itemView.findViewById(R.id.txtCoursesDate)
        val txtSessionHeadquarters: TextView = itemView.findViewById(R.id.txtSessionHeadquarters)
        val txtDurationOfTheCourse: TextView = itemView.findViewById(R.id.txtDurationOfTheCourse)
        val txtCourseFees: TextView = itemView.findViewById(R.id.txtCourseFees)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_training_courses_items_new, parent, false)
        )
    }

    fun addData(list: List<Training>) {
        trainings = ArrayList()
        trainings.addAll(list)
    }

    fun getList(): ArrayList<Training> {
        return trainings
    }

    fun removeItem(position: Int) {
        trainings.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtOrderNumber.text = "AHDFY"+ position.toString()

        if (trainings[position].date == "1") {
            holder.txtCoursesDate.text = context.getString(R.string.lbl_until_number_is_complete)
        }
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtCourseTitle.text = trainings[position].title_en
            holder.txtSessionHeadquarters.text = if(trainings[position].city != null) trainings[position].city!!.name_en else ""
            holder.txtDurationOfTheCourse.text = trainings[position].duration_en
            holder.txtCourseFees.text = trainings[position].price

        } else {
            holder.txtCourseTitle.text = trainings[position].title
            holder.txtSessionHeadquarters.text = if(trainings[position].city != null)  trainings[position].city!!.name_ar else ""
            holder.txtDurationOfTheCourse.text = trainings[position].duration
            holder.txtCourseFees.text = trainings[position].price
        }


        holder.imgDeleteTrainingCourse.tag = position
        holder.imgDeleteTrainingCourse.setOnClickListener(onClickListener)

    }

    override fun getItemCount(): Int = trainings.size
}