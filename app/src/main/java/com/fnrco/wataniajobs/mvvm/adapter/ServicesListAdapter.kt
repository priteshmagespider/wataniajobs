package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.MoreScreenService
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class ServicesListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    var list: List<MoreScreenService>,
    var prefManager: PrefManager
) : RecyclerView.Adapter<ServicesListAdapter.MyViewHolder>() {

    val listData: MutableList<MoreScreenService> = list as MutableList<MoreScreenService>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtServiceLbl: TextView = itemView.findViewById(R.id.txtServiceLbl)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_service_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtServiceLbl.tag = position
        holder.txtServiceLbl.setOnClickListener(onClickListener)
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtServiceLbl.text = listData[position].name_en
        } else {
            holder.txtServiceLbl.text = listData[position].name_ar
        }
    }


    override fun getItemCount(): Int {
        return listData.size
    }
}