package com.fnrco.wataniajobs.mvvm.model.response

data class AppliedExpiredJobPostListResponseBean(
    var type: String? = null,
    var posts: List<Post>? = null
)

