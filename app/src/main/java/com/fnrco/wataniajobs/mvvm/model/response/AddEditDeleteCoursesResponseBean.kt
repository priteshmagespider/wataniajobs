package com.fnrco.wataniajobs.mvvm.model.response

class AddEditDeleteCoursesResponseBean : ArrayList<AddEditDeleteCoursesResponseBeanItem>()

data class AddEditDeleteCoursesResponseBeanItem(
    val action_date: String? = null,
    val address: String? = null,
    val center_name: String? = null,
    val count_times: String? = null,
    val created_at: String? = null,
    val description: String? = null,
    val id: Int? = null,
    val job_seeker_id: Int? = null,
    val updated_at: String? = null,
    val user_id: Int? = null
)