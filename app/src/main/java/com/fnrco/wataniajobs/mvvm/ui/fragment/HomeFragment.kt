package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.*
import com.fnrco.wataniajobs.common.CityPicker
import com.fnrco.wataniajobs.mvvm.adapter.HomeBannerListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginActivity
import com.fnrco.wataniajobs.mvvm.ui.activity.SignUpActivity
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import android.view.inputmethod.EditorInfo

import android.widget.TextView.OnEditorActionListener
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.databinding.FragmentHomeBinding
import com.fnrco.wataniajobs.mvvm.model.request.SearchRequest
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class HomeFragment : Fragment(), View.OnClickListener, CityPicker.SelectCategory {
    private val mTAG = HomeFragment::class.java.simpleName
    private lateinit var binding: FragmentHomeBinding
    private lateinit var navController: NavController
    private lateinit var homeListAdapter: HomeListAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var cityPicker: CityPicker

    @ExperimentalCoroutinesApi
    private val viewModel: HomeViewModel by viewModels()
    lateinit var searchAllCityListAdapter: SearchAllCityListAdapter
    lateinit var changeLocationAdapter: ChangeLocationAdapter
    private var cities: List<CityList>? = null
    private var browseCities: String? = null
    private var city_id: Int? = 0
    private var city_name: String? = ""
    private var recentOrBasedOnYourCV: String? = ""
    private var recentOrBasedOnYourCVShare: String? = ""

    private lateinit var homeBannerListAdapter: HomeBannerListAdapter
    private lateinit var topJobListAdapter: TopJobsHomeAdapter
    private var posRecentSearchLike: Int = -1
    private var posRecentSearchShare: Int = -1
    private var posAutoBioGraphyLike: Int = -1
    private var posAutoBioGraphyShare: Int = -1
    val searchRequest: SearchRequest = SearchRequest()


    private lateinit var recentSearchListAdapter: RecentSearchListAdapter
    private lateinit var autoBioGraphyListAdapter: AutoBioGraphyListAdapter
    private var singlePost: PostSinglePost? = null


    @Inject
    lateinit var prefManager: PrefManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObserver()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.rvBannerList.visibility = View.GONE
        binding.viewLine.visibility = View.GONE

        if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
            binding.ivNotification.visibility = View.VISIBLE
            binding.ivHays.visibility = View.VISIBLE
            binding.clMainAlreadyHaveAnAcc.visibility = View.GONE
            binding.clMainYouDontHaveAnAcc.visibility = View.GONE

            binding.txtRecentSearches.visibility = View.VISIBLE
            binding.rvRecentSearches.visibility = View.VISIBLE
            binding.txtAutobiography.visibility = View.VISIBLE
            binding.rvAutobiography.visibility = View.VISIBLE

            binding.idCreateAnAccount.visibility = View.GONE
            binding.idAlreadyHaveAnAccount.visibility = View.GONE
            binding.tvDesc.visibility = View.GONE
            binding.idSignIn.visibility = View.GONE
            binding.layoutUserInfo.visibility = View.VISIBLE

            Log.d("TAG", "onCreateView: ${prefManager.getString(PrefConstant.PREF_USER_NAME)}")
            prefManager.getString(PrefConstant.PREF_USER_NAME)?.let {
                binding.tvName.text = it
            }
            binding.ivProfile.clipToOutline = true
            prefManager.getString(PrefConstant.PREF_USER_PHOTO)?.let {
                Log.d("profle image -", "${Constant.IMAGE_URL_WITH_PHOTOS}$it")
                Glide.with(requireContext())
                    .load("${Constant.IMAGE_URL_WITH_PHOTOS}$it")
                    .placeholder(R.drawable.userplaceholder)
                    .into(binding.ivProfile)
            }

        } else {
            binding.clMainAlreadyHaveAnAcc.visibility = View.GONE
            binding.clMainYouDontHaveAnAcc.visibility = View.GONE


//            binding.txtRecentSearches.visibility = View.GONE
//            binding.rvRecentSearches.visibility = View.GONE
            binding.txtAutobiography.visibility = View.GONE
            binding.rvAutobiography.visibility = View.GONE

            binding.idCreateAnAccount.visibility = View.VISIBLE
            binding.idAlreadyHaveAnAccount.visibility = View.VISIBLE
            binding.tvDesc.visibility = View.VISIBLE
            binding.idSignIn.visibility = View.VISIBLE
            binding.layoutUserInfo.visibility = View.GONE

        }


        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!.isEmpty()) {
            prefManager.setString(PrefConstant.PREF_CURRENT_LANGUAGE, "ar")
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        viewModel.setStateEvent(HomeStateEvent.HomeCategoriesList(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        viewModel.setStateEvent(HomeStateEvent.HomeCity(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
            viewModel.setStateEvent(
                HomeStateEvent.HomeRecentSearchList(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!
                )
            )
            binding.txtRecentSearches.text =
                requireActivity().getString(R.string.your_recent_searches)
        } else {
            viewModel.setStateEvent(
                HomeStateEvent.HomeTopJobs(
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    1
                )
            )
            binding.txtRecentSearches.text = requireActivity().getString(R.string.top_jobs_home)
        }

        if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
            viewModel.setStateEvent(
                HomeStateEvent.HomeAutoBioGraphyList(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!
                )
            )
        }

        viewModel.setStateEvent(
            HomeStateEvent.HomeBannerEvent(
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
            )
        )


        binding.edtAutoCompleteSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                val bundle = Bundle()
                Utils.print("Data---------------------->1::-----job_title---->" + binding.edtAutoCompleteSearch.text.toString())
//                Utils.print("Data---------------------->1::-----city---->" + binding.edtAllCity.text.toString())
                Utils.print("Data---------------------->1::-----city_id---->$city_id")
                bundle.putString(
                    Constant.HOME_JOB_TITLE,
                    binding.edtAutoCompleteSearch.text.toString()
                )
                bundle.putInt(Constant.CITY_ID, city_id!!)
                if (city_name != "") {
                    bundle.putString(Constant.CITY_NAME, city_name!!)
                } else {
                    bundle.putString(Constant.CITY_NAME, "")
                }
                navController.navigate(R.id.action_homeFragment_to_findJobsFragment, bundle)
            }
            false
        })
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("job_title")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter job_title-> $data")
//                searchRequest.jobTitle = data
//
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("city")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter city_id-> $data")
//                searchRequest.city = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("jobCat")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter jobCat-> $data")
//                searchRequest.jobCat = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("subCat")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter subCat-> $data")
//                searchRequest.subCat = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("from")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter from-> $data")
//                searchRequest.from = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("to")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter to-> $data")
//                searchRequest.to = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("jobType")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter jobType-> $data")
//                searchRequest.jobType = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("gender")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter gender-> $data")
//                searchRequest.gender = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("certificate")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter certificate-> $data")
//                searchRequest.certificate  = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("created_at")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter created_at-> $data")
//                searchRequest.created_at  = data
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("HOME_API_CALL")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "Filter HOME_API_CALL-> $data")
////                navigateToFindJobs()
//            }

        initUi()
    }

    private  fun navigateToFindJobs(){
        val bundle = bundleOf(
            "HOME_FILTER" to searchRequest
        )
        navController.navigate(
            R.id.action_filterJobListFragment_to_findJobsFragment,
            bundle
        )
    }

    private fun initUi() {
        city_id = 0
//        binding.idFindJobs.setOnClickListener(this)
//        binding.edtAllCity.setOnClickListener(this)
        binding.idAlreadyHaveAnAccount.setOnClickListener(this)
        binding.idSignIn.setOnClickListener(this)
        binding.idCreateAnAccount.setOnClickListener(this)
        binding.txtChangeLocation.setOnClickListener(this)
        binding.toggleButton.setOnClickListener(this)
        binding.clMainAlreadyHaveAnAcc.setOnClickListener(this)
        binding.clMainYouDontHaveAnAcc.setOnClickListener(this)
        binding.ivNotification.setOnClickListener(this)
        binding.imgFilterButton.setOnClickListener(this)
        binding.ivHays.setOnClickListener(this)

//        binding.toggleButton.text =
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") getString(R.string.lbl_language_arabic) else getString(
//                R.string.lbl_language_english
//            )
        Log.d(
            mTAG,
            "CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
    }



    private fun setObserver() {
        // TOp Jobs List

        viewModel.topJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<TopJobsResponseBean> -> {
                    displayProgressBar(false)
                    linearLayoutManager =
                        LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
                    binding.rvRecentSearches.layoutManager = linearLayoutManager
                    topJobListAdapter = TopJobsHomeAdapter(
                        requireActivity(), this, dataState.data.posts?.data!!,
                        prefManager
                    )
                    /* binding.recyclerViewJobs.addItemDecoration(
                         DividerItemDecoration(
                             requireContext(), LinearLayoutManager.VERTICAL
                         ).apply {
                             setDrawable(resources.getDrawable(R.drawable.divider_bg))
                         }
                     )*/
                    binding.rvRecentSearches.adapter = topJobListAdapter
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        viewModel.singlePostResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SinglePostResponseBean> -> {
                    displayProgressBar(false)
                    singlePost = dataState.data.post

                    val icon = BitmapFactory.decodeResource(
                        resources,
                        R.drawable.img_back_white
                    )
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "image/jpeg"
                    val bytes = ByteArrayOutputStream()
                    icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    val f = File(
                        (Environment.getExternalStorageDirectory()).toString() + "temporary_file.jpg"
                    )
                    try {
                        f.createNewFile()
                        val fo = FileOutputStream(f)
                        fo.write(bytes.toByteArray())
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    share.putExtra(
                        Intent.EXTRA_STREAM,
                        Uri.parse("${Constant.IMAGE_URL}${singlePost!!.com_logo}")
                    )
                    if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                        share.putExtra(
                            Intent.EXTRA_TEXT,
//                            singlePost!!.title_en
                            "" + "\n"
//                                    + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_en + "\n"
//                                    + resources.getString(R.string.address) + " : " + singlePost?.company?.address + "\n"
//                                    + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_en + "\n"
//                                    + resources.getString(R.string.announcement_date) + " : " + singlePost?.created_at + "\n"
//                                    + resources.getString(R.string.the_job_title) + "\n"
                                    + "Job title" + " : " + singlePost?.job__type?.title_en + "\n"
                                    + "Description" + " : " + Utils.html2text(singlePost?.description_en) + "\n"
                                    + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                    + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                    + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                    + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title_en + "\n"
//                                    + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                        )
                        share.type = "text/plain"
//                    startActivity(share)
                    } else {
                        share.putExtra(
                            Intent.EXTRA_TEXT,
//                            singlePost!!.title_en
                            "" + "\n"
//                                    + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_ar + "\n"
//                                    + resources.getString(R.string.address) + " : " + singlePost?.company?.address + "\n"
//                                    + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_ar + "\n"
//                                    + resources.getString(R.string.announcement_date) + " : " + singlePost?.created_at + "\n"
//                                    + resources.getString(R.string.the_job_title) + "\n"
                                    + "Job title" + " : " + singlePost?.job__type?.title_ar + "\n"
                                    + "Description" + " : " + Utils.html2text(singlePost?.description) + "\n"
                                    + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                    + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                    + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                    + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title + "\n"
//                                    + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                        )
                        share.type = "text/plain"
//                     startActivity(share)
                    }
                    startActivity(Intent.createChooser(share, "Share Image"))
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }




        viewModel.homeCategoriesListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SpecializationResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.specializations != null) {
                        linearLayoutManager = LinearLayoutManager(requireActivity())
                        binding.recyclerView.layoutManager = linearLayoutManager
                        homeListAdapter = HomeListAdapter(
                            requireActivity(),
                            this,
                            dataState.data.specializations!!,
                            prefManager
                        )
                        binding.recyclerView.adapter = homeListAdapter
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.homeListCityResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SearchListResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.posts != null) {
                        Utils.print("RESPONSE-------CITY----------->" + dataState.data.posts)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        viewModel.homeJobTitleListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SearchListResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.posts != null) {
                        Utils.print("RESPONSE_CITY:" + dataState.data.posts)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.cityListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<CityListResponse> -> {
                    displayProgressBar(false)
                    if (dataState.data.cities != null) {
                        Utils.print("RESPONSE_CITY_List:" + dataState.data.cities)
                        cities = dataState.data.cities
                        cityPicker = CityPicker(requireContext(), this)
                        cityPicker.setCategory(
                            dataState.data.cities,
                            prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) != "en"
                        )


                        searchAllCityListAdapter =
                            SearchAllCityListAdapter(
                                requireActivity(),
                                R.layout.drop_down_list_item,
                                R.id.txtDropDown,
                                dataState.data.cities!!
                            )

                        /* binding.edtAllCity.setAdapter(searchAllCityListAdapter)
                         binding.edtAllCity.setOnClickListener {
                             binding.edtAllCity.showDropDown()
                         }
                         binding.edtAllCity.setOnItemClickListener { _, _, position, _ ->
                             city_id = searchAllCityListAdapter.getSpecialityList()[position].id!!

                             if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == resources.getString(
                                     R.string.language
                                 )
                             ) {
                                 city_name =
                                     searchAllCityListAdapter.getSpecialityList()[position].name_en
                                 binding.edtAllCity.setText(searchAllCityListAdapter.getSpecialityList()[position].name_en)
                             } else {
                                 city_name =
                                     searchAllCityListAdapter.getSpecialityList()[position].name_ar
                                 binding.edtAllCity.setText(searchAllCityListAdapter.getSpecialityList()[position].name_ar)
                             }
                             searchAllCityListAdapter.notifyDataSetChanged()

                         }*/
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Home Banner Response */
        viewModel.homeBannerResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<WeAreHiringResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.specializations!!.isNotEmpty()) {
                        setBannerData(dataState.data)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Home Banner Response */
        viewModel.favouriteJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<FavoriteJobResponseBean> -> {
                    displayProgressBar(false)

                    if (dataState.data.message == "Add Job to Favourite Successfully") {
                        if (recentOrBasedOnYourCV == resources.getString(R.string.your_recent_searches)) {
                            if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                                recentSearchListAdapter.list[posRecentSearchLike].is_favourite = 1
                                recentSearchListAdapter.notifyItemChanged(posRecentSearchLike)
                            } else {
                                topJobListAdapter.list[posRecentSearchLike].is_favourite = 1
                                topJobListAdapter.notifyItemChanged(posRecentSearchLike)
                            }

                        } else {
                            autoBioGraphyListAdapter.list[posAutoBioGraphyLike].is_favourite = 1
                            autoBioGraphyListAdapter.notifyItemChanged(posAutoBioGraphyLike)
                        }


                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Home Banner Response */
        viewModel.removeFavouriteJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<RemoveFavoriteJobResponseBean> -> {
                    displayProgressBar(false)

                    if (dataState.data.message == "Remove Job From Favourite Successfully") {

                        if (recentOrBasedOnYourCV == resources.getString(R.string.your_recent_searches)) {
                            if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                                recentSearchListAdapter.list[posRecentSearchLike].is_favourite =
                                    0
                                recentSearchListAdapter.notifyItemChanged(posRecentSearchLike)
                            } else {
                                topJobListAdapter.list[posRecentSearchLike].is_favourite = 0
                                topJobListAdapter.notifyItemChanged(posRecentSearchLike)
                            }
                        } else {
                            autoBioGraphyListAdapter.list[posAutoBioGraphyLike].is_favourite = 0
                            autoBioGraphyListAdapter.notifyItemChanged(posAutoBioGraphyLike)
                        }

                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        viewModel.recentSearchListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<RecentSearchListResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.posts != null) {
                        Utils.print("RESPONSE-------RECENTSERACH----------->" + dataState.data.posts)

                        displayProgressBar(false)
                        if (dataState.data.posts!!.data!!.isEmpty()) {
                            Utils.print("Data---------------------->::FindJobsFragment-----searchListResponseBean---->" + dataState.data.posts!!.data!!.size)
                            binding.txtRecentSearches.visibility = View.GONE
                            binding.rvRecentSearches.visibility = View.GONE
                        } else {

                            binding.txtRecentSearches.visibility = View.VISIBLE
                            recentSearchListAdapter = RecentSearchListAdapter(
                                requireActivity(),
                                this,
                                dataState.data.posts!!.data!!,
                                prefManager
                            )
                            binding.rvRecentSearches.apply {
                                layoutManager = LinearLayoutManager(
                                    requireContext(),
                                    RecyclerView.HORIZONTAL,
                                    false
                                )
                                adapter = recentSearchListAdapter
                            }
                        }
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }


        viewModel.autoBioGraphyListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<AutoBioGraphyResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.posts != null) {
                        Utils.print("RESPONSE-------RECENTSERACH----------->" + dataState.data.posts)

                        displayProgressBar(false)
                        if (dataState.data.posts!!.data!!.isEmpty()) {
                            Utils.print("Data---------------------->::FindJobsFragment-----searchListResponseBean---->" + dataState.data.posts!!.data!!.size)
                            binding.txtAutobiography.visibility = View.GONE
                            binding.rvAutobiography.visibility = View.GONE
                        } else {

                            binding.txtAutobiography.visibility = View.VISIBLE
                            autoBioGraphyListAdapter = AutoBioGraphyListAdapter(
                                requireActivity(),
                                this,
                                dataState.data.posts!!.data!!,
                                prefManager
                            )
                            binding.rvAutobiography.apply {
                                layoutManager = LinearLayoutManager(
                                    requireContext(),
                                    RecyclerView.HORIZONTAL,
                                    false
                                )
                                adapter = autoBioGraphyListAdapter
                            }
                        }
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

    }

    private fun setBannerData(data: WeAreHiringResponseBean) {
        homeBannerListAdapter =
            HomeBannerListAdapter(requireContext(), this, data.specializations!!, prefManager)
        binding.rvBannerList.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            adapter = homeBannerListAdapter
        }
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.iv_notification -> {
                navController.navigate(R.id.action_homeFragment_to_notificationFragment)
            }
            R.id.idFindJobs -> {
                val bundle = Bundle()
                Utils.print("Data---------------------->1::-----job_title---->" + binding.edtAutoCompleteSearch.text.toString())
//                Utils.print("Data---------------------->1::-----city---->" + binding.edtAllCity.text.toString())
                Utils.print("Data---------------------->1::-----city_id---->$city_id")
                bundle.putString(
                    Constant.HOME_JOB_TITLE,
                    binding.edtAutoCompleteSearch.text.toString()
                )
                bundle.putInt(Constant.CITY_ID, city_id!!)
                if (city_name != "") {
                    bundle.putString(Constant.CITY_NAME, city_name!!)
                } else {
                    bundle.putString(Constant.CITY_NAME, "")
                }
                navController.navigate(R.id.action_homeFragment_to_findJobsFragment, bundle)
            }
            R.id.idAlreadyHaveAnAccount, R.id.idSignIn -> {
                val intent = Intent(requireActivity(), LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.clMainAlreadyHaveAnAcc -> {
                val intent = Intent(requireActivity(), LoginActivity::class.java)
                startActivity(intent)
            }
//            R.id.toggleButton -> {
//                val local =
//                    if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") "ar" else "en"
//                Utils.setLocale(
//                    requireContext(),
//                    local,
//                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
//                )
//            }
//                R.id.imgSeeMore,
            R.id.seeAll -> {
                if (browseCities != null) {
                    val bundle = Bundle()
                    bundle.putString(
                        Constant.TITLE, browseCities
                    )
                    navController.navigate(
                        R.id.action_homeFragment_to_findJobsDetailsFragment,
                        bundle
                    )
                } else {
                    val bundle = Bundle()
                    bundle.putString(
                        Constant.TITLE,
                        resources.getString(R.string.browse_jobs_in_saudi_arabia)
                    )
                    navController.navigate(
                        R.id.action_homeFragment_to_findJobsDetailsFragment,
                        bundle
                    )
                }

            }
            R.id.idCreateAnAccount -> {
                startActivity(
                    Intent(
                        requireContext(),
                        SignUpActivity::class.java
                    )
                )
            }
            R.id.clMainYouDontHaveAnAcc -> {
                startActivity(
                    Intent(
                        requireContext(),
                        SignUpActivity::class.java
                    )
                )
            }

            R.id.txtChangeLocation -> {
                cityPicker.showCategoryList()
            }

            R.id.clMainAutoBioGraphyList -> {
                val pos = v.tag.toString().toInt()
                val bundle = Bundle()
                bundle.putInt(Constant.POST_ID, autoBioGraphyListAdapter.list[pos].id!!)
                navController.navigate(
                    R.id.action_homeFragment_to_homeDetailFragment,
                    bundle
                )
            }
            R.id.clMain -> {
                val pos = v.tag.toString().toInt()
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    val bundle = Bundle()

                    bundle.putInt(Constant.POST_ID, topJobListAdapter.list[pos].id!!)
                    navController.navigate(
                        R.id.action_homeFragment_to_homeDetailFragment,
                        bundle
                    )
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    startActivity(intent)
                }
            }
            R.id.clMainRecent -> {
                val pos = v.tag.toString().toInt()
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    val bundle = Bundle()
                    bundle.putInt(Constant.POST_ID, recentSearchListAdapter.list[pos].id!!)
                    navController.navigate(
                        R.id.action_homeFragment_to_homeDetailFragment,
                        bundle
                    )
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    startActivity(intent)
                }
            }
            R.id.clMainHomeList -> {
                val pos = v.tag.toString().toInt()
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    val bundle = Bundle()
                    if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == resources.getString(
                            R.string.language_english
                        )
                    ) {
                        bundle.putString(
                            Constant.SPECIALITY_DATA_TITLE, homeListAdapter.list[pos].title_en
                        )
                    } else {
                        bundle.putString(
                            Constant.SPECIALITY_DATA_TITLE, homeListAdapter.list[pos].title
                        )
                    }
                    bundle.putInt(
                        Constant.SPECIALITY_CITY_ID, city_id!!
                    )
                    bundle.putInt(
                        "JOB_CAT_ID", homeListAdapter.list[pos].id!!
                    )
                    bundle.putInt(
                        Constant.SPECIALITY_DATA_COUNT, homeListAdapter.list[pos].count!!
                    )
                    navController.navigate(
                        R.id.action_homeFragment_to_findJobsFragment,
                        bundle
                    )
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    startActivity(intent)
                }

            }
            R.id.imgFilterButton -> {
                val bundle = bundleOf("FROM_HOME_FILTER_BUTTON" to searchRequest)
                navController.navigate(
                    R.id.action_homeFragment_to_homeFilterJobListFragment,
                    bundle
                )
            }
            R.id.cardHomeBanner -> {
                val pos = v.tag.toString().toInt()
                val bundle = bundleOf(
                    Constant.SPECIALITY_DATA_TITLE to if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") homeBannerListAdapter.listData[pos].title_en else homeBannerListAdapter.listData[pos].title,
                    Constant.SPECIALITY_CITY_ID to 0,
                    "JOB_CAT_ID" to homeBannerListAdapter.listData[pos].id
                )
                navController.navigate(
                    R.id.action_homeFragment_to_findJobsFragment,
                    bundle
                )
            }
            R.id.imgStarTopjob -> {
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    posRecentSearchLike = v.tag.toString().toInt()
                    recentOrBasedOnYourCV = resources.getString(R.string.your_recent_searches)
                    if (topJobListAdapter.list[posRecentSearchLike].is_favourite == 1) {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeRemoveFavouriteJobList(
                                topJobListAdapter.list[posRecentSearchLike].id!!
                            )
                        )
                        displayProgressBar(true)
                    } else {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeFavouriteJobList(
                                topJobListAdapter.list[posRecentSearchLike].id!!
                            )
                        )
                        displayProgressBar(true)
                    }
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    startActivity(intent)
                }

            }
            R.id.imgStar -> {
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    posRecentSearchLike = v.tag.toString().toInt()
                    recentOrBasedOnYourCV = resources.getString(R.string.your_recent_searches)
                    if (recentSearchListAdapter.list[posRecentSearchLike].is_favourite == 1) {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeRemoveFavouriteJobList(
                                recentSearchListAdapter.list[posRecentSearchLike].id!!
                            )
                        )
                        displayProgressBar(true)
                    } else {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeFavouriteJobList(
                                recentSearchListAdapter.list[posRecentSearchLike].id!!
                            )
                        )
                        displayProgressBar(true)
                    }
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    startActivity(intent)
                }

            }
            R.id.imgStarBioGraphy -> {
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    posAutoBioGraphyLike = v.tag.toString().toInt()
                    recentOrBasedOnYourCV = resources.getString(R.string.based_on_your_cv)
                    if (autoBioGraphyListAdapter.list[posAutoBioGraphyLike].is_favourite == 1) {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeRemoveFavouriteJobList(
                                autoBioGraphyListAdapter.list[posAutoBioGraphyLike].id!!
                            )
                        )
                        displayProgressBar(true)
                    } else {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeFavouriteJobList(
                                autoBioGraphyListAdapter.list[posAutoBioGraphyLike].id!!
                            )
                        )
                        displayProgressBar(true)
                    }
                } else {
                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                    startActivity(intent)
                }

            }
            R.id.imgShareRecentSearch -> {
                posRecentSearchShare = v.tag.toString().toInt()
                recentOrBasedOnYourCVShare = resources.getString(R.string.your_recent_searches)
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeDetailSinglePost(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!, recentSearchListAdapter.list[posRecentSearchShare].id!!
                        )
                    )
                } else {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeDetailSinglePost(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!, topJobListAdapter.list[posRecentSearchShare].id!!
                        )
                    )
                }


            }
            R.id.imgShareAutoBioGraphy -> {
                posAutoBioGraphyShare = v.tag.toString().toInt()
                recentOrBasedOnYourCVShare = resources.getString(R.string.your_recent_searches)

                viewModel.setStateEvent(
                    HomeStateEvent.HomeDetailSinglePost(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!, autoBioGraphyListAdapter.list[posAutoBioGraphyShare].id!!
                    )
                )
            }
            R.id.iv_hays ->{
                val openURL = Intent(android.content.Intent.ACTION_VIEW)
                openURL.data = Uri.parse("https://www.hays.ae/")
                startActivity(openURL)
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


    override fun onSelectCategory(position: Int) {
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") binding.txtChangeLocation.text =
            cities!![position].name_en else binding.txtChangeLocation.text =
            cities!![position].name_ar
        city_id = cities!![position].id
    }

}