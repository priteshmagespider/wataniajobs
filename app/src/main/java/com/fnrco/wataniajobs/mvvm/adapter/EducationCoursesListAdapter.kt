package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Course
import com.fnrco.wataniajobs.mvvm.model.response.Education
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant

class EducationCoursesListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Course>,
    val prefManager: PrefManager
) : RecyclerView.Adapter<EducationCoursesListAdapter.MyViewHolder>() {

    val listData = list as MutableList<Course>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtCompleteYear: TextView = itemView.findViewById(R.id.txtCompleteYear)
        val txtSpecialisation: TextView = itemView.findViewById(R.id.txtSpecialisation)
        val txtCityAndCompany: TextView = itemView.findViewById(R.id.txtCityAndCompany)
        val imgCoursesEdit: ImageView = itemView.findViewById(R.id.imgCoursesEdit)
        val imgCoursesDelete: ImageView = itemView.findViewById(R.id.imgCoursesDelete)
        val imgCertificate: ImageView = itemView.findViewById(R.id.imgCertificate)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_education_courses_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgCoursesEdit.tag = position
        holder.imgCoursesEdit.setOnClickListener(onClickListener)
        holder.imgCoursesDelete.tag = position
        holder.imgCoursesDelete.setOnClickListener(onClickListener)
        holder.txtCompleteYear.text = listData[position].action_date.toString()
        holder.txtSpecialisation.text = listData[position].address
        holder.txtCityAndCompany.text = "${listData[position].center_name} | ${
            context.resources.getString(
                R.string.lbl_hours,
                listData[position].count_times.toString()
            )
        }"

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en"){
            holder.txtCityAndCompany.textDirection = View.TEXT_DIRECTION_LTR
        }else{
            holder.txtCityAndCompany.textDirection = View.TEXT_DIRECTION_RTL
        }
        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${listData[position].image}")
            .placeholder(R.drawable.img_home)
            .into(holder.imgCertificate)
    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}