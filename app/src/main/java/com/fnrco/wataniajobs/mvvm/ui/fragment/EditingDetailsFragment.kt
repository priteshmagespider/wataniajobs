package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import com.fnrco.wataniajobs.adapter.ViewPagerFragmentAdapter
import com.fnrco.wataniajobs.databinding.FragmentEditingDetailsBinding


class EditingDetailsFragment : Fragment() {
    private val mTAG = HomeFragment::class.java.simpleName
    private lateinit var binding: FragmentEditingDetailsBinding
    lateinit var navController: NavController

    lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter

    lateinit var profileFragment: ProfileFragment
    lateinit var currentJobApplicationFragment: CurrentJobApplicationFragment

    private val tabsAthlete =
        arrayOf("Personal Metrics", "Personal Details")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEditingDetailsBinding.inflate(inflater, container, false)
        initUi()
        return binding.root
    }

    private fun initUi() {
        profileFragment = ProfileFragment()
        currentJobApplicationFragment = CurrentJobApplicationFragment()


        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.viewPager.isSaveFromParentEnabled = false
        viewPagerFragmentAdapter =
            ViewPagerFragmentAdapter(requireParentFragment().parentFragmentManager)
        viewPagerFragmentAdapter.addFragment(profileFragment, tabsAthlete[0])
        viewPagerFragmentAdapter.addFragment(
            currentJobApplicationFragment,
            tabsAthlete[1]
        )
        binding.viewPager.adapter = viewPagerFragmentAdapter
        viewPagerFragmentAdapter.notifyDataSetChanged()
    }

    companion object {

    }
}