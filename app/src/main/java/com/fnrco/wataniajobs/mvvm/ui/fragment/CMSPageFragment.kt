package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.databinding.FragmentCMSPageBinding
import com.fnrco.wataniajobs.mvvm.model.response.CMSPageResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject




@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class CMSPageFragment : Fragment() {

    private val mTAG = CMSPageFragment::class.java.simpleName
    private lateinit var binding: FragmentCMSPageBinding


    @ExperimentalCoroutinesApi
    private val viewModel: MoreScreenViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCMSPageBinding.inflate(layoutInflater, container, false)
        setObserver()
        when {
            requireArguments().containsKey(Constant.INTENT.PRIVACY_POLICY) -> {
                binding.txtCMSTitle.text = getString(com.fnrco.wataniajobs.R.string.lbl_privacy_policy)
                viewModel.setMoreScreenApiCall(
                    MoreScreenEvent.PrivacyPolicyEvent(
                        prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
                    )
                )
            }
            requireArguments().containsKey(Constant.INTENT.TERMS_CONDITION) -> {
                binding.txtCMSTitle.text = getString(com.fnrco.wataniajobs.R.string.lbl_terms_of_service)
                viewModel.setMoreScreenApiCall(
                    MoreScreenEvent.TermsConditionEvent(
                        prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
                    )
                )
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(
            mTAG,
            "CMSPageFragment CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setObserver() {
        /** PrivacyPolicy */
        viewModel.privacyPolicyResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<CMSPageResponseBean> -> {
                    displayProgressBar(false)
                    setPrivacyPolicyData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Terms Condition*/
        viewModel.termsConditionResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<CMSPageResponseBean> -> {
                    displayProgressBar(false)
                    setTermsConditionData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }


    private fun setPrivacyPolicyData(data: CMSPageResponseBean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtCMSContent.text =
//                    Html.fromHtml(
//                        "${data.setting!!.privacy_policy_en}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
            } else {
                val pish =
                    "<html><head><style type=\"text/css\">@font-face {font-family: droid_kufi_regular;src: url(\"file:///res/font/droid_kufi_regular.ttf\")}body {font-family: droid_kufi_regular;font-size: medium;text-align: justify;}</style></head><body>"
                val pas = "</body></html>"

                val unencodedHtml: String = data.setting!!.privacy_policy_ar!!
                val encodedHtml = Base64.encodeToString(
                    unencodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                val text =
                    ("<html><style type='text/css'>@font-face { font-family: droid_kufi_regular; src: url('fonts/droid_kufi_regular.ttf'); } body p {font-family: spqr;}</style>"
                            + "<body >" + "<p align=\"justify\" style=\"font-size: 22px; font-family: droid_kufi_regular;\">" +
                        data.setting!!.privacy_policy_ar!!.toString()
                     + "</p> " + "</body></html>")
                val myHtmlString = pish + encodedHtml + pas
            binding.webViewCMSContent.loadDataWithBaseURL("file:///android_asset/",text,"text/html","utf-8",null);
//            binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
//                binding.txtCMSContent.text =
//                    Html.fromHtml(
//                        "${data.setting!!.privacy_policy_ar}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
            }
        } else {
            val pish =
                "<html><head><style type=\"text/css\">@font-face {font-family: droid_kufi_regular;src: url(\"file:///res/font/droid_kufi_regular.ttf\")}body {font-family: droid_kufi_regular;font-size: medium;text-align: justify;}</style></head><body>"
            val pas = "</body></html>"

            val unencodedHtml: String = data.setting!!.privacy_policy_ar!!
            val encodedHtml = Base64.encodeToString(
                unencodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            val myHtmlString = pish + encodedHtml + pas
//            binding.webViewCMSContent.loadData(myHtmlString, "text/html", "base64")
//            binding.webViewCMSContent.loadData( myHtmlString, "text/html", "UTF-8")
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtCMSContent.text =
//                    Html.fromHtml("${data.setting.privacy_policy_en}")
//            } else {
//                binding.txtCMSContent.text =
//                    Html.fromHtml("${data.setting.privacy_policy_ar}")
//            }
        }
    }

    private fun setTermsConditionData(data: CMSPageResponseBean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtCMSContent.text =
//                    Html.fromHtml(
//                        "${data.setting!!.term_of_service_en}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//            } else {
//                binding.txtCMSContent.text =
//                    Html.fromHtml(
//                        "${data.setting!!.term_of_service_ar}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//            }
        } else {
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtCMSContent.text =
//                    Html.fromHtml("${data.setting!!.term_of_service_en}")
//            } else {
//                binding.txtCMSContent.text =
//                    Html.fromHtml("${data.setting!!.term_of_service_ar}")
//            }
        }
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }
}