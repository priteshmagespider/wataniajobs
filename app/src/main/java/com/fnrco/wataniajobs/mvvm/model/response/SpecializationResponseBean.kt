package com.fnrco.wataniajobs.mvvm.model.response

data class SpecializationResponseBean(
    var specializations: List<SpecializationData>? = null
)

data class SpecializationData(
    var id: Int? = null,
    var title_en: String? = null,
    var title: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    val roles: List<Role>?,
    var count: Int? = null
)

data class Role(
    val cat_id: Int?,
    val created_at: String?,
    val id: Int?,
    val title: String?,
    val title_en: String?,
    val updated_at: String?,
    var count: Int?,

)
