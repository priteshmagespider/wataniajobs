package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class AutoBioGraphyResponseBean(
    var posts: PostsAutoBioGraphy? = null
)

data class PostsAutoBioGraphy(
    var current_page: Int? = null,
    var `data`: List<DataAutoBioGraphy>? = null,
    var first_page_url: String? = null,
    var from: Int? = null,
    var last_page: Int? = null,
    var last_page_url: String? = null,
    var next_page_url: String? = null,
    var path: String? = null,
    var per_page: Int? = null,
    var prev_page_url: String? = null,
    var to: Int? = null,
    var total: Int? = null
)

data class DataAutoBioGraphy(
    var id: Int? = null,
    var is_favourite: Int? = null,
    var company_logo: String? = null,
    var com_id: String? = null,
    var title: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: String? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: String? = null,
    var computer_level: ComputerLevelAutoBioGraphy? = null,
    var MsOffice_level: String? = null,
    var other_condition: String? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: String? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: String? = null,
    var job_id: Int? = null,
    var website: String? = null,
    var com_email: String? = null,
    var com_name: String? = null,
    var com_logo: String? = null,
    var job_date: String? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var city: String? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: String? = null,
    var description: String? = null,
    var job_role: JobRoleAutoBioGraphy? = null,
    var specialization: SpecializationAutoBioGraphy? = null,
    var job_applicants: List<JobApplicantAutoBioGraphy>? = null,
    var qualification: QualificationAutoBioGraphy? = null,
    var job__type: JobTypeAutoBioGraphy? = null,
    var company: CompanyAutoBioGraphy? = null,
    var job_city: JobCityAutoBioGraphy? = null,
    var language_level: LanguageLevelAutoBioGraphy? = null,
    var office_level: OfficeLevelAutoBioGraphy? = null
)

data class CompanyAutoBioGraphy(
    var id: Int? = 0,
//    var user_id: UserId? = null,
    var user_id: Int? = null,
    var comNum: Int? = 0,
    var sector: String? = null,
    var mobile: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var city: String? = null,
    var address: String? = null,
    var logo: String? = null,
    var photo_approve: Int? = 0,
    var size: String? = null,
    var `field`: String? = null,
    var user_job_title: String? = null,
    var status: Int? = 0,
    var phone_is_verified: Int? = 0,
    var sms_try: Int? = 0,
    var phone_verification_code: Int? = 0,
    var career_objective: String? = null,
    var package_id: Int? = 0,
    var total_cv: Int? = 0,
    var expiration_date: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
):Serializable


data class JobCityAutoBioGraphy(
    var id: Int? = 0,
    var name_en: String? = null,
    var name_ar: String? = null
): Serializable

data class ComputerLevelAutoBioGraphy(
    var id: Int? = null,
    var title_ar: String? = null
)

data class JobRoleAutoBioGraphy(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class SpecializationAutoBioGraphy(
    var id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class JobApplicantAutoBioGraphy(
    var job_seeker_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var pivot: PivotAutoBioGraphy? = null
)

data class QualificationAutoBioGraphy(
    var id: Int? = null,
    var title_ar: String? = null
)

data class JobTypeAutoBioGraphy(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null
)

data class LanguageLevelAutoBioGraphy(
    var id: Int? = null,
    var title_ar: String? = null
)

data class OfficeLevelAutoBioGraphy(
    var id: Int? = null,
    var title_ar: String? = null
)

data class PivotAutoBioGraphy(
    var jobPost_id: Int? = null,
    var jobSeeker_id: Int? = null
)