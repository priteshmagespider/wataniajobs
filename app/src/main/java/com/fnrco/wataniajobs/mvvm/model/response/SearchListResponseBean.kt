package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class SearchListResponseBean(
//    var posts: MutableList<Post>? = null
    val count: Int?,
    val posts: Posts?
):Serializable

data class Posts(
    val current_page: Int?,
    val `data`: List<PostData>?,
    val first_page_url: String?,
    val from: Int?,
    val last_page: Int?,
    val last_page_url: String?,
    val next_page_url: String?,
    val path: String?,
    val per_page: Int?,
    val prev_page_url: Any?,
    val to: Int?,
    val total: Int?
)


data class Post(
    var type: Int = 0,
    var id: Int? = null,
    var is_favourite: Int? = null,
    var company_logo: String? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: Any? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: Any? = null,
    var computer_level: String? = null,
    var MsOffice_level: String? = null,
    var other_condition: Any? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: Any? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: Any? = null,
    var job_id: Any? = null,
    var website: Any? = null,
    var com_email: Any? = null,
    var com_name: Any? = null,
    var com_logo: Any? = null,
    var job_date: Any? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: Any? = null,
    var description_en: String? = null,
    var title_en: String? = "",
    var city: Any? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: Any? = null,
    var job__type: JobTypeSerach? = null,
    var job_city: JobCity? = null,
    var company: Company? = null
):Serializable

data class JobTypeSerach(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
):Serializable

data class JobCity(
    var id: Int? = 0,
    var name_en: String? = null,
    var name_ar: String? = null
):Serializable

data class Company(
    var id: Int? = 0,
    var user_id: Int? = null,
    var comNum: Int? = 0,
    var sector: String? = null,
    var mobile: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var city: String? = null,
    var address: String? = null,
    var logo: String? = null,
    var photo_approve: Int? = 0,
    var size: String? = null,
    var `field`: String? = null,
    var user_job_title: String? = null,
    var status: Int? = 0,
    var phone_is_verified: Int? = 0,
    var sms_try: Int? = 0,
    var phone_verification_code: Int? = 0,
    var career_objective: String? = null,
    var package_id: Int? = 0,
    var total_cv: Int? = 0,
    var expiration_date: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
):Serializable


data class UserId(
    var id: Int? = 0,
    var name: String? = null,
    var name_en: String? = null,
    var email: String? = null,
    var token: Int? = 0,
    var is_admin: Int? = 0,
    var provider: Int? = 0,
    var provider_id: Int? = 0,
    var mail: Int? = 0,
    var role: Int? = 0,
    var active: Int? = 0,
    var code_win: Int? = 0,
    var enter_by: Int? = 0,
    var created_at: String? = null,
    var updated_at: String? = null
):Serializable

data class PostData(
    var type: Int = 0,
    var id: Int? = null,
    var is_favourite: Int? = null,
    var company_logo: String? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: Any? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: Any? = null,
    var computer_level: String? = null,
    var MsOffice_level: String? = null,
    var other_condition: Any? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: Any? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: Any? = null,
    var job_id: Any? = null,
    var website: Any? = null,
    var com_email: Any? = null,
    var com_name: Any? = null,
    var com_logo: Any? = null,
    var job_date: Any? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: Any? = null,
    var description_en: String? = null,
    var title_en: String? = "",
    var city: Any? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: Any? = null,
    var job__type: JobTypeSerach? = null,
    var job_city: JobCity? = null,
    var company: Company? = null
):Serializable

data class ComputerLevel(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null,
    var created_at: Any? = null,
    var updated_at: Any? = null
):Serializable

data class Specialization(
    var id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
):Serializable

data class Qualification(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null,
    var created_at: Any? = null,
    var updated_at: Any? = null
):Serializable

data class JobType(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null,
    var created_at: Any? = null,
    var updated_at: Any? = null
):Serializable

data class LanguageLevel(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null,
    var created_at: Any? = null,
    var updated_at: Any? = null
):Serializable

data class OfficeLevel(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null,
    var created_at: Any? = null,
    var updated_at: Any? = null
):Serializable


data class JobRole(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)


