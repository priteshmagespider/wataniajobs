package com.fnrco.wataniajobs.mvvm.model.response

data class PhoneListResponseBean(
    val phones: List<Phone>?,
    val PrimaryPhone : String,
    val PrimaryMail : String,

)

data class Phone(
    val created_at: String?,
    val id: Int?,
    val phone: String?,
    val type: Any?,
    val updated_at: String?,
    val user_id: Int?,
    val isEditable : Boolean = false
)

