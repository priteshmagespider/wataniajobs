package com.fnrco.wataniajobs.mvvm.model.response

data class UpdateProfileResponseBean(
    var status: Boolean? = null,
    var message: String? = null
)