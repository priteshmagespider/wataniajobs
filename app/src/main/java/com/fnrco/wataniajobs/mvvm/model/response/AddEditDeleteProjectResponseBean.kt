package com.fnrco.wataniajobs.mvvm.model.response

class AddEditDeleteProjectResponseBean : ArrayList<AddEditDeleteProjectResponseBeanItem>()

data class AddEditDeleteProjectResponseBeanItem(
    val created_at: String? = null,
    val description: String? = null,
    val id: Int? = null,
    val job_seeker_id: Int? = null,
    val month: String? = null,
    val title: String? = null,
    val updated_at: String? = null,
    val user_id: Int? = null,
    val year: Int? = null
)