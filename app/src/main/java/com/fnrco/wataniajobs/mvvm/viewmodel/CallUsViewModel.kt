package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.response.CallUsResponseBean
import com.fnrco.wataniajobs.repository.CallUsRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CallUsViewModel
@ViewModelInject
constructor(
    private val callUsRepository: CallUsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    /** call us / contact us Response */
    private val _callUsResponse: MutableLiveData<DataState<CallUsResponseBean>> = MutableLiveData()

    val callUsResponse: LiveData<DataState<CallUsResponseBean>>
        get() = _callUsResponse


    fun setCallUsApiCall(callUsEvent: CallUsEvent) {
        viewModelScope.launch {
            when (callUsEvent) {
                is CallUsEvent.ContactUsEvent -> {
                    callUsRepository.callUsApiCall(
                        callUsEvent.name,
                        callUsEvent.email,
                        callUsEvent.message,
                        callUsEvent.type
                    ).onEach {
                        _callUsResponse.value = it
                    }
                        .launchIn(viewModelScope)
                }
                else -> {

                }
            }
        }
    }
}

sealed class CallUsEvent {

    data class ContactUsEvent(
        val name: String,
        val email: String,
        val message: String,
        val type: Int
    ) : CallUsEvent()
}