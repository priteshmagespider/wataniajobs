package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Experience
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class   ExperienceListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Experience>,
    val prefManager: PrefManager
) : RecyclerView.Adapter<ExperienceListAdapter.MyViewHolder>() {

    val listData = list as MutableList<Experience>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtStartDate: TextView = itemView.findViewById(R.id.txtStartDate)
        val txtTotalExp: TextView = itemView.findViewById(R.id.txtTotalExp)
        val txtJobTitle: TextView = itemView.findViewById(R.id.txtJobTitle)
        val txtCityAndCompany: TextView = itemView.findViewById(R.id.txtCityAndCompany)
        val imgExperienceDelete: ImageView = itemView.findViewById(R.id.imgExperienceDelete)
        val imgExperienceEdit: ImageView = itemView.findViewById(R.id.imgExperienceEdit)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_experience_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgExperienceEdit.tag = position
        holder.imgExperienceEdit.setOnClickListener(onClickListener)
        holder.imgExperienceDelete.tag = position
        holder.imgExperienceDelete.setOnClickListener(onClickListener)

        holder.txtStartDate.text =
            "${listData[position].start_date} - ${listData[position].end_date}"
        holder.txtTotalExp.text = context.resources.getString(
            R.string.lbl_total_exp,
            listData[position].total_experience_m
        )
        holder.txtJobTitle.text =
            context.resources.getString(R.string.lbl_job_title, listData[position].job_title)
        holder.txtCityAndCompany.text =
            "${listData[position].company_name} | ${listData[position].company_address}"

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en"){
            holder.txtCityAndCompany.textDirection = View.TEXT_DIRECTION_LTR
        }else{
            holder.txtCityAndCompany.textDirection = View.TEXT_DIRECTION_RTL
        }
    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return listData.size
    }
}