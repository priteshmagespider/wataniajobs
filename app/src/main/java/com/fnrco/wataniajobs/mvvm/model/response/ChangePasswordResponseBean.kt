package com.fnrco.wataniajobs.mvvm.model.response

data class ChangePasswordResponseBean(
    val result: String? = null
)