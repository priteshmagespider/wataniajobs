package com.fnrco.wataniajobs.mvvm.model.response

data class CategoriesListResponseBean(
    var categories: List<Role>? = null
)


data class CategoryListResponseBean(
    var id: Int? = null,
    var title_en: String? = null,
    var title: String? = null,
    var cat_id: Int? = null,
    var count: Int? = null
)