package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import com.fnrco.wataniajobs.mvvm.model.response.Post
import java.util.ArrayList

class SearchBusNameAdapter(context: Context, resource: Int) :
    ArrayAdapter<Post?>(context, resource), Filterable {
//    private val mlistData: List<Post>
    lateinit var mlistData: ArrayList<Post>

    fun setData(list: List<Post>) {
        mlistData.clear()
        mlistData.addAll(list)
    }

    override fun getCount(): Int {
        return mlistData.size
    }

    override fun getItem(position: Int): Post? {
        return mlistData[position]
    }





    fun getObject(position: Int): String {
        return mlistData[position].description.toString()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    filterResults.values = mlistData
                    filterResults.count = mlistData.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }

        }
    }

    /*override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val filterResults = FilterResults()
                *//*if (constraint != null) {
                    filterResults.values = mlistData
                    filterResults.count = mlistData.size
                }*//*
                return filterResults
            }

            override fun publishResults(
                constraint: CharSequence,
                results: FilterResults
            ) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }*/

    init {
        mlistData = ArrayList<Post>()
    }
}