package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.facebook.LoginStatusCallback
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.ActivityLoginBinding
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.LoginResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.UpdatePlayerResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.LoginEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.LoginViewModel
import com.fnrco.wataniajobs.mvvm.viewmodel.SignUpEvent
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.onesignal.OSSubscriptionObserver
import com.onesignal.OSSubscriptionStateChanges
import com.onesignal.OneSignal
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), View.OnClickListener, OSSubscriptionObserver {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivityLoginBinding

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: LoginViewModel by viewModels()


    private var lastClickTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)



        OneSignal.addSubscriptionObserver(this)

        val oneSignalUserID = OneSignal.getDeviceState()!!.userId
        Utils.print("ONESIGNALID---login----------------->$oneSignalUserID")
        if (oneSignalUserID != null) {
            prefManager.setString(PrefConstant.PREF_ONE_SIGNAL_ID, oneSignalUserID)
        }
        setContentView(binding.root)
        initUi()


        setObserver()
    }

    private fun initUi() {
        binding.txtForgotPassword.setOnClickListener(this)
        binding.txtRegisterNow.setOnClickListener(this)
        binding.btnLogin.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
        binding.txtPrefix.textDirection = View.TEXT_DIRECTION_LTR
//        binding.edtEmailMobNo.setText("")
        binding.edtEmailMobNo.isEnabled = false
//        Selection.setSelection(binding.edtEmailMobNo.text, binding.edtEmailMobNo.text.toString().length)
//        binding.edtEmailMobNo.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//            }
//
//            override fun afterTextChanged(s: Editable?) {
//                if(!s.toString().startsWith("")){
//                    binding.edtEmailMobNo.setText("")
//                    Selection.setSelection(binding.edtEmailMobNo.text, binding.edtEmailMobNo.text.toString().length)
//                }
//            }
//        })
    }

    private fun setObserver() {
        viewModel.updatePlayerResponse.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<UpdatePlayerResponseBean> -> {
                    displayProgressBar(false)
                    startActivity(Intent(this, HomeActivity::class.java))
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.loginResponseBean.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<LoginResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.error != null) {
                        Toast.makeText(this, dataState.data.message, Toast.LENGTH_SHORT).show()
                    } else {
                        if (dataState.data.user?.is_admin == 2) {
                            prefManager.setString(
                                PrefConstant.PREF_ACCESS_TOKEN,
                                dataState.data.access_token
                            )
                            if (dataState.data.user.name != null) {
                                prefManager.setString(
                                    PrefConstant.PREF_USER_NAME,
                                    dataState.data.user.name
                                )
                            } else {
                                prefManager.setString(
                                    PrefConstant.PREF_USER_NAME,
                                    dataState.data.user.name_en
                                )
                            }

                            prefManager.setString(
                                PrefConstant.PREF_USER_PHOTO,
                                dataState.data.user.photo
                            )

                            prefManager.setBoolean(PrefConstant.PREF_IS_LOGIN, true)
                            Utils.print(
                                "ONE_SIGNAL_LOGIN--------------->" + prefManager.getString(
                                    PrefConstant.PREF_ONE_SIGNAL_ID
                                )!!
                            )
                            viewModel.callLoginEvent(
                                LoginEvent.UpdatePlayer(
                                    prefManager.getString(
                                        PrefConstant.PREF_ONE_SIGNAL_ID
                                    )!!
                                )
                            )
                        } else {
                            displayError(getString(R.string.error_this_is_not_job_seeker_account))
                        }

                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.txtForgotPassword -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                val intent = Intent(
                    this,
                    ForgotPasswordActivity::class.java
                ).putExtra(
                    Constant.INTENT.LOGIN_TO_FORGOT_FOR_PHONE,
                    Constant.INTENT.LOGIN_TO_FORGOT_FOR_PHONE
                )
                startActivity(intent)
            }
            R.id.txtRegisterNow -> {
                val intent = Intent(
                    this,
                    SignUpActivity::class.java
                )
                startActivity(intent)
            }
            R.id.btnLogin -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                if (Utils.isConnected(this)) {
                    if (isValid()) {
                        val authRequest = AuthRequest()
                        authRequest.client_id = Constant.CLIENT_ID
                        authRequest.client_secret = Constant.CLIENT_SECRET
                        authRequest.grant_type = Constant.GRANT_TYPE
                        if (binding.edtMobNo.text.toString().startsWith("0")) {
                            val result = binding.edtMobNo.text.toString().trim().drop(1)
                            authRequest.username = "+966$result"
                            Log.d("WITH ZERO", "SIGNIN: ${authRequest.username}")
                        } else {
                            authRequest.username = "+966${binding.edtMobNo.text.toString().trim()}"
                            Log.d("WITHOUT ZERO", "SIGNIN: ${authRequest.username}")
                        }

                        authRequest.password = binding.editPass.text.toString().trim()

                        viewModel.callLoginEvent(LoginEvent.DoLogin(authRequest))
                    }
                } else {
                    Utils.showSnackBar(binding.mainLayout, this)
                }
            }
        }
    }

    private fun isValid(): Boolean {
//        binding.edtEmailMobNo.isErrorEnabled = false
//        binding.inputPassword.isErrorEnabled = false

        when {
            binding.edtMobNo.text.toString().isEmpty() -> {
                binding.edtMobNo.error = getString(R.string.error_enter_phone)
                binding.edtMobNo.requestFocus()
                return false
            }
            binding.editPass.text.toString().isEmpty() -> {
                binding.editPass.error = getString(R.string.error_enter_password)
                binding.editPass.requestFocus()
                return false
            }
            binding.editPass.text.toString().length < 6 -> {
                binding.editPass.error = getString(R.string.error_enter_password_long)
                binding.editPass.requestFocus()
                return false
            }
        }

        return true
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onOSSubscriptionChanged(stateChanges: OSSubscriptionStateChanges?) {
        /*Utils.print("PLAYER_ID--------isSubscribed--------------------->"+stateChanges!!.to.userId)
        if(stateChanges.to.userId!="")
        {
            prefManager.setString(PrefConstant.PREF_ONE_SIGNAL_ID, stateChanges.to.userId)
        }*/
        if (!stateChanges!!.from.isSubscribed &&
            stateChanges.to.isSubscribed
        ) {
            // The user is subscribed
            // Either the user subscribed for the first time
            // Or the user was subscribed -> unsubscribed -> subscribed
            stateChanges.to.userId
            // Make a POST call to your server with the user ID
        }
    }
}