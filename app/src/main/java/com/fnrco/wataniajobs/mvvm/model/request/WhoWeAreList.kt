package com.fnrco.wataniajobs.mvvm.model.request

class WhoWeAreList(
    var name: String,
    var isSelected: Boolean
)