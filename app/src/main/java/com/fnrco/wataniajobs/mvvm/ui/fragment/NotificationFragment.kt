package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.common.SwipeGesture
import com.fnrco.wataniajobs.databinding.FragmentNotificationBinding
import com.fnrco.wataniajobs.mvvm.adapter.NotificationListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.DeleteJobPostResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.NotificationListResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.SaveDeletePhoneEmailResponse
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginActivity
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginPopUpActivity
import com.fnrco.wataniajobs.mvvm.viewmodel.NotificationEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.NotificationViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class NotificationFragment : Fragment(), View.OnClickListener {

    private val mTAG = NotificationFragment::class.java.simpleName
    private lateinit var binding: FragmentNotificationBinding

    private lateinit var notificationListAdapter: NotificationListAdapter
    var pos: Int = -1

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: NotificationViewModel by viewModels()
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationBinding.inflate(layoutInflater, container, false)
        setObserver()
        viewModel.setNotificationApiCall(
            NotificationEvent.NotificationListEvent(
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
            )
        )
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        initUi()
    }

    private fun initUi() {
        binding.apply {
            imgBack.setOnClickListener {
                navController.popBackStack()
            }
        }
    }

    private fun setObserver() {
        /** Notification */
        viewModel.notificationResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<NotificationListResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.notifications!!.isEmpty()) {
                        binding.txtNoRecord.visibility = View.VISIBLE
                    } else {
                        binding.txtNoRecord.visibility = View.GONE
                        setData(dataState.data)
                    }

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Notification  delete*/
        viewModel.deleteNotification.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<DeleteJobPostResponseBean> -> {
                    displayProgressBar(false)
                    notificationListAdapter.deleteItem(pos)

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun setData(data: NotificationListResponseBean) {
        binding.rvNotificationList.layoutManager = LinearLayoutManager(requireContext())
        notificationListAdapter =
            NotificationListAdapter(requireContext(), data.notifications!!, prefManager, this)
        val swipeGesture = object : SwipeGesture(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        alertPopup(viewHolder.adapterPosition)

//                when(direction){
//                    ItemTouchHelper.LEFT ->{
//                        val alertDialog = AlertDialog.Builder(requireContext())
//                        alertDialog.setCancelable(true)
//                        alertDialog.setMessage(getString(R.string.notification_delete))
//                        alertDialog.setPositiveButton("OK", object : DialogInterface.OnClickListener{
//                            override fun onClick(dialog: DialogInterface?, which: Int) {
//
//                            }
//
//                        })
//                        alertDialog.setNegativeButton("Cancel" , object : DialogInterface.OnClickListener{
//                            override fun onClick(dialog: DialogInterface?, which: Int) {
//                                dialog!!.dismiss()
//                            }
//
//                        })
////                        notificationListAdapter.deleteItem(viewHolder.adapterPosition)
//                    }
//                }
            }
        }
        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(binding.rvNotificationList)
        binding.rvNotificationList.adapter = notificationListAdapter
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    fun alertPopup(index : Int){
        val alertDialog = AlertDialog.Builder(requireContext())
        alertDialog.setCancelable(false)
        alertDialog.setMessage(getString(R.string.notification_delete))
        alertDialog.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                pos = index
                viewModel.setNotificationApiCall(
                    NotificationEvent.DeleteNotification(
                        notificationListAdapter.list[pos].id.toString()
                    )
                )
            }

        })
        alertDialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                notificationListAdapter.notifyDataSetChanged()
                dialog!!.dismiss()
            }

        })
        alertDialog.show()
    }
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.clNotification -> {
                if (!prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    loginDialog(requireContext())
                } else {
                    val position = v.tag.toString().toInt()
                    Utils.print("Job_Type------------>" + notificationListAdapter.list[position].post_id)
                    if(notificationListAdapter.list[position].post_id !=null){
                        val bundle = Bundle()
                        bundle.putInt(Constant.POST_ID, notificationListAdapter.list[position].post_id!!)
                        navController.navigate(
                            R.id.action_notificationFragment_to_homeDetailFragment,
                            bundle
                        )
                    }

                }
            }
        }
    }


    private fun loginDialog(context: Context) {
        val intent = Intent(requireContext(), LoginPopUpActivity::class.java)
        startActivity(intent)
        /*MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.title_login_dialog)

            .setPositiveButton(R.string.login_in) { dialog, _ ->
                context.startActivity(Intent(context, LoginActivity::class.java))
                dialog.dismiss()
            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()*/
    }
}

//Latest jobs based on your profile => RED = 3
//Al Huda Company has posted a job => Green = 1
//Successfully applied for the position of Graphic Designer => BLUE = 2
//Congratulations, your CV has been featured! => YELLOW = 4