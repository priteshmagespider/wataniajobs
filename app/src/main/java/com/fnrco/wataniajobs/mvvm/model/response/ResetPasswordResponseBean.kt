package com.fnrco.wataniajobs.mvvm.model.response

data class ResetPasswordResponseBean(
    val message: String?
)