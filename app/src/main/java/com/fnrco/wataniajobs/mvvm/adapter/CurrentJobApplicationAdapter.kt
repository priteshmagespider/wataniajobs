package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobPost
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobPostData
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.card.MaterialCardView

class CurrentJobApplicationAdapter(
    var context: Context,
    val onClickListener: View.OnClickListener,
    var prefManager: PrefManager
) : RecyclerView.Adapter<CurrentJobApplicationAdapter.MyViewHolder>() {
    lateinit var posts: ArrayList<AppliedJobPostData>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardCurrentJobApplication: MaterialCardView =
            itemView.findViewById(R.id.cardCurrentJobApplication)
        val imgCompanyLogo: ImageView = itemView.findViewById(R.id.imgCompanyLogo)
        val imgMoreOption: ImageView = itemView.findViewById(R.id.imgMoreOption)
        val txtJobTitle: TextView = itemView.findViewById(R.id.txtJobTitle)
        val txtJobCity: TextView = itemView.findViewById(R.id.txtJobCity)
        val txtLocation: TextView = itemView.findViewById(R.id.txtLocation)
        val txtJobQualification: TextView = itemView.findViewById(R.id.txtJobQualification)
        val txtSubMissionDateStart: TextView = itemView.findViewById(R.id.txtSubMissionDateStart)
        val txtCompletionDate: TextView = itemView.findViewById(R.id.txtCompletionDate)
        val txtReviewStatus: TextView = itemView.findViewById(R.id.txtReviewStatus)
        val btnWithdraw: AppCompatButton = itemView.findViewById(R.id.btnWithdraw)
        val clViewedStatus: ConstraintLayout = itemView.findViewById(R.id.clViewedStatus)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_current_job_application_items, parent, false)
        )
    }

    fun addData(list: List<AppliedJobPostData>) {
        posts = ArrayList()
        posts.addAll(list)
    }

    fun getList(): ArrayList<AppliedJobPostData> {
        return posts
    }

    fun removeItem(position: Int) {
        posts.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.txtJobTitle.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtJobCity.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtLocation.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtJobQualification.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtSubMissionDateStart.textDirection = View.TEXT_DIRECTION_RTL
        //holder.txtCompletionDate.textDirection = View.TEXT_DIRECTION_RTL


        Utils.parseDateToddMMyyyy(
            posts[position].created_at,
            "yyyy-MM-dd HH:mm:ss",
            "dd-MM-yyyy"
        )
        holder.txtSubMissionDateStart.text =
            context.resources.getString(R.string.lbl_submission_date) + ":" + Utils.parseDateToddMMyyyy(
                posts?.get(position)!!.created_at,
                "yyyy-MM-dd HH:mm:ss",
                "dd-MM-yyyy"
            )
        holder.txtCompletionDate.text =
            context.resources.getString(R.string.lbl_completion_date) + ":" + Utils.parseDateToddMMyyyy(
                posts?.get(position)!!.updated_at,
                "yyyy-MM-dd HH:mm:ss",
                "dd-MM-yyyy"
            )

        posts[position].let { safeCompanyValue ->
            holder.txtLocation.text =
                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") safeCompanyValue.job_city?.name_en else safeCompanyValue.job_city?.name_ar
        }



        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtJobTitle.text = posts.get(position).title_en
            posts[position].qualification?.let { safeQualificationValue ->
                holder.txtJobQualification.text = safeQualificationValue.title_en
            } ?: run {
                holder.txtJobQualification.text = ""
            }

            posts[position].job_city?.let { safeJobCityValue ->
                holder.txtJobCity.text = safeJobCityValue.name_en
            } ?: run {
                holder.txtJobCity.text = ""
            }


        } else {
            holder.txtJobTitle.text = posts.get(position).title
            posts[position].qualification?.let { safeQualificationValue ->
                holder.txtJobQualification.text = safeQualificationValue.title_ar
            } ?: run {
                holder.txtJobQualification.text = ""
            }

            posts[position].job_city?.let { safeJobCityValue ->
                holder.txtJobCity.text = safeJobCityValue.name_ar
            } ?: run {
                holder.txtJobCity.text = ""
            }

        }

        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${posts[position].company_logo}")
            .placeholder(R.drawable.img_home)
            .into(holder.imgCompanyLogo)

        if (posts[position].status == "1") {
            holder.txtReviewStatus.text =
                context.resources.getString(R.string.lbl_your_application_has_been_reviewed)
        } else {
            holder.txtReviewStatus.text =
                context.resources.getString(R.string.lbl_your_application_has_been_reviewed)
        }
        if (posts[position].is_show == 1) {
            holder.clViewedStatus.visibility = View.VISIBLE
        }

        holder.imgMoreOption.tag = position
        holder.imgMoreOption.setOnClickListener(onClickListener)
        holder.btnWithdraw.tag = position
        holder.btnWithdraw.setOnClickListener(onClickListener)
//        for (i in posts[position].job_applicant!!.indices) {
//            if (posts[position].job_applicant!![i].user!!.name == prefManager.getString(PrefConstant.PREF_USER_NAME) || posts[position].job_applicant!![i].user!!.name_en == prefManager.getString(
//                    PrefConstant.PREF_USER_NAME
//                )
//            ){
//
//            }
//        }

    }

    override fun getItemCount(): Int {
        return posts.size
    }
}