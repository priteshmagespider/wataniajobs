package com.fnrco.wataniajobs.mvvm.model.response

data class LanguageAddDeleteResponseBean(
    val success: String? = null
)