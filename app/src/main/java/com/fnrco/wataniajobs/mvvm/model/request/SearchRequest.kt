package com.fnrco.wataniajobs.mvvm.model.request

import java.io.Serializable

class SearchRequest:Serializable {
    var jobTitle: String? = null
    var city: Int? = 0
    var jobCat: Int? = 0
    var subCat: Int? = 0
    var from: Int? = 0
    var to: Int? = 0
    var jobType: ArrayList<Int> = ArrayList()
    var gender: Int? = 0
    var certificate: Int? = 0
    var created_at: Int? = 0
}