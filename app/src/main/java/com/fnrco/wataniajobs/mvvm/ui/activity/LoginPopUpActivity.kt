package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.ActivityLoginPopupBinding
import com.fnrco.wataniajobs.mvvm.viewmodel.LoginViewModel
import com.fnrco.wataniajobs.pref.PrefManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginPopUpActivity : AppCompatActivity(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivityLoginPopupBinding

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: LoginViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginPopupBinding.inflate(layoutInflater)
        initUi()
        setContentView(binding.root)
    }

    private fun initUi() {
        binding.txtLoginNow.setOnClickListener(this)
        binding.btnCreateAnAccount.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.txtLoginNow -> {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.btnCreateAnAccount -> {
                startActivity(
                    Intent(
                        this,
                        SignUpActivity::class.java
                    )
                )
            }
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }


}