package com.fnrco.wataniajobs.mvvm.model.response

data class LanguageListResponseBean(
    val langs: List<Languages>?
)

data class Languages(
    val id: Int?,
    val name: String?
)