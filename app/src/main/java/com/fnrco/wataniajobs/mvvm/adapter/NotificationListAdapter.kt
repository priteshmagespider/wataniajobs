package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.compose.ui.text.style.TextDirection
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobPost
import com.fnrco.wataniajobs.mvvm.model.response.Notification
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.google.android.material.card.MaterialCardView

class NotificationListAdapter(
    val context: Context,
    var list: List<Notification>,
    var prefManager: PrefManager,
    var onClickListener: View.OnClickListener
) : RecyclerView.Adapter<NotificationListAdapter.MyViewHolder>() {

    private lateinit var mListener: onItemClickListener

    val listData: MutableList<Notification> = list as MutableList<Notification>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNotificationTitle: TextView = itemView.findViewById(R.id.txtNotificationTitle)
        val txtMessage: TextView = itemView.findViewById(R.id.txtMessage)
        val txtTimeAgo: TextView = itemView.findViewById(R.id.txtTimeAgo)
        val clNotification: ConstraintLayout = itemView.findViewById(R.id.clNotification)
        val imgNotification: ImageView = itemView.findViewById(R.id.imgNotification)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_notification_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtNotificationTitle.text = listData[position].title_en
            holder.txtMessage.text = listData[position].message_en
            holder.txtNotificationTitle.textDirection = View.TEXT_DIRECTION_LTR
        } else {
            holder.txtNotificationTitle.textDirection = View.TEXT_DIRECTION_RTL
            holder.txtNotificationTitle.text = listData[position].title_ar
            holder.txtMessage.text = listData[position].message_ar
        }
        when (listData[position].type) {
            "1" -> {
                holder.imgNotification.setImageResource(R.drawable.img_notification_share)
            }
            "2" -> {
                holder.imgNotification.setImageResource(R.drawable.img_notification_applied)
            }
            "3" -> {
                holder.imgNotification.setImageResource(R.drawable.img_notification_bag)
            }
            "4" -> {
                holder.imgNotification.setImageResource(R.drawable.img_notification_badge)
            }
        }
        holder.clNotification.tag = position
        holder.clNotification.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int = listData.size


    interface onItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener) {
        mListener = listener
    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }

}