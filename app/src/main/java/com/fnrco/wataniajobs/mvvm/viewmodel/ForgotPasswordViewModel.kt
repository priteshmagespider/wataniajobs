package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.ChangePasswordResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.ResetPasswordResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.SendVerifyOtpResponseBean
import com.fnrco.wataniajobs.repository.ForgotPasswordRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class ForgotPasswordViewModel
@ViewModelInject
constructor(
    private val forgotPasswordRepository: ForgotPasswordRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    /** Send Verify Otp  */
    private val _sendVerifyOtpForgotPasswordResponse: MutableLiveData<DataState<SendVerifyOtpResponseBean>> =
        MutableLiveData()

    val sendVerifyOtpForgotPasswordResponse: LiveData<DataState<SendVerifyOtpResponseBean>>
        get() = _sendVerifyOtpForgotPasswordResponse


    /** Reset Password  */
    private val _resetPasswordResponse: MutableLiveData<DataState<ResetPasswordResponseBean>> =
        MutableLiveData()

    val resetPasswordResponse: LiveData<DataState<ResetPasswordResponseBean>>
        get() = _resetPasswordResponse

    /** Change Password  */
    private val _changePasswordResponse: MutableLiveData<DataState<ChangePasswordResponseBean>> =
        MutableLiveData()

    val changePasswordResponse: LiveData<DataState<ChangePasswordResponseBean>>
        get() = _changePasswordResponse

    fun setForgotPasswordApiCall(forgotPasswordEvent: ForgotPasswordEvent) {
        viewModelScope.launch {
            when (forgotPasswordEvent) {
                is ForgotPasswordEvent.SendVerifyForgotPasswordOTPForPhone -> {
                    forgotPasswordRepository.sendVerifyOtpForPhoneApiCall(forgotPasswordEvent.authRequest)
                        .onEach {
                            _sendVerifyOtpForgotPasswordResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }

                is ForgotPasswordEvent.SendVerifyForgotPasswordOTPForEmail -> {
                    forgotPasswordRepository.sendVerifyOtpForEmail(forgotPasswordEvent.authRequest)
                        .onEach {
                            _sendVerifyOtpForgotPasswordResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is ForgotPasswordEvent.ResetPasswordEvent -> {
                    forgotPasswordRepository.resetPasswordApiCall(forgotPasswordEvent.authRequest)
                        .onEach {
                            _resetPasswordResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is ForgotPasswordEvent.ChangePasswordEvent -> {
                    forgotPasswordRepository.changePasswordApiCall(
                        forgotPasswordEvent.curPassword,
                        forgotPasswordEvent.newPassword
                    )
                        .onEach {
                            _changePasswordResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
            }
        }
    }

}

sealed class ForgotPasswordEvent {

    data class SendVerifyForgotPasswordOTPForPhone(val authRequest: AuthRequest) : ForgotPasswordEvent()
    data class SendVerifyForgotPasswordOTPForEmail(val authRequest: AuthRequest) : ForgotPasswordEvent()

    data class ResetPasswordEvent(val authRequest: AuthRequest) : ForgotPasswordEvent()

    data class ChangePasswordEvent(val curPassword: String, val newPassword: String) :
        ForgotPasswordEvent()
}