package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentCvExcellenceBinding
import com.fnrco.wataniajobs.databinding.FragmentCvExcellenceNewBinding
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobPostListResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.CvExcellenceResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.utils.DataState
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class CvExcellenceFragment : Fragment(),View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentCvExcellenceNewBinding
    lateinit var navController: NavController

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    lateinit var bottomSheetDialog: BottomSheetDialog
    lateinit var bottomSheetView: View

    lateinit var bottomSheetThankYouDialog: BottomSheetDialog
    lateinit var bottomSheetThankYouView: View
    var message: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCvExcellenceNewBinding.inflate(inflater, container, false)
        initUi()
        viewModel.setStateEvent(EditProfileStateEvent.CvExcellence)
        setObserver()

        return binding.root
    }

    private fun forCvExcellence() {
        bottomSheetDialog = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )

        bottomSheetView =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.dialoge_cv_excellence,
                    requireActivity().findViewById<View>(R.id.constraintCVExcellence) as ConstraintLayout?
                )


//        bottomSheetView.findViewById<TextView>(R.id.txtCvExcellence).text = message
        bottomSheetView.findViewById<TextView>(R.id.txtCvExcellence).text = "تمت معالجة طلبك ، حاليا قيد المراجعة"
        bottomSheetView.findViewById<View>(R.id.btnCVHighLighting)
            .setOnClickListener {
                forThankYouDialog()
                bottomSheetDialog.dismiss()
            }
        bottomSheetView.findViewById<View>(R.id.btnCancel)
            .setOnClickListener(View.OnClickListener {
                bottomSheetDialog.dismiss()
            })

        bottomSheetDialog.setContentView(bottomSheetView)
        bottomSheetDialog.show()
    }


    private fun forThankYouDialog() {
        bottomSheetThankYouDialog = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetThankYouView =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.dialoge_thank_you,
                    requireActivity().findViewById<View>(R.id.constraintThankYou) as ConstraintLayout?
                )
        bottomSheetThankYouView.findViewById<View>(R.id.btnOkay)
            .setOnClickListener {
                bottomSheetThankYouDialog.dismiss()
            }
        bottomSheetThankYouView.findViewById<View>(R.id.btnCancel)
            .setOnClickListener(View.OnClickListener {
                bottomSheetThankYouDialog.dismiss()
            })

        bottomSheetThankYouDialog.setContentView(bottomSheetThankYouView)
        bottomSheetThankYouDialog.show()
    }

    private fun initUi() {
        binding.btnCVHighLighting.setOnClickListener(this)
    }
    private fun setObserver() {
        viewModel.cvExcellenceResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<CvExcellenceResponseBean> -> {
                    displayProgressBar(false)
                    message = dataState.data.message
//                    binding.txtCvExcellence.text = dataState.data.message

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }
    companion object {

    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onClick(v: View?) {
        when(v!!.id)
        {
            R.id.btnCVHighLighting -> {
                forCvExcellence()
            }
        }
    }

}