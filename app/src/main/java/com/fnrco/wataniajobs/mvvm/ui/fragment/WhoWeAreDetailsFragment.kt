package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.*
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.WhoWeAreAdapter
import com.fnrco.wataniajobs.databinding.FragmentWhoWeAreDetailsBinding
import com.fnrco.wataniajobs.mvvm.adapter.ClientListAdapter
import com.fnrco.wataniajobs.mvvm.model.request.WhoWeAreList
import com.fnrco.wataniajobs.mvvm.model.response.CallUsResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.ClientsListResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.WhoWeAreResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.CallUsViewModel
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class WhoWeAreDetailsFragment : Fragment(), View.OnClickListener {

    private val mTAG = WhoWeAreDetailsFragment::class.java.simpleName
    private lateinit var binding: FragmentWhoWeAreDetailsBinding
    private lateinit var whoWeAreAdapter: WhoWeAreAdapter

    @ExperimentalCoroutinesApi
    private val viewModel by viewModels<MoreScreenViewModel>()

    @Inject
    lateinit var prefManager: PrefManager


    private var whoWeAreList: ArrayList<WhoWeAreList> = ArrayList()
    private lateinit var clientListAdapter: ClientListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWhoWeAreDetailsBinding.inflate(layoutInflater)
        setObserver()

        binding.cardClientList.visibility = View.GONE
        viewModel.setMoreScreenApiCall(
            MoreScreenEvent.AboutEvent(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
        Log.d(
            mTAG,
            "WhoWeAreDetailsFragment CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        whoWeAreList.add(
            WhoWeAreList(
                requireActivity().getString(R.string.lbl_about_the_company),
                true
            )
        )
        whoWeAreList.add(
            WhoWeAreList(
                requireActivity().getString(R.string.lbl_director_speech),
                false
            )
        )
        whoWeAreList.add(
            WhoWeAreList(
                requireActivity().getString(R.string.lbl_vision_goals),
                false
            )
        )
        whoWeAreList.add(WhoWeAreList(requireActivity().getString(R.string.lbl_clients), false))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Utils.switchToCzLocale(
                requireContext(),
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                ""
            );
        }
        binding.apply {
            rvWhoWeAre.layoutManager =
                LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
            whoWeAreAdapter =
                WhoWeAreAdapter(requireContext(), whoWeAreList, this@WhoWeAreDetailsFragment)
            rvWhoWeAre.adapter = whoWeAreAdapter
        }

    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                findNavController().popBackStack()
            }
            R.id.txtWhoWerOption -> {
                val pos = view.tag.toString().toInt()
                Log.d(mTAG, "onClick: $pos")
//                if (!whoWeAreAdapter.list[pos].isSelected) {
//                    whoWeAreAdapter.list[pos].isSelected = true
//                }
                whoWeAreAdapter.isSelected(pos)
                when (pos) {
                    0 -> {
                        binding.txtVision.visibility = View.GONE
                        binding.txtGoals.visibility = View.GONE
                        binding.cardWhoWeAreGoals.visibility = View.GONE
                        binding.cardClientList.visibility = View.GONE
                        binding.webViewCMSContent.visibility = View.VISIBLE
                        binding.webViewCMSVision.visibility = View.GONE
                        binding.webViewCMSGoals.visibility = View.GONE
                        viewModel.setMoreScreenApiCall(
                            MoreScreenEvent.AboutEvent(
                                prefManager.getString(
                                    PrefConstant.PREF_CURRENT_LANGUAGE
                                )!!
                            )
                        )
                    }
                    1 -> {
                        binding.txtVision.visibility = View.GONE
                        binding.txtGoals.visibility = View.GONE
                        binding.cardWhoWeAreGoals.visibility = View.GONE
                        binding.cardClientList.visibility = View.GONE
                        binding.webViewCMSContent.visibility = View.VISIBLE
                        binding.webViewCMSVision.visibility = View.GONE
                        binding.webViewCMSGoals.visibility = View.GONE
                        viewModel.setMoreScreenApiCall(
                            MoreScreenEvent.SpeechEvent(
                                prefManager.getString(
                                    PrefConstant.PREF_CURRENT_LANGUAGE
                                )!!
                            )
                        )
                    }
                    2 -> {
                        binding.txtVision.visibility = View.VISIBLE
                        binding.txtGoals.visibility = View.VISIBLE
                        binding.cardWhoWeAreGoals.visibility = View.VISIBLE
                        binding.cardClientList.visibility = View.GONE
                        binding.webViewCMSContent.visibility = View.GONE
                        binding.webViewCMSVision.visibility = View.VISIBLE
                        binding.webViewCMSGoals.visibility = View.VISIBLE
                        viewModel.setMoreScreenApiCall(
                            MoreScreenEvent.VisionGoalsEvent(
                                prefManager.getString(
                                    PrefConstant.PREF_CURRENT_LANGUAGE
                                )!!
                            )
                        )
                    }
                    3 -> {
                        binding.txtVision.visibility = View.GONE
                        binding.txtGoals.visibility = View.GONE
                        binding.cardClientList.visibility = View.VISIBLE
                        binding.nsvWhoWeAre.visibility = View.GONE

                        viewModel.setMoreScreenApiCall(
                            MoreScreenEvent.ClientListEvent(
                                prefManager.getString(
                                    PrefConstant.PREF_CURRENT_LANGUAGE
                                )!!
                            )
                        )
                    }
                }
            }
        }
    }

    private fun setObserver() {
        /** About */
        viewModel.aboutResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<WhoWeAreResponseBean> -> {
                    displayProgressBar(false)

                    setAboutData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
                else -> {}
            }
        }
        /** Speech */
        viewModel.speechResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<WhoWeAreResponseBean> -> {
                    displayProgressBar(false)
                    speechData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        /** Vision Goals */
        viewModel.visionGoalsResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<WhoWeAreResponseBean> -> {
                    displayProgressBar(false)
                    visionGoalsData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Clients */
        viewModel.clientsListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<ClientsListResponseBean> -> {
                    displayProgressBar(false)
                    setData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun setData(data: ClientsListResponseBean) {
        binding.rvClientsList.layoutManager = LinearLayoutManager(requireContext())
        clientListAdapter = ClientListAdapter(requireContext(), data.clients!!, prefManager)
        binding.rvClientsList.adapter = clientListAdapter
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun setAboutData(whoWeAreResponseBean: WhoWeAreResponseBean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Utils.switchToCzLocale(
                requireContext(),
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                ""
            );
        }
        Log.d(
            mTAG,
            "setAboutData CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        Log.d(
            mTAG,
            "setObserver: ${Constant.IMAGE_URL}${whoWeAreResponseBean.setting!!.about_photo}"
        )
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.about_app_image}")
            .into(binding.imgWhoWeAre)

        binding.txtTitle.text = getString(R.string.lbl_about_the_company)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val unencodedHtml: String = whoWeAreResponseBean.setting.about_app_en!!.toString()
            val encodedHtml = Base64.encodeToString(
                unencodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
        } else {
            val unencodedHtml: String =
                "<html dir=\"rtl\" lang=\"\"><body>${whoWeAreResponseBean.setting.about_app_ar!!.toString()}</body></html>"
            val encodedHtml = Base64.encodeToString(
                unencodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtContent.text =
//                    Html.fromHtml(
//
//                        "${whoWeAreResponseBean.setting.about_app_en}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//            } else {
//                binding.txtContent.text =
//                    Html.fromHtml(
//                        "${whoWeAreResponseBean.setting.about_app_ar}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//            }
//        } else {
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtContent.text =
//                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.about_app_en}")
//            } else {
//                binding.txtContent.text =
//                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.about_app_ar}")
//            }
//        }
    }

    private fun speechData(whoWeAreResponseBean: WhoWeAreResponseBean) {
        binding.webViewCMSVision.visibility = View.GONE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Utils.switchToCzLocale(
                requireContext(),
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                ""
            );
        }
        Log.d(
            mTAG,
            "speechData CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        Log.d(
            mTAG,
            "setObserver: ${Constant.IMAGE_URL}${whoWeAreResponseBean.setting!!.speech_photo}"
        )
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.speech_photo}")
            .into(binding.imgWhoWeAre)

        binding.txtTitle.text = getString(R.string.lbl_director_speech)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            binding.txtTitle.text = getString(R.string.lbl_director_speech)
            val unencodedHtml: String = whoWeAreResponseBean.setting.speech_en!!.toString()
            val encodedHtml = Base64.encodeToString(
                unencodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")


        } else {
            binding.txtTitle.text = getString(R.string.lbl_director_speech)
            val unencodedHtml: String = "<html dir=\"rtl\" lang=\"\"><body>${whoWeAreResponseBean.setting.speech_ar!!.toString()!!.toString()}</body></html>"
            val encodedHtml = Base64.encodeToString(
                unencodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
        }
    }

    private fun visionGoalsData(whoWeAreResponseBean: WhoWeAreResponseBean) {
        Log.d(
            mTAG,
            "visionGoalsData CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        Log.d(
            mTAG,
            "setObserver: ${Constant.IMAGE_URL}${whoWeAreResponseBean.setting!!.vision_photo}"
        )
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        binding.txtVision.visibility = View.VISIBLE
        binding.txtGoals.visibility = View.VISIBLE
        binding.cardWhoWeAreGoals.visibility = View.VISIBLE
        binding.webViewCMSVision.visibility = View.VISIBLE
        binding.webViewCMSVision.visibility = View.VISIBLE
        binding.webViewCMSGoals.visibility = View.VISIBLE
        binding.txtContentGoals.visibility = View.GONE
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.vision_photo}")
            .into(binding.imgWhoWeAre)
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.mission_photo}")
            .into(binding.imgWhoWeAreGoals)

        binding.txtTitle.text = getString(R.string.lbl_vision_goals)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val unEncodedHtml: String = whoWeAreResponseBean.setting.vision_en!!.toString()
            val encodedHtml = Base64.encodeToString(
                unEncodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            val unEncodedHtmlGoals: String = whoWeAreResponseBean.setting.mission_en!!.toString()
            val encodedHtmlGoals = Base64.encodeToString(
                unEncodedHtmlGoals.toByteArray(),
                Base64.NO_PADDING
            )
            binding.webViewCMSVision.loadData(encodedHtml, "text/html", "base64")
            binding.webViewCMSGoals.loadData(encodedHtmlGoals, "text/html", "base64")
        } else {
            val unEncodedHtml: String = "<html dir=\"rtl\" lang=\"\"><body>${whoWeAreResponseBean.setting.vision_ar!!.toString()}</body></html>"
            val encodedHtml = Base64.encodeToString(
                unEncodedHtml.toByteArray(),
                Base64.NO_PADDING
            )
            val unEncodedHtmlGoals: String = "<html dir=\"rtl\" lang=\"\"><body>${whoWeAreResponseBean.setting.mission_ar!!.toString()}</body></html>"
            val encodedHtmlGoals = Base64.encodeToString(
                unEncodedHtmlGoals.toByteArray(),
                Base64.NO_PADDING
            )
            binding.webViewCMSVision.loadData(encodedHtml, "text/html", "base64")
            binding.webViewCMSGoals.loadData(encodedHtmlGoals, "text/html", "base64")
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtContent.text =
//                    Html.fromHtml(
//                        "${whoWeAreResponseBean.setting.vision_en}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//                binding.txtContentGoals.text =
//                    Html.fromHtml(
//                        "${whoWeAreResponseBean.setting.mission_en}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//            } else {
//                binding.txtContent.text =
//                    Html.fromHtml(
//                        "${whoWeAreResponseBean.setting.vision_ar}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//                binding.txtContentGoals.text =
//                    Html.fromHtml(
//                        "${whoWeAreResponseBean.setting.mission_ar}",
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//            }
//        } else {
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
//                binding.txtContent.text =
//                    Html.fromHtml("${whoWeAreResponseBean.setting.vision_en}</p>")
//                binding.txtContentGoals.text =
//                    Html.fromHtml("${whoWeAreResponseBean.setting.vision_en}</p>")
//            } else {
//                binding.txtContent.text =
//                    Html.fromHtml("${whoWeAreResponseBean.setting.mission_en}</p>")
//                binding.txtContentGoals.text =
//                    Html.fromHtml("${whoWeAreResponseBean.setting.mission_ar}")
//            }
//        }
    }
}