package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Certificate
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class CertificatesListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Certificate>,
    val prefManager: PrefManager
) : RecyclerView.Adapter<CertificatesListAdapter.MyViewHolder>() {

    val listData = list as MutableList<Certificate>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtMonthYear: TextView = itemView.findViewById(R.id.txtMonthYear)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtFromBy: TextView = itemView.findViewById(R.id.txtFromBy)
        val txtDescription: TextView = itemView.findViewById(R.id.txtDescription)
        val imgCertificateDelete: ImageView = itemView.findViewById(R.id.imgCertificateDelete)
        val imgCertificateEdit: ImageView = itemView.findViewById(R.id.imgCertificateEdit)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_certificates_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgCertificateEdit.tag = position
        holder.imgCertificateEdit.setOnClickListener(onClickListener)

        holder.imgCertificateDelete.tag = position
        holder.imgCertificateDelete.setOnClickListener(onClickListener)

        holder.txtMonthYear.text = "${listData[position].month}-${listData[position].year}"
        holder.txtTitle.text = listData[position].title
        holder.txtFromBy.text = listData[position].from_by
        holder.txtDescription.text = listData[position].description.toString()

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en"){
            holder.txtDescription.textDirection = View.TEXT_DIRECTION_LTR
        }else{
            holder.txtDescription.textDirection = View.TEXT_DIRECTION_RTL
        }
    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return listData.size
    }
}