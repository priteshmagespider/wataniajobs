package com.fnrco.wataniajobs.mvvm.model.response

data class SignUpResponseBean(
    var user: UserSignUp? = null,
    var errors: List<String>? = null
)

data class UserSignUp(
    var name: String? = null,
    var email: String? = null,
    var is_admin: Int? = null,
    var active: Int? = null,
    var updated_at: String? = null,
    var created_at: String? = null,
    var id: Int? = null,
    var token: String? = null
)


data class test(
    var errors: List<String>? = null
)