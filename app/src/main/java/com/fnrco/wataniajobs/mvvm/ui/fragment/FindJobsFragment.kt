package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.FindJobsListAdapter
import com.fnrco.wataniajobs.databinding.FragmentFindJobsBinding
import com.fnrco.wataniajobs.mvvm.model.request.SearchRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginPopUpActivity
import com.fnrco.wataniajobs.mvvm.viewmodel.FindJobsStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.FindJobsViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class FindJobsFragment : Fragment(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentFindJobsBinding
    private lateinit var navController: NavController
    private lateinit var findJobsListAdapter: FindJobsListAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var job_title: String = ""
    private var jobTitle: String = ""
    private var city_id: Int = 0
    private var city_name: String = ""

    private var jobCat: Int = 0
    private var subCat: Int = 0
    private var from: Int = 0
    private var to: Int = 0
//    private var jobType: Int = 0
    private var jobType : ArrayList<Int> = ArrayList()
    private var gender: Int = 0
    private var certificate: Int = 0
    private var created_at: Int = 0

    private var singlePost: PostSinglePost? = null
    var searchRequest: SearchRequest = SearchRequest()

    private var posFindAJobLike: Int = -1
    private var page: Int = 1

    @ExperimentalCoroutinesApi
    private val viewModel: FindJobsViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager

    var pastVisiblesItems: Int = -1

    var visibleItemCount: Int = -1
    var totalItemCount: Int = -1
    lateinit var layoutManager: LinearLayoutManager
    private var loading= true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFindJobsBinding.inflate(inflater, container, false)
        if (arguments?.containsKey(Constant.HOME_JOB_TITLE) == true) {
            job_title = requireArguments().getString(Constant.HOME_JOB_TITLE) as String
            Log.d("TAG", "JOB_TITLE: HOME_JOB_TITLE $job_title")
            city_id = requireArguments().getInt(Constant.CITY_ID)
            city_name = requireArguments().getString(Constant.CITY_NAME) as String
            searchRequest.jobTitle = job_title
            searchRequest.city = city_id
            viewModel.setStateEvent(
                FindJobsStateEvent.SearchJobList(
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    job_title,
                    city_id,
                    jobCat,
                    subCat,
                    from,
                    to,
                    jobType,
                    gender,
                    certificate,
                    created_at,
                    page

                )
            )
            binding.imgBack.visibility = View.VISIBLE
            Utils.print("Data---------------------->::FindJobsFragment-----job_title---->$job_title")
            Utils.print("Data---------------------->::FindJobsFragment-----city_id---->$city_id")
            Utils.print("Data---------------------->::FindJobsFragment-----city_name---->$city_name")

            if (job_title == "") {
                binding.txtCandidateTitle.text = getString(R.string.find_your_dream_job)
            } else {
                binding.txtCandidateTitle.text = "$job_title, $city_name"
                binding.edtSearch.setText("$job_title, $city_name")
            }
        } else if (arguments?.containsKey(Constant.SPECIALITY_DATA_TITLE) == true) {
            jobTitle = requireArguments().getString(Constant.SPECIALITY_DATA_TITLE) as String
            Log.d("TAG", "JOB_TITLE: SPECIALITY_DATA_TITLE $job_title")
            city_id = requireArguments().getInt(Constant.SPECIALITY_CITY_ID, 0)
            jobCat = requireArguments().getInt("JOB_CAT_ID", 0)

            searchRequest.jobTitle = job_title
            searchRequest.city = city_id
            viewModel.setStateEvent(
                FindJobsStateEvent.SearchJobList(
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    "",
                    city_id,
                    jobCat,
                    subCat,
                    from,
                    to,
                    jobType,
                    gender,
                    certificate,
                    created_at,
                    page
                )
            )
            binding.imgBack.visibility = View.VISIBLE
            binding.txtCandidateTitle.text = jobTitle
        } else if(arguments?.containsKey(Constant.HOME_FILTER)==true){
            searchRequest = requireArguments().getSerializable(Constant.HOME_FILTER)!! as SearchRequest
            Log.d("TAG", "onCreateView: searchRequest--> $searchRequest")
            job_title = searchRequest.jobTitle!!
            city_id = searchRequest.city!!
            jobCat = searchRequest.jobCat!!
            subCat = searchRequest.subCat!!
            from = searchRequest.from!!
            to = searchRequest.to!!
            jobType.addAll(searchRequest.jobType)
            gender = searchRequest.gender!!
            certificate = searchRequest.certificate!!
            created_at = searchRequest.created_at!!
            viewModel.setStateEvent(
                FindJobsStateEvent.SearchJobList(
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    job_title,
                    city_id,
                    jobCat,
                    subCat,
                    from,
                    to,
                    jobType,
                    gender,
                    certificate,
                    created_at,
                    page
                )
            )
        }
        else {
            viewModel.setStateEvent(
                FindJobsStateEvent.SearchJobList(
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    job_title,
                    city_id,
                    jobCat,
                    subCat,
                    from,
                    to,
                    jobType,
                    gender,
                    certificate,
                    created_at,
                    page
                )
            )
            binding.txtCandidateTitle.text = getString(R.string.find_your_dream_job)
        }
        setObserver()
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("job_title")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter job_title-> $data")
                job_title = data
                searchRequest.jobTitle = job_title
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("city")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter city_id-> $data")
                city_id = data
                searchRequest.city = city_id
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("jobCat")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter jobCat-> $data")
                jobCat = data
                searchRequest.jobCat = jobCat
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("subCat")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter subCat-> $data")
                subCat = data
                searchRequest.subCat = subCat
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("from")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter from-> $data")
                from = data
                searchRequest.from = subCat
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("to")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter to-> $data")
                to = data
                searchRequest.to = to
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<ArrayList<Int>>("jobType")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter jobType-> $data")
                jobType.addAll(data)
                searchRequest.jobType.addAll(jobType)
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("gender")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter gender-> $data")
                gender = data
                searchRequest.gender = gender
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("certificate")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter certificate-> $data")
                certificate = data
                searchRequest.certificate = certificate
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("created_at")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter created_at-> $data")
                created_at = data
                searchRequest.created_at = created_at
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("API_CALL")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter API_CALL-> $data")
                viewModel.setStateEvent(
                    FindJobsStateEvent.SearchJobList(
                        prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                        job_title,
                        city_id,
                        jobCat,
                        subCat,
                        from,
                        to,
                        jobType,
                        gender,
                        certificate,
                        created_at,
                        page
                    )
                )
            }
        initUi()

    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        binding.imgFilter.setOnClickListener(this)

        binding.edtSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    performSearch()
                    return true
                }
                return false
            }

        })
        layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount =  layoutManager.itemCount
                    pastVisiblesItems =  layoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            viewModel.setStateEvent(
                                FindJobsStateEvent.SearchJobList(
                                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                                    job_title,
                                    city_id,
                                    jobCat,
                                    subCat,
                                    from,
                                    to,
                                    jobType,
                                    gender,
                                    certificate,
                                    created_at,
                                    page
                                )
                            )
                        }
                    }
                }
            }
        })
        page = 1
    }

    private fun performSearch() {
        Utils.closeKeyBoard(requireContext(), binding.mainView)
        page = 1
        viewModel.setStateEvent(
            FindJobsStateEvent.SearchJobList(
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                binding.edtSearch.text.toString(),
                city_id,
                jobCat,
                subCat,
                from,
                to,
                jobType,
                gender,
                certificate,
                created_at,
                page
            )
        )
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.clMain -> {
                if (!prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    loginDialog(requireContext())
                } else {
                    val pos = v.tag.toString().toInt()
                    val bundle = Bundle()
                    bundle.putInt(Constant.POST_ID, findJobsListAdapter.list[pos].id!!)
                    navController.navigate(
                        R.id.action_findJobsFragment_to_homeDetailFragment,
                        bundle
                    )
                }

            }
            R.id.imgBack -> {
                findNavController().navigateUp()
            }
            R.id.imgShare -> {
                val pos = v.tag.toString().toInt()
                viewModel.setStateEvent(
                    FindJobsStateEvent.HomeDetailSinglePost(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!, findJobsListAdapter.list[pos].id!!
                    )
                )
            }
            R.id.imgStarFindAJob -> {
                posFindAJobLike = v.tag.toString().toInt()
                if (findJobsListAdapter.list[posFindAJobLike].is_favourite == 1) {
                    viewModel.setStateEvent(FindJobsStateEvent.HomeRemoveFavouriteJobList(findJobsListAdapter.list[posFindAJobLike].id!!))
                    displayProgressBar(true)
                } else {
                    viewModel.setStateEvent(FindJobsStateEvent.HomeFavouriteJobList(findJobsListAdapter.list[posFindAJobLike].id!!))
                    displayProgressBar(true)
                }



                /*var pos = v.tag.toString().toInt()
                viewModel.setStateEvent(
                    FindJobsStateEvent.HomeDetailSinglePost(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!, findJobsListAdapter.list[pos].id!!
                    )
                )*/
            }
            R.id.imgFilter -> {
                searchRequest.jobType.clear()
                jobType.clear()
                searchRequest.jobTitle = jobTitle
                val bundle = bundleOf(
                    Constant.JOB_TITLE_FILTER to searchRequest
                )
                navController.navigate(
                    R.id.action_findJobsFragment_to_filterJobListFragment,
                    bundle
                )
            }
        }
    }


    private fun setObserver() {
        /** Home Banner Response */
        viewModel.favouriteJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<FavoriteJobResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.message == "Add Job to Favourite Successfully") {
                        findJobsListAdapter.list[posFindAJobLike].is_favourite = 1
                        findJobsListAdapter.notifyItemChanged(posFindAJobLike)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Home Banner Response */
        viewModel.removeFavouriteJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<RemoveFavoriteJobResponseBean> -> {
                    displayProgressBar(false)

                    if (dataState.data.message == "Remove Job From Favourite Successfully") {

                        findJobsListAdapter.list[posFindAJobLike].is_favourite = 0
                        findJobsListAdapter.notifyItemChanged(posFindAJobLike)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }



        viewModel.searchListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SearchListResponseBean> -> {
                    displayProgressBar(false)
                    val listWithAd = mutableListOf<PostData>()
                    listWithAd.clear()
                    if (dataState.data.posts!!.data!!.isNotEmpty()) {
                        if (page == 1) {
                            dataState.data.posts.data!!.forEachIndexed { index, post ->
                                if (index % 4 == 0 && index > 0) {
                                    listWithAd.add(PostData(type = FindJobsListAdapter.VIEW_TYPE_ADV))
                                }
                                post.type = FindJobsListAdapter.VIEW_TYPE_ITEM
                                listWithAd.add(post)
                            }
                            if (dataState.data.posts.data.isNotEmpty()) {
                                if (dataState.data.posts.data.size % 4 == 0) {
                                    listWithAd.add(PostData(type = FindJobsListAdapter.VIEW_TYPE_ADV))
                                }
                            }
                            val list: List<PostData> = listWithAd
                            findJobsListAdapter = FindJobsListAdapter(
                                requireActivity(),
                                this,
                                prefManager
                            )
                            findJobsListAdapter.addData(list, true)
                            binding.recyclerView.adapter = findJobsListAdapter
                            binding.idNoDataAvailable.visibility = View.GONE
                        } else {
                            dataState.data.posts.data!!.forEachIndexed { index, post ->
                                if (index % 4 == 0 && index > 0) {
                                    listWithAd.add(PostData(type = FindJobsListAdapter.VIEW_TYPE_ADV))
                                }
                                post.type = FindJobsListAdapter.VIEW_TYPE_ITEM
                                listWithAd.add(post)
                            }
                            if (dataState.data.posts.data.isNotEmpty()) {
                                if (dataState.data.posts.data.size % 4 == 0) {
                                    listWithAd.add(PostData(type = FindJobsListAdapter.VIEW_TYPE_ADV))
                                }
                            }
                            val list: List<PostData> = listWithAd
                            findJobsListAdapter.addData(list, false)
                            findJobsListAdapter.notifyDataSetChanged()
                            binding.idNoDataAvailable.visibility = View.GONE
                        }
                        page++
                        loading = true

                        findJobsListAdapter.setCallBack(object : FindJobsListAdapter.CallBack {
                            override fun advClick() {
                                val bundle = Bundle()
                                bundle.putString(
                                    Constant.TITLE,
                                    resources.getString(R.string.browse_jobs_in_saudi_arabia)
                                )
                                navController.navigate(
                                    R.id.action_findJob_to_findJobsDetailsFragment,
                                    bundle
                                )
                            }
                        })
                    } else {
                        if (page == 1) {
                            binding.idNoDataAvailable.visibility = View.VISIBLE
                        }

                        loading = false
                    }


//                    if (dataState.data.posts!!.isEmpty()) {
//                        Utils.print("Data---------------------->::FindJobsFragment-----searchListResponseBean---->" + dataState.data.posts!!.size)
//                        binding.idNoDataAvailable.visibility = View.VISIBLE
//                        binding.recyclerView.visibility = View.GONE
//                    } else {
//                        binding.idNoDataAvailable.visibility = View.GONE
//                        binding.recyclerView.visibility = View.VISIBLE
//                        linearLayoutManager = LinearLayoutManager(requireActivity())
//                        binding.recyclerView.layoutManager = linearLayoutManager
////                        binding.recyclerView.addItemDecoration(
////                            DividerItemDecoration(
////                                requireContext(), LinearLayoutManager.VERTICAL
////                            ).apply {
////                                setDrawable(resources.getDrawable(R.drawable.divider_bg))
////                            }
////                        )
//
//                        dataState.data.posts!!.forEachIndexed { index, post ->
//                            if (index % 4 == 0 && index > 0) {
//                                listWithAd.add(Post(type = FindJobsListAdapter.VIEW_TYPE_ADV))
//                            }
//                            post.type = FindJobsListAdapter.VIEW_TYPE_ITEM
//                            listWithAd.add(post)
//                        }
//                        if (dataState.data.posts!!.size > 0) {
//                            if (dataState.data.posts!!.size % 4 == 0) {
//                                listWithAd.add(Post(type = FindJobsListAdapter.VIEW_TYPE_ADV))
//                            }
//                        }
//                        val list: List<Post> = listWithAd
//                        findJobsListAdapter = FindJobsListAdapter(
//                            requireActivity(),
//                            this,
//                            list,
//                            prefManager
//                        )
//                        binding.recyclerView.adapter = findJobsListAdapter
//                        findJobsListAdapter.setCallBack(object : FindJobsListAdapter.CallBack {
//                            override fun advClick() {
//                                val bundle = Bundle()
//                                bundle.putString(
//                                    Constant.TITLE,
//                                    resources.getString(R.string.browse_jobs_in_saudi_arabia)
//                                )
//                                navController.navigate(
//                                    R.id.action_findJob_to_findJobsDetailsFragment,
//                                    bundle
//                                )
//                            }
//                        })
//                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }




        viewModel.singlePostResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SinglePostResponseBean> -> {
                    displayProgressBar(false)
                    singlePost = dataState.data.post
                    Utils.print("HEHHEHEHEHRHEHRHEHR-------------->" + singlePost)


                    val icon = BitmapFactory.decodeResource(
                        resources,
                        R.drawable.img_back_white
                    )
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "image/jpeg"
                    val bytes = ByteArrayOutputStream()
                    icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    val f = File(
                        (Environment.getExternalStorageDirectory()).toString() + "temporary_file.jpg"
                    )
                    try {
                        f.createNewFile()
                        val fo = FileOutputStream(f)
                        fo.write(bytes.toByteArray())
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    share.putExtra(
                        Intent.EXTRA_STREAM,
                        Uri.parse("${Constant.IMAGE_URL}${singlePost!!.com_logo}")
                    )
                    if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                        share.putExtra(
                            Intent.EXTRA_TEXT,
//                            singlePost!!.title_en
                            "" + "\n"
//                                    + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_en + "\n"
//                                    + resources.getString(R.string.address) + " : " + singlePost?.company?.address  + "\n"
//                                    + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_en + "\n"
//                                    + resources.getString(R.string.announcement_date) + " : " +  singlePost?.created_at + "\n"
                                    + "Job title" + " : " + singlePost?.job__type?.title_en + "\n"
                                    + "Description" + " : " + Utils.html2text(singlePost?.description_en) + "\n"
                                    + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                    + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                    + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                    + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title_en + "\n"
//                                    + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                        )
                        share.type = "text/plain"
//                    startActivity(share)
                    } else {
                        share.putExtra(
                            Intent.EXTRA_TEXT,
//                            singlePost!!.title_en
                            "" + "\n"
//                                    + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_ar + "\n"
//                                    + resources.getString(R.string.address) + " : " + singlePost?.company?.address  + "\n"
//                                    + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_ar + "\n"
//                                    + resources.getString(R.string.announcement_date) + " : " +  singlePost?.created_at + "\n"
//
                                    + "Job title" + " : " + singlePost?.job__type?.title_ar + "\n"
                                    + "Description" + " : " + Utils.html2text(singlePost?.description) + "\n"
                                    + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                    + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                    + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                    + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title + "\n"
//                                    + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                        )
                        share.type = "text/plain"
//                     startActivity(share)
                    }
                    startActivity(Intent.createChooser(share, "Share Image"))
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


    private fun loginDialog(context: Context) {
        var intent = Intent(requireContext(), LoginPopUpActivity::class.java)
        startActivity(intent)
        /*MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.title_login_dialog)

            .setPositiveButton(R.string.login_in) { dialog, _ ->
                context.startActivity(Intent(context, LoginActivity::class.java))
                dialog.dismiss()
            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()*/
    }

}