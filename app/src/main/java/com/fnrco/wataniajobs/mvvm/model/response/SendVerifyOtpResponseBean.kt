package com.fnrco.wataniajobs.mvvm.model.response

data class SendVerifyOtpResponseBean(
    val message: String?
)