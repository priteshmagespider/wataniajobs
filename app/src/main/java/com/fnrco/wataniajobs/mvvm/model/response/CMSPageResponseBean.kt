package com.fnrco.wataniajobs.mvvm.model.response

data class CMSPageResponseBean(
    val setting: CMSPageSetting?
)

data class CMSPageSetting(
    val term_of_service_ar: String?,
    val term_of_service_en: String?,
    val privacy_policy_ar: String?,
    val privacy_policy_en: String?
)