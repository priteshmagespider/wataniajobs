package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.ServicesAdapter
import com.fnrco.wataniajobs.adapter.WhoWeAreAdapter
import com.fnrco.wataniajobs.databinding.FragmentServicesDetailsBinding
import com.fnrco.wataniajobs.databinding.FragmentWhoWeAreDetailsBinding
import com.fnrco.wataniajobs.mvvm.adapter.ClientListAdapter
import com.fnrco.wataniajobs.mvvm.adapter.ServicesListAdapter
import com.fnrco.wataniajobs.mvvm.model.request.WhoWeAreList
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.CallUsViewModel
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class ServicesDetailsFragment : Fragment(), View.OnClickListener {

    private val mTAG = ServicesDetailsFragment::class.java.simpleName
    private lateinit var binding: FragmentServicesDetailsBinding
    private lateinit var whoWeAreAdapter: WhoWeAreAdapter
    private lateinit var servicesAdapter: ServicesAdapter

    @ExperimentalCoroutinesApi
    private val viewModel by viewModels<MoreScreenViewModel>()

    @Inject
    lateinit var prefManager: PrefManager


    private var whoWeAreList: ArrayList<WhoWeAreList> = ArrayList()
    private lateinit var clientListAdapter: ClientListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentServicesDetailsBinding.inflate(layoutInflater)
        if (Utils.isConnected(requireContext())) {
            viewModel.setMoreScreenApiCall(
                MoreScreenEvent.ServiceListEvent(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!
                )
            )
        } else {
            Utils.showSnackBar(binding.mainView, requireContext())
        }
        setObserver()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)


    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                findNavController().popBackStack()
            }
            R.id.txtServiceLbl -> {
                val pos = view.tag.toString().toInt()
                servicesAdapter.isSelected(pos)
                setServiceData(servicesAdapter.list[pos])
            }
        }
    }

    private fun setObserver() {
        /** Services Goals */
        viewModel.serviceListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<MoreScreenServiceResponseBean> -> {
                    displayProgressBar(false)
                    setData(dataState.data)

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun setData(data: MoreScreenServiceResponseBean) {
        binding.nsvServices.visibility = View.VISIBLE
        binding.rvServices.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        servicesAdapter =
            ServicesAdapter(requireContext(), this, data.services!!, prefManager)
        binding.rvServices.adapter = servicesAdapter
        setServiceData(servicesAdapter.list[0])
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun setServiceData(list: MoreScreenService) {
        Log.d(mTAG, "setServiceData: ${Constant.IMAGE_URL_WITHOUT_PHOTOS}${list.image}")
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${list.image}")
            .into(binding.imgWhoWeAre)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            binding.txtTitle.text = list.name_en
        } else {
            binding.txtTitle.text = list.name_ar
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                val unEncodedHtml: String = list.content_en!!.toString()
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${list.content_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            } else {
                val unEncodedHtml: String = "<html dir=\"rtl\" lang=\"\"><body>${list.content_ar!!.toString()}</body></html>"
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${list.content_ar}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            }
        } else {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                val unEncodedHtml: String = list.content_en!!.toString()
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml("<p>${list.content_en}</p>")
            } else {
                val unEncodedHtml: String = "<html dir=\"rtl\" lang=\"\"><body>${list.content_ar!!.toString()}</body></html>"
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml("<p>${list.content_ar}</p>")
            }
        }
    }

}