@file:Suppress("DEPRECATION")

package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.request.SearchRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.repository.FindJobsRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class FindJobsViewModel
@ViewModelInject
constructor(
    private val findJobsRepository: FindJobsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
) : ViewModel() {


    private val _cityListResponse: MutableLiveData<DataState<CityListResponse>> =
        MutableLiveData()

    val cityListResponse: LiveData<DataState<CityListResponse>>
        get() = _cityListResponse


    private val _majorListResponse: MutableLiveData<DataState<SpecializationResponseBean>> =
        MutableLiveData()

    val majorListResponse: LiveData<DataState<SpecializationResponseBean>>
        get() = _majorListResponse


    private val _favouriteListResponse: MutableLiveData<DataState<FavouriteJobResponseBean>> =
        MutableLiveData()

    val favouriteListResponse: LiveData<DataState<FavouriteJobResponseBean>>
        get() = _favouriteListResponse


    private val _jobTypeListResponse: MutableLiveData<DataState<JobTypeResponseBean>> =
        MutableLiveData()

    val jobTypeListResponse: LiveData<DataState<JobTypeResponseBean>>
        get() = _jobTypeListResponse


    private val _jobRoleResponse: MutableLiveData<DataState<JobRoleResponseBean>> =
        MutableLiveData()

    val jobRoleResponse: LiveData<DataState<JobRoleResponseBean>>
        get() = _jobRoleResponse

    private val _favouriteJobResponse: MutableLiveData<DataState<FavoriteJobResponseBean>> =
        MutableLiveData()

    val favouriteJobResponse: LiveData<DataState<FavoriteJobResponseBean>>
        get() = _favouriteJobResponse

    private val _removeFavouriteJobResponse: MutableLiveData<DataState<RemoveFavoriteJobResponseBean>> =
        MutableLiveData()

    val removeFavouriteJobResponse: LiveData<DataState<RemoveFavoriteJobResponseBean>>
        get() = _removeFavouriteJobResponse


    private val _qualificationResponse: MutableLiveData<DataState<QualificationResponseBean>> =
        MutableLiveData()

    val qualificationResponse: LiveData<DataState<QualificationResponseBean>>
        get() = _qualificationResponse


    private val _searchListResponse: MutableLiveData<DataState<SearchListResponseBean>> =
        MutableLiveData()

    val searchListResponse: LiveData<DataState<SearchListResponseBean>>
        get() = _searchListResponse


    private val _singlePostResponse: MutableLiveData<DataState<SinglePostResponseBean>> =
        MutableLiveData()

    val singlePostResponse: LiveData<DataState<SinglePostResponseBean>>
        get() = _singlePostResponse


    fun setStateEvent(findJobsStateEvent: FindJobsStateEvent) {
        viewModelScope.launch {
            when (findJobsStateEvent) {
                is FindJobsStateEvent.FilterJobsCity -> {
                    findJobsRepository.cityList(findJobsStateEvent.language)
                        .onEach {
                            _cityListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.HomeFavouriteJobList -> {
                    findJobsRepository.addFavoriteJob(findJobsStateEvent.job_id)
                        .onEach {
                            _favouriteJobResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.HomeRemoveFavouriteJobList -> {
                    findJobsRepository.removeFavoriteJob(findJobsStateEvent.job_id)
                        .onEach {
                            _removeFavouriteJobResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.HomeDetailSinglePost -> {
                    findJobsRepository.singlePost(findJobsStateEvent.language, findJobsStateEvent.post_id)
                        .onEach {
                            _singlePostResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.FilterCategoriesList -> {
                    findJobsRepository.categoriesList(findJobsStateEvent.language)
                        .onEach {
                            _majorListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.FavouriteJobsList -> {
                    findJobsRepository.favouriteJobsList(findJobsStateEvent.language)
                        .onEach {
                            _favouriteListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.JobTypeList -> {
                    findJobsRepository.jobTypeList(findJobsStateEvent.language)
                        .onEach {
                            _jobTypeListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.JobRoleList -> {
                    findJobsRepository.roleList(findJobsStateEvent.language)
                        .onEach {
                            _jobRoleResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is FindJobsStateEvent.QualificationList -> {
                    findJobsRepository.qualificationList(findJobsStateEvent.language)
                        .onEach {
                            _qualificationResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is FindJobsStateEvent.SearchJobList -> {
                    findJobsRepository.searchList(
                        findJobsStateEvent.language,
                        findJobsStateEvent.jobTitle,
                        findJobsStateEvent.city,
                        findJobsStateEvent.jobCat,
                        findJobsStateEvent.subCat,
                        findJobsStateEvent.from,
                        findJobsStateEvent.to,
                        findJobsStateEvent.jobType,
                        findJobsStateEvent.gender,
                        findJobsStateEvent.certificate,
                        findJobsStateEvent.created_at,
                        findJobsStateEvent.page

                    )
                        .onEach {
                            _searchListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                else -> {

                }
            }
        }
    }
}

sealed class FindJobsStateEvent {
    data class FilterJobsCity(val language: String) : FindJobsStateEvent()
    data class FilterCategoriesList(val language: String) : FindJobsStateEvent()
    data class FavouriteJobsList(val language: String) : FindJobsStateEvent()
    data class JobTypeList(val language: String) : FindJobsStateEvent()
    data class JobRoleList(val language: String) : FindJobsStateEvent()
    data class QualificationList(val language: String) : FindJobsStateEvent()
    data class HomeFavouriteJobList(val job_id: Int) : FindJobsStateEvent()
    data class HomeRemoveFavouriteJobList(val job_id: Int) : FindJobsStateEvent()
    data class HomeDetailSinglePost(val language: String, val post_id: Int) : FindJobsStateEvent()
    data class SearchJobList(
        val language: String,
        val jobTitle: String,
        val city: Int,
        val jobCat: Int,
        val subCat: Int,
        val from: Int,
        val to: Int,
        val jobType: List<Int>,
        val gender: Int,
        val certificate: Int,
        val created_at: Int,
        val page : Int
    ) : FindJobsStateEvent()

}