@file:Suppress("DEPRECATION")

package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.repository.EditProfileRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class EditProfileViewModel
@ViewModelInject
constructor(
    private val editProfileRepository: EditProfileRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val _saveDeletePhoneEmail: MutableLiveData<DataState<SaveDeletePhoneEmailResponse>> =
        MutableLiveData()

    val saveDeletePhoneEmail: LiveData<DataState<SaveDeletePhoneEmailResponse>>
        get() = _saveDeletePhoneEmail

    private val _deletePhoneEmail: MutableLiveData<DataState<SaveDeletePhoneEmailResponse>> =
        MutableLiveData()

    val deletePhoneEmail: LiveData<DataState<SaveDeletePhoneEmailResponse>>
        get() = _deletePhoneEmail


    private val _showPhoneList: MutableLiveData<DataState<PhoneListResponseBean>> =
        MutableLiveData()

    val showPhoneList: LiveData<DataState<PhoneListResponseBean>>
        get() = _showPhoneList

    private val _showEmailList: MutableLiveData<DataState<PhoneListResponseBean>> =
        MutableLiveData()

    val showEmailList: LiveData<DataState<PhoneListResponseBean>>
        get() = _showEmailList

    private val _specializationListResponse: MutableLiveData<DataState<BasicDataSpecializationResponse>> =
        MutableLiveData()

    val specializationListResponse: LiveData<DataState<BasicDataSpecializationResponse>>
        get() = _specializationListResponse

    private val _categoryListResponse: MutableLiveData<DataState<CategoriesListResponseBean>> =
        MutableLiveData()

    val categoryListResponse: LiveData<DataState<CategoriesListResponseBean>>
        get() = _categoryListResponse

    private val _cityListResponse: MutableLiveData<DataState<CityListResponse>> =
        MutableLiveData()

    val cityListResponse: LiveData<DataState<CityListResponse>>
        get() = _cityListResponse


    private val _profileDataResponse: MutableLiveData<DataState<ProfileResponseBean>> =
        MutableLiveData()

    val profileDataResponse: LiveData<DataState<ProfileResponseBean>>
        get() = _profileDataResponse

    /** Personal Edit Info */
    private val _personalInfoResponse: MutableLiveData<DataState<EditPersonalInfoResponseBean>> =
        MutableLiveData()

    val personalInfoResponse: LiveData<DataState<EditPersonalInfoResponseBean>>
        get() = _personalInfoResponse

    /** Communication Data Edit Info */
    private val _communicationDataResponse: MutableLiveData<DataState<EditCommunicationDataResponseBean>> =
        MutableLiveData()

    val communicationDataResponse: LiveData<DataState<EditCommunicationDataResponseBean>>
        get() = _communicationDataResponse


    /** Communication Data Edit Info */
    private val _jobPostAppliedDataResponse: MutableLiveData<DataState<AppliedJobResponseBean>> =
        MutableLiveData()

    val jobPostAppliedDataResponse: LiveData<DataState<AppliedJobResponseBean>>
        get() = _jobPostAppliedDataResponse


    /** Communication Data Edit Info */
    private val _expiredJobAppliedDataResponse: MutableLiveData<DataState<ExpiredJobResponseBean>> =
        MutableLiveData()

    val expiredJobAppliedDataResponse: LiveData<DataState<ExpiredJobResponseBean>>
        get() = _expiredJobAppliedDataResponse


    /** Training Courses Data Edit Info */
    private val _trainingCoursesDataResponse: MutableLiveData<DataState<TrainingCoursesMoreResponseBean>> =
        MutableLiveData()

    val trainingCoursesDataResponse: LiveData<DataState<TrainingCoursesMoreResponseBean>>
        get() = _trainingCoursesDataResponse

    /** Degree/ Qualification list */
    private val _qualificationResponse: MutableLiveData<DataState<QualificationResponseBean>> =
        MutableLiveData()

    val qualificationResponse: LiveData<DataState<QualificationResponseBean>>
        get() = _qualificationResponse

    /** Add Education Api Call */
    private val _addEducationResponse: MutableLiveData<DataState<EditEducationResponseBean>> =
        MutableLiveData()

    val addEducationResponse: LiveData<DataState<EditEducationResponseBean>>
        get() = _addEducationResponse

    /** Edit Education Api Call */
    private val _editEducationResponse: MutableLiveData<DataState<EditEducationResponseBean>> =
        MutableLiveData()

    val editEducationResponse: LiveData<DataState<EditEducationResponseBean>>
        get() = _editEducationResponse

    /** Delete Education Api Call */
    private val _deleteEducationResponse: MutableLiveData<DataState<DeleteEducationResponseBean>> =
        MutableLiveData()

    val deleteEducationResponse: LiveData<DataState<DeleteEducationResponseBean>>
        get() = _deleteEducationResponse

    /** Add Experience Api Call */
    private val _addEditDeleteExperienceResponse: MutableLiveData<DataState<AddEditDeleteExperienceResponseBean>> =
        MutableLiveData()

    val addEditDeleteExperienceResponse: LiveData<DataState<AddEditDeleteExperienceResponseBean>>
        get() = _addEditDeleteExperienceResponse

    /** Delete Experience Api Call */
    private val _deleteEditDeleteExperienceResponse: MutableLiveData<DataState<AddEditDeleteExperienceResponseBean>> =
        MutableLiveData()

    val deleteEditDeleteExperienceResponse: LiveData<DataState<AddEditDeleteExperienceResponseBean>>
        get() = _deleteEditDeleteExperienceResponse

    /** Edit Experience Api Call */
    private val _editExperienceResponse: MutableLiveData<DataState<AddEditDeleteExperienceResponseBean>> =
        MutableLiveData()

    val editExperienceResponse: LiveData<DataState<AddEditDeleteExperienceResponseBean>>
        get() = _editExperienceResponse

    /** Add Edit Short Description Api Call */
    private val _addEditShortDescResponse: MutableLiveData<DataState<EditEducationResponseBean>> =
        MutableLiveData()

    val addEditShortDescResponse: LiveData<DataState<EditEducationResponseBean>>
        get() = _addEditShortDescResponse

    /** Add Skill Api Call */
    private val _addSkillResponse: MutableLiveData<DataState<AddDeleteSkillResponseBean>> =
        MutableLiveData()

    val addSkillResponse: LiveData<DataState<AddDeleteSkillResponseBean>>
        get() = _addSkillResponse

    /** Delete Skill Api Call */
    private val _deleteSkillResponse: MutableLiveData<DataState<AddDeleteSkillResponseBean>> =
        MutableLiveData()

    val deleteSkillResponse: LiveData<DataState<AddDeleteSkillResponseBean>>
        get() = _deleteSkillResponse

    /** Add interest Api Call */
    private val _addInterestResponse: MutableLiveData<DataState<AddDeleteInterestResponseBean>> =
        MutableLiveData()

    val addInterestResponse: LiveData<DataState<AddDeleteInterestResponseBean>>
        get() = _addInterestResponse

    /** Delete interest Api Call */
    private val _deleteInterestResponse: MutableLiveData<DataState<AddDeleteInterestResponseBean>> =
        MutableLiveData()

    val deleteInterestResponse: LiveData<DataState<AddDeleteInterestResponseBean>>
        get() = _deleteInterestResponse

    /** Add Courses Api Call */
    private val _addCoursesResponse: MutableLiveData<DataState<AddEditDeleteCoursesResponseBean>> =
        MutableLiveData()

    val addCoursesResponse: LiveData<DataState<AddEditDeleteCoursesResponseBean>>
        get() = _addCoursesResponse

    /** Edit Courses Api Call */
    private val _editCoursesResponse: MutableLiveData<DataState<AddEditDeleteCoursesResponseBean>> =
        MutableLiveData()

    val editCoursesResponse: LiveData<DataState<AddEditDeleteCoursesResponseBean>>
        get() = _editCoursesResponse


    /** Delete Job Post Api Call */
    private val _deleteJobPostResponse: MutableLiveData<DataState<DeleteJobPostResponseBean>> =
        MutableLiveData()

    val deleteJobPostResponse: LiveData<DataState<DeleteJobPostResponseBean>>
        get() = _deleteJobPostResponse

    /** Edit Courses Api Call */
    private val _deleteCourseResponse: MutableLiveData<DataState<AddEditDeleteCoursesResponseBean>> =
        MutableLiveData()

    val deleteCourseResponse: LiveData<DataState<AddEditDeleteCoursesResponseBean>>
        get() = _deleteCourseResponse

    /** Add Certificate Api Call */
    private val _addCertificateResponse: MutableLiveData<DataState<AddEditDeleteCertificateResponseBean>> =
        MutableLiveData()

    val addCertificateResponse: LiveData<DataState<AddEditDeleteCertificateResponseBean>>
        get() = _addCertificateResponse

    /** Edit Certificate Api Call */
    private val _editCertificateResponse: MutableLiveData<DataState<UpdateCertificateResponseBean>> =
        MutableLiveData()

    val editCertificateResponse: LiveData<DataState<UpdateCertificateResponseBean>>
        get() = _editCertificateResponse

    /** Delete Certificate Api Call */
    private val _deleteCertificateResponse: MutableLiveData<DataState<AddEditDeleteCertificateResponseBean>> =
        MutableLiveData()

    val deleteCertificateResponse: LiveData<DataState<AddEditDeleteCertificateResponseBean>>
        get() = _deleteCertificateResponse

    /** Add project  Api Call */
    private val _addProjectResponse: MutableLiveData<DataState<AddEditDeleteProjectResponseBean>> =
        MutableLiveData()

    val addProjectResponse: LiveData<DataState<AddEditDeleteProjectResponseBean>>
        get() = _addProjectResponse

    /** Edit project  Api Call */
    private val _editProjectResponse: MutableLiveData<DataState<AddEditDeleteProjectResponseBean>> =
        MutableLiveData()

    val editProjectResponse: LiveData<DataState<AddEditDeleteProjectResponseBean>>
        get() = _editProjectResponse

    /** Delete project  Api Call */
    private val _deleteProjectResponse: MutableLiveData<DataState<AddEditDeleteProjectResponseBean>> =
        MutableLiveData()

    val deleteProjectResponse: LiveData<DataState<AddEditDeleteProjectResponseBean>>
        get() = _deleteProjectResponse

    /** Computer Level List */
    private val _computerLevelList: MutableLiveData<DataState<ComputerLevelResponseBean>> =
        MutableLiveData()

    val computerLevelList: LiveData<DataState<ComputerLevelResponseBean>>
        get() = _computerLevelList

    /** Add Language  Api Call */
    private val _addLanguageResponse: MutableLiveData<DataState<LanguageAddDeleteResponseBean>> =
        MutableLiveData()

    val addLanguageResponse: LiveData<DataState<LanguageAddDeleteResponseBean>>
        get() = _addLanguageResponse

    /** Delete Language  Api Call */
    private val _deleteLanguageResponse: MutableLiveData<DataState<LanguageAddDeleteResponseBean>> =
        MutableLiveData()

    val deleteLanguageResponse: LiveData<DataState<LanguageAddDeleteResponseBean>>
        get() = _deleteLanguageResponse


    /** Delete Language  Api Call */
    private val _CvExcellenceResponse: MutableLiveData<DataState<CvExcellenceResponseBean>> =
        MutableLiveData()

    val cvExcellenceResponse: LiveData<DataState<CvExcellenceResponseBean>>
        get() = _CvExcellenceResponse


    /** Delete Language  Api Call */
    private val _uploadCVResponse: MutableLiveData<DataState<UploadCvResponseBean>> =
        MutableLiveData()

    val uploadCVResponse: LiveData<DataState<UploadCvResponseBean>>
        get() = _uploadCVResponse

    /** Social Update  Api Call */
    private val _socialUpdate: MutableLiveData<DataState<SocialResponseBean>> =
        MutableLiveData()

    val socialUpdate: LiveData<DataState<SocialResponseBean>>
        get() = _socialUpdate

    /** Basic data Update  Api Call */
    private val _basicDataUpdate: MutableLiveData<DataState<BasicDataUpdateResponseBean>> =
        MutableLiveData()

    val basicDataUpdate: LiveData<DataState<BasicDataUpdateResponseBean>>
        get() = _basicDataUpdate

    /** Update Company logo */
    private val _updateUserImage: MutableLiveData<DataState<BasicDataUpdateResponseBean>> =
        MutableLiveData()
    val updateUserImage: LiveData<DataState<BasicDataUpdateResponseBean>>
        get() = _updateUserImage

    /** Send Verify Otp  */
    private val _sendVerifyOtpResponse: MutableLiveData<DataState<SendVerifyOtpResponseBean>> =
        MutableLiveData()

    val sendVerifyOtpResponse: LiveData<DataState<SendVerifyOtpResponseBean>>
        get() = _sendVerifyOtpResponse

    /** Language List API */
    private val _languageList: MutableLiveData<DataState<LanguageListResponseBean>> =
        MutableLiveData()

    val languageList: LiveData<DataState<LanguageListResponseBean>>
        get() = _languageList


    fun setStateEvent(homeStateEvent: EditProfileStateEvent) {
        viewModelScope.launch {
            when (homeStateEvent) {
                is EditProfileStateEvent.HomeSpecializationList -> {
                    editProfileRepository.basicDataCategoriesList(homeStateEvent.language)
                        .onEach {
                            _specializationListResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.HomeCategoriesList -> {
                    editProfileRepository.categoriesList(homeStateEvent.language)
                        .onEach {
                            _categoryListResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.EditProfileCity -> {
                    editProfileRepository.cityList(homeStateEvent.language)
                        .onEach {
                            _cityListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.ProfileData -> {
                    editProfileRepository.profileDataList(homeStateEvent.language)
                        .onEach {
                            _profileDataResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.PersonalInfoEdit -> {
                    editProfileRepository.personalEditApiCall(
                        homeStateEvent.address,
                        homeStateEvent.city,
                        homeStateEvent.gender,
                        homeStateEvent.birthDate,
                        homeStateEvent.Dependents,
                        homeStateEvent.marital_status,
                        homeStateEvent.experience,
                        homeStateEvent.salary,
                    )
                        .onEach {
                            _personalInfoResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.CommunicationDataEdit -> {
                    editProfileRepository.communicationDataApiCall(
                        homeStateEvent.email,
                        homeStateEvent.phone
                    )
                        .onEach {
                            _communicationDataResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.JobPostAppliedData -> {
                    editProfileRepository.jobPostAppliedList(
                        homeStateEvent.language
                    )
                        .onEach {
                            _jobPostAppliedDataResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.ExpiredJobAppliedData -> {
                    editProfileRepository.expiredJobApplicationList(
                        homeStateEvent.language
                    )
                        .onEach {
                            _expiredJobAppliedDataResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.TrainingCoursesData -> {
                    editProfileRepository.trainingCoursesList(
                        homeStateEvent.language
                    )
                        .onEach {
                            _trainingCoursesDataResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.QualificationList -> {
                    editProfileRepository.qualificationList(
                        homeStateEvent.language
                    )
                        .onEach {
                            _qualificationResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.AddEducationEvent -> {
                    editProfileRepository.addEducationApiCall(
                        homeStateEvent.educational_institution,
                        homeStateEvent.general_appreciation,
                        homeStateEvent.specialization,
                        homeStateEvent.degree,
                        homeStateEvent.end_date_educational,
                        homeStateEvent.description_education,
                        homeStateEvent.id
                    )
                        .onEach {
                            _addEducationResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.EditEducationEvent -> {
                    editProfileRepository.editEducationApiCall(
                        homeStateEvent.educational_institution,
                        homeStateEvent.general_appreciation,
                        homeStateEvent.specialization,
                        homeStateEvent.degree,
                        homeStateEvent.end_date_educational,
                        homeStateEvent.description_education,
                        homeStateEvent.id,
                    )
                        .onEach {
                            _editEducationResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.DeleteEducationEvent -> {
                    editProfileRepository.deleteEducationApiCall(
                        homeStateEvent.edu_id
                    )
                        .onEach {
                            _deleteEducationResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.AddExperienceEvent -> {
                    editProfileRepository.addExperienceApiCall(
                        homeStateEvent.position_in_company,
                        homeStateEvent.company_name,
                        homeStateEvent.location,
                        homeStateEvent.start_jop_M,
                        homeStateEvent.start_jop_Y,
                        homeStateEvent.end_jop_M,
                        homeStateEvent.end_jop_Y,
                        homeStateEvent.to_present,
                        homeStateEvent.description_job
                    )
                        .onEach {
                            _addEditDeleteExperienceResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.DeleteExperienceEvent -> {
                    editProfileRepository.deleteExperienceApiCall(
                        homeStateEvent.exp_id
                    )
                        .onEach {
                            _deleteEditDeleteExperienceResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.EditExperienceEvent -> {
                    editProfileRepository.editExperienceApiCall(
                        homeStateEvent.position_in_company,
                        homeStateEvent.company_name,
                        homeStateEvent.location,
                        homeStateEvent.start_jop_M,
                        homeStateEvent.start_jop_Y,
                        homeStateEvent.end_jop_M,
                        homeStateEvent.end_jop_Y,
                        homeStateEvent.to_present,
                        homeStateEvent.description_job,
                        homeStateEvent.id
                    )
                        .onEach {
                            _editExperienceResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.AddEditShortDescription -> {
                    editProfileRepository.addEditExperienceApiCall(
                        homeStateEvent.career_objective
                    )
                        .onEach {
                            _addEditShortDescResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.AddSkillEvent -> {
                    editProfileRepository.addSkillApiCall(
                        homeStateEvent.skills_name
                    )
                        .onEach {
                            _addSkillResponse.value = it
                        }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.DeleteSkillEvent -> {
                    editProfileRepository.deleteSkillApiCall(
                        homeStateEvent.skill_id
                    ).onEach {
                        _deleteSkillResponse.value = it
                    }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.AddInterestEvent -> {
                    editProfileRepository.addInterestApiCall(
                        homeStateEvent.interest
                    ).onEach {
                        _addInterestResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteInterestEvent -> {
                    editProfileRepository.deleteInterestApiCall(
                        homeStateEvent.interest_id
                    ).onEach {
                        _deleteInterestResponse.value = it
                    }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.AddCoursesEvent -> {
                    editProfileRepository.addCoursesApiCall(
                        homeStateEvent.address,
                        homeStateEvent.center_name,
                        homeStateEvent.action_date,
                        homeStateEvent.count_times,
                        homeStateEvent.description,
                        homeStateEvent.logo
                    ).onEach {
                        _addCoursesResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.EditCoursesEvent -> {
                    editProfileRepository.editCoursesApiCall(
                        homeStateEvent.address,
                        homeStateEvent.center_name,
                        homeStateEvent.action_date,
                        homeStateEvent.count_times,
                        homeStateEvent.description,
                        homeStateEvent.course_id,
                        homeStateEvent.logo

                    ).onEach {
                        _editCoursesResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteJobPost -> {
                    editProfileRepository.deleteJobPost(
                        homeStateEvent.job_post_id

                    ).onEach {
                        _deleteJobPostResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteTrainingCoursesEvent -> {
                    editProfileRepository.deleteTrainingCourses(
                        homeStateEvent.training_courses_id

                    ).onEach {
                        _deleteJobPostResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteCourseEvent -> {
                    editProfileRepository.deleteCoursesApiCall(
                        homeStateEvent.course_id

                    ).onEach {
                        _deleteCourseResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.AddCertificateEvent -> {
                    editProfileRepository.addCertificateApiCall(
                        homeStateEvent.title,
                        homeStateEvent.from_by,
                        homeStateEvent.month,
                        homeStateEvent.year,
                        homeStateEvent.description

                    ).onEach {
                        _addCertificateResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.EditCertificateEvent -> {
                    editProfileRepository.editCertificateApiCall(
                        homeStateEvent.title,
                        homeStateEvent.from_by,
                        homeStateEvent.month,
                        homeStateEvent.year,
                        homeStateEvent.description,
                        homeStateEvent.certificate_id
                    ).onEach {
                        _editCertificateResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteCertificateEvent -> {
                    editProfileRepository.deleteCertificateApiCall(
                        homeStateEvent.certificate_id
                    ).onEach {
                        _deleteCertificateResponse.value = it
                    }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.AddProjectEvent -> {
                    editProfileRepository.addProjectApiCall(
                        homeStateEvent.title,
                        homeStateEvent.month,
                        homeStateEvent.year,
                        homeStateEvent.description,
                    ).onEach {
                        _addProjectResponse.value = it
                    }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.EditProjectEvent -> {
                    editProfileRepository.editProjectApiCall(
                        homeStateEvent.title,
                        homeStateEvent.month,
                        homeStateEvent.year,
                        homeStateEvent.description,
                        homeStateEvent.project_id
                    ).onEach {
                        _editProjectResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteProjectEvent -> {
                    editProfileRepository.deleteProjectApiCall(
                        homeStateEvent.project_id
                    ).onEach {
                        _deleteProjectResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.ComputerLevelList -> {
                    editProfileRepository.computerLevelListApiCall(homeStateEvent.lang)
                        .onEach {
                            _computerLevelList.value = it
                        }
                        .launchIn(viewModelScope)
                }

                is EditProfileStateEvent.SaveLanguageEvent -> {
                    editProfileRepository.addLanguageApiCall(
                        homeStateEvent.language,
                        homeStateEvent.level
                    ).onEach {
                        _addLanguageResponse.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.DeleteLanguageEvent -> {
                    editProfileRepository.deleteLanguageApiCall(homeStateEvent.id)
                        .onEach {
                            _deleteLanguageResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.CvExcellence -> {
                    editProfileRepository.cvExcellence()
                        .onEach {
                            _CvExcellenceResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.BioGraphicalFile -> {
                    editProfileRepository.uploadCV(homeStateEvent.fileCV)
                        .onEach {
                            _uploadCVResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.SocialUpdateEvent -> {
                    editProfileRepository.socialUpdateApiCall(
                        homeStateEvent.fb,
                        homeStateEvent.twitter,
                        homeStateEvent.linkedIn,
                        homeStateEvent.website,
                        homeStateEvent.youtube,
                        homeStateEvent.instagram
                    ).onEach {
                        _socialUpdate.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.BasicDataUpdateEvent -> {
                    editProfileRepository.basicDataUpdateApiCall(
                        homeStateEvent.name,
                        homeStateEvent.job_title_en,
                        homeStateEvent.job_title,
                        homeStateEvent.specilization,
                        homeStateEvent.jobRole,
                        homeStateEvent.job_status
                    ).onEach {
                        _basicDataUpdate.value = it
                    }.launchIn(viewModelScope)
                }
                is EditProfileStateEvent.UpdateUserProfileImage -> {
                    editProfileRepository.updateUserImageApiCall(
                        homeStateEvent.photo
                    ).onEach {
                        _basicDataUpdate.value = it
                    }.launchIn(viewModelScope)
                }

                is EditProfileStateEvent.SendVerifyOTPPhone -> {
                    editProfileRepository.sendVerifyOtpForPhoneApiCall(homeStateEvent.authRequest)
                        .onEach {
                            _sendVerifyOtpResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.ShowPhoneList -> {
                    editProfileRepository.showPhoneListApiCall()
                        .onEach {
                            _showPhoneList.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.ShowEmailList -> {
                    editProfileRepository.showEmailListApiCall()
                        .onEach {
                            _showEmailList.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.SaveDeletePhoneEmail -> {
                    editProfileRepository.saveDeletePhoneEmailApiCall(
                        homeStateEvent.phone,
                        homeStateEvent.type
                    )
                        .onEach {
                            _saveDeletePhoneEmail.value = it
                        }
                        .launchIn(viewModelScope)
                }

                is EditProfileStateEvent.DeletePhoneEmail -> {
                    editProfileRepository.deletePhoneEmailApiCall(
                        homeStateEvent.id,
                    )
                        .onEach {
                            _deletePhoneEmail.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is EditProfileStateEvent.LanguageList -> {
                    editProfileRepository.languageListApiCall(
                        homeStateEvent.lang
                    )
                        .onEach {
                            _languageList.value = it
                        }
                        .launchIn(viewModelScope)
                }

                else -> {}
            }
        }
    }
}


sealed class EditProfileStateEvent {
    data class SendVerifyOTPPhone(val authRequest: AuthRequest) : EditProfileStateEvent()
    data class UpdateUserProfileImage(val photo: String) : EditProfileStateEvent()
    data class HomeSpecializationList(val language: String) : EditProfileStateEvent()
    data class HomeCategoriesList(val language: String) : EditProfileStateEvent()
    data class EditProfileCity(val language: String) : EditProfileStateEvent()
    data class ProfileData(val language: String) : EditProfileStateEvent()
    data class JobPostAppliedData(val language: String) : EditProfileStateEvent()
    data class ExpiredJobAppliedData(val language: String) : EditProfileStateEvent()
    data class TrainingCoursesData(val language: String) : EditProfileStateEvent()

    data class BioGraphicalFile(val fileCV: String) : EditProfileStateEvent()

    data class PersonalInfoEdit(
        val address: String,
        val city: String,
        val gender: String,
        val birthDate: String,
        val Dependents: String,
        val marital_status: String,
        val experience: String,
        val salary: String
    ) : EditProfileStateEvent()

    data class CommunicationDataEdit(
        val email: String,
        val phone: String
    ) : EditProfileStateEvent()

    data class QualificationList(val language: String) : EditProfileStateEvent()

    data class AddEducationEvent(
        val educational_institution: String,
        val general_appreciation: String,
        val specialization: String,
        val degree: String,
        val end_date_educational: String,
        val description_education: String,
        val id: Int
    ) : EditProfileStateEvent()

    data class EditEducationEvent(
        val educational_institution: String,
        val general_appreciation: String,
        val specialization: String,
        val degree: String,
        val end_date_educational: String,
        val description_education: String,
        val id: String
    ) : EditProfileStateEvent()

    data class DeleteEducationEvent(val edu_id: String) : EditProfileStateEvent()
    data class DeleteTrainingCoursesEvent(val training_courses_id: Int) : EditProfileStateEvent()

    data class AddExperienceEvent(
        val position_in_company: String,
        val company_name: String,
        val location: String,
        val start_jop_M: String,
        val start_jop_Y: String,
        val end_jop_M: String,
        val end_jop_Y: String,
        val to_present: String,
        val description_job: String
    ) : EditProfileStateEvent()

    data class DeleteExperienceEvent(val exp_id: String) : EditProfileStateEvent()

    data class EditExperienceEvent(
        val position_in_company: String,
        val company_name: String,
        val location: String,
        val start_jop_M: String,
        val start_jop_Y: String,
        val end_jop_M: String,
        val end_jop_Y: String,
        val to_present: String,
        val description_job: String,
        val id: Int
    ) : EditProfileStateEvent()


    data class AddEditShortDescription(val career_objective: String) : EditProfileStateEvent()

    data class AddSkillEvent(val skills_name: String) : EditProfileStateEvent()

    data class DeleteSkillEvent(val skill_id: String) : EditProfileStateEvent()

    data class AddInterestEvent(val interest: String) : EditProfileStateEvent()

    data class DeleteInterestEvent(val interest_id: String) : EditProfileStateEvent()

    data class AddCoursesEvent(
        val address: String,
        val center_name: String,
        val action_date: String,
        val count_times: String,
        val description: String,
        val logo: String,
    ) : EditProfileStateEvent()

    data class EditCoursesEvent(
        val address: String,
        val center_name: String,
        val action_date: String,
        val count_times: String,
        val description: String,
        val course_id: Int,
        val logo: String
    ) : EditProfileStateEvent()

    data class DeleteJobPost(
        val job_post_id: Int
    ) : EditProfileStateEvent()

    data class DeleteCourseEvent(val course_id: Int) : EditProfileStateEvent()

    data class AddCertificateEvent(
        val title: String,
        val from_by: String,
        val month: String,
        val year: String,
        val description: String
    ) : EditProfileStateEvent()

    data class EditCertificateEvent(
        val title: String,
        val from_by: String,
        val month: String,
        val year: String,
        val description: String,
        val certificate_id: Int,
    ) : EditProfileStateEvent()

    data class DeleteCertificateEvent(val certificate_id: Int) : EditProfileStateEvent()


    data class AddProjectEvent(
        val title: String,
        val month: String,
        val year: String,
        val description: String
    ) : EditProfileStateEvent()

    data class EditProjectEvent(
        val title: String,
        val month: String,
        val year: String,
        val description: String,
        val project_id: Int,
    ) : EditProfileStateEvent()

    data class DeleteProjectEvent(val project_id: Int) : EditProfileStateEvent()

    data class ComputerLevelList(val lang: String) : EditProfileStateEvent()

    data class SaveLanguageEvent(
        val language: String,
        val level: String
    ) : EditProfileStateEvent()

    data class DeleteLanguageEvent(val id: Int) : EditProfileStateEvent()
    object CvExcellence : EditProfileStateEvent()


    data class SocialUpdateEvent(
        val fb: String,
        val twitter: String,
        val linkedIn: String,
        val website: String,
        val youtube: String,
        val instagram: String
    ) : EditProfileStateEvent()

    data class BasicDataUpdateEvent(
        val name: String,
        val job_title_en: String,
        val job_title: String,
        val specilization: Int,
        val jobRole: Int,
        val job_status: Int
    ) : EditProfileStateEvent()

    object ShowPhoneList : EditProfileStateEvent()
    object ShowEmailList : EditProfileStateEvent()

    data class SaveDeletePhoneEmail(
        val phone: String,
        val type: String,
    ) : EditProfileStateEvent()

    data class DeletePhoneEmail(
        val id: String,
    ) : EditProfileStateEvent()

    data class LanguageList(val lang: String) : EditProfileStateEvent()
}