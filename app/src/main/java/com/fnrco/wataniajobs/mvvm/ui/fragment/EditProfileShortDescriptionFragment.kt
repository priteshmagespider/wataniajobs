package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentEditProfileShortDescriptionBinding
import com.fnrco.wataniajobs.mvvm.adapter.CityDropDownListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.EditCommunicationDataResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.EditEducationResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.ProfileResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class EditProfileShortDescriptionFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentEditProfileShortDescriptionBinding
    private lateinit var navController: NavController
    private lateinit var profileResponseBean: ProfileResponseBean

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    private lateinit var cityDropDownListAdapter: CityDropDownListAdapter

    private var gender = ""
    private var maritalStatus = ""
    var city: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditProfileShortDescriptionBinding.inflate(layoutInflater)



        if (requireArguments().containsKey(Constant.PROFILE_DATA)) {
            profileResponseBean =
                requireArguments().getSerializable(Constant.PROFILE_DATA) as ProfileResponseBean

        }


        viewModel.setStateEvent(
            EditProfileStateEvent.EditProfileCity(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )

        initUi()
        getProfileDataApiCall()
        setObserver()
        return binding.root
    }

    private fun setObserver() {
        /** Short Description  */
        viewModel.addEditShortDescResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditEducationResponseBean> -> {
                    displayProgressBar(false)
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "API_CALL", "API_CALL"
                    )
                    findNavController().popBackStack()
                    getProfileDataApiCall()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /**User Profile data */
        viewModel.profileDataResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<ProfileResponseBean> -> {
                    displayProgressBar(false)
                    Log.d(mTAG, "setObserver: ${dataState.data}")
                    /*   listener?.setProfileScreenData(
                           dataState.data.user?.job_seeker?.cv!!, dataState.data.user?.email!!,
                           dataState.data.user?.job_seeker.phone!!
                       )*/
                    /** Short Description */
                    binding.edtShortDesc.setText(dataState.data.user?.job_seeker?.career_objective)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

    }


    private fun initUi() {
        binding.txtSave.setOnClickListener(this)
        binding.txtCancel.setOnClickListener(this)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
    }


    private fun getProfileDataApiCall() {
        viewModel.setStateEvent(
            EditProfileStateEvent.ProfileData(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCancel -> {
                findNavController().popBackStack()
            }

            R.id.txtSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (binding.edtShortDesc.text.toString().isEmpty()) {
                        binding.edtShortDesc.error =
                            getString(R.string.lbl_error_short_desc)
                        binding.edtShortDesc.requestFocus()
                    } else {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.AddEditShortDescription(
                                binding.edtShortDesc.text.toString()
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


}