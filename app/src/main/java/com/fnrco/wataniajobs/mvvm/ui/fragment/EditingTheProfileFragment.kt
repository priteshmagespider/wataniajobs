package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.LanguagesListAdapter
import com.fnrco.wataniajobs.adapter.QualificationListAdapter
import com.fnrco.wataniajobs.adapter.SpecializationDropDownListAdapter
import com.fnrco.wataniajobs.databinding.FragmentEditingTheProfileBinding
import com.fnrco.wataniajobs.mvvm.adapter.*
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File
import java.io.FileOutputStream
import java.util.*
import javax.inject.Inject


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class EditingTheProfileFragment : Fragment(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentEditingTheProfileBinding
    lateinit var  languageListResponseBean: LanguageListResponseBean
    lateinit var navController: NavController
    private var basicDataOpen: Boolean = false
    private var personalInfoOpen: Boolean = false
    private var shortDescriptionOpen: Boolean = false
    private var educationOpen: Boolean = false
    private var experiencesOpen: Boolean = false
    private var skillOpen: Boolean = false
    private var communicationOpen: Boolean = false
    private var bioGraphicalDataEditOpen: Boolean = false
    private var interestEditOpen: Boolean = false
    private var educationalCoursesOpen: Boolean = false
    private var certificatesOpen: Boolean = false
    private var projectsOpen: Boolean = false
    private var languageOpen: Boolean = false
    private var socialOpen: Boolean = false
    private var fileCv: String = ""
    private var cvUrl: String = ""
    private var logo: String = ""
    private lateinit var jobRoleDropDownListAdapter: JobRoleDropDownListAdapter
    private lateinit var cityDropDownListAdapter: CityDropDownListAdapter
    private lateinit var specializationDropDownListAdapter: SpecializationDropDownListAdapter
    private lateinit var qualificationListAdapter: QualificationListAdapter

    private lateinit var educationListAdapter: EducationListAdapter
    private lateinit var experienceListAdapter: ExperienceListAdapter
    private lateinit var skillListAdapter: SkillListAdapter
    private lateinit var interestListAdapter: InterestListAdapter
    private lateinit var educationCoursesListAdapter: EducationCoursesListAdapter

    private lateinit var specializationList: List<SpecializationBasicData>
    private lateinit var cityList: List<CityList>
    private lateinit var jobRoleList: List<CategoryListResponseBean>
    private lateinit var qualificationResponseBean: QualificationResponseBean
    private lateinit var computerLevelResponseBean: ComputerLevelResponseBean
    private lateinit var socialResponseBean: SocialResponseBean
    private lateinit var certificatesListAdapter: CertificatesListAdapter
    private lateinit var projectsListAdapter: ProjectsListAdapter
    private lateinit var languageListAdapter: LanguageListAdapter
    private lateinit var profileResponseBean: ProfileResponseBean
    private lateinit var computerLevelDropDownListAdapter: ComputerLevelDropDownListAdapter
    private lateinit var imgUploadedCertificate: ImageView
    private var computer_level: Int = -1

    lateinit var bottomSheetDialog: BottomSheetDialog
    lateinit var bottomSheetView: View

    lateinit var bottomSheetDialogEducation: BottomSheetDialog
    lateinit var bottomSheetViewEducation: View

    lateinit var bottomSheetDialogExperience: BottomSheetDialog
    lateinit var bottomSheetViewExperience: View

    lateinit var bottomSheetDialogSkill: BottomSheetDialog
    lateinit var bottomSheetViewSkill: View

    lateinit var bottomSheetDialogInterest: BottomSheetDialog
    lateinit var bottomSheetViewInterest: View

    lateinit var bottomSheetDialogEducationalCourses: BottomSheetDialog
    lateinit var bottomSheetViewEducationalCourses: View

    lateinit var bottomSheetDialogCertificate: BottomSheetDialog
    lateinit var bottomSheetViewCertificate: View

    lateinit var bottomSheetDialogProjects: BottomSheetDialog
    lateinit var bottomSheetViewProjects: View

    lateinit var bottomSheetDialogLanguage: BottomSheetDialog
    lateinit var bottomSheetViewLanguage: View

    lateinit var bottomSheetDialogSocial: BottomSheetDialog
    lateinit var bottomSheetViewSocial: View

    lateinit var bottomSheetDeleteEducation: BottomSheetDialog
    lateinit var bottomSheetViewDeleteEducation: View

    lateinit var bottomSheetDeleteExperience : BottomSheetDialog
    lateinit var bottomSheetViewDeleteExperience : View

    lateinit var bottomSheetDeleteSkill : BottomSheetDialog
    lateinit var bottomSheetViewDeleteSkill : View

    lateinit var bottomSheetDeleteInterest : BottomSheetDialog
    lateinit var bottomSheetViewDeleteInterest : View

    lateinit var bottomSheetDeleteEducationalCourses : BottomSheetDialog
    lateinit var bottomSheetViewDeleteEducationalCourses : View

    lateinit var bottomSheetDialogDeleteCertificate: BottomSheetDialog
    lateinit var bottomSheetViewDeleteCertificate : View

    lateinit var bottomSheetDialogDeleteProject: BottomSheetDialog
    lateinit var bottomSheetViewDeleteProject : View

    lateinit var bottomSheetDialogDeleteLanguage: BottomSheetDialog
    lateinit var bottomSheetViewDeleteLanguage : View

    private var gender = ""
    private var genderId = ""
    private var maritalStatus = ""
    private var maritalStatusId = ""

    private var posEditDelete: Int = -1
    private var posEditEducation: Int = -1
    private var posEditExperience: Int = -1
    private var posEditEducationCourses: Int = -1
    private var posEditCertificate: Int = -1
    private var posEditProjects: Int = -1


    private var pos: Int = -1
    private var degreeId: String = ""
    private var lanugageId: String = ""

    private var job_status: Int = -1
    lateinit var imgUploadedCv: ImageView
    lateinit var cvProgressbar: ProgressBar
    lateinit var educationProgressbar: ProgressBar
    lateinit var experienceProgressbar: ProgressBar
    lateinit var skillProgressbar: ProgressBar
    lateinit var interestProgressbar: ProgressBar
    lateinit var educationalCoursesProgressbar: ProgressBar
    lateinit var certificateProgressbar: ProgressBar
    lateinit var projectsProgressbar: ProgressBar
    lateinit var languageProgressbar: ProgressBar
    lateinit var socialProgressbar: ProgressBar

    var city: String = ""

    private var expId = -1

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager

    private var specializationId: Int? = 0
    private var jobRoleId: Int? = 0

    private var endDate = ""
    private val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    private val month = c.get(Calendar.MONTH)
    private val day = c.get(Calendar.DAY_OF_MONTH)

    private var sMonthId = -1
    private var eMonthId = -1
    private var course_id = -1
    private var certificateId = -1
    private var projectId = -1
    private var certificateMonth = -1
    private var projectMonth = -1

    var fullName: String = ""
    var job_title: String = ""
    var specialization: String = ""
    var specializationPosition: Int = 0
    var career_role: String = ""
    var functional_status: String = ""

    private var arrayJobStatusSize = arrayOf(
        "I am looking for a job.",
        "I am working and looking for new opportunities.",
        "I am not looking for a job."
    )
    private var arrayJobStatusArSize = arrayOf(
        "أنا أبحث عن عمل.",
        "أنا أعمل وأبحث عن فرص جديدة.",
        "أنا لا أبحث عن عمل."
    )

    private var arrayTypeSize = arrayOf(
        "Male",
        "Female"
    )

    private var arrayTypeArSize = arrayOf(
        "ذكر",
        "أنثى"
    )


    private var arraySocialStatusSize = arrayOf(
        "Married",
        "Unmarried",
        "Absolute",
        "Widower"
    )

    private var arraySocialStatusArSize = arrayOf(
        "متزوج",
        "اعزب",
        "مطلق",
        "أرمل"
    )

    private var monthArrayEn = arrayOf(
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObserver()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditingTheProfileBinding.inflate(inflater, container, false)
        initUi()

        viewModel.setStateEvent(
            EditProfileStateEvent.HomeCategoriesList(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
        viewModel.setStateEvent(
            EditProfileStateEvent.ComputerLevelList(
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
            )
        )
        viewModel.setStateEvent(
            EditProfileStateEvent.LanguageList(
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
            )
        )


        updateUi()
        getProfileDataApiCall()
        return binding.root
    }

    private fun initUi() {
        binding.layoutBasicData.viewBasicData.setOnClickListener(this)
        binding.layoutPersonalInfo.viewPersonalInfo.setOnClickListener(this)
        binding.layoutShortDescription.viewShortDescription.setOnClickListener(this)
        binding.layoutCommunicationData.viewCommunicationData.setOnClickListener(this)
        binding.layoutCommunicationData.imgCommunicationDataDropUp.setOnClickListener(this)
        binding.layoutBioGraphicalFile.viewBioGraphicalFile.setOnClickListener(this)
        binding.layoutBioGraphicalFile.txtBioGraphicalFileName.setOnClickListener(this)
        binding.layoutBioGraphicalFile.imgRight.setOnClickListener(this)
        binding.layoutPersonalInfo.edtDateOfBirth.setOnClickListener(this)
        binding.layoutPersonalInfo.edtCity.setOnClickListener(this)
        binding.layoutEducation.viewEducation.setOnClickListener(this)
        binding.layoutExperiences.viewExperiences.setOnClickListener(this)
        binding.layoutSkill.viewSkills.setOnClickListener(this)
        binding.layoutInterests.viewInterests.setOnClickListener(this)
        binding.layoutEducationalCourses.viewEducationalCourses.setOnClickListener(this)
        binding.layoutCertificates.viewCertificates.setOnClickListener(this)
        binding.layoutProjects.viewProjects.setOnClickListener(this)
        binding.layoutLanguages.viewLanguages.setOnClickListener(this)
        binding.layoutSocialNetwork.viewSocialNetwork.setOnClickListener(this)


//        Modification
        binding.layoutBasicData.btnBasicDataModification.setOnClickListener(this)
        binding.layoutBasicData.imgEditBasicData.setOnClickListener(this)
        binding.layoutPersonalInfo.btnPersonalInfoModification.setOnClickListener(this)
        binding.layoutPersonalInfo.imgPersonalInfoModification.setOnClickListener(this)
        binding.layoutCommunicationData.btnCommunicationDataModification.setOnClickListener(this)
        binding.layoutCommunicationData.imgCommunicationDataModification.setOnClickListener(this)
        binding.layoutShortDescription.btnShortDescModification.setOnClickListener(this)
        binding.layoutShortDescription.imgShortDescModification.setOnClickListener(this)
        binding.layoutBioGraphicalFile.btnBioGraphicalModification.setOnClickListener(this)
        binding.layoutBioGraphicalFile.imgBioGraphicalModification.setOnClickListener(this)
        binding.layoutEducation.btnEducationInstituteModification.setOnClickListener(this)
//        binding.layoutEducation.imgEducationInstituteModification.setOnClickListener(this)
        binding.layoutExperiences.btnExperienceAdd.setOnClickListener(this)
        binding.layoutSkill.btnSkillAdd.setOnClickListener(this)
        binding.layoutInterests.btnInterestsAdd.setOnClickListener(this)
        binding.layoutEducationalCourses.btnCoursesAdd.setOnClickListener(this)
        binding.layoutEducationalCourses.edtDateOfStarting.setOnClickListener(this)
        binding.layoutCertificates.btnCertificateAdd.setOnClickListener(this)
        binding.layoutProjects.btnProjectsAdd.setOnClickListener(this)
        binding.layoutLanguages.btnLanguageAdd.setOnClickListener(this)
        binding.layoutSocialNetwork.btnSocialAdd.setOnClickListener(this)
        binding.layoutBioGraphicalFile.clBioGraphicalUpload.setOnClickListener(this)
        binding.layoutBioGraphicalFile.btnBioGraphicalSave.setOnClickListener(this)


//        Save,Cancel
        binding.layoutBasicData.btnBasicDataSave.setOnClickListener(this)
        binding.layoutBasicData.btnBasicDataCancel.setOnClickListener(this)

        binding.layoutPersonalInfo.btnPresonalInfoCancel.setOnClickListener(this)
        binding.layoutPersonalInfo.btnPresonalInfoSave.setOnClickListener(this)

        binding.layoutCommunicationData.btnCommunicationDataCancel.setOnClickListener(this)
        binding.layoutCommunicationData.btnCommunicationDataSave.setOnClickListener(this)

        binding.layoutShortDescription.btnShortDescSave.setOnClickListener(this)
        binding.layoutShortDescription.btnShortDescCancel.setOnClickListener(this)

//        binding.layoutBioGraphicalFile.btnBioGraphicalSave.setOnClickListener(this)
        binding.layoutBioGraphicalFile.btnBioGraphicalCancel.setOnClickListener(this)

//        binding.layoutEducation.btnEducationalInstituteSave.setOnClickListener(this)
//        binding.layoutEducation.btnEducationalInstituteCancel.setOnClickListener(this)

        binding.layoutExperiences.btnExperienceSave.setOnClickListener(this)
        binding.layoutExperiences.btnExperienceCancel.setOnClickListener(this)

        binding.layoutSkill.btnSkillCancel.setOnClickListener(this)
        binding.layoutSkill.btnSkillSave.setOnClickListener(this)

        binding.layoutInterests.btnInterestsSave.setOnClickListener(this)
        binding.layoutInterests.btnInterestsCancel.setOnClickListener(this)

        binding.layoutEducationalCourses.btnCoursesCancel.setOnClickListener(this)
        binding.layoutEducationalCourses.btnCoursesSave.setOnClickListener(this)

        binding.layoutCertificates.btnCertificateCancel.setOnClickListener(this)
        binding.layoutCertificates.btnCertificateSave.setOnClickListener(this)

        binding.layoutProjects.btnProjectSave.setOnClickListener(this)
        binding.layoutProjects.btnProjectCancel.setOnClickListener(this)

        binding.layoutLanguages.btnLanguageCancel.setOnClickListener(this)
        binding.layoutLanguages.btnLanguageSave.setOnClickListener(this)

        binding.layoutSocialNetwork.btnSocialCancel.setOnClickListener(this)
        binding.layoutSocialNetwork.btnSocialSave.setOnClickListener(this)

        binding.layoutExperiences.cbUntilNow.setOnCheckedChangeListener { _, isChecked ->

            binding.layoutExperiences.edtToMonth.isEnabled = !isChecked
            binding.layoutExperiences.edtToYear.isEnabled = !isChecked

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("API_CALL")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter name-> $data")
                getProfileDataApiCall()
            }

        /*navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("name")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter name-> $data")
                fullName = data
                binding.layoutBasicData.txtFullName.text = fullName
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("job_title")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter job_title-> $data")
                job_title = data
                binding.layoutBasicData.txtJobTitle.text = job_title
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("specialization")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter specialization_id-> $data")
                specializationId = data
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("specialization_position")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter specialization_position-> $data")
                specializationPosition = data
                binding.layoutBasicData.txtSpecialization.text = specializationDropDownListAdapter.getSpecialityList()[data].title_en
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("career_role")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter career_role-> $data")
                career_role = data
                binding.layoutBasicData.txtCareerRole.text = career_role
            }
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("functional_status")
            ?.observe(viewLifecycleOwner) { data ->
                Log.d(mTAG, "Filter functional_status-> $data")
                functional_status = data
                binding.layoutBasicData.txtFunctionalStatus.text = functional_status
            }*/

    }

    private fun setObserver() {
        /** Language list Api call */
        viewModel.languageList.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<LanguageListResponseBean> -> {
                    displayProgressBar(false)
                    languageListResponseBean = dataState.data
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Category list response */
        viewModel.categoryListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<CategoriesListResponseBean> -> {
                    displayProgressBar(false)
//                    setDropDownJobRole(dataState.data)
//                    jobRoleList = dataState.data.categories
                    viewModel.setStateEvent(
                        EditProfileStateEvent.EditProfileCity(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!
                        )
                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Category list response */
        viewModel.uploadCVResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<UploadCvResponseBean> -> {
                    displayProgressBar(false)
                    cvProgressbar.visibility = View.GONE
                    if (dataState.data.message == "cv Updated Successfully") {
                        getProfileDataApiCall()
                        fileCv = ""
                        bottomSheetDialog.dismiss()
//                        binding.layoutBioGraphicalFile.clEditBioGraphicalData.visibility = View.GONE
//                        binding.layoutBioGraphicalFile.clBioGraphicalData.visibility = View.GONE
                        Toast.makeText(requireContext(), "Error", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(requireContext(), dataState.data.message, Toast.LENGTH_LONG)
                            .show()
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /*** City list Response*/

        viewModel.cityListResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<CityListResponse> -> {
                    displayProgressBar(false)
                    setDropDownCities(dataState.data)
                    cityList = dataState.data.cities!!
                    viewModel.setStateEvent(
                        EditProfileStateEvent.HomeSpecializationList(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!
                        )
                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Specialization */
        viewModel.specializationListResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<BasicDataSpecializationResponse> -> {
                    displayProgressBar(false)
                    setDropDownSpecialization(dataState.data)
                    specializationList = dataState.data.specializations!!
                    viewModel.setStateEvent(
                        EditProfileStateEvent.QualificationList(
                            prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
                        )
                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Qualification Response */
        viewModel.qualificationResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<QualificationResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.qualifications != null) {
                        setQualificationData(dataState.data.qualifications)
                        qualificationResponseBean = dataState.data
                    }
                    getProfileDataApiCall()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })


        /**User Profile data */
        viewModel.profileDataResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<ProfileResponseBean> -> {
                    displayProgressBar(false)
                    Log.d(mTAG, "setObserver: ${dataState.data}")
                    /*   listener?.setProfileScreenData(
                           dataState.data.user?.job_seeker?.cv!!, dataState.data.user?.email!!,
                           dataState.data.user?.job_seeker.phone!!
                       )*/
                    setData(dataState.data)
                    profileResponseBean = dataState.data
                    cvUrl = dataState.data.user?.job_seeker?.cv!!
                    Log.d("TAG", "setObserver: $cvUrl")
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Personal info edit   */
        viewModel.personalInfoResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditPersonalInfoResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.status!!) {
                        getProfileDataApiCall()
                        binding.layoutPersonalInfo.clEditPersonalInfo.visibility = View.GONE
                        binding.layoutPersonalInfo.clPersonalInfo.visibility = View.VISIBLE
                    }

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Communication data info edit   */
        viewModel.communicationDataResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditCommunicationDataResponseBean> -> {
                    displayProgressBar(false)
                    /**Communication Data Set*/
                    binding.layoutCommunicationData.txtMobileNumber.text = dataState.data.phone
                    binding.layoutCommunicationData.txtEmail.text = dataState.data.email

                    binding.layoutCommunicationData.edtEmail.setText(dataState.data.email)
                    binding.layoutCommunicationData.edtMobileNumber.setText(dataState.data.phone)
                    binding.layoutCommunicationData.clEditCommunicationData.visibility = View.GONE
                    binding.layoutCommunicationData.clCommunicationData.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Edit Education data info edit   */
        viewModel.addEducationResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditEducationResponseBean> -> {
                    displayProgressBar(false)
                    educationProgressbar.visibility = View.GONE
                    bottomSheetDialogEducation.dismiss()
                    getProfileDataApiCall()
//                    binding.layoutEducation.clEducation.visibility = View.VISIBLE
//                    binding.layoutEducation.clAddEditEducation.visibility = View.GONE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Edit Education data info edit   */
        viewModel.editEducationResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditEducationResponseBean> -> {
                    displayProgressBar(false)
                    educationProgressbar.visibility = View.GONE
                    bottomSheetDialogEducation.dismiss()
                    getProfileDataApiCall()

                    posEditEducation = -1
//                    binding.layoutEducation.clEducation.visibility = View.VISIBLE
//                    binding.layoutEducation.clAddEditEducation.visibility = View.GONE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** delete Education */
        viewModel.deleteEducationResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<DeleteEducationResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.status!!) {
                        educationListAdapter.deleteItem(posEditDelete)
//                        bottomSheetDeleteEducation.dismiss()
                        deleteEducationDialog(true)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Add Experience */
        viewModel.addEditDeleteExperienceResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteExperienceResponseBean> -> {
                    displayProgressBar(false)
                    getProfileDataApiCall()
                    experienceProgressbar.visibility = View.GONE
                    bottomSheetDialogExperience.dismiss()

                    binding.layoutExperiences.clAddEditExperience.visibility = View.GONE
                    binding.layoutExperiences.clExperience.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Experience */
        viewModel.deleteEditDeleteExperienceResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteExperienceResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.isNotEmpty()) {
                        getProfileDataApiCall()
                        experienceListAdapter.deleteItem(posEditDelete)
                        deleteExperienceDialog(true)
                    }

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Edit Experience */
        viewModel.editExperienceResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteExperienceResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.isNotEmpty()) {
                        getProfileDataApiCall()
                        experienceProgressbar.visibility = View.VISIBLE
                        bottomSheetDialogExperience.dismiss()
//                        binding.layoutExperiences.clAddEditExperience.visibility = View.GONE
//                        binding.layoutExperiences.clExperience.visibility = View.VISIBLE
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Short Description  */
        viewModel.addEditShortDescResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditEducationResponseBean> -> {
                    displayProgressBar(false)
                    binding.layoutShortDescription.clEditShortDesc.visibility = View.GONE
                    binding.layoutShortDescription.clShortDesc.visibility = View.VISIBLE
                    getProfileDataApiCall()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Add Skill  */
        viewModel.addSkillResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddDeleteSkillResponseBean> -> {
                    displayProgressBar(false)
//                    binding.layoutSkill.clEditSkill.visibility = View.GONE
//                    binding.layoutSkill.clSkill.visibility = View.VISIBLE
                    binding.layoutSkill.edtSkill.setText("")

                    bottomSheetDialogSkill.dismiss()
                    getProfileDataApiCall()

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Skill  */
        viewModel.deleteSkillResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddDeleteSkillResponseBean> -> {
                    displayProgressBar(false)
                    binding.skillProgressBar.visibility = View.GONE
                    skillListAdapter.deleteItem(pos)
                    skillDeleteDialog(pos, true)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** add Interest  */
        viewModel.addInterestResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddDeleteInterestResponseBean> -> {
                    displayProgressBar(false)
//                    binding.layoutInterests.clEditInterests.visibility = View.GONE
//                    binding.layoutInterests.clInterests.visibility = View.VISIBLE
                    binding.layoutInterests.edtInterests.setText("")
                    interestProgressbar.visibility = View.GONE
                    bottomSheetDialogInterest.dismiss()
                    getProfileDataApiCall()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Interest  */
        viewModel.deleteInterestResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddDeleteInterestResponseBean> -> {
                    displayProgressBar(false)
                    binding.interestsProgressBar.visibility = View.GONE
                    interestListAdapter.deleteItem(pos)
                    interestDeleteDialog(pos, true)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Add Courses Interest  */
        viewModel.addCoursesResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteCoursesResponseBean> -> {
                    displayProgressBar(false)
                    educationalCoursesProgressbar.visibility = View.GONE
                    bottomSheetDialogEducationalCourses.dismiss()
                    getProfileDataApiCall()
                    binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.GONE
                    binding.layoutEducationalCourses.clEducationalCourses.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Edit Courses Interest  */
        viewModel.editCoursesResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteCoursesResponseBean> -> {
                    displayProgressBar(false)
                    educationalCoursesProgressbar.visibility = View.GONE
                    bottomSheetDialogEducationalCourses.dismiss()
                    getProfileDataApiCall()
//                    binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.GONE
//                    binding.layoutEducationalCourses.clEducationalCourses.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Courses Interest  */
        viewModel.deleteCourseResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteCoursesResponseBean> -> {
                    displayProgressBar(false)
                    educationCoursesListAdapter.deleteItem(pos)
                    deleteCourseDialog(true)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Add Certificate Interest  */
        viewModel.addCertificateResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteCertificateResponseBean> -> {
                    displayProgressBar(false)
                    certificateProgressbar.visibility = View.GONE
                    bottomSheetDialogCertificate.dismiss()
                    getProfileDataApiCall()
//                    binding.layoutCertificates.clEditCertificates.visibility = View.GONE
//                    binding.layoutCertificates.clCertificates.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Edit Certificate Interest  */
        viewModel.editCertificateResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<UpdateCertificateResponseBean> -> {
                    displayProgressBar(false)
                    certificateProgressbar.visibility = View.GONE
                    bottomSheetDialogCertificate.dismiss()
                    getProfileDataApiCall()
//                    binding.layoutCertificates.clEditCertificates.visibility = View.GONE
//                    binding.layoutCertificates.clCertificates.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Courses Interest  */
        viewModel.deleteCertificateResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteCertificateResponseBean> -> {
                    displayProgressBar(false)
                    certificatesListAdapter.deleteItem(pos)
                    deleteCertificateDialog(true)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Add project Interest  */
        viewModel.addProjectResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteProjectResponseBean> -> {
                    displayProgressBar(false)
                    projectsProgressbar.visibility = View.GONE
                    bottomSheetDialogProjects.dismiss()
                    getProfileDataApiCall()
//                    binding.layoutProjects.clEditProject.visibility = View.GONE
//                    binding.layoutProjects.clProject.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Edit project Interest  */
        viewModel.editProjectResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteProjectResponseBean> -> {
                    displayProgressBar(false)
                    projectsProgressbar.visibility = View.GONE
                    bottomSheetDialogProjects.dismiss()
//                    binding.layoutProjects.clEditProject.visibility = View.GONE
//                    binding.layoutProjects.clProject.visibility = View.VISIBLE
                    getProfileDataApiCall()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** delete project Interest  */
        viewModel.deleteProjectResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AddEditDeleteProjectResponseBean> -> {
                    displayProgressBar(false)
                    projectsListAdapter.deleteItem(pos)
                    deleteProjectDialog(true)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Computer Level list */
        viewModel.computerLevelList.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<ComputerLevelResponseBean> -> {
                    displayProgressBar(false)
                    setDropDownComputerLevel(dataState.data)
                    computerLevelResponseBean = dataState.data
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Add Language Interest  */
        viewModel.addLanguageResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<LanguageAddDeleteResponseBean> -> {
                    displayProgressBar(false)
                    languageProgressbar.visibility = View.GONE
                    bottomSheetDialogLanguage.dismiss()
                    getProfileDataApiCall()
//                    binding.layoutLanguages.clEditLanguages.visibility = View.GONE
//                    binding.layoutLanguages.clLanguages.visibility = View.VISIBLE
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Language Interest  */
        viewModel.deleteLanguageResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<LanguageAddDeleteResponseBean> -> {
                    displayProgressBar(false)
                    languageListAdapter.deleteItem(pos)
                    deleteLanguageDialog(true)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        /** Delete Language Interest  */
        viewModel.socialUpdate.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<SocialResponseBean> -> {
                    displayProgressBar(false)
                    socialProgressbar.visibility = View.GONE
                    bottomSheetDialogSocial.dismiss()
                    getProfileDataApiCall()
                    socialResponseBean = dataState.data
//                    binding.layoutSocialNetwork.clEditSocialNetwork.visibility = View.GONE
//                    binding.layoutSocialNetwork.clSocialNetwork.visibility = View.VISIBLE

                    binding.layoutSocialNetwork.edtFb.setText(dataState.data.fb ?: "")
                    binding.layoutSocialNetwork.edtLink.setText(dataState.data.linkedIn ?: "")
                    binding.layoutSocialNetwork.edtTwitt.setText(dataState.data.twitter ?: "")
                    binding.layoutSocialNetwork.edtYoutube.setText(dataState.data.youtube ?: "")
                    binding.layoutSocialNetwork.edtInsta.setText(dataState.data.instagram ?: "")
                    binding.layoutSocialNetwork.edtWeb.setText(dataState.data.website ?: "")

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Basic Data Update Interest  */
        viewModel.basicDataUpdate.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<BasicDataUpdateResponseBean> -> {
                    displayProgressBar(false)
                    binding.layoutBasicData.clEditBasicData.visibility = View.GONE
                    binding.layoutBasicData.clBasicData.visibility = View.VISIBLE
                    getProfileDataApiCall()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }


    private fun getProfileDataApiCall() {
        viewModel.setStateEvent(
            EditProfileStateEvent.ProfileData(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
    }

    private fun setDropDownComputerLevel(computerLevelResponseBean: ComputerLevelResponseBean) {
        /**
         *  Set Language list into dropdown
         */
        computerLevelDropDownListAdapter =
            ComputerLevelDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                computerLevelResponseBean.levels!!,
                prefManager
            )

        binding.layoutLanguages.edtProficiency.setAdapter(computerLevelDropDownListAdapter)

        binding.layoutLanguages.edtProficiency.setOnClickListener {
            binding.layoutLanguages.edtProficiency.showDropDown()
        }

        binding.layoutLanguages.edtProficiency.setOnItemClickListener { _, _, position, _ ->
//            speciality_id = specialityDropDownListAdapter.getSpecialityList()[position].id
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.layoutLanguages.edtProficiency.setText(
                    computerLevelDropDownListAdapter.getList()[position].title_en
                )
            } else {
                binding.layoutLanguages.edtProficiency.setText(
                    computerLevelDropDownListAdapter.getList()[position].title_ar
                )
            }
            computer_level = computerLevelDropDownListAdapter.getList()[position].id!!
            computerLevelDropDownListAdapter.notifyDataSetChanged()
        }
    }

    private fun setQualificationData(qualificationList: List<QualificationList>?) {
        /**
         * Set JobRole list into dropdown
         */
        qualificationListAdapter =
            QualificationListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                qualificationList!!
            )

        binding.layoutEducation.edtDegree.setAdapter(qualificationListAdapter)
        binding.layoutEducation.edtDegree.setOnClickListener {
            binding.layoutEducation.edtDegree.showDropDown()
        }
        binding.layoutEducation.edtDegree.setOnItemClickListener { _, _, position, _ ->

            if (qualificationListAdapter.getSpecialityList()[position].title_en == null) {
                binding.layoutEducation.edtDegree.setText(qualificationListAdapter.getSpecialityList()[position].title_ar)
            } else {
                binding.layoutEducation.edtDegree.setText(qualificationListAdapter.getSpecialityList()[position].title_en)
            }
            degreeId = qualificationListAdapter.getSpecialityList()[position].id!!.toString()
            qualificationListAdapter.notifyDataSetChanged()
        }
    }

    private fun setData(data: ProfileResponseBean) {

        binding.apply {

            layoutCommunicationData.imgMobileStatus.setImageResource(if (data.user?.job_seeker?.phone_is_verified == 0) R.drawable.img_unverified else R.drawable.img_verified)
            layoutCommunicationData.txtMobileStatus.setText(if (data.user!!.job_seeker!!.phone_is_verified == 0) R.string.unverified else R.string.verified)
            layoutCommunicationData.txtMobileStatus.setTextColor(
                if (data.user!!.job_seeker!!.phone_is_verified == 0) requireActivity().getColor(
                    R.color.color_primary
                ) else requireActivity().getColor(R.color.green)
            )
        }
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            /**Basic Data Set*/
            binding.layoutBasicData.txtFullName.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutBasicData.txtJobTitle.textDirection = View.TEXT_DIRECTION_LTR
            fullName = ((data.user?.name_en ?: data.user?.name).toString())
            binding.layoutBasicData.txtFullName.text = if(fullName != "null") fullName else ""
            job_title = ((data.user?.job_seeker!!.job_title_en
                ?: data.user?.job_seeker!!.job_title).toString())
            binding.layoutBasicData.txtJobTitle.text =  if(job_title != "null") job_title else ""
            specialization = data.user?.job_seeker?.sepecilization?.title_en.toString()
            binding.layoutBasicData.txtSpecialization.text =if(specialization != "null")  specialization else ""
            career_role = data.user?.job_seeker?.job_role?.title_en.toString()
            binding.layoutBasicData.txtCareerRole.text = if(career_role != "null")  career_role else ""
            if (data.user?.job_seeker?.job_status != null) {
                functional_status =
                    arrayJobStatusSize[if (data.user?.job_seeker?.job_status!!.toInt() == 0) data.user?.job_seeker?.job_status!!.toInt() else data.user?.job_seeker?.job_status!!.toInt() - 1]
                binding.layoutBasicData.txtFunctionalStatus.text = functional_status
                binding.layoutBasicData.edtFunctionalStatus.setText(functional_status, false)
                job_status = data.user?.job_seeker?.job_status!!.toInt()
            }else {
                binding.layoutBasicData.txtFunctionalStatus.text = ""
            }

            binding.layoutBasicData.edtName.setText(data.user?.name_en ?: data.user?.name)
            binding.layoutBasicData.edtTheJobTitle.setText(data.user?.job_seeker?.job_title_en)
            binding.layoutBasicData.edtSpecialization.setText("${data.user?.job_seeker?.sepecilization?.title_en}")
            binding.layoutBasicData.edtCareerRole.setText("${data.user?.job_seeker?.job_role?.title_en}")


            /**Personal Information Data Set*/
            binding.layoutPersonalInfo.txtAddress.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtCity.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtType.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtDateOfBirth.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtNumberOfDependants.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtSocialStatus.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtNumberOfExp.textDirection = View.TEXT_DIRECTION_LTR
            binding.layoutPersonalInfo.txtMinimumSalary.textDirection = View.TEXT_DIRECTION_LTR

            binding.layoutPersonalInfo.txtAddress.text = if(data.user?.job_seeker?.address !=null) data.user?.job_seeker?.address else ""
            binding.layoutPersonalInfo.txtCity.text = data.user?.job_seeker?.city_seeker?.name_en
            gender = when (data.user?.job_seeker?.gender) {
                "1" -> {
                    resources.getString(R.string.male)
                }
                "2" -> {
                    resources.getString(R.string.female)
                }
                else -> {
                    resources.getString(R.string.undefined)
                }
            }
            binding.layoutPersonalInfo.txtType.text = gender

            binding.layoutPersonalInfo.txtDateOfBirth.text = if(data.user?.job_seeker?.birthDate!=null) data.user?.job_seeker?.birthDate else ""
            binding.layoutPersonalInfo.txtNumberOfDependants.text =
              if(data.user?.job_seeker?.Dependents!=null)  data.user?.job_seeker?.Dependents.toString() else ""

            maritalStatus = when (data.user?.job_seeker?.marital_status) {
                1 -> {
                    getString(R.string.married)
                }
                2 -> {
                    getString(R.string.unmarried)
                }
                3 -> {
                    getString(R.string.absolute)
                }
                else -> {
                    getString(R.string.widower)
                }
            }
            binding.layoutPersonalInfo.txtSocialStatus.text = maritalStatus
            binding.layoutPersonalInfo.txtNumberOfExp.text =
            if(data.user?.job_seeker?.experience!=null) data.user?.job_seeker?.experience.toString() else ""
            binding.layoutPersonalInfo.txtMinimumSalary.text =
                if(data.user?.job_seeker?.salary != null)  data.user?.job_seeker?.salary.toString() else ""


            binding.layoutPersonalInfo.edtAddress.setText(data.user?.job_seeker?.address)
            binding.layoutPersonalInfo.edtCity.setText(data.user?.job_seeker?.city_seeker?.name_en)
            binding.layoutPersonalInfo.edtType.setText(gender, false)
            binding.layoutPersonalInfo.edtDateOfBirth.setText(data.user?.job_seeker?.birthDate)
            binding.layoutPersonalInfo.edtFunctionalStatus.setText(data.user?.job_seeker?.Dependents.toString())
            binding.layoutPersonalInfo.edtSocialStatus.setText(maritalStatus, false)
            binding.layoutPersonalInfo.edtNumberOfYearsOfExperience.setText(data.user?.job_seeker?.experience.toString())
            binding.layoutPersonalInfo.edtMinimumSalary.setText(data.user?.job_seeker?.salary.toString())


        } else {
            /**Basic Data Set*/

            binding.layoutBasicData.txtFullName.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutBasicData.txtJobTitle.textDirection = View.TEXT_DIRECTION_RTL
            fullName = ((data.user?.name ?: data.user?.name_en).toString())
            binding.layoutBasicData.txtFullName.text = if(fullName != "null") fullName  else ""
            job_title = ((data.user?.job_seeker?.job_title
                ?: data.user?.job_seeker?.job_title_en).toString())
            binding.layoutBasicData.txtJobTitle.text =
                if(job_title != "null") job_title else ""
            specialization = data.user?.job_seeker?.sepecilization?.title.toString()
            binding.layoutBasicData.txtSpecialization.text =  if(specialization != "null")  specialization else ""

            career_role = data.user?.job_seeker?.job_role?.title.toString()
            binding.layoutBasicData.txtCareerRole.text = if(career_role != "null") career_role else ""
            if (data.user?.job_seeker?.job_status != null) {
//                val jobStatus: Int = data.user?.job_seeker?.job_status?.toInt()!!
//                Log.d("JOB_STATUS", "setData: $jobStatus")
//                functional_status =
//                   if(jobStatus == 0) arrayJobStatusArSize[0] else arrayJobStatusArSize[jobStatus - 1]

                functional_status =
                    arrayJobStatusArSize[if (data.user?.job_seeker?.job_status!!.toInt() == 0) data.user?.job_seeker?.job_status!!.toInt() else data.user?.job_seeker?.job_status!!.toInt() - 1]
                binding.layoutBasicData.txtFunctionalStatus.text = functional_status
                binding.layoutBasicData.edtFunctionalStatus.setText(functional_status, false)
            }else {
                binding.layoutBasicData.txtFunctionalStatus.text = ""
            }

            binding.layoutBasicData.edtName.setText(data.user!!.name)
            binding.layoutBasicData.edtTheJobTitle.setText(data.user?.job_seeker?.job_title)
            binding.layoutBasicData.edtSpecialization.setText(data.user?.job_seeker?.sepecilization?.title)
            binding.layoutBasicData.edtCareerRole.setText(data.user?.job_seeker?.job_role?.title)


            /**Personal Information Data Set*/
            binding.layoutPersonalInfo.txtAddress.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtCity.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtType.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtDateOfBirth.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtNumberOfDependants.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtSocialStatus.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtNumberOfExp.textDirection = View.TEXT_DIRECTION_RTL
            binding.layoutPersonalInfo.txtMinimumSalary.textDirection = View.TEXT_DIRECTION_RTL

            binding.layoutPersonalInfo.txtAddress.text = if(data.user?.job_seeker?.address !=null) data.user?.job_seeker?.address else ""
            binding.layoutPersonalInfo.txtCity.text = data.user?.job_seeker?.city_seeker?.name_ar
            gender = when (data.user?.job_seeker?.gender) {
                "1" -> {
                    resources.getString(R.string.male)
                }
                "2" -> {
                    resources.getString(R.string.female)
                }
                else -> {
                    resources.getString(R.string.undefined)
                }
            }
            binding.layoutPersonalInfo.txtType.text = gender

            binding.layoutPersonalInfo.txtDateOfBirth.text = if(data.user?.job_seeker?.birthDate!=null) data.user?.job_seeker?.birthDate else ""
            binding.layoutPersonalInfo.txtNumberOfDependants.text =
                if(data.user?.job_seeker?.Dependents!=null)  data.user?.job_seeker?.Dependents.toString() else ""


            maritalStatus = when (data.user?.job_seeker?.marital_status) {
                1 -> {
                    getString(R.string.married)
                }
                2 -> {
                    getString(R.string.unmarried)
                }
                3 -> {
                    getString(R.string.absolute)
                }
                else -> {
                    getString(R.string.widower)
                }
            }
            binding.layoutPersonalInfo.txtSocialStatus.text = maritalStatus
            binding.layoutPersonalInfo.txtNumberOfExp.text =
              if(data.user?.job_seeker?.experience!=null)  data.user?.job_seeker?.experience.toString() else ""
            binding.layoutPersonalInfo.txtMinimumSalary.text =
              if(data.user?.job_seeker?.salary != null)  data.user?.job_seeker?.salary.toString() else ""


            binding.layoutPersonalInfo.edtAddress.setText(data.user?.job_seeker?.address)
            binding.layoutPersonalInfo.edtCity.setText(data.user?.job_seeker?.city_seeker?.name_ar)
            binding.layoutPersonalInfo.edtType.setText(gender, false)
            binding.layoutPersonalInfo.edtDateOfBirth.setText(data.user?.job_seeker?.birthDate)
            binding.layoutPersonalInfo.edtFunctionalStatus.setText(data.user?.job_seeker?.Dependents.toString())
            binding.layoutPersonalInfo.edtSocialStatus.setText(maritalStatus, false)
            binding.layoutPersonalInfo.edtNumberOfYearsOfExperience.setText(data.user?.job_seeker?.experience.toString())
            binding.layoutPersonalInfo.edtMinimumSalary.setText(data.user?.job_seeker?.salary.toString())


        }

        /** Short Description */
        binding.layoutShortDescription.txtShortDescription.text =
            data.user?.job_seeker?.career_objective
        binding.layoutShortDescription.edtShortDesc.setText(data.user?.job_seeker?.career_objective)


        /**Communication Data Set*/
        binding.layoutCommunicationData.txtEmail.text = data.user?.email
        binding.layoutCommunicationData.txtMobileNumber.text = data.user?.job_seeker?.phone

        binding.layoutCommunicationData.edtEmail.setText(data.user?.email)
        binding.layoutCommunicationData.edtMobileNumber.setText(data.user?.job_seeker?.phone)


        /** Education List Data */
        if (data.user?.job_seeker?.educations!!.isNotEmpty()) {
            binding.layoutEducation.rvEducationList.visibility = View.VISIBLE
            educationListAdapter =
                EducationListAdapter(
                    requireContext(), this, data.user?.job_seeker?.educations!!,
                    prefManager
                )
            binding.layoutEducation.rvEducationList.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = educationListAdapter
            }
            /*binding.layoutEducation.rvEducationList.addItemDecoration(
                DividerItemDecoration(
                    requireContext(), LinearLayoutManager.VERTICAL
                ).apply {
                    setDrawable(resources.getDrawable(R.drawable.divider_bg))
                }
            )*/
        } else {
            binding.layoutEducation.rvEducationList.visibility = View.GONE
        }


        if (data.user?.experience!!.isNotEmpty()) {
            binding.layoutExperiences.rvExperiencesList.visibility = View.VISIBLE
            experienceListAdapter =
                ExperienceListAdapter(requireContext(), this, data.user?.experience!!, prefManager)
            binding.layoutExperiences.rvExperiencesList.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = experienceListAdapter
            }
            /*binding.layoutExperiences.rvExperiencesList.addItemDecoration(
                DividerItemDecoration(
                    requireContext(), LinearLayoutManager.VERTICAL
                ).apply {
                    setDrawable(resources.getDrawable(R.drawable.divider_bg))
                }
            )*/
        } else {
            binding.layoutExperiences.rvExperiencesList.visibility = View.GONE
        }

        if (data.user?.skills!!.isNotEmpty()) {
            binding.layoutSkill.rvSkillsList.visibility = View.VISIBLE
            skillListAdapter =
                SkillListAdapter(requireContext(), this, data.user?.skills)
            binding.layoutSkill.rvSkillsList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                adapter = skillListAdapter
            }
        } else {
            binding.layoutSkill.rvSkillsList.visibility = View.GONE
        }

        if (data.user?.interest!!.isNotEmpty()) {
            binding.layoutInterests.rvInterestsList.visibility = View.VISIBLE
            interestListAdapter =
                InterestListAdapter(requireContext(), this, data.user?.interest)
            binding.layoutInterests.rvInterestsList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                adapter = interestListAdapter
            }
        } else {
            binding.layoutInterests.rvInterestsList.visibility = View.GONE
        }

        if (data.user?.courses!!.isNotEmpty()) {
            binding.layoutEducationalCourses.rvEducationalCoursesList.visibility = View.VISIBLE
            educationCoursesListAdapter =
                EducationCoursesListAdapter(
                    requireContext(),
                    this,
                    data.user?.courses!!,
                    prefManager
                )
            binding.layoutEducationalCourses.rvEducationalCoursesList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                adapter = educationCoursesListAdapter
            }
            /*binding.layoutEducationalCourses.rvEducationalCoursesList.addItemDecoration(
                    DividerItemDecoration(
                        requireContext(), LinearLayoutManager.VERTICAL
                    ).apply {
                        setDrawable(resources.getDrawable(R.drawable.divider_bg))
                    }
                )*/
        } else {
            binding.layoutEducationalCourses.rvEducationalCoursesList.visibility = View.GONE
        }

        if (data.user?.certificates!!.isNotEmpty()) {
            binding.layoutCertificates.rvCertificatesList.visibility = View.VISIBLE
            certificatesListAdapter =
                CertificatesListAdapter(
                    requireContext(),
                    this,
                    data.user?.certificates!!,
                    prefManager
                )
            binding.layoutCertificates.rvCertificatesList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                adapter = certificatesListAdapter
            }
            /*binding.layoutCertificates.rvCertificatesList.addItemDecoration(
                DividerItemDecoration(
                    requireContext(), LinearLayoutManager.VERTICAL
                ).apply {
                    setDrawable(resources.getDrawable(R.drawable.divider_bg))
                }
            )*/
        } else {
            binding.layoutCertificates.rvCertificatesList.visibility = View.GONE
        }

        if (data.user?.projects!!.isNotEmpty()) {
            binding.layoutProjects.rvProjectsList.visibility = View.VISIBLE
            projectsListAdapter =
                ProjectsListAdapter(requireContext(), this, data.user?.projects!!, prefManager)
            binding.layoutProjects.rvProjectsList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                adapter = projectsListAdapter
            }
            /*binding.layoutProjects.rvProjectsList.addItemDecoration(
                DividerItemDecoration(
                    requireContext(), LinearLayoutManager.VERTICAL
                ).apply {
                    setDrawable(resources.getDrawable(R.drawable.divider_bg))
                }
            )*/
        } else {
            binding.layoutProjects.rvProjectsList.visibility = View.GONE
        }

        if (data.user?.langs!!.isNotEmpty()) {
            binding.layoutLanguages.rvLanguagesList.visibility = View.VISIBLE
//            binding.layoutLanguages.txtNoRecord.visibility = View.GONE
            languageListAdapter =
                LanguageListAdapter(requireContext(), this, data.user?.langs!!)
            binding.layoutLanguages.rvLanguagesList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                adapter = languageListAdapter
            }
        } else {
            binding.layoutLanguages.rvLanguagesList.visibility = View.GONE
//            binding.layoutLanguages.txtNoRecord.visibility = View.VISIBLE
        }


        binding.layoutSocialNetwork.edtFb.setText(data.user?.job_seeker?.fb ?: "")
        binding.layoutSocialNetwork.edtLink.setText(data.user?.job_seeker?.linkedIn ?: "")
        binding.layoutSocialNetwork.edtTwitt.setText(data.user?.job_seeker?.twitter ?: "")
        binding.layoutSocialNetwork.edtYoutube.setText(data.user?.job_seeker?.youtube ?: "")
        binding.layoutSocialNetwork.edtInsta.setText(data.user?.job_seeker?.instagram ?: "")
        binding.layoutSocialNetwork.edtWeb.setText(data.user?.job_seeker?.website ?: "")

    }


    private fun updateUi() {
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val companySizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayJobStatusSize
                )

            (binding.layoutBasicData.edtFunctionalStatus as? AutoCompleteTextView)?.setAdapter(
                companySizeAdapter
            )
            binding.layoutBasicData.edtFunctionalStatus.setOnItemClickListener { _, _, position, _ ->
                job_status = position + 1
                Log.d(mTAG, "updateUi:job_status $job_status")
            }
            binding.layoutBasicData.edtFunctionalStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        } else {
            val companySizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayJobStatusArSize
                )
            (binding.layoutBasicData.edtFunctionalStatus as? AutoCompleteTextView)?.setAdapter(
                companySizeAdapter
            )
            binding.layoutBasicData.edtFunctionalStatus.setOnItemClickListener { _, _, position, _ ->
                job_status = position + 1
                Log.d(mTAG, "updateUi:job_status $job_status")
            }
            binding.layoutBasicData.edtFunctionalStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        }

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayTypeSize
                )
            (binding.layoutPersonalInfo.edtType as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.layoutPersonalInfo.edtType.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        } else {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayTypeArSize
                )
            (binding.layoutPersonalInfo.edtType as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.layoutPersonalInfo.edtType.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        }



        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arraySocialStatusSize
                )
            (binding.layoutPersonalInfo.edtSocialStatus as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.layoutPersonalInfo.edtSocialStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        } else {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arraySocialStatusArSize
                )
            (binding.layoutPersonalInfo.edtSocialStatus as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.layoutPersonalInfo.edtSocialStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        }

        /** Year drop down */
        val years: ArrayList<String> = ArrayList()
        val thisYear = Calendar.getInstance()[Calendar.YEAR]
        for (i in 1900..thisYear) {
            years.add(i.toString())
        }
        val yearAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                years
            )
        years.reverse()
        (binding.layoutEducation.edtTheYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        binding.layoutEducation.edtTheYear.dropDownWidth = WindowManager.LayoutParams.MATCH_PARENT

        /** Experience screen */
        (binding.layoutExperiences.edtFromYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        binding.layoutExperiences.edtFromYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT

        (binding.layoutExperiences.edtToYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        binding.layoutExperiences.edtToYear.dropDownWidth = WindowManager.LayoutParams.WRAP_CONTENT

        (binding.layoutCertificates.edtReleaseYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        binding.layoutCertificates.edtReleaseYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT

        (binding.layoutProjects.edtFromYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        binding.layoutProjects.edtFromYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT

        val monthAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                monthArrayEn
            )
        (binding.layoutExperiences.edtFromMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        binding.layoutExperiences.edtFromMonth.setOnItemClickListener { _, _, position, _ ->
            sMonthId = position + 1
            Log.d(mTAG, "updateUi:sMonthId $sMonthId")
        }
        binding.layoutExperiences.edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT

        (binding.layoutExperiences.edtToMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        binding.layoutExperiences.edtToMonth.setOnItemClickListener { _, _, position, _ ->
            eMonthId = position + 1
            Log.d(mTAG, "updateUi:eMonthId $eMonthId")
        }
        binding.layoutExperiences.edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT


        (binding.layoutCertificates.edtReleaseMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        binding.layoutCertificates.edtReleaseMonth.setOnItemClickListener { _, _, position, _ ->
            certificateMonth = position + 1
            Log.d(mTAG, "updateUi:certificateMonth $certificateMonth")
        }
        binding.layoutCertificates.edtReleaseMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT


        (binding.layoutProjects.edtFromMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        binding.layoutProjects.edtFromMonth.setOnItemClickListener { _, _, position, _ ->
            projectMonth = position + 1
            Log.d(mTAG, "updateUi:certificateMonth $certificateMonth")
        }
        binding.layoutProjects.edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT

    }


    private fun isBasicDataValid(): Boolean {
        binding.layoutBasicData.inputName.isErrorEnabled = false
        binding.layoutBasicData.inputTheJobTitle.isErrorEnabled = false
        binding.layoutBasicData.inputSpecialization.isErrorEnabled = false
        binding.layoutBasicData.inputCareerRole.isErrorEnabled = false
        binding.layoutBasicData.inputFunctionalStatus.isErrorEnabled = false

        when {
            binding.layoutBasicData.edtName.text.toString().isEmpty() -> {
                binding.layoutBasicData.inputName.error =
                    getString(R.string.error_enter_full_name)
                binding.layoutBasicData.inputName.requestFocus()
                return false
            }
            binding.layoutBasicData.edtTheJobTitle.text.toString().isEmpty() -> {
                binding.layoutBasicData.inputTheJobTitle.error =
                    getString(R.string.error_enter_job_title)
                binding.layoutBasicData.inputTheJobTitle.requestFocus()
                return false
            }
            binding.layoutBasicData.edtSpecialization.text.toString().isEmpty() -> {
                binding.layoutBasicData.inputSpecialization.error =
                    getString(R.string.error_enter_specialization)
                binding.layoutBasicData.inputSpecialization.requestFocus()
                return false
            }

            binding.layoutBasicData.edtCareerRole.text.toString().isEmpty() -> {
                binding.layoutBasicData.inputCareerRole.error =
                    getString(R.string.error_enter_career_role)
                binding.layoutBasicData.inputCareerRole.requestFocus()
                return false
            }
            binding.layoutBasicData.edtFunctionalStatus.text.toString().isEmpty() -> {
                binding.layoutBasicData.inputFunctionalStatus.error =
                    getString(R.string.error_enter_functional_status)
                binding.layoutBasicData.inputFunctionalStatus.requestFocus()
                return false
            }
        }

        return true
    }


    private fun isCommunicationDataValid(): Boolean {
        binding.layoutCommunicationData.inputMobileNumber.isErrorEnabled = false
        binding.layoutCommunicationData.inputEmail.isErrorEnabled = false

        when {
            binding.layoutCommunicationData.edtMobileNumber.text.toString().isEmpty() -> {
                binding.layoutCommunicationData.inputMobileNumber.error =
                    getString(R.string.error_enter_mobile_number)
                binding.layoutCommunicationData.inputMobileNumber.requestFocus()
                return false
            }
            binding.layoutCommunicationData.edtEmail.text.toString().isEmpty() -> {
                binding.layoutCommunicationData.inputEmail.error =
                    getString(R.string.error_enter_e_mail)
                binding.layoutCommunicationData.inputEmail.requestFocus()
                return false
            }
        }

        return true
    }


    private fun isPersonalInfoDataValid(): Boolean {
        binding.layoutPersonalInfo.inputAddress.isErrorEnabled = false
        binding.layoutPersonalInfo.inputCity.isErrorEnabled = false
        binding.layoutPersonalInfo.inputType.isErrorEnabled = false
        binding.layoutPersonalInfo.inputDateOfBirth.isErrorEnabled = false
        binding.layoutPersonalInfo.inputFunctionalStatus.isErrorEnabled = false
        binding.layoutPersonalInfo.inputSocialStatus.isErrorEnabled = false
        binding.layoutPersonalInfo.inputNumberOfYearsOfExperience.isErrorEnabled = false
        binding.layoutPersonalInfo.inputMinimumSalary.isErrorEnabled = false

        when {
            binding.layoutPersonalInfo.edtAddress.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputAddress.error =
                    getString(R.string.error_enter_address)
                binding.layoutPersonalInfo.inputAddress.requestFocus()
                return false
            }
            binding.layoutPersonalInfo.edtCity.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputCity.error =
                    getString(R.string.error_enter_city)
                binding.layoutPersonalInfo.inputCity.requestFocus()
                return false
            }
            binding.layoutPersonalInfo.edtType.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputType.error =
                    getString(R.string.error_enter_type)
                binding.layoutPersonalInfo.inputType.requestFocus()
                return false
            }

            binding.layoutPersonalInfo.edtDateOfBirth.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputDateOfBirth.error =
                    getString(R.string.error_enter_date_of_birth)
                binding.layoutPersonalInfo.inputDateOfBirth.requestFocus()
                return false
            }
            binding.layoutPersonalInfo.edtFunctionalStatus.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputFunctionalStatus.error =
                    getString(R.string.error_enter_number_of_dependants)
                binding.layoutPersonalInfo.inputFunctionalStatus.requestFocus()
                return false
            }
            binding.layoutPersonalInfo.edtSocialStatus.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputSocialStatus.error =
                    getString(R.string.error_enter_social_status)
                binding.layoutPersonalInfo.inputSocialStatus.requestFocus()
                return false
            }
            binding.layoutPersonalInfo.edtNumberOfYearsOfExperience.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputNumberOfYearsOfExperience.error =
                    getString(R.string.error_enter_number_of_years_of_experience)
                binding.layoutPersonalInfo.inputNumberOfYearsOfExperience.requestFocus()
                return false
            }
            binding.layoutPersonalInfo.edtMinimumSalary.text.toString().isEmpty() -> {
                binding.layoutPersonalInfo.inputMinimumSalary.error =
                    getString(R.string.error_enter_minimum_salary)
                binding.layoutPersonalInfo.inputMinimumSalary.requestFocus()
                return false
            }
        }

        return true
    }

    private fun isEducationDataValid(): Boolean {
        binding.layoutEducation.inputEducationalInstitute.isErrorEnabled = false
        binding.layoutEducation.inputAppreciation.isErrorEnabled = false
        binding.layoutEducation.inputTheField.isErrorEnabled = false
        binding.layoutEducation.inputDegree.isErrorEnabled = false
        binding.layoutEducation.inputTheYear.isErrorEnabled = false
        binding.layoutEducation.inputTheDesc.isErrorEnabled = false
        when {
            binding.layoutEducation.edtEducationalInstitute.text.toString().isEmpty() -> {
                binding.layoutEducation.inputEducationalInstitute.error =
                    getString(R.string.error_enter_education_institute)
                binding.layoutEducation.inputEducationalInstitute.requestFocus()
                return false
            }
            binding.layoutEducation.edtAppreciation.text.toString().isEmpty() -> {
                binding.layoutEducation.inputAppreciation.error =
                    getString(R.string.error_enter_appreciation)
                binding.layoutEducation.inputAppreciation.requestFocus()
                return false
            }
            binding.layoutEducation.edtTheField.text.toString().isEmpty() -> {
                binding.layoutEducation.inputTheField.error =
                    getString(R.string.error_enter_specialisation_field)
                binding.layoutEducation.inputTheField.requestFocus()
                return false
            }
            degreeId == "" -> {
                binding.layoutEducation.inputDegree.error =
                    getString(R.string.error_please_select_degree)
                binding.layoutEducation.inputDegree.requestFocus()
                return false
            }
            binding.layoutEducation.edtTheYear.text.toString().isEmpty() -> {
                binding.layoutEducation.inputTheYear.error =
                    getString(R.string.error_select_education_year)
                binding.layoutEducation.inputTheYear.requestFocus()
                return false
            }
            binding.layoutEducation.edtTheDesc.text.toString().isEmpty() -> {
                binding.layoutEducation.inputTheDesc.error =
                    getString(R.string.error_please_enter_description)
                binding.layoutEducation.inputTheDesc.requestFocus()
                return false
            }
        }
        return true
    }

    private fun isExperienceValid(): Boolean {
        binding.layoutExperiences.inputJobTitle.isErrorEnabled = false
        binding.layoutExperiences.inputCompanyName.isErrorEnabled = false
        binding.layoutExperiences.inputCompanyLocation.isErrorEnabled = false
        binding.layoutExperiences.inputFromMonth.isErrorEnabled = false
        binding.layoutExperiences.inputFromYear.isErrorEnabled = false
        binding.layoutExperiences.inputToMonth.isErrorEnabled = false
        binding.layoutExperiences.inputToYear.isErrorEnabled = false
        binding.layoutExperiences.inputDesc.isErrorEnabled = false

        when {
            binding.layoutExperiences.edtJobTitle.text.toString().isEmpty() -> {
                binding.layoutExperiences.inputJobTitle.error =
                    getString(R.string.error_enter_job_title)
                binding.layoutExperiences.inputJobTitle.requestFocus()
                return false
            }
            binding.layoutExperiences.edtCompanyName.text.toString().isEmpty() -> {
                binding.layoutExperiences.inputCompanyName.error =
                    getString(R.string.error_company_name)
                binding.layoutExperiences.inputCompanyName.requestFocus()
                return false
            }
            binding.layoutExperiences.edtCompanyLocation.text.toString().isEmpty() -> {
                binding.layoutExperiences.inputCompanyLocation.error =
                    getString(R.string.error_company_location)
                binding.layoutExperiences.inputCompanyLocation.requestFocus()
                return false
            }
            sMonthId == -1 -> {
                binding.layoutExperiences.inputFromMonth.error =
                    getString(R.string.error_starting_month)
                binding.layoutExperiences.inputFromMonth.requestFocus()
                return false
            }
            binding.layoutExperiences.edtFromYear.text.toString().isEmpty() -> {
                binding.layoutExperiences.inputFromYear.error =
                    getString(R.string.error_starting_year)
                binding.layoutExperiences.inputFromYear.requestFocus()
                return false
            }

            !binding.layoutExperiences.cbUntilNow.isChecked -> {
                when {
                    eMonthId == -1 -> {
                        binding.layoutExperiences.inputToMonth.error =
                            getString(R.string.error_end_month)
                        binding.layoutExperiences.inputToMonth.requestFocus()
                        return false
                    }
                    binding.layoutExperiences.edtToYear.text.toString().isEmpty() -> {
                        binding.layoutExperiences.inputToYear.error =
                            getString(R.string.error_end_year)
                        binding.layoutExperiences.inputToYear.requestFocus()
                        return false
                    }
                }
            }

        }
        return true
    }

    private fun isCoursesValid(): Boolean {
        binding.layoutEducationalCourses.inputAddress.isErrorEnabled = false
        binding.layoutEducationalCourses.inputCenterName.isErrorEnabled = false
        binding.layoutEducationalCourses.inputDateOfStarting.isErrorEnabled = false
        binding.layoutEducationalCourses.inputNumberOfHours.isErrorEnabled = false
        binding.layoutEducationalCourses.inputCourseDesc.isErrorEnabled = false

        when {
            binding.layoutEducationalCourses.edtAddress.text.toString().isEmpty() -> {
                binding.layoutEducationalCourses.inputAddress.error =
                    getString(R.string.error_enter_address)
                binding.layoutEducationalCourses.inputAddress.requestFocus()
                return false
            }
            binding.layoutEducationalCourses.edtCenterName.text.toString().isEmpty() -> {
                binding.layoutEducationalCourses.inputCenterName.error =
                    getString(R.string.error_enter_center_name)
                binding.layoutEducationalCourses.inputCenterName.requestFocus()
                return false
            }
            binding.layoutEducationalCourses.edtDateOfStarting.text.toString().isEmpty() -> {
                binding.layoutEducationalCourses.inputDateOfStarting.error =
                    getString(R.string.error_date_of_course_starting)
                binding.layoutEducationalCourses.inputDateOfStarting.requestFocus()
                return false
            }
            binding.layoutEducationalCourses.edtNumberOfHours.text.toString().isEmpty() -> {
                binding.layoutEducationalCourses.inputNumberOfHours.error =
                    getString(R.string.error_enter_number_of_hours)
                binding.layoutEducationalCourses.inputNumberOfHours.requestFocus()
                return false
            }
            binding.layoutEducationalCourses.edtCourseDesc.text.toString().isEmpty() -> {
                binding.layoutEducationalCourses.inputCourseDesc.error =
                    getString(R.string.error_please_enter_description)
                binding.layoutEducationalCourses.inputCourseDesc.requestFocus()
                return false
            }
        }

        return true
    }


    private fun isCertificateDataValid(): Boolean {

        binding.layoutCertificates.inputCertificateName.isErrorEnabled = false
        binding.layoutCertificates.inputIssuer.isErrorEnabled = false
        binding.layoutCertificates.inputReleaseMonth.isErrorEnabled = false
        binding.layoutCertificates.inputReleaseYear.isErrorEnabled = false
        binding.layoutCertificates.inputCertificateDesc.isErrorEnabled = false

        when {
            binding.layoutCertificates.edtCertificateName.text.toString().isEmpty() -> {
                binding.layoutCertificates.inputCertificateName.error =
                    getString(R.string.error_enter_certificate_name)
                binding.layoutCertificates.inputCertificateName.requestFocus()
                return false
            }
            binding.layoutCertificates.edtIssuer.text.toString().isEmpty() -> {
                binding.layoutCertificates.inputIssuer.error =
                    getString(R.string.error_enter_issuer)
                binding.layoutCertificates.inputIssuer.requestFocus()
                return false
            }
            binding.layoutCertificates.edtReleaseMonth.text.toString().isEmpty() -> {
                binding.layoutCertificates.inputReleaseMonth.error =
                    getString(R.string.error_select_month)
                binding.layoutCertificates.inputReleaseMonth.requestFocus()
                return false
            }

            binding.layoutCertificates.edtReleaseYear.text.toString().isEmpty() -> {
                binding.layoutCertificates.inputReleaseYear.error =
                    getString(R.string.error_select_year)
                binding.layoutCertificates.inputReleaseYear.requestFocus()
                return false
            }
            binding.layoutCertificates.edtCertificateDesc.text.toString().isEmpty() -> {
                binding.layoutCertificates.inputCertificateDesc.error =
                    getString(R.string.error_please_enter_description)
                binding.layoutCertificates.inputCertificateDesc.requestFocus()
                return false
            }
        }
        return true
    }

    private fun isProjectDataValid(): Boolean {
        binding.layoutProjects.inputTitle.isErrorEnabled = false
        binding.layoutProjects.inputFromMonth.isErrorEnabled = false
        binding.layoutProjects.inputFromYear.isErrorEnabled = false
        binding.layoutProjects.inputProjectDesc.isErrorEnabled = false
        when {
            binding.layoutProjects.edtTitle.text.toString().isEmpty() -> {
                binding.layoutProjects.inputTitle.error =
                    getString(R.string.error_enter_title)
                binding.layoutProjects.inputTitle.requestFocus()
                return false
            }
            binding.layoutProjects.edtFromMonth.text.toString().isEmpty() -> {
                binding.layoutProjects.inputFromMonth.error =
                    getString(R.string.error_select_month)
                binding.layoutProjects.inputFromMonth.requestFocus()
                return false
            }
            binding.layoutProjects.edtFromYear.text.toString().isEmpty() -> {
                binding.layoutProjects.inputFromYear.error =
                    getString(R.string.error_select_year)
                binding.layoutProjects.inputFromYear.requestFocus()
                return false
            }
            binding.layoutProjects.edtProjectDesc.text.toString().isEmpty() -> {
                binding.layoutProjects.inputProjectDesc.error =
                    getString(R.string.error_please_enter_description)
                binding.layoutProjects.inputProjectDesc.requestFocus()
                return false
            }
        }
        return true
    }

    private fun isLanguageDataValid(): Boolean {
        binding.layoutLanguages.inputLanguage.isErrorEnabled = false
        binding.layoutLanguages.inputProficiency.isErrorEnabled = false
        when {
            binding.layoutLanguages.edtLanguage.text.toString().isEmpty() -> {
                binding.layoutLanguages.inputLanguage.error =
                    getString(R.string.error_enter_lanugage)
                binding.layoutLanguages.inputLanguage.requestFocus()
                return false
            }
            binding.layoutLanguages.edtProficiency.text.toString().isEmpty() -> {
                binding.layoutLanguages.inputProficiency.error =
                    getString(R.string.error_select_proficiency)
                binding.layoutLanguages.inputProficiency.requestFocus()
                return false
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            /*R.id.viewBasicData -> {
                basicDataOpenClose(basicDataOpen)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewPersonalInfo -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(personalInfoOpen)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewCommunicationData -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(communicationOpen)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }

            R.id.viewShortDescription -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(shortDescriptionOpen)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewBioGraphicalFile -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(bioGraphicalDataEditOpen)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }

            R.id.viewSkills -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(skillOpen)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewEducation -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                educationOpenClose(educationOpen)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewExperiences -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(experiencesOpen)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }

            R.id.viewInterests -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(interestEditOpen)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)

            }
            R.id.viewEducationalCourses -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(educationalCoursesOpen)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewCertificates -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(certificatesOpen)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(true)
            }

            R.id.viewProjects -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(projectsOpen)
                languageOpenClose(true)
                socialOpenClose(true)
            }
            R.id.viewLanguages -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(languageOpen)
                socialOpenClose(true)
            }
            R.id.viewSocialNetwork -> {
                basicDataOpenClose(true)
                personalInfoOpenClose(true)
                communicationOpenClose(true)
                shortDescriptionOpenClose(true)
                biographicalFilesOpenClose(true)
                educationOpenClose(true)
                experiencesOpenClose(true)
                skillOpenClose(true)
                interestOpen(true)
                educationalCoursesOpenClose(true)
                certificateOpenClose(true)
                projectOpenClose(true)
                languageOpenClose(true)
                socialOpenClose(socialOpen)
            }*/


            /**Save Personal Info */
            R.id.btnBasicDataSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isBasicDataValid()) {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.BasicDataUpdateEvent(
                                binding.layoutBasicData.edtName.text.toString(),
                                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") binding.layoutBasicData.edtTheJobTitle.text.toString() else binding.layoutBasicData.edtTheJobTitle.text.toString(),
                                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") binding.layoutBasicData.edtTheJobTitle.text.toString() else binding.layoutBasicData.edtTheJobTitle.text.toString(),
                                specializationId!!,
                                jobRoleId!!,
                                if (job_status == -1) 1 else job_status
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnPresonalInfoSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isPersonalInfoDataValid()) {
                        personalInfoEditApiCall()
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnCommunicationDataSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isCommunicationDataValid()) {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.CommunicationDataEdit(
                                binding.layoutCommunicationData.edtEmail.text.toString(),
                                binding.layoutCommunicationData.edtMobileNumber.text.toString()
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnShortDescSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (binding.layoutShortDescription.edtShortDesc.text.toString().isEmpty()) {
                        binding.layoutShortDescription.edtShortDesc.error =
                            getString(R.string.lbl_error_short_desc)
                        binding.layoutShortDescription.edtShortDesc.requestFocus()
                    } else {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.AddEditShortDescription(
                                binding.layoutShortDescription.edtShortDesc.text.toString()
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.clBioGraphicalUpload -> {

            }


            R.id.btnEducationalInstituteSave -> {
//                if (Utils.isConnected(requireContext())) {
//                    if (posEditDelete != -1) {
//                        viewModel.setStateEvent(
//                            EditProfileStateEvent.EditEducationEvent(
//                                binding.layoutEducation.edtEducationalInstitute.text.toString(),
//                                binding.layoutEducation.edtAppreciation.text.toString(),
//                                binding.layoutEducation.edtTheField.text.toString(),
//                                degreeId,
//                                binding.layoutEducation.edtTheYear.text.toString(),
//                                binding.layoutEducation.edtTheDesc.text.toString(),
//                                educationListAdapter.listData[posEditDelete].id.toString()
//                            )
//                        )
//                    } else {
//                        if (isEducationDataValid()) {
//                            viewModel.setStateEvent(
//                                EditProfileStateEvent.AddEducationEvent(
//                                    binding.layoutEducation.edtEducationalInstitute.text.toString(),
//                                    binding.layoutEducation.edtAppreciation.text.toString(),
//                                    binding.layoutEducation.edtTheField.text.toString(),
//                                    degreeId,
//                                    binding.layoutEducation.edtTheYear.text.toString(),
//                                    binding.layoutEducation.edtTheDesc.text.toString(),
//                                    profileResponseBean.user.id!!
//                                )
//                            )
//                        }
//                    }
//                } else {
//                    view?.let { Utils.showSnackBar(it, requireContext()) }
//                }
            }
            R.id.btnExperienceSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isExperienceValid()) {
                        if (binding.layoutExperiences.edtDesc.text.toString().isEmpty()) {
                            binding.layoutExperiences.inputDesc.error =
                                getString(R.string.error_please_enter_description)
                            binding.layoutExperiences.inputDesc.requestFocus()
                            return
                        }
                        if (expId == -1) {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddExperienceEvent(
                                    binding.layoutExperiences.edtJobTitle.text.toString(),
                                    binding.layoutExperiences.edtCompanyName.text.toString(),
                                    binding.layoutExperiences.edtCompanyLocation.text.toString(),
                                    sMonthId.toString(),
                                    binding.layoutExperiences.edtFromYear.text.toString(),
                                    if (binding.layoutExperiences.cbUntilNow.isChecked) "" else eMonthId.toString(),
                                    if (binding.layoutExperiences.cbUntilNow.isChecked) "" else binding.layoutExperiences.edtToYear.text.toString(),
                                    if (binding.layoutExperiences.cbUntilNow.isChecked) "Present" else "",
                                    binding.layoutExperiences.edtDesc.text.toString(),
                                )
                            )
                        } else {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditExperienceEvent(
                                    binding.layoutExperiences.edtJobTitle.text.toString(),
                                    binding.layoutExperiences.edtCompanyName.text.toString(),
                                    binding.layoutExperiences.edtCompanyLocation.text.toString(),
                                    sMonthId.toString(),
                                    binding.layoutExperiences.edtFromYear.text.toString(),
                                    if (binding.layoutExperiences.cbUntilNow.isChecked) "" else eMonthId.toString(),
                                    if (binding.layoutExperiences.cbUntilNow.isChecked) "" else binding.layoutExperiences.edtToYear.text.toString(),
                                    if (binding.layoutExperiences.cbUntilNow.isChecked) "Present" else "",
                                    binding.layoutExperiences.edtDesc.text.toString(),
                                    expId
                                )
                            )
                        }
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnSkillSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (binding.layoutSkill.edtSkill.text.toString().isEmpty()) {
                        binding.layoutSkill.edtSkill.requestFocus()
                        binding.layoutSkill.edtSkill.error =
                            getString(R.string.error_enter_skill)
                        return
                    } else {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.AddSkillEvent(
                                binding.layoutSkill.edtSkill.text.toString()
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnInterestsSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (binding.layoutInterests.edtInterests.text.toString().isEmpty()) {
                        binding.layoutInterests.edtInterests.requestFocus()
                        binding.layoutInterests.edtInterests.error =
                            getString(R.string.error_enter_interest)
                        return
                    } else {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.AddInterestEvent(
                                binding.layoutInterests.edtInterests.text.toString()
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnCoursesSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isCoursesValid()) {
                        if (course_id == -1) {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddCoursesEvent(
                                    binding.layoutEducationalCourses.edtAddress.text.toString(),
                                    binding.layoutEducationalCourses.edtCenterName.text.toString(),
                                    binding.layoutEducationalCourses.edtDateOfStarting.text.toString(),
                                    binding.layoutEducationalCourses.edtNumberOfHours.text.toString(),
                                    binding.layoutEducationalCourses.edtCourseDesc.text.toString(),
                                    logo
                                )
                            )
                        } else {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditCoursesEvent(
                                    binding.layoutEducationalCourses.edtAddress.text.toString(),
                                    binding.layoutEducationalCourses.edtCenterName.text.toString(),
                                    binding.layoutEducationalCourses.edtDateOfStarting.text.toString(),
                                    binding.layoutEducationalCourses.edtNumberOfHours.text.toString(),
                                    binding.layoutEducationalCourses.edtCourseDesc.text.toString(),
                                    course_id,
                                    logo
                                )
                            )
                        }
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnCertificateSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isCertificateDataValid()) {
                        if (certificateId == -1) {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddCertificateEvent(
                                    binding.layoutCertificates.edtCertificateName.text.toString(),
                                    binding.layoutCertificates.edtIssuer.text.toString(),
                                    certificateMonth.toString(),
                                    binding.layoutCertificates.edtReleaseYear.text.toString(),
                                    binding.layoutCertificates.edtCertificateDesc.text.toString(),
                                )
                            )
                        } else {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditCertificateEvent(
                                    binding.layoutCertificates.edtCertificateName.text.toString(),
                                    binding.layoutCertificates.edtIssuer.text.toString(),
                                    certificateMonth.toString(),
                                    binding.layoutCertificates.edtReleaseYear.text.toString(),
                                    binding.layoutCertificates.edtCertificateDesc.text.toString(),
                                    certificateId
                                )
                            )
                        }
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnProjectSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isProjectDataValid()) {
                        if (projectId == -1) {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddProjectEvent(
                                    binding.layoutProjects.edtTitle.text.toString(),
                                    projectMonth.toString(),
                                    binding.layoutProjects.edtFromYear.text.toString(),
                                    binding.layoutProjects.edtProjectDesc.text.toString(),
                                )
                            )
                        } else {
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditProjectEvent(
                                    binding.layoutProjects.edtTitle.text.toString(),
                                    projectMonth.toString(),
                                    binding.layoutProjects.edtFromYear.text.toString(),
                                    binding.layoutProjects.edtProjectDesc.text.toString(),
                                    projectId
                                )
                            )
                        }
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnLanguageSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isLanguageDataValid()) {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.SaveLanguageEvent(
                                binding.layoutLanguages.edtLanguage.text.toString(),
                                binding.layoutLanguages.edtProficiency.text.toString()
                            )
                        )
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.btnSocialSave -> {
                if (Utils.isConnected(requireContext())) {
                    viewModel.setStateEvent(
                        EditProfileStateEvent.SocialUpdateEvent(
                            binding.layoutSocialNetwork.edtFb.text.toString(),
                            binding.layoutSocialNetwork.edtTwitt.text.toString(),
                            binding.layoutSocialNetwork.edtLink.text.toString(),
                            binding.layoutSocialNetwork.edtWeb.text.toString(),
                            binding.layoutSocialNetwork.edtYoutube.text.toString(),
                            binding.layoutSocialNetwork.edtInsta.text.toString()
                        )
                    )
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
            R.id.edtDateOfBirth -> {
                birthDatePicker()
            }
            R.id.edtDateOfStarting -> {
                courseDatePicker()
            }

            R.id.btnBasicDataCancel -> {
                binding.layoutBasicData.clEditBasicData.visibility = View.GONE
                binding.layoutBasicData.clBasicData.visibility = View.VISIBLE
            }
            R.id.btnPresonalInfoCancel -> {
                binding.layoutPersonalInfo.clEditPersonalInfo.visibility = View.GONE
                binding.layoutPersonalInfo.clPersonalInfo.visibility = View.VISIBLE
            }
            R.id.imgCommunicationDataModification,
            R.id.btnCommunicationDataModification -> {
                val bundle = Bundle()
                bundle.putSerializable(Constant.PROFILE_DATA, profileResponseBean)
                navController.navigate(
                    R.id.editProfileCommunicationFragment,
                    bundle
                )
                /*binding.layoutCommunicationData.clEditCommunicationData.visibility = View.VISIBLE
                binding.layoutCommunicationData.clCommunicationData.visibility = View.GONE*/
            }
            R.id.imgEditBasicData,
            R.id.btnBasicDataModification -> {
                val bundle = Bundle()
                bundle.putSerializable(Constant.PROFILE_DATA, profileResponseBean)
                navController.navigate(
                    R.id.editProfileFragment,
                    bundle
                )
//                binding.layoutBasicData.clEditBasicData.visibility = View.VISIBLE
//                binding.layoutBasicData.clBasicData.visibility = View.GONE
            }
            R.id.imgPersonalInfoModification,
            R.id.btnPersonalInfoModification -> {
                val bundle = Bundle()
                bundle.putSerializable(Constant.PROFILE_DATA, profileResponseBean)
                navController.navigate(
                    R.id.editProfilePersonalInfoFragment,
                    bundle
                )
                /*binding.layoutPersonalInfo.clEditPersonalInfo.visibility = View.VISIBLE
                binding.layoutPersonalInfo.clPersonalInfo.visibility = View.GONE*/
            }
            R.id.imgShortDescModification,
            R.id.btnShortDescModification -> {
                val bundle = Bundle()
                bundle.putSerializable(Constant.PROFILE_DATA, profileResponseBean)
                navController.navigate(
                    R.id.editProfileShortDescriptionFragment,
                    bundle
                )

//                binding.layoutShortDescription.clEditShortDesc.visibility = View.VISIBLE
//                binding.layoutShortDescription.clShortDesc.visibility = View.GONE
            }
            R.id.imgBioGraphicalModification,
            R.id.btnBioGraphicalModification -> {
                forBiographicalUpload()
                /*val bundle = Bundle()
                bundle.putSerializable(Constant.PROFILE_DATA,profileResponseBean)
                navController.navigate(
                    R.id.editProfileShortDescriptionFragment,
                    bundle)*/
//
//                binding.layoutBioGraphicalFile.clEditBioGraphicalData.visibility = View.VISIBLE
//                binding.layoutBioGraphicalFile.clBioGraphicalData.visibility = View.GONE
            }
//            R.id.imgEducationInstituteModification,
            R.id.btnEducationInstituteModification -> {
//                binding.layoutEducation.clAddEditEducation.visibility = View.VISIBLE
//                binding.layoutEducation.clEducation.visibility = View.GONE
//                binding.layoutEducation.edtEducationalInstitute.setText("")
//                binding.layoutEducation.edtAppreciation.setText("")
//                binding.layoutEducation.edtTheField.setText("")
//                binding.layoutEducation.edtDegree.setText("")
//                binding.layoutEducation.edtTheYear.setText("")
//                binding.layoutEducation.edtTheDesc.setText("")
//                posEditDelete = -1
                posEditEducation = -1
                educationAddEditBottomSheet(posEditEducation)
            }
            R.id.btnExperienceAdd -> {
//                binding.layoutExperiences.clAddEditExperience.visibility = View.VISIBLE
//                binding.layoutExperiences.clExperience.visibility = View.GONE
//                binding.layoutExperiences.edtJobTitle.setText("")
//                binding.layoutExperiences.edtCompanyName.setText("")
//                binding.layoutExperiences.edtCompanyLocation.setText("")
//                binding.layoutExperiences.edtFromMonth.setText("")
//                binding.layoutExperiences.edtFromYear.setText("")
//                binding.layoutExperiences.edtToMonth.setText("")
//                binding.layoutExperiences.edtToYear.setText("")
//                binding.layoutExperiences.edtDesc.setText("")
//                expId = -1
                posEditExperience = -1
                experienceAddEditBottomSheet(posEditExperience)
            }
            R.id.btnSkillAdd -> {
//                binding.layoutSkill.clEditSkill.visibility = View.VISIBLE
//                binding.layoutSkill.clSkill.visibility = View.GONE
                skillAddEditBottomSheet()
            }
            R.id.btnInterestsAdd -> {
                interestAddEditBottomSheet()
//                binding.layoutInterests.clEditInterests.visibility = View.VISIBLE
//                binding.layoutInterests.clInterests.visibility = View.GONE
            }
            R.id.btnCoursesAdd -> {
//                binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.VISIBLE
//                binding.layoutEducationalCourses.clEducationalCourses.visibility = View.GONE
//                binding.layoutEducationalCourses.edtAddress.setText("")
//                binding.layoutEducationalCourses.edtCenterName.setText("")
//                binding.layoutEducationalCourses.edtDateOfStarting.setText("")
//                binding.layoutEducationalCourses.edtNumberOfHours.setText("")
//                binding.layoutEducationalCourses.edtCourseDesc.setText("")
                course_id = -1
                posEditEducationCourses = -1
                educationalCoursesAddEditBottomSheet(posEditEducationCourses)
            }
            R.id.btnCertificateAdd -> {
//                binding.layoutCertificates.clCertificates.visibility = View.GONE
//                binding.layoutCertificates.clEditCertificates.visibility = View.VISIBLE
//                binding.layoutCertificates.edtCertificateName.setText("")
//                binding.layoutCertificates.edtIssuer.setText("")
//                binding.layoutCertificates.edtReleaseMonth.setText("")
//                binding.layoutCertificates.edtReleaseYear.setText("")
//                binding.layoutCertificates.edtCertificateDesc.setText("")
                certificateId = -1
                posEditCertificate = -1
                certificateAddEditBottomSheet(posEditCertificate)
            }
            R.id.btnProjectsAdd -> {
//                binding.layoutProjects.clProject.visibility = View.GONE
//                binding.layoutProjects.clEditProject.visibility = View.VISIBLE
//                binding.layoutProjects.edtTitle.setText("")
//                binding.layoutProjects.edtFromMonth.setText("")
//                binding.layoutProjects.edtFromYear.setText("")
//                binding.layoutProjects.edtProjectDesc.setText("")
                posEditProjects = -1
                projectsAddEditBottomSheet(posEditProjects)

            }
            R.id.btnLanguageAdd -> {
//                binding.layoutLanguages.clLanguages.visibility = View.GONE
//                binding.layoutLanguages.clEditLanguages.visibility = View.VISIBLE
//                binding.layoutLanguages.edtLanguage.setText("")
//                binding.layoutLanguages.edtProficiency.setText("")
                languagesAddEditBottomSheet(-1)
            }
            R.id.btnSocialAdd -> {
//                binding.layoutSocialNetwork.clSocialNetwork.visibility = View.GONE
//                binding.layoutSocialNetwork.clEditSocialNetwork.visibility = View.VISIBLE
                socialAddEditBottomSheet()
            }


            /**Cancel */
            R.id.btnCommunicationDataCancel -> {
                binding.layoutCommunicationData.clEditCommunicationData.visibility = View.GONE
                binding.layoutCommunicationData.clCommunicationData.visibility = View.VISIBLE
            }

            /**Open */
            R.id.btnBioGraphicalSave -> {
//                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url))
//                startActivity(browserIntent)
                getUrlFromIntent(cvUrl)
            }
            R.id.btnShortDescCancel -> {
                binding.layoutShortDescription.clEditShortDesc.visibility = View.GONE
                binding.layoutShortDescription.clShortDesc.visibility = View.VISIBLE
            }
            R.id.btnBioGraphicalCancel -> {
                binding.layoutBioGraphicalFile.clEditBioGraphicalData.visibility = View.GONE
                binding.layoutBioGraphicalFile.clBioGraphicalData.visibility = View.VISIBLE
            }
            R.id.btnEducationalInstituteCancel -> {
                binding.layoutEducation.clAddEditEducation.visibility = View.GONE
                binding.layoutEducation.clEducation.visibility = View.VISIBLE
            }
            R.id.btnExperienceCancel -> {
                binding.layoutExperiences.clAddEditExperience.visibility = View.GONE
                binding.layoutExperiences.clExperience.visibility = View.VISIBLE
            }
            R.id.btnSkillCancel -> {
                binding.layoutSkill.clEditSkill.visibility = View.GONE
                binding.layoutSkill.clSkill.visibility = View.VISIBLE
            }
            R.id.btnInterestsCancel -> {
                binding.layoutInterests.clEditInterests.visibility = View.GONE
                binding.layoutInterests.clInterests.visibility = View.VISIBLE
            }
            R.id.btnCoursesCancel -> {
                binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.GONE
                binding.layoutEducationalCourses.clEducationalCourses.visibility = View.VISIBLE
            }
            R.id.btnCertificateCancel -> {
                binding.layoutCertificates.clCertificates.visibility = View.VISIBLE
                binding.layoutCertificates.clEditCertificates.visibility = View.GONE
            }
            R.id.btnProjectCancel -> {
                binding.layoutProjects.clProject.visibility = View.VISIBLE
                binding.layoutProjects.clEditProject.visibility = View.GONE
            }
            R.id.btnLanguageCancel -> {
                binding.layoutLanguages.clLanguages.visibility = View.VISIBLE
                binding.layoutLanguages.clEditLanguages.visibility = View.GONE
            }
            R.id.btnSocialCancel -> {
                binding.layoutSocialNetwork.clSocialNetwork.visibility = View.VISIBLE
                binding.layoutSocialNetwork.clEditSocialNetwork.visibility = View.GONE
            }

            R.id.imgEducationEdit -> {
                posEditEducation = v.tag.toString().toInt()
                val pos = v.tag.toString().toInt()
                educationAddEditBottomSheet(posEditEducation)
//                openEditEducation(educationListAdapter.listData[posEditDelete])
            }
            R.id.imgEducationDelete -> {
                posEditDelete = v.tag.toString().toInt()
                deleteEducationDialog(false)
            }

            R.id.imgExperienceEdit -> {
                posEditDelete = v.tag.toString().toInt()
                posEditExperience = v.tag.toString().toInt()
                experienceAddEditBottomSheet(posEditExperience)
//                openEditExperience(experienceListAdapter.listData[posEditDelete])
            }
            R.id.imgExperienceDelete -> {
                posEditDelete = v.tag.toString().toInt()
                deleteExperienceDialog(false)
            }
            R.id.imgSkillDelete -> {
                pos = v.tag.toString().toInt()
                skillDeleteDialog(pos, false)
//                binding.skillProgressBar.visibility = View.VISIBLE
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteSkillEvent(
//                        skillListAdapter.listData[pos].id!!.toString()
//                    )
//                )
            }
            R.id.imgInterestDelete -> {
                pos = v.tag.toString().toInt()
//                binding.interestsProgressBar.visibility = View.VISIBLE
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteInterestEvent(
//                        interestListAdapter.listData[pos].id!!.toString()
//                    )
//                )
                interestDeleteDialog(pos, false)
            }
            R.id.imgCoursesEdit -> {
                pos = v.tag.toString().toInt()
                posEditEducationCourses = v.tag.toString().toInt()
                educationalCoursesAddEditBottomSheet(posEditEducationCourses)
//                editCourses(educationCoursesListAdapter.listData[pos])
            }
            R.id.imgCoursesDelete -> {
                pos = v.tag.toString().toInt()
                deleteCourseDialog(false)
            }
            R.id.imgCertificateEdit -> {
                pos = v.tag.toString().toInt()
                posEditCertificate = v.tag.toString().toInt()
//                editCertificate(certificatesListAdapter.listData[pos])
                certificateAddEditBottomSheet(posEditCertificate)
            }
            R.id.imgCertificateDelete -> {
                pos = v.tag.toString().toInt()
                deleteCertificateDialog(false)
            }

            R.id.imgProjectEdit -> {
                pos = v.tag.toString().toInt()
                posEditProjects = v.tag.toString().toInt()
//                projectEdit(projectsListAdapter.list[pos])
                projectsAddEditBottomSheet(posEditProjects)
            }
            R.id.imgProjectDelete -> {
                pos = v.tag.toString().toInt()
                deleteProjectDialog(false)
            }
            R.id.imgLanguageDelete -> {
                pos = v.tag.toString().toInt()
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteLanguageEvent(
//                        languageListAdapter.listData[pos].id!!
//                    )
//                )
                deleteLanguageDialog(false)
            }
            R.id.imgRight,
            R.id.txtBioGraphicalFileName -> {
                if (cvUrl.isNotEmpty()) {
                    getUrlFromIntent(cvUrl)
                }
            }
        }
    }

    private fun getUrlFromIntent(cv: String) {
        val url = "${Constant.CV_URL}${cv}"
        Log.d("TAG", "getUrlFromIntent: $url")
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    private fun deleteLanguageDialog(isDeleted : Boolean) {

        if (!isDeleted) {
            bottomSheetDialogDeleteLanguage = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteLanguage =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDialogDeleteLanguage.setContentView(bottomSheetViewDeleteLanguage)
            bottomSheetDialogDeleteLanguage.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteLanguage.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteLanguage.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteLanguage.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteLanguage.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteLanguage.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteLanguage.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogDeleteLanguage.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDialogDeleteLanguage.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                EditProfileStateEvent.DeleteLanguageEvent(
                    languageListAdapter.listData[pos].id!!
                )
            )
        }
    }

    private fun deleteProjectDialog(isDeleted : Boolean) {

//        MaterialAlertDialogBuilder(requireContext(), R.style.MyAlertDialogTheme)
//            .setTitle(R.string.app_name)
//            .setMessage(getString(R.string.project_delete_message))
//
//            .setPositiveButton(R.string.lbl_ok) { dialog, _ ->
//                dialog.dismiss()
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteProjectEvent(
//                        projectsListAdapter.listData[pos].id!!
//                    )
//                )
//
//            }
//            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
//                dialog.dismiss()
//            }
//            .show()

        if (!isDeleted) {
            bottomSheetDialogDeleteProject = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteProject =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDialogDeleteProject.setContentView(bottomSheetViewDeleteProject)
            bottomSheetDialogDeleteProject.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteProject.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteProject.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteProject.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteProject.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteProject.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteProject.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogDeleteProject.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDialogDeleteProject.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                EditProfileStateEvent.DeleteProjectEvent(
                    projectsListAdapter.listData[pos].id!!
                )
            )
        }
    }


    private fun deleteEducationDialog(isDeleted: Boolean) {

        /* MaterialAlertDialogBuilder(requireContext(), R.style.MyAlertDialogTheme)
             .setTitle(R.string.app_name)
             .setMessage(getString(R.string.education_delete_message))

             .setPositiveButton(R.string.lbl_ok) { dialog, _ ->
                 dialog.dismiss()
                 viewModel.setStateEvent(
                     EditProfileStateEvent.DeleteEducationEvent(
                         educationListAdapter.listData[posEditDelete].id.toString()
                     )
                 )

             }
             .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                 dialog.dismiss()
             }
             .show()*/

        if (!isDeleted) {
            bottomSheetDeleteEducation = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteEducation =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDeleteEducation.setContentView(bottomSheetViewDeleteEducation)
            bottomSheetDeleteEducation.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteEducation.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteEducation.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteEducation.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteEducation.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteEducation.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteEducation.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDeleteEducation.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDeleteEducation.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                EditProfileStateEvent.DeleteEducationEvent(
                    educationListAdapter.listData[posEditDelete].id.toString()
                )
            )
        }
    }

    private fun deleteExperienceDialog(isDeleted : Boolean) {
//        MaterialAlertDialogBuilder(requireContext(), R.style.MyAlertDialogTheme)
//            .setTitle(R.string.app_name)
//            .setMessage(getString(R.string.experience_delete_message))
//
//            .setPositiveButton(R.string.lbl_ok) { dialog, _ ->
//                dialog.dismiss()
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteExperienceEvent(
//                        experienceListAdapter.listData[posEditDelete].id.toString()
//                    )
//                )
//
//            }
//            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
//                dialog.dismiss()
//            }
//            .show()

        if (!isDeleted) {
            bottomSheetDeleteExperience = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteExperience =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDeleteExperience.setContentView(bottomSheetViewDeleteExperience)
            bottomSheetDeleteExperience.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteExperience.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteExperience.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteExperience.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteExperience.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteExperience.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteExperience.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDeleteExperience.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDeleteExperience.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                    EditProfileStateEvent.DeleteExperienceEvent(
                        experienceListAdapter.listData[posEditDelete].id.toString()
                    )
                )
        }
    }

    private fun skillDeleteDialog(pos : Int, isDeleted : Boolean){
        if (!isDeleted) {
            bottomSheetDeleteSkill = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteSkill =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDeleteSkill.setContentView(bottomSheetViewDeleteSkill)
            bottomSheetDeleteSkill.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteSkill.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteSkill.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteSkill.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteSkill.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteSkill.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteSkill.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDeleteSkill.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDeleteSkill.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                    EditProfileStateEvent.DeleteSkillEvent(
                        skillListAdapter.listData[pos].id!!.toString()
                    )
                )
        }
    }


    private fun interestDeleteDialog(pos : Int, isDeleted : Boolean){
        if (!isDeleted) {
            bottomSheetDeleteInterest = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteInterest =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDeleteInterest.setContentView(bottomSheetViewDeleteInterest)
            bottomSheetDeleteInterest.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteInterest.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteInterest.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteInterest.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteInterest.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteInterest.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteInterest.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDeleteInterest.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDeleteInterest.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                    EditProfileStateEvent.DeleteInterestEvent(
                        interestListAdapter.listData[pos].id!!.toString()
                    )
                )
        }
    }

    private fun deleteCourseDialog(isDeleted : Boolean) {
//        MaterialAlertDialogBuilder(requireContext(), R.style.MyAlertDialogTheme)
//            .setTitle(R.string.app_name)
//            .setMessage(getString(R.string.are_you_delete_course))
//
//            .setPositiveButton(R.string.lbl_ok) { dialog, _ ->
//                dialog.dismiss()
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteCourseEvent(
//                        educationCoursesListAdapter.listData[pos].id!!
//                    )
//                )
//            }
//            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
//                dialog.dismiss()
//            }
//            .show()

        if (!isDeleted) {
            bottomSheetDeleteEducationalCourses = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteEducationalCourses =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDeleteEducationalCourses.setContentView(bottomSheetViewDeleteEducationalCourses)
            bottomSheetDeleteEducationalCourses.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteEducationalCourses.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteEducationalCourses.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteEducationalCourses.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteEducationalCourses.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteEducationalCourses.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteEducationalCourses.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDeleteEducationalCourses.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDeleteEducationalCourses.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                    EditProfileStateEvent.DeleteCourseEvent(
                        educationCoursesListAdapter.listData[pos].id!!
                    )
                )
        }
    }

    private fun deleteCertificateDialog(isDeleted: Boolean) {

//        MaterialAlertDialogBuilder(requireContext(), R.style.MyAlertDialogTheme)
//            .setTitle(R.string.app_name)
//            .setMessage(getString(R.string.certificate_delete_message))
//
//            .setPositiveButton(R.string.lbl_ok) { dialog, _ ->
//                dialog.dismiss()
//                viewModel.setStateEvent(
//                    EditProfileStateEvent.DeleteCertificateEvent(
//                        certificatesListAdapter.listData[pos].id!!
//                    )
//                )
//
//            }
//            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
//                dialog.dismiss()
//            }
//            .show()

        if (!isDeleted) {
            bottomSheetDialogDeleteCertificate = BottomSheetDialog(
                requireActivity(), R.style.BottomsheetDialogThem
            )
            bottomSheetViewDeleteCertificate =
                LayoutInflater.from(requireActivity())
                    .inflate(
                        R.layout.bottom_sheet_delete,
                        requireActivity().findViewById<View>(R.id.constraintForDeleteEducation) as ConstraintLayout?
                    )
            bottomSheetDialogDeleteCertificate.setContentView(bottomSheetViewDeleteCertificate)
            bottomSheetDialogDeleteCertificate.show()
        }
        val txtWantToDelete: TextView =
            bottomSheetViewDeleteCertificate.findViewById(R.id.txtWantToDelete)!!
        val txtDeleteMessage: TextView =
            bottomSheetViewDeleteCertificate.findViewById(R.id.txtDeleteMessage)!!
        val txtCancel: TextView = bottomSheetViewDeleteCertificate.findViewById(R.id.txtCancel)!!
        val txtDelete: TextView = bottomSheetViewDeleteCertificate.findViewById(R.id.txtDelete)!!
        val txtOK: TextView = bottomSheetViewDeleteCertificate.findViewById(R.id.txtOK)!!
        val progressBar: ProgressBar =
            bottomSheetViewDeleteCertificate.findViewById(R.id.progress_bar)!!

        if (isDeleted) {
            txtWantToDelete.visibility = View.INVISIBLE
            txtDeleteMessage.text = requireActivity().getString(R.string.deleted_successfully)
            txtOK.visibility = View.VISIBLE
            txtCancel.visibility = View.GONE
            txtDelete.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogDeleteCertificate.dismiss()
        }
        txtOK.setOnClickListener {
            bottomSheetDialogDeleteCertificate.dismiss()
        }
        txtDelete.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            viewModel.setStateEvent(
                EditProfileStateEvent.DeleteCertificateEvent(
                    certificatesListAdapter.listData[pos].id!!
                )
            )
        }
    }

    private fun basicDataOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutBasicData.clEditBasicData.visibility = View.GONE
            binding.layoutBasicData.clBasicData.visibility = View.GONE
            binding.layoutBasicData.imgBasicDataDropUp.setImageResource(R.drawable.img_drop_down)
            basicDataOpen = false
        } else {
            binding.layoutBasicData.clEditBasicData.visibility = View.GONE
            binding.layoutBasicData.clBasicData.visibility = View.VISIBLE
            basicDataOpen = true
            binding.layoutBasicData.imgBasicDataDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }


    private fun personalInfoOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutPersonalInfo.clEditPersonalInfo.visibility = View.GONE
            binding.layoutPersonalInfo.clPersonalInfo.visibility = View.GONE
            binding.layoutPersonalInfo.imgPersonalInfoDropUp.setImageResource(R.drawable.img_drop_down)
            personalInfoOpen = false
        } else {
            binding.layoutPersonalInfo.clEditPersonalInfo.visibility = View.GONE
            binding.layoutPersonalInfo.clPersonalInfo.visibility = View.VISIBLE
            personalInfoOpen = true
            binding.layoutPersonalInfo.imgPersonalInfoDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun shortDescriptionOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutShortDescription.clShortDesc.visibility = View.GONE
            binding.layoutShortDescription.imgShortDescriptionDropUp.setImageResource(R.drawable.img_drop_down)
            shortDescriptionOpen = false
        } else {
            binding.layoutShortDescription.clShortDesc.visibility = View.VISIBLE
            shortDescriptionOpen = true
            binding.layoutShortDescription.imgShortDescriptionDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun educationOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutEducation.clEducation.visibility = View.GONE
            binding.layoutEducation.clAddEditEducation.visibility = View.GONE
            binding.layoutEducation.imgEducationDropUp.setImageResource(R.drawable.img_drop_down)
            educationOpen = false
        } else {
            binding.layoutEducation.clEducation.visibility = View.VISIBLE
            binding.layoutEducation.clAddEditEducation.visibility = View.GONE
            educationOpen = true
            binding.layoutEducation.imgEducationDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun experiencesOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutExperiences.clExperience.visibility = View.GONE
            binding.layoutExperiences.clAddEditExperience.visibility = View.GONE
            binding.layoutExperiences.imgExperiencesDropUp.setImageResource(R.drawable.img_drop_down)
            experiencesOpen = false
        } else {
            binding.layoutExperiences.clExperience.visibility = View.VISIBLE
            binding.layoutExperiences.clAddEditExperience.visibility = View.GONE
            experiencesOpen = true
            binding.layoutExperiences.imgExperiencesDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }


    private fun skillOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutSkill.clSkill.visibility = View.GONE
            binding.layoutSkill.clEditSkill.visibility = View.GONE
            binding.layoutSkill.imgSkillsDropUp.setImageResource(R.drawable.img_drop_down)
            skillOpen = false
        } else {
            binding.layoutSkill.clSkill.visibility = View.VISIBLE
            binding.layoutSkill.clEditSkill.visibility = View.GONE
            skillOpen = true
            binding.layoutSkill.imgSkillsDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun communicationOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutCommunicationData.clEditCommunicationData.visibility = View.GONE
            binding.layoutCommunicationData.clCommunicationData.visibility = View.GONE
            binding.layoutCommunicationData.imgCommunicationDataDropUp.setImageResource(R.drawable.img_drop_down)
            communicationOpen = false
        } else {
            binding.layoutCommunicationData.clEditCommunicationData.visibility = View.GONE
            binding.layoutCommunicationData.clCommunicationData.visibility = View.VISIBLE
            communicationOpen = true
            binding.layoutCommunicationData.imgCommunicationDataDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun interestOpen(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutInterests.clEditInterests.visibility = View.GONE
            binding.layoutInterests.clInterests.visibility = View.GONE
            binding.layoutInterests.imgInterestsDropUp.setImageResource(R.drawable.img_drop_down)
            interestEditOpen = false
        } else {
            binding.layoutInterests.clEditInterests.visibility = View.GONE
            binding.layoutInterests.clInterests.visibility = View.VISIBLE
            interestEditOpen = true
            binding.layoutInterests.imgInterestsDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun biographicalFilesOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutBioGraphicalFile.clBioGraphicalData.visibility = View.GONE
            binding.layoutBioGraphicalFile.clEditBioGraphicalData.visibility = View.GONE
            binding.layoutBioGraphicalFile.imgBioGraphicalFileDropUp.setImageResource(R.drawable.img_drop_down)
            bioGraphicalDataEditOpen = false
        } else {
            binding.layoutBioGraphicalFile.clBioGraphicalData.visibility = View.VISIBLE
            binding.layoutBioGraphicalFile.clEditBioGraphicalData.visibility = View.GONE
            bioGraphicalDataEditOpen = true
            binding.layoutBioGraphicalFile.imgBioGraphicalFileDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun educationalCoursesOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutEducationalCourses.clEducationalCourses.visibility = View.GONE
            binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.GONE
            binding.layoutEducationalCourses.imgEducationalCoursesDropUp.setImageResource(R.drawable.img_drop_down)
            educationalCoursesOpen = false
        } else {
            binding.layoutEducationalCourses.clEducationalCourses.visibility = View.VISIBLE
            binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.GONE
            educationalCoursesOpen = true
            binding.layoutEducationalCourses.imgEducationalCoursesDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun certificateOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutCertificates.clCertificates.visibility = View.GONE
            binding.layoutCertificates.clEditCertificates.visibility = View.GONE
            binding.layoutCertificates.imgCertificatesDropUp.setImageResource(R.drawable.img_drop_down)
            certificatesOpen = false
        } else {
            binding.layoutCertificates.clCertificates.visibility = View.VISIBLE
            binding.layoutCertificates.clEditCertificates.visibility = View.GONE
            certificatesOpen = true
            binding.layoutCertificates.imgCertificatesDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun projectOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutProjects.clProject.visibility = View.GONE
            binding.layoutProjects.clEditProject.visibility = View.GONE
            binding.layoutProjects.imgProjectsDropUp.setImageResource(R.drawable.img_drop_down)
            projectsOpen = false
        } else {
            binding.layoutProjects.clProject.visibility = View.VISIBLE
            binding.layoutProjects.clEditProject.visibility = View.GONE
            projectsOpen = true
            binding.layoutProjects.imgProjectsDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun languageOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutLanguages.clLanguages.visibility = View.GONE
            binding.layoutLanguages.clEditLanguages.visibility = View.GONE
            binding.layoutLanguages.imgLanguagesDropUp.setImageResource(R.drawable.img_drop_down)
            languageOpen = false
        } else {
            binding.layoutLanguages.clLanguages.visibility = View.VISIBLE
            binding.layoutLanguages.clEditLanguages.visibility = View.GONE
            languageOpen = true
            binding.layoutLanguages.imgLanguagesDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }

    private fun socialOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.layoutSocialNetwork.clSocialNetwork.visibility = View.GONE
            binding.layoutSocialNetwork.clEditSocialNetwork.visibility = View.GONE
            binding.layoutSocialNetwork.imgSocialNetworkDropUp.setImageResource(R.drawable.img_drop_down)
            socialOpen = false
        } else {
            binding.layoutSocialNetwork.clSocialNetwork.visibility = View.VISIBLE
            binding.layoutSocialNetwork.clEditSocialNetwork.visibility = View.GONE
            socialOpen = true
            binding.layoutSocialNetwork.imgSocialNetworkDropUp.setImageResource(R.drawable.img_up_arrow)
        }
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun setDropDownSpecialization(specializationsListResponseBean: BasicDataSpecializationResponse) {
        /**
         * Set Specialization list into dropdown
         */
        specializationDropDownListAdapter =
            SpecializationDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                specializationsListResponseBean.specializations!!,
                prefManager
            )

        binding.layoutBasicData.edtSpecialization.setAdapter(specializationDropDownListAdapter)
        binding.layoutBasicData.edtSpecialization.setOnClickListener {
            binding.layoutBasicData.edtSpecialization.showDropDown()
        }
        binding.layoutBasicData.edtSpecialization.setOnItemClickListener { _, _, position, _ ->
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.layoutBasicData.edtSpecialization.setText(
                    specializationDropDownListAdapter.getSpecialityList()[position].title_en
                )
            } else {
                binding.layoutBasicData.edtSpecialization.setText(
                    specializationDropDownListAdapter.getSpecialityList()[position].title
                )
            }
            specializationId = specializationDropDownListAdapter.getSpecialityList()[position].id!!

            specializationDropDownListAdapter.notifyDataSetChanged()
        }
    }


    private fun setDropDownJobRole(categoriesResponseBean: List<Role>) {
        /**
         *  Set JobRole list into dropdown
         */
        jobRoleDropDownListAdapter =
            JobRoleDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                categoriesResponseBean,
                prefManager
            )

        binding.layoutBasicData.edtCareerRole.setAdapter(jobRoleDropDownListAdapter)
        binding.layoutBasicData.edtCareerRole.setOnClickListener {
            binding.layoutBasicData.edtCareerRole.showDropDown()
        }
        binding.layoutBasicData.edtCareerRole.setOnItemClickListener { _, _, position, _ ->
//            speciality_id = specialityDropDownListAdapter.getSpecialityList()[position].id
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.layoutBasicData.edtCareerRole.setText(jobRoleDropDownListAdapter.getSpecialityList()[position].title_en)
            } else {
                binding.layoutBasicData.edtCareerRole.setText(jobRoleDropDownListAdapter.getSpecialityList()[position].title)
            }
            jobRoleId = jobRoleDropDownListAdapter.getSpecialityList()[position].id!!
            jobRoleDropDownListAdapter.notifyDataSetChanged()
        }
    }


    private fun setDropDownCities(cityListResponse: CityListResponse) {
        /**
         *  Set JobRole list into dropdown
         */
        cityDropDownListAdapter =
            CityDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                cityListResponse.cities!!,
                prefManager
            )

        binding.layoutPersonalInfo.edtCity.setAdapter(cityDropDownListAdapter)
        binding.layoutPersonalInfo.edtCity.setOnClickListener {
            binding.layoutPersonalInfo.edtCity.showDropDown()
        }
        binding.layoutPersonalInfo.edtCity.setOnItemClickListener { _, _, position, _ ->
//            speciality_id = specialityDropDownListAdapter.getSpecialityList()[position].id
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.layoutPersonalInfo.edtCity.setText(cityDropDownListAdapter.getSpecialityList()[position].name_en)
            } else {
                binding.layoutPersonalInfo.edtCity.setText(cityDropDownListAdapter.getSpecialityList()[position].name_ar)
            }
            city = cityDropDownListAdapter.getSpecialityList()[position].id!!.toString()
            cityDropDownListAdapter.notifyDataSetChanged()
        }
    }


    private fun personalInfoEditApiCall() {
        genderId = when (binding.layoutPersonalInfo.edtType.text.toString()) {
            "Male" -> {
                "1"
            }
            "Female" -> {
                "2"
            }
            else -> {
                "3"
            }
        }
        maritalStatusId =
            when (binding.layoutPersonalInfo.edtSocialStatus.text.toString()) {
                "Married" -> {
                    "1"
                }
                "Unmarried" -> {
                    "2"
                }
                "Absolute" -> {
                    "3"
                }
                else -> {
                    "4"
                }
            }
        viewModel.setStateEvent(
            EditProfileStateEvent.PersonalInfoEdit(
                binding.layoutPersonalInfo.edtAddress.text.toString(),
                city,
                genderId,
                binding.layoutPersonalInfo.edtDateOfBirth.text.toString(),
                binding.layoutPersonalInfo.edtFunctionalStatus.text.toString(),
                maritalStatusId,
                binding.layoutPersonalInfo.edtNumberOfYearsOfExperience.text.toString(),
                binding.layoutPersonalInfo.edtMinimumSalary.text.toString(),
            )
        )
    }


    private fun birthDatePicker() {
        val dpd = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->

                // Display Selected date in editText
                endDate = "$year-${monthOfYear + 1}-${dayOfMonth}"
                Log.d(mTAG, "DATE -> $endDate")
                binding.layoutPersonalInfo.edtDateOfBirth.setText(endDate)
            },
            year,
            month,
            day
        )
        dpd.datePicker.maxDate = Date().time
        dpd.show()
    }

    private fun courseDatePicker() {
        val dpd = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->

                // Display Selected date in editText
                endDate = "$year-${monthOfYear + 1}-${dayOfMonth}"
                Log.d(mTAG, "DATE -> $endDate")
                binding.layoutEducationalCourses.edtDateOfStarting.setText(endDate)
            },
            year,
            month,
            day
        )
        dpd.datePicker.maxDate = Date().time
        dpd.show()
    }


    private fun openEditEducation(education: Education) {
        binding.layoutEducation.clEducation.visibility = View.GONE
        binding.layoutEducation.clAddEditEducation.visibility = View.VISIBLE
        binding.layoutEducation.edtEducationalInstitute.setText(education.educational_institution)
        binding.layoutEducation.edtAppreciation.setText(education.general_appreciation)
        binding.layoutEducation.edtTheField.setText(education.specialization)
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            binding.layoutEducation.edtDegree.setText(
                qualificationResponseBean.qualifications?.filter { it.id == education.degree?.toInt() }!!
                    .map { it.title_en }[0]
            )
        } else {
            binding.layoutEducation.edtDegree.setText(
                qualificationResponseBean.qualifications?.filter { it.id == education.degree?.toInt() }!!
                    .map { it.title_ar }[0]
            )
        }
        degreeId = education.degree!!

        binding.layoutEducation.edtTheYear.setText(education.end_date_educational, false)
        binding.layoutEducation.edtTheDesc.setText(education.description_education)
    }

    private fun openEditExperience(experience: Experience) {
        binding.layoutExperiences.clExperience.visibility = View.GONE
        binding.layoutExperiences.clAddEditExperience.visibility = View.VISIBLE

        binding.layoutExperiences.edtJobTitle.setText(experience.job_title.toString())
        binding.layoutExperiences.edtCompanyName.setText(experience.company_name.toString())
        binding.layoutExperiences.edtCompanyLocation.setText(experience.company_address.toString())
        val sMonth = Utils.parseDateToddMMyyyy(experience.start_date, "yyyy-MM", "MMMM")
        val sYear = Utils.parseDateToddMMyyyy(experience.start_date, "yyyy-MM", "yyyy")
        sMonthId = Utils.parseDateToddMMyyyy(experience.start_date, "yyyy-MM", "MM")!!.toInt()
        Log.d(mTAG, "openEditExperience: sMonth $sMonth")
        Log.d(mTAG, "openEditExperience: sYear $sYear")
        Log.d(mTAG, "openEditExperience: sMonthId $sMonthId")
        binding.layoutExperiences.edtFromMonth.setText(sMonth, false)
        binding.layoutExperiences.edtFromYear.setText(sYear, false)
        if (experience.end_date != "Present") {
            val eMonth = Utils.parseDateToddMMyyyy(experience.end_date, "yyyy-MM", "MMMM")
            val eYear = Utils.parseDateToddMMyyyy(experience.end_date, "yyyy-MM", "yyyy")
            eMonthId = Utils.parseDateToddMMyyyy(experience.end_date, "yyyy-MM", "MM")!!.toInt()
            Log.d(mTAG, "openEditExperience: eMonth $eMonth")
            Log.d(mTAG, "openEditExperience: eYear $eYear")
            Log.d(mTAG, "openEditExperience: eMonthId $eMonthId")

            binding.layoutExperiences.edtToMonth.setText(eMonth, false)
            binding.layoutExperiences.edtToYear.setText(eYear, false)
        } else {
            eMonthId = -1
            binding.layoutExperiences.cbUntilNow.isChecked = true
            binding.layoutExperiences.edtToMonth.setText("")
            binding.layoutExperiences.edtToYear.setText("")
        }
        expId = experience.id!!
        binding.layoutExperiences.edtDesc.setText(experience.job_description.toString())
    }

    private fun editCourses(course: Course) {
        binding.layoutEducationalCourses.clEducationalCourses.visibility = View.GONE
        binding.layoutEducationalCourses.clEditEducationalCourses.visibility = View.VISIBLE
        binding.layoutEducationalCourses.edtAddress.setText(course.address)
        binding.layoutEducationalCourses.edtCenterName.setText(course.center_name)
        binding.layoutEducationalCourses.edtDateOfStarting.setText(course.action_date.toString())
        binding.layoutEducationalCourses.edtNumberOfHours.setText(course.count_times.toString())
        binding.layoutEducationalCourses.edtCourseDesc.setText(course.description.toString())
        course_id = course.id!!
    }

    private fun editCertificate(certificate: Certificate) {
        binding.layoutCertificates.clEditCertificates.visibility = View.VISIBLE
        binding.layoutCertificates.clCertificates.visibility = View.GONE

        binding.layoutCertificates.edtCertificateName.setText(certificate.title)
        binding.layoutCertificates.edtIssuer.setText(certificate.from_by)
        binding.layoutCertificates.edtReleaseMonth.setText(
            if (certificate.month.toString() != "-1")
                monthArrayEn[certificate.month?.toInt()!! - 1] else "",
            false
        )
        certificateMonth = certificate.month?.toInt()!!
        binding.layoutCertificates.edtReleaseYear.setText(certificate.year.toString(), false)
        binding.layoutCertificates.edtCertificateDesc.setText(certificate.description.toString())
        certificateId = certificate.id!!
    }

    private fun projectEdit(project: Project) {
        binding.layoutProjects.clEditProject.visibility = View.VISIBLE
        binding.layoutProjects.clProject.visibility = View.GONE


        binding.layoutProjects.edtTitle.setText(project.title)
        binding.layoutProjects.edtFromMonth.setText(
            monthArrayEn[project.month?.toInt()!! - 1],
            false
        )
        projectMonth = project.month!!.toInt()
        binding.layoutProjects.edtFromYear.setText(project.year.toString(), false)
        binding.layoutProjects.edtProjectDesc.setText(project.description.toString())
        projectId = project.id!!
    }

    private val startForCvResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.data?.also {
                        requireContext().contentResolver?.takePersistableUriPermission(
                            it,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                        )
                        val file =
                            DocumentUtils.getFile(
                                requireContext(),
                                it
                            )//use pdf as file
                        fileCv = file.toString()
                        Utils.print("Single_Image_path>$file")
                        Glide.with(requireContext())
                            .load(R.drawable.img_report_place_holder)
                            .centerCrop()
                            .into(imgUploadedCv)
                    }
                }
                else -> {
                    Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT).show()
                }
            }
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1112 -> {
                /*data?.data?.also {
                    *//*requireContext().contentResolver?.takePersistableUriPermission(
                                documentUri,
                                Intent.FLAG_GRANT_READ_URI_PERMISSION
                            )
                            val file =
                                DocumentUtils.getFile(
                                    requireContext(),
                                    documentUri
                                )//use pdf as file
                            fileCv = file.toString()
                            biographicalFilesOpenClose(false)*//*
//                    Utils.print("Single_Image_path>$file")
                            Glide.with(requireContext())
                                .load(R.drawable.img_report_place_holder)
                                .centerCrop()
                                .into(binding.layoutBioGraphicalFile.imgCompanyLogo)

                        }*/
            }

            else -> {
                Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }


    object DocumentUtils {
        fun getFile(mContext: Context?, documentUri: Uri): File {
            val inputStream = mContext?.contentResolver?.openInputStream(documentUri)
            var file = File("")
            inputStream.use { input ->
                file =
                    File(mContext?.cacheDir, System.currentTimeMillis().toString() + ".pdf")
                FileOutputStream(file).use { output ->
                    val buffer =
                        ByteArray(4 * 1024) // or other buffer size
                    var read: Int = -1
                    while (input?.read(buffer).also {
                            if (it != null) {
                                read = it
                            }
                        } != -1) {
                        output.write(buffer, 0, read)
                    }
                    output.flush()
                }
            }
            return file
        }
    }

    // Biographical Upload

    private fun forBiographicalUpload() {
        bottomSheetDialog = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetView =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_upload_cv,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )
        val txtCancel: TextView = bottomSheetView.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetView.findViewById(R.id.txtSave)!!
        val txtUploadConsent: TextView = bottomSheetView.findViewById(R.id.txtUploadConsent)!!
        val clBioGraphicalUpload: ConstraintLayout =
            bottomSheetView.findViewById(R.id.clBioGraphicalUpload)!!
        val clDownload: ConstraintLayout =
            bottomSheetView.findViewById(R.id.clDownload)!!
        val imgUpload: ImageView = bottomSheetView.findViewById(R.id.imgUpload)!!
        imgUploadedCv = bottomSheetView.findViewById(R.id.imgUploadedCv)!!
        cvProgressbar = bottomSheetView.findViewById(R.id.progress_bar)!!

        clDownload.setOnClickListener {
            val pdf_url = "${Constant.CV_URL}${profileResponseBean.user?.job_seeker?.cv}"
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url))
            startActivity(browserIntent)
        }
        clBioGraphicalUpload.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                type = "application/pdf"
                addCategory(Intent.CATEGORY_OPENABLE)
//                    flags = flags or Intent.FLAG_GRANT_READ_URI_PERMISSION
            }
//                startActivityForResult(intent, 1112)
            startForCvResult.launch(intent)
        }
        imgUpload.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                type = "application/pdf"
                addCategory(Intent.CATEGORY_OPENABLE)
//                    flags = flags or Intent.FLAG_GRANT_READ_URI_PERMISSION
            }
//                startActivityForResult(intent, 1112)
            startForCvResult.launch(intent)
        }

        txtSave.setOnClickListener {
            if (fileCv.isNotEmpty()) {
                cvProgressbar.visibility = View.VISIBLE
                viewModel.setStateEvent(EditProfileStateEvent.BioGraphicalFile(fileCv))
            } else {
                Toast.makeText(requireActivity(), "Please upload cv.", Toast.LENGTH_SHORT).show()
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.setContentView(bottomSheetView)
        bottomSheetDialog.show()
    }


    private fun educationAddEditBottomSheet(pos: Int) {
        bottomSheetDialogEducation = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewEducation =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_education_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewEducation.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewEducation.findViewById(R.id.txtSave)!!
        val inputEducationalInstitute: TextInputLayout =
            bottomSheetViewEducation.findViewById(R.id.inputEducationalInstitute)!!
        val inputAppreciation: TextInputLayout =
            bottomSheetViewEducation.findViewById(R.id.inputAppreciation)!!
        val inputTheField: TextInputLayout =
            bottomSheetViewEducation.findViewById(R.id.inputTheField)!!
        val inputDegree: TextInputLayout = bottomSheetViewEducation.findViewById(R.id.inputDegree)!!
        val inputTheYear: TextInputLayout =
            bottomSheetViewEducation.findViewById(R.id.inputTheYear)!!
        val inputTheDesc: TextInputLayout =
            bottomSheetViewEducation.findViewById(R.id.inputTheDesc)!!

        educationProgressbar = bottomSheetViewEducation.findViewById(R.id.progress_bar)!!
        val edtEducationalInstitute: TextInputEditText =
            bottomSheetViewEducation.findViewById(R.id.edtEducationalInstitute)!!
        val edtAppreciation: TextInputEditText =
            bottomSheetViewEducation.findViewById(R.id.edtAppreciation)!!
        val edtTheField: TextInputEditText =
            bottomSheetViewEducation.findViewById(R.id.edtTheField)!!
        val edtDegree: AutoCompleteTextView =
            bottomSheetViewEducation.findViewById(R.id.edtDegree)!!
        val edtTheYear: AutoCompleteTextView =
            bottomSheetViewEducation.findViewById(R.id.edtTheYear)!!
        val edtTheDesc: TextInputEditText = bottomSheetViewEducation.findViewById(R.id.edtTheDesc)!!

        val btnCancel: Button =
            bottomSheetViewEducation.findViewById(R.id.btnCancel)
        val btnSave: Button =
            bottomSheetViewEducation.findViewById(R.id.btnSave)!!
        if (posEditEducation != -1) {
            edtEducationalInstitute.setText(educationListAdapter.listData[pos].educational_institution)
            edtAppreciation.setText(educationListAdapter.listData[pos].general_appreciation)
            edtTheField.setText(educationListAdapter.listData[pos].specialization)
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                edtDegree.setText(
                    qualificationResponseBean.qualifications?.filter { it.id == educationListAdapter.listData[pos].degree?.toInt() }!!
                        .map { it.title_en }[0]
                )
            } else {
                edtDegree.setText(
                    qualificationResponseBean.qualifications?.filter { it.id == educationListAdapter.listData[pos].degree?.toInt() }!!
                        .map { it.title_ar }[0]
                )
            }
            degreeId = educationListAdapter.listData[pos].degree!!

            edtTheYear.setText(educationListAdapter.listData[pos].end_date_educational, false)
            edtTheDesc.setText(educationListAdapter.listData[pos].description_education)
        } else {

            edtEducationalInstitute.setText("")
            edtAppreciation.setText("")
            edtTheField.setText("")
            edtDegree.setText("")
            edtTheYear.setText("")
            edtTheDesc.setText("")
            posEditEducation = -1
        }

        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                if (posEditEducation != -1) {
                    educationProgressbar.visibility = View.VISIBLE
                    viewModel.setStateEvent(
                        EditProfileStateEvent.EditEducationEvent(
                            edtEducationalInstitute.text.toString(),
                            edtAppreciation.text.toString(),
                            edtTheField.text.toString(),
                            degreeId,
                            edtTheYear.text.toString(),
                            edtTheDesc.text.toString(),
                            educationListAdapter.listData[posEditEducation].id.toString()
                        )
                    )
                } else {
                    inputEducationalInstitute.isErrorEnabled = false
                    inputAppreciation.isErrorEnabled = false
                    inputTheField.isErrorEnabled = false
                    inputDegree.isErrorEnabled = false
                    inputTheYear.isErrorEnabled = false
                    inputTheDesc.isErrorEnabled = false
                    when {
                        edtEducationalInstitute.text.toString().isEmpty() -> {
                            inputEducationalInstitute.error =
                                getString(R.string.error_enter_education_institute)
                            inputEducationalInstitute.requestFocus()
                            return@setOnClickListener
                        }
                        edtAppreciation.text.toString().isEmpty() -> {
                            inputAppreciation.error =
                                getString(R.string.error_enter_appreciation)
                            inputAppreciation.requestFocus()
                            return@setOnClickListener
                        }
                        edtTheField.text.toString().isEmpty() -> {
                            inputTheField.error =
                                getString(R.string.error_enter_specialisation_field)
                            inputTheField.requestFocus()
                            return@setOnClickListener
                        }
                        degreeId == "" -> {
                            inputDegree.error =
                                getString(R.string.error_please_select_degree)
                            inputDegree.requestFocus()
                            return@setOnClickListener
                        }
                        edtTheYear.text.toString().isEmpty() -> {
                            inputTheYear.error =
                                getString(R.string.error_select_education_year)
                            inputTheYear.requestFocus()
                            return@setOnClickListener
                        }
                        edtTheDesc.text.toString().isEmpty() -> {
                            inputTheDesc.error =
                                getString(R.string.error_please_enter_description)
                            inputTheDesc.requestFocus()
                            return@setOnClickListener
                        }
                        else -> {
                            educationProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddEducationEvent(
                                    edtEducationalInstitute.text.toString(),
                                    edtAppreciation.text.toString(),
                                    edtTheField.text.toString(),
                                    degreeId,
                                    edtTheYear.text.toString(),
                                    edtTheDesc.text.toString(),
                                    profileResponseBean.user!!.id!!
                                )
                            )
                        }
                    }
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogEducation.dismiss()
        }


        qualificationListAdapter =
            QualificationListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                qualificationResponseBean.qualifications!!
            )

        edtDegree.setAdapter(qualificationListAdapter)
        edtDegree.setOnClickListener {
            edtDegree.showDropDown()
        }
        edtDegree.setOnItemClickListener { _, _, position, _ ->

            if (qualificationListAdapter.getSpecialityList()[position].title_en == null) {
                edtDegree.setText(qualificationListAdapter.getSpecialityList()[position].title_ar)
            } else {
                edtDegree.setText(qualificationListAdapter.getSpecialityList()[position].title_en)
            }
            degreeId = qualificationListAdapter.getSpecialityList()[position].id!!.toString()
            qualificationListAdapter.notifyDataSetChanged()
        }

        val years: ArrayList<String> = ArrayList()
        val thisYear = Calendar.getInstance()[Calendar.YEAR]
        for (i in 1900..thisYear) {
            years.add(i.toString())
        }
        val yearAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                years
            )
        years.reverse()
        (edtTheYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        edtTheYear.dropDownWidth = WindowManager.LayoutParams.MATCH_PARENT

        bottomSheetDialogEducation.setContentView(bottomSheetViewEducation)
        bottomSheetDialogEducation.show()
    }


    private fun experienceAddEditBottomSheet(pos: Int) {
        bottomSheetDialogExperience = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewExperience =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_experience_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewExperience.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewExperience.findViewById(R.id.txtSave)!!
        val inputJobTitle: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputJobTitle)!!
        val inputCompanyName: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputCompanyName)!!
        val inputCompanyLocation: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputCompanyLocation)!!
        val inputFromMonth: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputFromMonth)!!
        val inputFromYear: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputFromYear)!!
        val inputToMonth: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputToMonth)!!
        val inputToYear: TextInputLayout =
            bottomSheetViewExperience.findViewById(R.id.inputToYear)!!

        val inputDesc: TextInputLayout = bottomSheetViewExperience.findViewById(R.id.inputDesc)!!

        experienceProgressbar = bottomSheetViewExperience.findViewById(R.id.progress_bar)!!
        val edtJobTitle: TextInputEditText =
            bottomSheetViewExperience.findViewById(R.id.edtJobTitle)!!
        val edtCompanyName: TextInputEditText =
            bottomSheetViewExperience.findViewById(R.id.edtCompanyName)!!
        val edtCompanyLocation: TextInputEditText =
            bottomSheetViewExperience.findViewById(R.id.edtCompanyLocation)!!
        val edtFromMonth: AutoCompleteTextView =
            bottomSheetViewExperience.findViewById(R.id.edtFromMonth)!!
        val edtFromYear: AutoCompleteTextView =
            bottomSheetViewExperience.findViewById(R.id.edtFromYear)!!
        val edtToMonth: AutoCompleteTextView =
            bottomSheetViewExperience.findViewById(R.id.edtToMonth)!!
        val edtToYear: AutoCompleteTextView =
            bottomSheetViewExperience.findViewById(R.id.edtToYear)!!
        val edtDesc: TextInputEditText =
            bottomSheetViewExperience.findViewById(R.id.edtDesc)!!


        val cbUntilNow: CheckBox = bottomSheetViewExperience.findViewById(R.id.cbUntilNow)!!

        if (pos != -1) {
            edtJobTitle.setText(experienceListAdapter.listData[pos].job_title.toString())
            edtCompanyName.setText(experienceListAdapter.listData[pos].company_name.toString())
            edtCompanyLocation.setText(experienceListAdapter.listData[pos].company_address.toString())
            val sMonth = Utils.parseDateToddMMyyyy(
                experienceListAdapter.listData[pos].start_date,
                "yyyy-MM",
                "MMMM"
            )
            val sYear = Utils.parseDateToddMMyyyy(
                experienceListAdapter.listData[pos].start_date,
                "yyyy-MM",
                "yyyy"
            )
            sMonthId = Utils.parseDateToddMMyyyy(
                experienceListAdapter.listData[pos].start_date,
                "yyyy-MM",
                "MM"
            )!!.toInt()
            Log.d(mTAG, "openEditExperience: sMonth $sMonth")
            Log.d(mTAG, "openEditExperience: sYear $sYear")
            Log.d(mTAG, "openEditExperience: sMonthId $sMonthId")
            edtFromMonth.setText(sMonth, false)
            edtFromYear.setText(sYear, false)
            if (experienceListAdapter.listData[pos].end_date != "Present") {
                val eMonth = Utils.parseDateToddMMyyyy(
                    experienceListAdapter.listData[pos].end_date,
                    "yyyy-MM",
                    "MMMM"
                )
                val eYear = Utils.parseDateToddMMyyyy(
                    experienceListAdapter.listData[pos].end_date,
                    "yyyy-MM",
                    "yyyy"
                )
                eMonthId = Utils.parseDateToddMMyyyy(
                    experienceListAdapter.listData[pos].end_date,
                    "yyyy-MM",
                    "MM"
                )!!.toInt()
                Log.d(mTAG, "openEditExperience: eMonth $eMonth")
                Log.d(mTAG, "openEditExperience: eYear $eYear")
                Log.d(mTAG, "openEditExperience: eMonthId $eMonthId")

                edtToMonth.setText(eMonth, false)
                edtToYear.setText(eYear, false)
            } else {
                eMonthId = -1
                cbUntilNow.isChecked = true
                edtToMonth.setText("")
                edtToYear.setText("")
            }
            expId = experienceListAdapter.listData[pos].id!!
            edtDesc.setText(experienceListAdapter.listData[pos].job_description.toString())
        } else {
            edtJobTitle.setText("")
            edtCompanyName.setText("")
            edtCompanyLocation.setText("")
            edtFromMonth.setText("")
            edtFromYear.setText("")
            edtToMonth.setText("")
            edtToYear.setText("")
            edtDesc.setText("")
            expId = -1
        }

        val years: ArrayList<String> = ArrayList()
        val thisYear = Calendar.getInstance()[Calendar.YEAR]
        for (i in 1900..thisYear) {
            years.add(i.toString())
        }
        val yearAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                years
            )
        years.reverse()


        /** Experience screen */
        (edtFromYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        edtFromYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT

        (edtToYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        edtToYear.dropDownWidth = WindowManager.LayoutParams.WRAP_CONTENT



        (edtFromYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        edtFromYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT

        val monthAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                monthArrayEn
            )
        (edtFromMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        edtFromMonth.setOnItemClickListener { _, _, position, _ ->
            sMonthId = position + 1
            Log.d(mTAG, "updateUi:sMonthId $sMonthId")
        }
        edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT

        (edtToMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        edtToMonth.setOnItemClickListener { _, _, position, _ ->
            eMonthId = position + 1
            Log.d(mTAG, "updateUi:eMonthId $eMonthId")
        }
        edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT



        cbUntilNow.setOnCheckedChangeListener { _, isChecked ->

            edtToMonth.isEnabled = !isChecked
            edtToYear.isEnabled = !isChecked

        }


        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                inputJobTitle.isErrorEnabled = false
                inputCompanyName.isErrorEnabled = false
                inputCompanyLocation.isErrorEnabled = false
                inputFromMonth.isErrorEnabled = false
                inputFromYear.isErrorEnabled = false
                inputToMonth.isErrorEnabled = false
                inputToYear.isErrorEnabled = false
                inputDesc.isErrorEnabled = false

                when {
                    edtJobTitle.text.toString().isEmpty() -> {
                        inputJobTitle.error =
                            getString(R.string.error_enter_job_title)
                        inputJobTitle.requestFocus()
                        return@setOnClickListener
                    }
                    edtCompanyName.text.toString().isEmpty() -> {
                        inputCompanyName.error =
                            getString(R.string.error_company_name)
                        inputCompanyName.requestFocus()
                        return@setOnClickListener
                    }
                    edtCompanyLocation.text.toString().isEmpty() -> {
                        inputCompanyLocation.error =
                            getString(R.string.error_company_location)
                        inputCompanyLocation.requestFocus()
                        return@setOnClickListener
                    }
                    sMonthId == -1 -> {
                        inputFromMonth.error =
                            getString(R.string.error_starting_month)
                        inputFromMonth.requestFocus()
                        return@setOnClickListener
                    }
                    edtFromYear.text.toString().isEmpty() -> {
                        inputFromYear.error =
                            getString(R.string.error_starting_year)
                        inputFromYear.requestFocus()
                        return@setOnClickListener
                    }
                    !cbUntilNow.isChecked && eMonthId == -1 -> {
                        inputToMonth.error =
                            getString(R.string.error_end_month)
                        inputToMonth.requestFocus()
                    }
                    !cbUntilNow.isChecked && edtToYear.text.toString().isEmpty() -> {
                        inputToYear.error =
                            getString(R.string.error_end_year)
                        inputToYear.requestFocus()
                    }
                    else -> {
                        if (edtDesc.text.toString().isEmpty()) {
                            inputDesc.error =
                                getString(R.string.error_please_enter_description)
                            inputDesc.requestFocus()
                            return@setOnClickListener
                        }
                        if (expId == -1) {
                            experienceProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddExperienceEvent(
                                    edtJobTitle.text.toString(),
                                    edtCompanyName.text.toString(),
                                    edtCompanyLocation.text.toString(),
                                    sMonthId.toString(),
                                    edtFromYear.text.toString(),
                                    if (cbUntilNow.isChecked) "" else eMonthId.toString(),
                                    if (cbUntilNow.isChecked) "" else edtToYear.text.toString(),
                                    if (cbUntilNow.isChecked) "Present" else "",
                                    edtDesc.text.toString(),
                                )
                            )
                        } else {
                            experienceProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditExperienceEvent(
                                    edtJobTitle.text.toString(),
                                    edtCompanyName.text.toString(),
                                    edtCompanyLocation.text.toString(),
                                    sMonthId.toString(),
                                    edtFromYear.text.toString(),
                                    if (cbUntilNow.isChecked) "" else eMonthId.toString(),
                                    if (cbUntilNow.isChecked) "" else edtToYear.text.toString(),
                                    if (cbUntilNow.isChecked) "Present" else "",
                                    edtDesc.text.toString(),
                                    expId
                                )
                            )
                        }
                    }

                }

            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogExperience.dismiss()
        }

        bottomSheetDialogExperience.setContentView(bottomSheetViewExperience)
        bottomSheetDialogExperience.show()
    }

    private fun skillAddEditBottomSheet() {
        bottomSheetDialogSkill = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewSkill =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_skill,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewSkill.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewSkill.findViewById(R.id.txtSave)!!
        val edtSkill: EditText = bottomSheetViewSkill.findViewById(R.id.edtSkill)!!
        val btnSkillSave: Button = bottomSheetViewSkill.findViewById(R.id.btnSkillSave)!!
        skillProgressbar = bottomSheetViewSkill.findViewById(R.id.progress_bar)!!

        val rvBottomSkillList: RecyclerView =
            bottomSheetViewSkill.findViewById(R.id.rvBottomSkillList)!!

        if (profileResponseBean.user?.skills!!.isNotEmpty()) {
            rvBottomSkillList.visibility = View.VISIBLE
            skillListAdapter =
                SkillListAdapter(requireContext(), this, profileResponseBean.user?.skills)
            rvBottomSkillList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                adapter = skillListAdapter
            }
        } else {
            rvBottomSkillList.visibility = View.GONE
        }

        txtCancel.setOnClickListener {
            bottomSheetDialogSkill.dismiss()
        }
        btnSkillSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                if (edtSkill.text.toString().isEmpty()) {
                    edtSkill.requestFocus()
                    edtSkill.error =
                        getString(R.string.error_enter_skill)

                } else {
                    skillProgressbar.visibility = View.VISIBLE
                    viewModel.setStateEvent(
                        EditProfileStateEvent.AddSkillEvent(
                            edtSkill.text.toString()
                        )
                    )
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }

        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                if (edtSkill.text.toString().isEmpty()) {
                    edtSkill.requestFocus()
                    edtSkill.error =
                        getString(R.string.error_enter_skill)

                } else {
                    skillProgressbar.visibility = View.VISIBLE
                    viewModel.setStateEvent(
                        EditProfileStateEvent.AddSkillEvent(
                            edtSkill.text.toString()
                        )
                    )
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }

        bottomSheetDialogSkill.setContentView(bottomSheetViewSkill)
        bottomSheetDialogSkill.show()
    }

    private fun interestAddEditBottomSheet() {
        bottomSheetDialogInterest = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewInterest =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_interest,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewInterest.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewInterest.findViewById(R.id.txtSave)!!
        val edtInterests: EditText = bottomSheetViewInterest.findViewById(R.id.edtInterests)!!
        val btnInterestsSave: Button = bottomSheetViewInterest.findViewById(R.id.btnInterestsSave)!!

        interestProgressbar = bottomSheetViewInterest.findViewById(R.id.progress_bar)!!

        val rvBottomInterestsList: RecyclerView =
            bottomSheetViewInterest.findViewById(R.id.rvBottomInterestsList)!!

        if (profileResponseBean.user?.interest!!.isNotEmpty()) {
            rvBottomInterestsList.visibility = View.VISIBLE
            interestListAdapter =
                InterestListAdapter(requireContext(), this, profileResponseBean.user?.interest)
            rvBottomInterestsList.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                adapter = interestListAdapter
            }
        } else {
            rvBottomInterestsList.visibility = View.GONE
        }


        txtCancel.setOnClickListener {
            bottomSheetDialogInterest.dismiss()
        }
        btnInterestsSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                if (edtInterests.text.toString().isEmpty()) {
                    edtInterests.requestFocus()
                    edtInterests.error =
                        getString(R.string.error_enter_interest)

                } else {
                    interestProgressbar.visibility = View.VISIBLE
                    viewModel.setStateEvent(
                        EditProfileStateEvent.AddInterestEvent(
                            edtInterests.text.toString()
                        )
                    )
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }

        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                if (edtInterests.text.toString().isEmpty()) {
                    edtInterests.requestFocus()
                    edtInterests.error =
                        getString(R.string.error_enter_interest)

                } else {
                    interestProgressbar.visibility = View.VISIBLE
                    viewModel.setStateEvent(
                        EditProfileStateEvent.AddInterestEvent(
                            edtInterests.text.toString()
                        )
                    )
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }

        bottomSheetDialogInterest.setContentView(bottomSheetViewInterest)
        bottomSheetDialogInterest.show()
    }


    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            when (resultCode) {
                Activity.RESULT_OK -> {
                    val uri: Uri = data?.data!!
                    logo = uri.path.toString()
                    Glide.with(requireContext())
                        .load(uri)
                        .into(imgUploadedCertificate)
//                    binding.mainView.fullScroll(View.FOCUS_UP)
//                        viewModel.setStateEvent(EditProfileStateEvent.UpdateUserProfileImage(logo))
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(
                        requireContext(),
                        ImagePicker.getError(data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

    private fun educationalCoursesAddEditBottomSheet(pos: Int) {
        bottomSheetDialogEducationalCourses = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewEducationalCourses =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_educational_courses_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewEducationalCourses.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewEducationalCourses.findViewById(R.id.txtSave)!!

        val inputAddress: TextInputLayout =
            bottomSheetViewEducationalCourses.findViewById(R.id.inputAddress)!!
        val inputCenterName: TextInputLayout =
            bottomSheetViewEducationalCourses.findViewById(R.id.inputCenterName)!!
        val inputDateOfStarting: TextInputLayout =
            bottomSheetViewEducationalCourses.findViewById(R.id.inputDateOfStarting)!!
        val inputNumberOfHours: TextInputLayout =
            bottomSheetViewEducationalCourses.findViewById(R.id.inputNumberOfHours)!!
        val inputCourseDesc: TextInputLayout =
            bottomSheetViewEducationalCourses.findViewById(R.id.inputCourseDesc)!!

        val edtAddress: TextInputEditText =
            bottomSheetViewEducationalCourses.findViewById(R.id.edtAddress)!!
        val edtCenterName: TextInputEditText =
            bottomSheetViewEducationalCourses.findViewById(R.id.edtCenterName)!!
        val edtDateOfStarting: TextInputEditText =
            bottomSheetViewEducationalCourses.findViewById(R.id.edtDateOfStarting)!!
        val edtNumberOfHours: TextInputEditText =
            bottomSheetViewEducationalCourses.findViewById(R.id.edtNumberOfHours)!!
        val edtCourseDesc: TextInputEditText =
            bottomSheetViewEducationalCourses.findViewById(R.id.edtCourseDesc)!!
        educationalCoursesProgressbar =
            bottomSheetViewEducationalCourses.findViewById(R.id.progress_bar)!!
        val clBioGraphicalUpload: ConstraintLayout =
            bottomSheetViewEducationalCourses.findViewById(R.id.clBioGraphicalUpload)!!
        val imgUpload: ImageView =
            bottomSheetViewEducationalCourses.findViewById(R.id.imgUpload)!!
        imgUploadedCertificate =
            bottomSheetViewEducationalCourses.findViewById(R.id.imgUploadedCertificate)!!

        if (pos != -1) {
            edtAddress.setText(educationCoursesListAdapter.listData[pos].address)
            edtCenterName.setText(educationCoursesListAdapter.listData[pos].center_name)
            edtDateOfStarting.setText(educationCoursesListAdapter.listData[pos].action_date.toString())
            edtNumberOfHours.setText(educationCoursesListAdapter.listData[pos].count_times.toString())
            edtCourseDesc.setText(educationCoursesListAdapter.listData[pos].description.toString())
            Glide.with(requireActivity())
                .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${educationCoursesListAdapter.listData[pos].image}")
                .placeholder(R.drawable.img_home)
                .error(R.drawable.img_home)
                .into(imgUploadedCertificate)
            course_id = educationCoursesListAdapter.listData[pos].id!!
        } else {
            edtAddress.setText("")
            edtCenterName.setText("")
            edtDateOfStarting.setText("")
            edtNumberOfHours.setText("")
            edtCourseDesc.setText("")
            course_id = -1
        }


        edtDateOfStarting.setOnClickListener {
            val dpd = DatePickerDialog(
                requireContext(),
                { _, year, monthOfYear, dayOfMonth ->

                    // Display Selected date in editText
                    endDate = "$year-${monthOfYear + 1}-${dayOfMonth}"
                    Log.d(mTAG, "DATE -> $endDate")
                    edtDateOfStarting.setText(endDate)
                },
                year,
                month,
                day
            )
            dpd.datePicker.maxDate = Date().time
            dpd.show()
        }
        imgUploadedCertificate.setOnClickListener {
            ImagePicker.with(requireActivity())
                .compress(512)
                .crop(16f, 9f)
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
        }


        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {

                inputAddress.isErrorEnabled = false
                inputCenterName.isErrorEnabled = false
                inputDateOfStarting.isErrorEnabled = false
                inputNumberOfHours.isErrorEnabled = false
                inputCourseDesc.isErrorEnabled = false
                when {
                    edtAddress.text.toString().isEmpty() -> {
                        inputAddress.error =
                            getString(R.string.error_enter_address)
                        inputAddress.requestFocus()

                    }
                    edtCenterName.text.toString().isEmpty() -> {
                        inputCenterName.error =
                            getString(R.string.error_enter_center_name)
                        inputCenterName.requestFocus()

                    }
                    edtDateOfStarting.text.toString().isEmpty() -> {
                        inputDateOfStarting.error =
                            getString(R.string.error_date_of_course_starting)
                        inputDateOfStarting.requestFocus()

                    }
                    edtNumberOfHours.text.toString().isEmpty() -> {
                        inputNumberOfHours.error =
                            getString(R.string.error_enter_number_of_hours)
                        inputNumberOfHours.requestFocus()

                    }
                    edtCourseDesc.text.toString().isEmpty() -> {
                        inputCourseDesc.error =
                            getString(R.string.error_please_enter_description)
                        inputCourseDesc.requestFocus()

                    }
                    else -> {
                        if (pos == -1) {
                            if (logo.isEmpty()) {
                                imgUploadedCertificate.requestFocus()
                                Toast.makeText(
                                    requireContext(),
                                    "Please upload certificate image",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                educationalCoursesProgressbar.visibility = View.VISIBLE
                                viewModel.setStateEvent(
                                    EditProfileStateEvent.AddCoursesEvent(
                                        edtAddress.text.toString(),
                                        edtCenterName.text.toString(),
                                        edtDateOfStarting.text.toString(),
                                        edtNumberOfHours.text.toString(),
                                        edtCourseDesc.text.toString(),
                                        logo
                                    )
                                )
                            }
                        } else {
                            educationalCoursesProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditCoursesEvent(
                                    edtAddress.text.toString(),
                                    edtCenterName.text.toString(),
                                    edtDateOfStarting.text.toString(),
                                    edtNumberOfHours.text.toString(),
                                    edtCourseDesc.text.toString(),
                                    course_id,
                                    if (logo.isNotEmpty()) logo else ""
                                )
                            )
                        }
                    }
                }

            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogEducationalCourses.dismiss()
        }

        bottomSheetDialogEducationalCourses.setContentView(bottomSheetViewEducationalCourses)
        bottomSheetDialogEducationalCourses.show()
    }


    private fun certificateAddEditBottomSheet(pos: Int) {
        bottomSheetDialogCertificate = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewCertificate =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_certificate_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewCertificate.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewCertificate.findViewById(R.id.txtSave)!!

        val inputCertificateName: TextInputLayout =
            bottomSheetViewCertificate.findViewById(R.id.inputCertificateName)!!
        val inputIssuer: TextInputLayout =
            bottomSheetViewCertificate.findViewById(R.id.inputIssuer)!!
        val inputReleaseMonth: TextInputLayout =
            bottomSheetViewCertificate.findViewById(R.id.inputReleaseMonth)!!
        val inputReleaseYear: TextInputLayout =
            bottomSheetViewCertificate.findViewById(R.id.inputReleaseYear)!!
        val inputCertificateDesc: TextInputLayout =
            bottomSheetViewCertificate.findViewById(R.id.inputCertificateDesc)!!


        val edtCertificateName: TextInputEditText =
            bottomSheetViewCertificate.findViewById(R.id.edtCertificateName)!!
        val edtIssuer: TextInputEditText =
            bottomSheetViewCertificate.findViewById(R.id.edtIssuer)!!
        val edtReleaseMonth: AutoCompleteTextView =
            bottomSheetViewCertificate.findViewById(R.id.edtReleaseMonth)!!
        val edtReleaseYear: AutoCompleteTextView =
            bottomSheetViewCertificate.findViewById(R.id.edtReleaseYear)!!
        val edtCertificateDesc: TextInputEditText =
            bottomSheetViewCertificate.findViewById(R.id.edtCertificateDesc)!!
        certificateProgressbar =
            bottomSheetViewCertificate.findViewById(R.id.progress_bar)!!

        if (pos != -1) {
            edtCertificateName.setText(certificatesListAdapter.listData[pos].title)
            edtIssuer.setText(certificatesListAdapter.listData[pos].from_by)
            edtReleaseMonth.setText(
                if (certificatesListAdapter.listData[pos].month.toString() != "-1")
                    monthArrayEn[certificatesListAdapter.listData[pos].month?.toInt()!! - 1] else "",
                false
            )
            certificateMonth = certificatesListAdapter.listData[pos].month?.toInt()!!
            edtReleaseYear.setText(certificatesListAdapter.listData[pos].year.toString(), false)
            edtCertificateDesc.setText(certificatesListAdapter.listData[pos].description.toString())
            certificateId = certificatesListAdapter.listData[pos].id!!
        } else {
            edtCertificateName.setText("")
            edtIssuer.setText("")
            edtReleaseMonth.setText("")
            edtReleaseYear.setText("")
            edtCertificateDesc.setText("")
            certificateId = -1
        }


        /** Year drop down */
        val years: ArrayList<String> = ArrayList()
        val thisYear = Calendar.getInstance()[Calendar.YEAR]
        for (i in 1900..thisYear) {
            years.add(i.toString())
        }
        val yearAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                years
            )
        years.reverse()

        (edtReleaseYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        edtReleaseYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT

        val monthAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                monthArrayEn
            )


        (edtReleaseMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        edtReleaseMonth.setOnItemClickListener { _, _, position, _ ->
            certificateMonth = position + 1
            Log.d(mTAG, "updateUi:certificateMonth $certificateMonth")
        }
        edtReleaseMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT


        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                inputCertificateName.isErrorEnabled = false
                inputIssuer.isErrorEnabled = false
                inputReleaseMonth.isErrorEnabled = false
                inputReleaseYear.isErrorEnabled = false
                inputCertificateDesc.isErrorEnabled = false

                when {
                    edtCertificateName.text.toString().isEmpty() -> {
                        inputCertificateName.error =
                            getString(R.string.error_enter_certificate_name)
                        inputCertificateName.requestFocus()

                    }
                    edtIssuer.text.toString().isEmpty() -> {
                        inputIssuer.error =
                            getString(R.string.error_enter_issuer)
                        inputIssuer.requestFocus()

                    }
                    edtReleaseMonth.text.toString().isEmpty() -> {
                        inputReleaseMonth.error =
                            getString(R.string.error_select_month)
                        inputReleaseMonth.requestFocus()

                    }

                    edtReleaseYear.text.toString().isEmpty() -> {
                        inputReleaseYear.error =
                            getString(R.string.error_select_year)
                        inputReleaseYear.requestFocus()

                    }
                    edtCertificateDesc.text.toString().isEmpty() -> {
                        inputCertificateDesc.error =
                            getString(R.string.error_please_enter_description)
                        inputCertificateDesc.requestFocus()

                    }
                    else -> {
                        if (certificateId == -1) {
                            certificateProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddCertificateEvent(
                                    edtCertificateName.text.toString(),
                                    edtIssuer.text.toString(),
                                    certificateMonth.toString(),
                                    edtReleaseYear.text.toString(),
                                    edtCertificateDesc.text.toString(),
                                )
                            )
                        } else {
                            certificateProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditCertificateEvent(
                                    edtCertificateName.text.toString(),
                                    edtIssuer.text.toString(),
                                    certificateMonth.toString(),
                                    edtReleaseYear.text.toString(),
                                    edtCertificateDesc.text.toString(),
                                    certificateId
                                )
                            )
                        }
                    }
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogCertificate.dismiss()
        }

        bottomSheetDialogCertificate.setContentView(bottomSheetViewCertificate)
        bottomSheetDialogCertificate.show()
    }

    private fun projectsAddEditBottomSheet(pos: Int) {
        bottomSheetDialogProjects = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewProjects =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_projects_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewProjects.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewProjects.findViewById(R.id.txtSave)!!

        val inputTitle: TextInputLayout =
            bottomSheetViewProjects.findViewById(R.id.inputTitle)!!
        val inputFromMonth: TextInputLayout =
            bottomSheetViewProjects.findViewById(R.id.inputFromMonth)!!
        val inputFromYear: TextInputLayout =
            bottomSheetViewProjects.findViewById(R.id.inputFromYear)!!
        val inputProjectDesc: TextInputLayout =
            bottomSheetViewProjects.findViewById(R.id.inputProjectDesc)!!


        val edtTitle: TextInputEditText =
            bottomSheetViewProjects.findViewById(R.id.edtTitle)!!
        val edtFromMonth: AutoCompleteTextView =
            bottomSheetViewProjects.findViewById(R.id.edtFromMonth)!!
        val edtFromYear: AutoCompleteTextView =
            bottomSheetViewProjects.findViewById(R.id.edtFromYear)!!
        val edtProjectDesc: TextInputEditText =
            bottomSheetViewProjects.findViewById(R.id.edtProjectDesc)!!

        projectsProgressbar =
            bottomSheetViewProjects.findViewById(R.id.progress_bar)!!

        if (pos != -1) {
            edtTitle.setText(projectsListAdapter.list[pos].title)
            edtFromMonth.setText(
                monthArrayEn[projectsListAdapter.list[pos].month?.toInt()!! - 1],
                false
            )
            projectMonth = projectsListAdapter.list[pos].month!!.toInt()
            edtFromYear.setText(projectsListAdapter.list[pos].year.toString(), false)
            edtProjectDesc.setText(projectsListAdapter.list[pos].description.toString())
            projectId = projectsListAdapter.list[pos].id!!
        } else {

            edtTitle.setText("")
            edtFromMonth.setText("")
            edtFromYear.setText("")
            edtProjectDesc.setText("")
        }

        /** Year drop down */
        val years: ArrayList<String> = ArrayList()
        val thisYear = Calendar.getInstance()[Calendar.YEAR]
        for (i in 1900..thisYear) {
            years.add(i.toString())
        }
        val yearAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                years
            )
        years.reverse()
        /** Experience screen */
        (edtFromYear as? AutoCompleteTextView)?.setAdapter(
            yearAdapter
        )
        edtFromYear.dropDownWidth =
            WindowManager.LayoutParams.WRAP_CONTENT


        val monthAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                monthArrayEn
            )
        (edtFromMonth as? AutoCompleteTextView)?.setAdapter(
            monthAdapter
        )
        edtFromMonth.setOnItemClickListener { _, _, position, _ ->
            projectMonth = position + 1
            Log.d(mTAG, "updateUi:sMonthId $sMonthId")
        }
        edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT


        edtFromMonth.dropDownWidth =
            WindowManager.LayoutParams.MATCH_PARENT




        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                inputTitle.isErrorEnabled = false
                inputFromMonth.isErrorEnabled = false
                inputFromYear.isErrorEnabled = false
                inputProjectDesc.isErrorEnabled = false
                when {
                    edtTitle.text.toString().isEmpty() -> {
                        inputTitle.error =
                            getString(R.string.error_enter_title)
                        inputTitle.requestFocus()
                    }
                    edtFromMonth.text.toString().isEmpty() -> {
                        inputFromMonth.error =
                            getString(R.string.error_select_month)
                        inputFromMonth.requestFocus()
                    }
                    edtFromYear.text.toString().isEmpty() -> {
                        inputFromYear.error =
                            getString(R.string.error_select_year)
                        inputFromYear.requestFocus()
                    }
                    edtProjectDesc.text.toString().isEmpty() -> {
                        inputProjectDesc.error =
                            getString(R.string.error_please_enter_description)
                        inputProjectDesc.requestFocus()
                    }
                    else -> {
                        if (projectId == -1) {
                            projectsProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.AddProjectEvent(
                                    edtTitle.text.toString(),
                                    projectMonth.toString(),
                                    edtFromYear.text.toString(),
                                    edtProjectDesc.text.toString(),
                                )
                            )
                        } else {
                            projectsProgressbar.visibility = View.VISIBLE
                            viewModel.setStateEvent(
                                EditProfileStateEvent.EditProjectEvent(
                                    edtTitle.text.toString(),
                                    projectMonth.toString(),
                                    edtFromYear.text.toString(),
                                    edtProjectDesc.text.toString(),
                                    projectId
                                )
                            )
                        }
                    }
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogProjects.dismiss()
        }

        bottomSheetDialogProjects.setContentView(bottomSheetViewProjects)
        bottomSheetDialogProjects.show()
    }

   inner class EmojiFilter : InputFilter{
        override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int,
                            dend: Int
        ): CharSequence {
            if(source == null || source.isBlank()){
                return ""
            }
            return source
        }
    }

    private fun languagesAddEditBottomSheet(pos: Int) {
        bottomSheetDialogLanguage = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewLanguage =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_language_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewLanguage.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewLanguage.findViewById(R.id.txtSave)!!



        val inputLanguage: TextInputLayout =
            bottomSheetViewLanguage.findViewById(R.id.inputLanguage)!!
        val inputProficiency: TextInputLayout =
            bottomSheetViewLanguage.findViewById(R.id.inputProficiency)!!

        val edtLanguage: AutoCompleteTextView =
            bottomSheetViewLanguage.findViewById(R.id.edtLanguage)!!
        val edtProficiency: AutoCompleteTextView =
            bottomSheetViewLanguage.findViewById(R.id.edtProficiency)!!


        /**
         * Set Language list into dropdown
         */
        val  languageListAdapter : LanguagesListAdapter =
            LanguagesListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                languageListResponseBean.langs!!
            )

        edtLanguage.setAdapter(languageListAdapter)
        edtLanguage.setOnClickListener {
            edtLanguage.showDropDown()
        }
        edtLanguage.setOnItemClickListener { _, _, position, _ ->


                edtLanguage.setText(languageListAdapter.getSpecialityList()[position].name)


            lanugageId = languageListAdapter.getSpecialityList()[position].id!!.toString()
            languageListAdapter.notifyDataSetChanged()
        }
        val emojiFilter = EmojiFilter()
        edtLanguage.filters = arrayOf(emojiFilter)

        languageProgressbar =
            bottomSheetViewLanguage.findViewById(R.id.progress_bar)!!
        edtLanguage.setText("")
        edtProficiency.setText("")
        /**
         *  Set Language list into dropdown
         */
        computerLevelDropDownListAdapter =
            ComputerLevelDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                computerLevelResponseBean.levels!!,
                prefManager
            )

        edtProficiency.setAdapter(computerLevelDropDownListAdapter)

        edtProficiency.setOnClickListener {
            edtProficiency.showDropDown()
        }

        edtProficiency.setOnItemClickListener { _, _, position, _ ->
//            speciality_id = specialityDropDownListAdapter.getSpecialityList()[position].id
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                edtProficiency.setText(
                    computerLevelDropDownListAdapter.getList()[position].title_en
                )
            } else {
                edtProficiency.setText(
                    computerLevelDropDownListAdapter.getList()[position].title_ar
                )
            }
            computer_level = computerLevelDropDownListAdapter.getList()[position].id!!
            computerLevelDropDownListAdapter.notifyDataSetChanged()
        }


        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                inputLanguage.isErrorEnabled = false
                inputProficiency.isErrorEnabled = false
                when {
                    edtLanguage.text.toString().isEmpty() -> {
                        inputLanguage.error =
                            getString(R.string.error_enter_lanugage)
                        inputLanguage.requestFocus()
                    }
                    edtProficiency.text.toString().isEmpty() -> {
                        inputProficiency.error =
                            getString(R.string.error_select_proficiency)
                        inputProficiency.requestFocus()
                    }
                    else -> {
                        languageProgressbar.visibility = View.VISIBLE
                        viewModel.setStateEvent(
                            EditProfileStateEvent.SaveLanguageEvent(
                                lanugageId,
                                computer_level.toString()
                            )
                        )
                    }
                }
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogLanguage.dismiss()
        }

        bottomSheetDialogLanguage.setContentView(bottomSheetViewLanguage)
        bottomSheetDialogLanguage.show()
    }


    private fun socialAddEditBottomSheet() {
        bottomSheetDialogSocial = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetViewSocial =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_social_add_edit,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val txtCancel: TextView = bottomSheetViewSocial.findViewById(R.id.txtCancel)!!
        val txtSave: TextView = bottomSheetViewSocial.findViewById(R.id.txtSave)!!


        val edtFb: EditText = bottomSheetViewSocial.findViewById(R.id.edtFb)!!
        val edtLink: EditText = bottomSheetViewSocial.findViewById(R.id.edtLink)!!
        val edtTwitt: EditText = bottomSheetViewSocial.findViewById(R.id.edtTwitt)!!
        val edtYoutube: EditText = bottomSheetViewSocial.findViewById(R.id.edtYoutube)!!
        val edtInsta: EditText = bottomSheetViewSocial.findViewById(R.id.edtInsta)!!
        val edtWeb: EditText = bottomSheetViewSocial.findViewById(R.id.edtWeb)!!

        edtFb.setText(profileResponseBean.user?.job_seeker?.fb ?: "")
        edtLink.setText(profileResponseBean.user?.job_seeker?.linkedIn ?: "")
        edtTwitt.setText(profileResponseBean.user?.job_seeker?.twitter ?: "")
        edtYoutube.setText(profileResponseBean.user?.job_seeker?.youtube ?: "")
        edtInsta.setText(profileResponseBean.user?.job_seeker?.instagram ?: "")
        edtWeb.setText(profileResponseBean.user?.job_seeker?.website ?: "")

        socialProgressbar =
            bottomSheetViewSocial.findViewById(R.id.progress_bar)!!


        txtSave.setOnClickListener {
            if (Utils.isConnected(requireContext())) {
                socialProgressbar.visibility = View.VISIBLE
                viewModel.setStateEvent(
                    EditProfileStateEvent.SocialUpdateEvent(
                        edtFb.text.toString(),
                        edtTwitt.text.toString(),
                        edtLink.text.toString(),
                        edtWeb.text.toString(),
                        edtYoutube.text.toString(),
                        edtInsta.text.toString()
                    )
                )
            } else {
                view?.let { Utils.showSnackBar(it, requireContext()) }
            }
        }
        txtCancel.setOnClickListener {
            bottomSheetDialogSocial.dismiss()
        }

        bottomSheetDialogSocial.setContentView(bottomSheetViewSocial)
        bottomSheetDialogSocial.show()
    }
}

