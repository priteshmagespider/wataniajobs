package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.JobRoleListAdapter
import com.fnrco.wataniajobs.adapter.SpecializationDropDownListAdapter
import com.fnrco.wataniajobs.databinding.FragmentEditProfileBinding
import com.fnrco.wataniajobs.mvvm.adapter.JobStatusListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class EditProfileFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentEditProfileBinding
    private lateinit var navController: NavController
    private lateinit var profileResponseBean: ProfileResponseBean

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    private lateinit var specializationList: List<SpecializationBasicData>


    private lateinit var jobStatusListAdapter: JobStatusListAdapter


    private var specializationId: Int? = 0
    private var specializationPosition: Int? = 0
    private var jobRoleId: Int? = 0
    private var job_status: Int = -1
    private lateinit var specializationDropDownListAdapter: SpecializationDropDownListAdapter
    private lateinit var specializationsListResponseBean: BasicDataSpecializationResponse
    private lateinit var jobRoleDropDownListAdapter: JobRoleListAdapter
    private lateinit var jobRoleList: List<Role>
    var functional_status: String = ""


    private var arrayJobStatusSize = arrayOf(
        "I am looking for a job.",
        "I am working and looking for new opportunities.",
        "I am not looking for a job."
    )
    private var arrayJobStatusArSize = arrayOf(
        "أنا أبحث عن عمل.",
        "أنا أعمل وأبحث عن فرص جديدة.",
        "أنا لا أبحث عن عمل."
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditProfileBinding.inflate(layoutInflater)



        if(requireArguments().containsKey(Constant.PROFILE_DATA))
        {
            profileResponseBean =
                requireArguments().getSerializable(Constant.PROFILE_DATA) as ProfileResponseBean
//            specializationPosition = requireArguments().getInt("specializationPosition",0)
//            Utils.print("data---->$specializationPosition")
//            if(specializationPosition!=0)
//            {
//                specializationId = specializationDropDownListAdapter.getSpecialityList()[specializationPosition!!].id
//            }
            setData(profileResponseBean)
        }


        viewModel.setStateEvent(
            EditProfileStateEvent.HomeSpecializationList(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )

        initUi()
        getProfileDataApiCall()
        setObserver()
        return binding.root
    }

    private fun setObserver() {
        /** Basic Data Update Interest  */
        viewModel.basicDataUpdate.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<BasicDataUpdateResponseBean> -> {
                    displayProgressBar(false)

                    /*findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "name", binding.edtName.text.toString(),
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "job_title", binding.edtTheJobTitle.text.toString()
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "specialization", binding.edtSpecialization.text.toString()
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "career_role", binding.edtCareerRole.text.toString()
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "functional_status", binding.edtFunctionalStatus.text.toString()
                    )
                    findNavController().popBackStack()*/

                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "API_CALL", "API_CALL"
                    )
                    findNavController().popBackStack()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Category list response */
        viewModel.categoryListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<CategoriesListResponseBean> -> {
                    displayProgressBar(false)
//                    setDropDownJobRole(dataState.data)
                    jobRoleList = dataState.data.categories!!
                    viewModel.setStateEvent(
                        EditProfileStateEvent.EditProfileCity(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!
                        )
                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }


        /** Specialization */
        viewModel.specializationListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<BasicDataSpecializationResponse> -> {
                    displayProgressBar(false)
                    setDropDownSpecialization(dataState.data)
                    specializationList = dataState.data.specializations!!

                    viewModel.setStateEvent(
                        EditProfileStateEvent.HomeCategoriesList(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!
                        )
                    )
                    viewModel.setStateEvent(
                        EditProfileStateEvent.QualificationList(
                            prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
                        )
                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /**User Profile data */
        viewModel.profileDataResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<ProfileResponseBean> -> {
                    displayProgressBar(false)
                    Log.d(mTAG, "setObserver: ${dataState.data}")
                    /*   listener?.setProfileScreenData(
                           dataState.data.user?.job_seeker?.cv!!, dataState.data.user?.email!!,
                           dataState.data.user?.job_seeker.phone!!
                       )*/
                    setData(dataState.data)
                    profileResponseBean = dataState.data
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

    }


    private fun setDropDownJobRole(categoriesResponseBean: List<Role>) {
        /**
         *  Set JobRole list into dropdown
         */
        jobRoleDropDownListAdapter =
            JobRoleListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                categoriesResponseBean
            )

        binding.edtCareerRole.setAdapter(jobRoleDropDownListAdapter)
        binding.edtCareerRole.setOnClickListener {
            binding.edtCareerRole.showDropDown()
        }
        binding.edtCareerRole.setOnItemClickListener { _, _, position, _ ->
            jobRoleId = jobRoleDropDownListAdapter.getSpecialityList()[position].id
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.edtCareerRole.setText(jobRoleDropDownListAdapter.getSpecialityList()[position].title_en)
            } else {
                binding.edtCareerRole.setText(jobRoleDropDownListAdapter.getSpecialityList()[position].title)
            }
            jobRoleId = jobRoleDropDownListAdapter.getSpecialityList()[position].id!!
            jobRoleDropDownListAdapter.notifyDataSetChanged()
        }
    }

    private fun initUi() {
        binding.txtSave.setOnClickListener(this)
        binding.txtCancel.setOnClickListener(this)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val arrayJobStatusSize: MutableList<String> = ArrayList()
            arrayJobStatusSize.add("I am looking for a job.")
            arrayJobStatusSize.add("I am working and looking for new opportunities.")
            arrayJobStatusSize.add("I am not looking for a job.")

            jobStatusListAdapter =
                JobStatusListAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayJobStatusSize,
                    prefManager
                )

            binding.edtFunctionalStatus.setAdapter(jobStatusListAdapter)
            binding.edtFunctionalStatus.setOnClickListener {
                binding.edtFunctionalStatus.showDropDown()
            }
            binding.edtFunctionalStatus.setOnItemClickListener { _, _, position, _ ->
//            speciality_id = specialityDropDownListAdapter.getSpecialityList()[position].id
                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                    binding.edtFunctionalStatus.setText(jobStatusListAdapter.getSpecialityList()[position])
                } else {
                    binding.edtFunctionalStatus.setText(jobStatusListAdapter.getSpecialityList()[position])
                }
                job_status = position + 1
                Log.d(mTAG, "updateUi:job_status $job_status")
                jobStatusListAdapter.notifyDataSetChanged()
            }
        } else {
            val arrayJobStatusArSize: MutableList<String> = ArrayList()
            arrayJobStatusArSize.add("أنا أبحث عن عمل.")
            arrayJobStatusArSize.add("أنا أعمل وأبحث عن فرص جديدة.")
            arrayJobStatusArSize.add("أنا لا أبحث عن عمل.")

            jobStatusListAdapter =
                JobStatusListAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayJobStatusArSize,
                    prefManager
                )

            binding.edtFunctionalStatus.setAdapter(jobStatusListAdapter)
            binding.edtFunctionalStatus.setOnClickListener {
                binding.edtFunctionalStatus.showDropDown()
            }
            binding.edtFunctionalStatus.setOnItemClickListener { _, _, position, _ ->
                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                    binding.edtFunctionalStatus.setText(jobStatusListAdapter.getSpecialityList()[position])
                } else {
                    binding.edtFunctionalStatus.setText(jobStatusListAdapter.getSpecialityList()[position])
                }
                job_status = position + 1
                Log.d(mTAG, "updateUi:job_status $job_status")
                jobStatusListAdapter.notifyDataSetChanged()
            }
        }


        /*if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val companySizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayJobStatusSize
                )

            (binding.edtFunctionalStatus as? AutoCompleteTextView)?.setAdapter(
                companySizeAdapter
            )
            binding.edtFunctionalStatus.setOnItemClickListener { _, _, position, _ ->
                job_status = position + 1
                Log.d(mTAG, "updateUi:job_status $job_status")
            }
            binding.edtFunctionalStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        } else {
            val companySizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayJobStatusArSize
                )
            (binding.edtFunctionalStatus as? AutoCompleteTextView)?.setAdapter(
                companySizeAdapter
            )
            binding.edtFunctionalStatus.setOnItemClickListener { _, _, position, _ ->
                job_status = position + 1
                Log.d(mTAG, "updateUi:job_status $job_status")
            }
            binding.edtFunctionalStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        }*/
    }

    private fun setData(data: ProfileResponseBean) {
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            binding.edtName.setText(data.user?.name_en ?: data.user?.name)
            binding.edtTheJobTitle.setText(if(data.user?.job_seeker?.job_title_en != null) data.user?.job_seeker?.job_title_en else "")
//            binding.edtSpecialization.setText("${data.user?.job_seeker?.sepecilization?.title_en}")

            binding.edtSpecialization.setText(if(data.user?.job_seeker?.sepecilization?.title_en!= null) data.user?.job_seeker?.sepecilization?.title_en else "" )
            specializationId = data.user?.job_seeker?.sepecilization?.id
            binding.edtCareerRole.setText(if(data.user?.job_seeker?.job_role?.title_en!=null) data.user?.job_seeker?.job_role?.title_en else "")
            jobRoleId = data.user?.job_seeker?.job_role?.id
            binding.edtFunctionalStatus.setText(if(data.user?.job_seeker?.job_role?.title_en!=null) data.user?.job_seeker?.job_role?.title_en else "")

            if (data.user?.job_seeker?.job_status != null) {
                functional_status =
                    arrayJobStatusSize[if(data.user?.job_seeker?.job_status!!.toInt() ==0) data.user?.job_seeker?.job_status!!.toInt() else data.user?.job_seeker?.job_status!!.toInt() - 1]
                binding.edtFunctionalStatus.setText(functional_status, false)
                job_status = data.user?.job_seeker?.job_status!!.toInt()
            }
        } else {
            binding.edtName.setText(data.user?.name?:data.user?.name_en)
            binding.edtTheJobTitle.setText(if(data.user?.job_seeker?.job_title!=null) data.user?.job_seeker?.job_title else "")
            binding.edtSpecialization.setText(if(data.user?.job_seeker?.sepecilization?.title!=null) data.user?.job_seeker?.sepecilization?.title else "")
            specializationId = data.user?.job_seeker?.sepecilization?.id
            binding.edtCareerRole.setText(if(data.user?.job_seeker?.job_role?.title!=null) data.user?.job_seeker?.job_role?.title else "")
            jobRoleId = data.user?.job_seeker?.job_role?.id
            binding.edtFunctionalStatus.setText(if(data.user?.job_seeker?.job_role?.title!=null) data.user?.job_seeker?.job_role?.title else "")
            if (data.user?.job_seeker?.job_status != null) {
                val jobStatus: Int = data.user?.job_seeker?.job_status?.toInt()!!
                functional_status =
                    arrayJobStatusArSize[jobStatus - 1]
                binding.edtFunctionalStatus.setText(functional_status, false)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
    }


    private fun getProfileDataApiCall() {
        viewModel.setStateEvent(
            EditProfileStateEvent.ProfileData(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCancel -> {
                findNavController().popBackStack()
            }

            R.id.txtSave -> {
                if (Utils.isConnected(requireContext())) {
                    Utils.print("SPECIALIZATION------------->$specializationId")
                    if (isBasicDataValid()) {
                        viewModel.setStateEvent(
                            EditProfileStateEvent.BasicDataUpdateEvent(
                                binding.edtName.text.toString(),
                                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") binding.edtTheJobTitle.text.toString() else binding.edtTheJobTitle.text.toString(),
                                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") binding.edtTheJobTitle.text.toString() else binding.edtTheJobTitle.text.toString(),
                                specializationId!!,
                                jobRoleId!!,
                                if (job_status == -1) 1 else job_status
                            )
                        )
                    }
                } else {
                    view.let { Utils.showSnackBar(it, requireContext()) }
                }
            }


//            action_editingTheProfileFragment_to_editProfileFragment
        }
    }


    private fun isBasicDataValid(): Boolean {
//        binding.inputName.isErrorEnabled = false
//        binding.inputTheJobTitle.isErrorEnabled = false
        binding.inputSpecialization.isErrorEnabled = false
        binding.inputCareerRole.isErrorEnabled = false
        binding.inputFunctionalStatus.isErrorEnabled = false

        when {
            binding.edtName.text.toString().isEmpty() -> {
                binding.edtName.error =
                    getString(R.string.error_enter_full_name)
                binding.edtName.requestFocus()
                return false
            }
            binding.edtTheJobTitle.text.toString().isEmpty() -> {
                binding.edtTheJobTitle.error =
                    getString(R.string.error_enter_job_title)
                binding.edtTheJobTitle.requestFocus()
                return false
            }
            binding.edtSpecialization.text.toString().isEmpty() -> {
                binding.inputSpecialization.error =
                    getString(R.string.error_enter_specialization)
                binding.inputSpecialization.requestFocus()
                return false
            }

            binding.edtCareerRole.text.toString().isEmpty() -> {
                binding.inputCareerRole.error =
                    getString(R.string.error_enter_career_role)
                binding.inputCareerRole.requestFocus()
                return false
            }
            binding.edtFunctionalStatus.text.toString().isEmpty() -> {
                binding.inputFunctionalStatus.error =
                    getString(R.string.error_enter_functional_status)
                binding.inputFunctionalStatus.requestFocus()
                return false
            }
        }

        return true
    }

    private fun setDropDownSpecialization(specializationsListResponseBean: BasicDataSpecializationResponse) {
        /**
         * Set Specialization list into dropdown
         */
        specializationDropDownListAdapter =
            SpecializationDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                specializationsListResponseBean.specializations!!,
                prefManager
            )
        binding.edtSpecialization.setAdapter(specializationDropDownListAdapter)
        binding.edtSpecialization.setOnClickListener {
            binding.edtSpecialization.showDropDown()
        }
        binding.edtSpecialization.setOnItemClickListener { _, _, position, _ ->
            if(specializationDropDownListAdapter.getSpecialityList()[position].roles != null)
            setDropDownJobRole(specializationDropDownListAdapter.getSpecialityList()[position].roles!!)
            specializationId = specializationDropDownListAdapter.getSpecialityList()[position].id
            specializationPosition = position
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.edtSpecialization.setText(
                    specializationDropDownListAdapter.getSpecialityList()[position].title_en
                )
            } else {
                binding.edtSpecialization.setText(
                    specializationDropDownListAdapter.getSpecialityList()[position].title
                )
            }
            specializationDropDownListAdapter.notifyDataSetChanged()
        }
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


}