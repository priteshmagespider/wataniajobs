package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentChangePasswordBinding
import com.fnrco.wataniajobs.mvvm.model.response.ChangePasswordResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.ForgotPasswordEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.ForgotPasswordViewModel
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class ChangePasswordFragment : Fragment(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentChangePasswordBinding
    lateinit var navController: NavController

    @ExperimentalCoroutinesApi
    private val viewModel by viewModels<ForgotPasswordViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangePasswordBinding.inflate(inflater, container, false)
        setObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()

    }

    private fun initUi() {
        binding.btnSubmit.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnSubmit -> {
                if (Utils.isConnected(requireContext())) {
                    if (isValid()) {
                        viewModel.setForgotPasswordApiCall(
                            ForgotPasswordEvent.ChangePasswordEvent(
                                binding.edtCurrentPassword.text.toString(),
                                binding.edtNewPassword.text.toString()
                            )
                        )
                    }

                } else {
                    Utils.showSnackBar(view, requireContext())
                }
            }
        }
    }

    private fun isValid(): Boolean {
        binding.inputCurrentPassword.isErrorEnabled = false
        binding.inputNewPassword.isErrorEnabled = false
        binding.inputConfirmPassword.isErrorEnabled = false

        when {
            binding.edtCurrentPassword.text.toString().isEmpty() -> {
                binding.inputCurrentPassword.error =
                    getString(R.string.error_enter_current_password)
                binding.inputCurrentPassword.requestFocus()
                return false
            }
            binding.edtNewPassword.text.toString().isEmpty() -> {
                binding.inputNewPassword.error = getString(R.string.error_enter_new_password)
                binding.inputNewPassword.requestFocus()
                return false
            }
            binding.edtNewPassword.text.toString().length < 6 -> {
                binding.inputNewPassword.error = getString(R.string.error_enter_password_long)
                binding.inputNewPassword.requestFocus()
                return false
            }
            binding.edtConfirmPassword.text.toString() != binding.edtNewPassword.text.toString() -> {
                binding.inputConfirmPassword.error =
                    getString(R.string.error_enter_confirm_password_not_match)
                binding.inputConfirmPassword.requestFocus()
                return false
            }
        }
        return true
    }

    private fun setObserver() {
        /** Change Password Response*/
        viewModel.changePasswordResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<ChangePasswordResponseBean> -> {
                    displayProgressBar(false)
                    displayError(dataState.data.result)
                    if (dataState.data.result == "password Change Successfully") {
                        binding.edtCurrentPassword.setText("")
                        binding.edtNewPassword.setText("")
                        binding.edtConfirmPassword.setText("")
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

}