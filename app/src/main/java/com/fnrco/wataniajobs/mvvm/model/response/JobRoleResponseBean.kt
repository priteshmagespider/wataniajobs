package com.fnrco.wataniajobs.mvvm.model.response

data class JobRoleResponseBean(
    var categories: List<Role>? = null
)

data class Category(
    var id: Int? = null,
    var title_en: String? = null,
    var title: String? = null,
    var cat_id: Int? = null,
    var count: Int? = null,
    val created_at: String? =null,
    val updated_at: String? = null
)