package com.fnrco.wataniajobs.mvvm.model.response

data class TopJobsResponseBean(
    var posts: PostsTopJobs? = null
)

data class PostsTopJobs(
    var current_page: Int? = null,
    var `data`: List<DataTopJobs>? = null,
    var first_page_url: String? = null,
    var from: Int? = null,
    var last_page: Int? = null,
    var last_page_url: String? = null,
    var next_page_url: String? = null,
    var path: String? = null,
    var per_page: Int? = null,
    var prev_page_url: Any? = null,
    var to: Int? = null,
    var total: Int? = null
)

data class DataTopJobs(
    var id: Int? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var Description: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: Any? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: Any? = null,
    var computer_level: Any? = null,
    var MsOffice_level: String? = null,
    var other_condition: Any? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var company_logo: String? = null,
    var is_favourite: Int? = null,
    var statue_job: Any? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: Any? = null,
    var job_id: Any? = null,
    var website: Any? = null,
    var com_email: Any? = null,
    var com_name: Any? = null,
    var com_logo: Any? = null,
    var job_date: Any? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: Any? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var city: Any? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: Any? = null,
    var job_role: JobRoleTopJobs? = null,
    var specialization: SpecializationTopJobs? = null,
    var qualification: QualificationTopJobs? = null,
    var job__type: JobTypeTopJobs? = null,
    var company: CompanyTopJobs? = null,
    var job_city: JobCityTopJobs? = null,
    var language_level: Any? = null,
    var office_level: Any? = null
)

data class JobRoleTopJobs(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class SpecializationTopJobs(
    var id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class QualificationTopJobs(
    var id: Int? = null,
    var title_en: String? = null
)

data class JobTypeTopJobs(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)

data class JobCityTopJobs(
    var id: Int? = null,
    var name_en: String? = null,
    var name_ar: String? = null
)

data class CompanyTopJobs(
    var id: Int? = null,
    var user_id: Int? = null,
    var comNum: Any? = null,
    var sector: Any? = null,
    var mobile: Any? = null,
    var phone: Any? = null,
    var website: Any? = null,
    var city: Any? = null,
    var address: String? = null,
    var logo: String? = null,
    var photo_approve: Any? = null,
    var size: Any? = null,
    var `field`: Any? = null,
    var user_job_title: Any? = null,
    var status: Int? = null,
    var phone_is_verified: Int? = null,
    var sms_try: Int? = null,
    var phone_verification_code: Any? = null,
    var career_objective: Any? = null,
    var package_id: Any? = null,
    var total_cv: Any? = null,
    var expiration_date: Any? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)