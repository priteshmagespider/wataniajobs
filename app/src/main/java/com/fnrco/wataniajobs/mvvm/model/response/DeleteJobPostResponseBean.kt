package com.fnrco.wataniajobs.mvvm.model.response

data class DeleteJobPostResponseBean(
    var message: String? = null
)