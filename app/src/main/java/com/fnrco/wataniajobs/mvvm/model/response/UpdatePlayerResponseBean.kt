package com.fnrco.wataniajobs.mvvm.model.response

data class UpdatePlayerResponseBean(
    var message: String? = null
)