package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobPost
import com.fnrco.wataniajobs.mvvm.model.response.ExpiredJobPost
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.card.MaterialCardView

class ExpireJobApplicationAdapter(
    var context: Context,
    val onClickListener: View.OnClickListener,
    var prefManager: PrefManager
) : RecyclerView.Adapter<ExpireJobApplicationAdapter.MyViewHolder>() {
    lateinit var arrayListPosts : ArrayList<ExpiredJobPost>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardCurrentJobApplication: MaterialCardView =
            itemView.findViewById(R.id.cardCurrentJobApplication)
        val imgCompanyLogo: ImageView = itemView.findViewById(R.id.imgCompanyLogo)
        val imgMoreOption: ImageView = itemView.findViewById(R.id.imgMoreOption)
        val txtJobTitle: TextView = itemView.findViewById(R.id.txtJobTitle)
        val txtJobCity: TextView = itemView.findViewById(R.id.txtJobCity)
        val txtLocation: TextView = itemView.findViewById(R.id.txtLocation)
        val txtJobQualification: TextView = itemView.findViewById(R.id.txtJobQualification)
        val txtSubMissionDateStart: TextView = itemView.findViewById(R.id.txtSubMissionDateStart)
        val txtCompletionDate: TextView = itemView.findViewById(R.id.txtCompletionDate)
        val txtReviewStatus: TextView = itemView.findViewById(R.id.txtReviewStatus)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_expire_job_application_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtJobTitle.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtJobCity.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtLocation.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtJobQualification.textDirection = View.TEXT_DIRECTION_RTL
        holder.txtSubMissionDateStart.textDirection = View.TEXT_DIRECTION_RTL
//      /  holder.txtCompletionDate.textDirection = View.TEXT_DIRECTION_RTL




        arrayListPosts[position].job_city?.let { safeCompanyValue ->
            holder.txtLocation.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en")  safeCompanyValue.name_en else safeCompanyValue.name_ar
        } ?: run {
            holder.txtLocation.text = ""
        }



        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtJobTitle.text = arrayListPosts[position].title
            holder.txtJobQualification.text = arrayListPosts[position].com_name.toString()
//            arrayListPosts[position].qualification?.let { safeQualificationValue ->
//                holder.txtJobQualification.text = safeQualificationValue.title_en
//            } ?: run {
//                holder.txtJobQualification.text = ""
//            }

//            arrayListPosts[position].job_city?.let { safeJobCityValue ->
//                holder.txtJobCity.text = safeJobCityValue.name_en
//            } ?: run {
//                holder.txtJobCity.text = ""
//            }


        } else {
            holder.txtJobTitle.text = arrayListPosts[position].title
            holder.txtJobQualification.text = arrayListPosts[position].com_name.toString()
//            arrayListPosts[position].qualification?.let { safeQualificationValue ->
//                holder.txtJobQualification.text = safeQualificationValue.title_ar
//            } ?: run {
//                holder.txtJobQualification.text = ""
//            }

            arrayListPosts[position].job_city?.let { safeJobCityValue ->
                holder.txtJobCity.text = safeJobCityValue.name_ar
            } ?: run {
                holder.txtJobCity.text = ""
            }

        }

        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${arrayListPosts[position].company_logo}")
            .placeholder(R.drawable.img_home)
            .into(holder.imgCompanyLogo)

//        if(arrayListPosts[position].status == "1")
//        {
//            holder.txtReviewStatus.text = context.resources.getString(R.string.lbl_your_application_has_been_reviewed)
//        }else{
//            holder.txtReviewStatus.text = context.resources.getString(R.string.lbl_your_application_has_been_reviewed)
//        }

        holder.imgMoreOption.tag = position
        holder.imgMoreOption.setOnClickListener(onClickListener)
        Log.d("TAG", "onBindViewHolder CREATED_AT: ${arrayListPosts[position].end_at}")
        Log.d("TAG", "onBindViewHolder CREATED_AT: ${arrayListPosts[position].created_at}")
        //Utils.parseDateToddMMyyyy(arrayListPosts[position].created_at,"yyyy-MM-dd HH:mm:ss","dd-MM-yyyy")
       if(arrayListPosts[position].created_at !=null){
           holder.txtSubMissionDateStart.text =
               context.resources.getString(R.string.lbl_submission_date) + ":" + Utils.parseDateToddMMyyyy(arrayListPosts?.get(position)!!.created_at,"yyyy-MM-dd HH:mm:ss","dd-MM-yyyy")
       }

        if(arrayListPosts?.get(position).end_at!=null){
            holder.txtCompletionDate.text =
                context.resources.getString(R.string.lbl_completion_date) + ":" +  arrayListPosts?.get(position)!!.end_at
        }



    }

    override fun getItemCount(): Int {
        return arrayListPosts.size
    }

    fun addData(list :List<ExpiredJobPost>)
    {
        arrayListPosts = ArrayList()
        arrayListPosts.addAll(list)
    }

    fun removeData(position: Int)
    {
        arrayListPosts.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getList() : ArrayList<ExpiredJobPost>{
        return arrayListPosts
    }

}