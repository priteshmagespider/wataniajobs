package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class FavouriteJobResponseBean(
    var posts: PostsFavouriteJob? = null
)

data class PostsFavouriteJob(
    var current_page: Int? = null,
    var `data`: List<DataFavouriteJob>? = null,
    var first_page_url: String? = null,
    var from: Int? = null,
    var last_page: Int? = null,
    var last_page_url: String? = null,
    var next_page_url: String? = null,
    var path: String? = null,
    var per_page: Int? = null,
    var prev_page_url: String? = null,
    var to: Int? = null,
    var total: Int? = null
)

data class DataFavouriteJob(
    var id: Int? = null,
    var com_id: String? = null,
    var title: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: String? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: String? = null,
    var computer_level: ComputerLevelFavouriteJob? = null,
    var MsOffice_level: String? = null,
    var other_condition: String? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: String? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: String? = null,
    var job_id: Int? = null,
    var website: String? = null,
    var com_email: String? = null,
    var com_name: String? = null,
    var com_logo: String? = null,
    var job_date: String? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var job_city: JobCityFavouriteJob? = null,
    var city: String? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: String? = null,
    var description: String? = null,
    var company_logo: String? = null,
    var is_favourite: Int? = null,
    var job_role: JobRoleFavouriteJob? = null,
    var specialization: SpecializationFavouriteJob? = null,
    var job_applicants: List<JobApplicantFavouriteJob>? = null,
    var qualification: QualificationFavouriteJob? = null,
    var job__type: JobTypeFavouriteJob? = null,
    var language_level: LanguageLevelFavouriteJob? = null,
    var office_level: OfficeLevelFavouriteJob? = null,
    var company: CompanyFavouriteJob? = null
)


data class CompanyFavouriteJob(
    var id: Int? = 0,
//    var user_id: UserId? = null,
    var user_id: Int? = null,
    var comNum: Int? = 0,
    var sector: String? = null,
    var mobile: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var city: String? = null,
    var address: String? = null,
    var logo: String? = null,
    var photo_approve: Int? = 0,
    var size: String? = null,
    var `field`: String? = null,
    var user_job_title: String? = null,
    var status: Int? = 0,
    var phone_is_verified: Int? = 0,
    var sms_try: Int? = 0,
    var phone_verification_code: Int? = 0,
    var career_objective: String? = null,
    var package_id: Int? = 0,
    var total_cv: Int? = 0,
    var expiration_date: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
):Serializable

data class JobCityFavouriteJob(
    var id: Int? = 0,
    var name_en: String? = null,
    var name_ar: String? = null
): Serializable

data class ComputerLevelFavouriteJob(
    var id: Int? = null,
    var title_ar: String? = null
)

data class JobRoleFavouriteJob(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class SpecializationFavouriteJob(
    var id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class JobApplicantFavouriteJob(
    var job_seeker_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var pivot: PivotFavouriteJob? = null
)

data class QualificationFavouriteJob(
    var id: Int? = null,
    var title_ar: String? = null
)

data class JobTypeFavouriteJob(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null
)

data class LanguageLevelFavouriteJob(
    var id: Int? = null,
    var title_ar: String? = null
)

data class OfficeLevelFavouriteJob(
    var id: Int? = null,
    var title_ar: String? = null
)

data class PivotFavouriteJob(
    var jobPost_id: Int? = null,
    var jobSeeker_id: Int? = null
)