package com.fnrco.wataniajobs.mvvm.model.response


data class CityListResponseBean(
    var cities: List<City>? = null
)

data class City(
    var id: Int? = null,
    var name_en: String? = null,
    var name_ar: String? = null
)