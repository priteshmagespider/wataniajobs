package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Lang


class LanguageListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Lang>
) : RecyclerView.Adapter<LanguageListAdapter.MyViewHolder>() {

    val listData = list as MutableList<Lang>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtLanguageName: TextView = itemView.findViewById(R.id.txtLanguageName)
        val txtLanguageProf: TextView = itemView.findViewById(R.id.txtLanguageProf)
        val imgLanguageDelete: ImageView = itemView.findViewById(R.id.imgLanguageDelete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_language_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgLanguageDelete.tag = position
        holder.imgLanguageDelete.setOnClickListener(onClickListener)

        holder.txtLanguageName.text = listData[position].language?.name.toString()
        holder.txtLanguageProf.text = listData[position].level?.title.toString()

    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}