package com.fnrco.wataniajobs.mvvm.model.response

data class ExpiredJobResponseBean(
    val posts: List<ExpiredJobPost>?,
    val type: String?
)

data class ExpiredJobPost(
    val id: Int?,
    val city: Any?,
    val com_logo: Any?,
    val com_name: Any?,
    val country: String?,
    val created_at: String?,
    val end_at: String?,
    val company_logo: String?,
    val is_favourite: Int?,
    val jobCategory: Int?,
    val job_city: JobCity?,
    val job_role: JobRole?,
    val photo_shared: Any?,
    val title: String?,
    val website: Any?
)

data class ExpiredJobJobCity(
    val id: Int?,
    val name_en: String?
)

data class ExpiredJobJobRole(
    val id: Int?,
    val title_en: Any?
)