package com.fnrco.wataniajobs.mvvm.model.response

data class BasicDataSpecializationResponse(
    var specializations: List<SpecializationBasicData>? = null

)

data class RoleBasicData(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)


data class SpecializationBasicData(
    var id: Int? = null,
    var title_en: String? = null,
    var title: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var count: Int? = null,
    var roles: List<Role>? = null
)