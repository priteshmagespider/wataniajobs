package com.fnrco.wataniajobs.mvvm.model.response

data class DeleteEducationResponseBean(
    val status: Boolean? = null
)