package com.fnrco.wataniajobs.mvvm.model.response

data class LoginResponseBean(
    val access_token: String?,
    val expires_in: Int?,
    val refresh_token: String?,
    val token_type: String?,
    val user: LoginUser?,
    val message: String?,
    val error: String? = null

)

data class LoginUser(
    val active: Any?,
    val code_win: Any?,
    val created_at: String?,
    val email: String?,
    val enter_by: Any?,
    val id: Int?,
    val is_admin: Int?,
    val mail: Any?,
    val name: String?,
    val photo: String?,
    val name_en: String?,
    val provider: Any?,
    val provider_id: Any?,
    val role: String?,
    val token: String?,
    val updated_at: String?
)