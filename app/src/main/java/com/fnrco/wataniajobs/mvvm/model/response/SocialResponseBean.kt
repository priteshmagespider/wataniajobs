package com.fnrco.wataniajobs.mvvm.model.response

data class SocialResponseBean(
    val fb: String? = null,
    val instagram: String? = null,
    val linkedIn: String? = null,
    val twitter: String? = null,
    val website: String? = null,
    val youtube: String? = null
)