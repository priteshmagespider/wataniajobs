package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Client
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class ClientListAdapter(
    var context: Context,
    var list: List<Client>,
    var prefManager: PrefManager
) : RecyclerView.Adapter<ClientListAdapter.MyViewHolder>() {

    var listData: MutableList<Client> = list as MutableList<Client>
    lateinit var clientsImageListAdapter: ClientsImageListAdapter

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtClientIndustry: TextView = itemView.findViewById(R.id.txtClientIndustry)
        var rvClientsImages: RecyclerView = itemView.findViewById(R.id.rvClientsImages)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_client_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            holder.txtClientIndustry.text = listData[position].cat_en
        } else {
            holder.txtClientIndustry.text = listData[position].cat
        }

        holder.rvClientsImages.layoutManager =
            GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
        clientsImageListAdapter = ClientsImageListAdapter(context, listData[position].clients!!)
        holder.rvClientsImages.adapter = clientsImageListAdapter
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}