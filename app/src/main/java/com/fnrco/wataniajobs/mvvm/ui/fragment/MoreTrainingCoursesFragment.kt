package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentMoreTrainingCoursesBinding
import com.fnrco.wataniajobs.mvvm.adapter.TrainingCoursesMoreListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.MoreScreenServiceResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.RegisterTrainingCourse
import com.fnrco.wataniajobs.mvvm.model.response.TrainingCoursesMoreResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class MoreTrainingCoursesFragment : Fragment(), View.OnClickListener {

    private val TAG = MoreTrainingCoursesFragment::class.java.simpleName
    lateinit var binding: FragmentMoreTrainingCoursesBinding
    lateinit var navController: NavController

    lateinit var trainingCoursesMoreListAdapter: TrainingCoursesMoreListAdapter

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: MoreScreenViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObserver()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMoreTrainingCoursesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        binding.txtNoticeText.text = requireActivity().getString(R.string.loud)
        val text = if(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") "<ul>" +
                "<li> The applicant must be a Saudi national.</li>\n" +
                "<li> The applicant must be registered on the site as a job seeker.</li>\n" +
                "<li> The applicant must complete his personal profile 100% to be accepted.</li>\n" +
                "<li> Registration will close after the completion of the total number of the course.</li>\n" +
                "<li> After the application is completed, an email will be sent to the candidates for the course with the date and location of the course.</li>\n" +
                "</ul>" else "<ul>" +
                "<li> يجب أن يكون المتقدم سعودي الجنسية.</li>\n" +
                "<li> لانج. يجب أن يكون المتقدم مسجلاً في الموقع كباحث عن عمل.</li>\n" +
                "<li> يجب على مقدم الطلب إكمال ملفه الشخصي بنسبة 100٪ ليتم قبوله.</li>\n" +
                "<li> سيغلق التسجيل بعد اكتمال العدد الإجمالي للدورة.</li>\n" +
                "<li> بعد اكتمال الطلب ، سيتم إرسال بريد إلكتروني إلى المرشحين للدورة مع تاريخ ومكان الدورة.</li>\n" +
                "</ul>"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.txtNoticeText.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        } else {
            binding.txtNoticeText.text = Html.fromHtml(text)
        }
        if (Utils.isConnected(requireContext())) {
            viewModel.setMoreScreenApiCall(
                MoreScreenEvent.MoreTrainingCoursesEvent(
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
                )
            )
        } else {
            Utils.showSnackBar(binding.mainView, requireContext())
        }
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            binding.txtNoticeText.textDirection = View.TEXT_DIRECTION_LTR
        } else {
            binding.txtNoticeText.textDirection = View.TEXT_DIRECTION_RTL
        }
    }

    private fun setObserver() {
        viewModel.moreTrainingCourseResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<TrainingCoursesMoreResponseBean> -> {
                    displayProgressBar(false)
                    setData(dataState.data)

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(TAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(TAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Register new course */
        viewModel.registerCourseResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<RegisterTrainingCourse> -> {
                    displayProgressBar(false)
//                    displayError(dataState.data.success)
                    viewModel.setMoreScreenApiCall(
                        MoreScreenEvent.MoreTrainingCoursesEvent(
                            prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!
                        )
                    )
                    alreadyCoursesAppliedBottomSheet()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(TAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(TAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun setData(data: TrainingCoursesMoreResponseBean) {
        trainingCoursesMoreListAdapter =
            TrainingCoursesMoreListAdapter(requireContext(), this, data.trainings!!, prefManager)
        binding.rvMoreTrainingCourses.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = trainingCoursesMoreListAdapter
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    "ISO_COURSES", "ISO_COURSES"
                )
                findNavController().popBackStack()
            }
            R.id.btnRegister -> {
                val pos = view.tag.toString().toInt()
                if (trainingCoursesMoreListAdapter.trainingList[pos].is_apply) {
//                    alreadyCoursesAppliedBottomSheet()
                } else {
                    viewModel.setMoreScreenApiCall(
                        MoreScreenEvent.RegisterTrainingCourseEvent(
                            trainingCoursesMoreListAdapter.trainingList[pos].id!!
                        )
                    )
                }


            }
        }
    }

    private fun alreadyCoursesAppliedBottomSheet() {
        val bottomSheetDialogSocial = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        val bottomSheetViewSocial =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_course_applied,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )
        val txtOK: TextView = bottomSheetViewSocial.findViewById(R.id.txtOK)
        txtOK.setOnClickListener {
            bottomSheetDialogSocial.dismiss()
        }

        bottomSheetDialogSocial.setContentView(bottomSheetViewSocial)
        bottomSheetDialogSocial.show()
    }
}