package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Training
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.google.android.material.card.MaterialCardView

class TrainingCoursesMoreListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    var trainingList: List<Training>,
    var prefManager: PrefManager
) : RecyclerView.Adapter<TrainingCoursesMoreListAdapter.MyViewHolder>() {

    val listData: MutableList<Training> = trainingList as MutableList<Training>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardTrainingCourses: MaterialCardView = itemView.findViewById(R.id.cardTrainingCourses)
        val txtCourseTitle: TextView = itemView.findViewById(R.id.txtCourseTitle)
        val txtSessionHeadquarters: TextView = itemView.findViewById(R.id.txtSessionHeadquarters)
        val txtDurationOfTheCourse: TextView = itemView.findViewById(R.id.txtDurationOfTheCourse)
        val txtCourseFees: TextView = itemView.findViewById(R.id.txtCourseFees)
        val btnRegister: Button = itemView.findViewById(R.id.btnRegister)
        val txtOrderNumber: TextView = itemView.findViewById(R.id.txtOrderNumber)
        val txtCoursesDate: TextView = itemView.findViewById(R.id.txtCoursesDate)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_training_courses_more_items_new, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtOrderNumber.text = "AHDFY"+ listData[position].id
        if (listData[position].date == "1") {
            holder.txtCoursesDate.text = context.getString(R.string.lbl_until_number_is_complete)
        }

        holder.btnRegister.tag = position
        holder.btnRegister.setOnClickListener(onClickListener)

        holder.btnRegister.text = if(listData[position].is_apply) context.getString(R.string._course_already_registered) else context.getString(R.string.register_now_training)

        holder.txtCourseTitle.text = listData[position].title_en ?: listData[position].title
        holder.txtSessionHeadquarters.text =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") listData[position].city?.name_en else listData[position].city?.name_ar

        holder.txtDurationOfTheCourse.text =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") listData[position].duration_en else listData[position].duration
        holder.txtCourseFees.text =  if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en")  "SAR ${listData[position].price}" else  " ريال${listData[position].price}"
    }

    override fun getItemCount(): Int = listData.size
}