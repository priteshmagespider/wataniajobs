package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.fnrco.wataniajobs.databinding.ActivitySplashBinding
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import javax.inject.Inject

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivitySplashBinding

    @Inject
    lateinit var prefManager: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Utils.fullScreen(this)


//        val bundle = intent.extras

        /*if (bundle != null) {
            for (key in bundle.keySet()) {
                val value = bundle[key]
                Utils.print(
                    "HomeActivityNotification", String.format(
                        "%s %s (%s)", key,
                        value.toString(), value!!.javaClass.name
                    )
                )
                Utils.print("HomeNotificationData-------->"+value.toString())
                val custom=JSONObject.wrap(bundle.get(key))
                val property: String = bundle as String
                Utils.print("HomeNotificationData---property----->"+property)
                Utils.print("HomeNotificationData---custom----->"+custom)
            }
        }*/


        /*val bundle = intent.extras
        Utils.print("HomeNotificationData-bundle------->$bundle")
        if (bundle != null) {
            *//*val appointment_id = bundle.get("custom")

            Log.d(mTAG, "Call Notification: ${bundle.get("custom")}")
            Log.d(mTAG, "Call Notification: ${bundle.get("doctor_name")}")*//*

        } else {

                initUi()
        }*/
        if (intent.hasExtra("CURRENT_LANG")) {
            prefManager.setString(
                PrefConstant.PREF_CURRENT_LANGUAGE,
                intent.extras!!.getString("CURRENT_LANG")
            )
            Log.d(
                mTAG,
                "CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
            )
            initUi()
        } else {
            Log.d(
                mTAG,
                "CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
            )
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!.isEmpty() || prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)==null) {
                prefManager.setString(
                    PrefConstant.PREF_CURRENT_LANGUAGE,
                    "ar"
                )
                Utils.setLocale(
                    this,
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    ""
                )
            } else {
                Utils.setLocale(
                    this,
                    prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                    ""
                )
//                prefManager.setString(
//                    PrefConstant.PREF_CURRENT_LANGUAGE,
//                    "ar"
//                )
//                Utils.setLocale(this, "ar", "")

            }
            Log.d(
                mTAG,
                "CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
            )
        }
    }

    private fun initUi() {
        Handler(Looper.getMainLooper()).postDelayed({
//            finish()
//            startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
            navigate()
        }, 500)
    }

    fun navigate() {
        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
        finishAffinity()
    }
}