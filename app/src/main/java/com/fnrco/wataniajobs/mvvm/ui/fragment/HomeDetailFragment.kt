package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.JobDetailsAdapter
import com.fnrco.wataniajobs.adapter.ShareListAdapter
import com.fnrco.wataniajobs.adapter.TopJobsAdapter
import com.fnrco.wataniajobs.databinding.FragmentHomeDetailBinding
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginPopUpActivity
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.jsoup.Jsoup
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class HomeDetailFragment : Fragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentHomeDetailBinding
    private lateinit var navController: NavController

    private lateinit var topJobListAdapter: TopJobsAdapter
    private lateinit var jobDetailsListAdapter: JobDetailsAdapter
    private lateinit var sharedListAdapter: ShareListAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var linearLayoutManagerJobDetails: LinearLayoutManager
    private lateinit var linearLayoutManagerShare: LinearLayoutManager
    private var post_id: Int = 0
    private var singlePost: PostSinglePost? = null
    var position = -1
    var isPostLikeClicked: Boolean = false

    @Inject
    lateinit var prefManager: PrefManager


    @ExperimentalCoroutinesApi
    private val viewModel: HomeViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeDetailBinding.inflate(inflater, container, false)

        if (requireArguments().containsKey(Constant.POST_ID)) {
            post_id = requireArguments().getSerializable(Constant.POST_ID) as Int
            Utils.print("POST_ID---------------------->$post_id")
            viewModel.setStateEvent(
                HomeStateEvent.HomeDetailSinglePost(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!, post_id
                )
            )
        }

        viewModel.setStateEvent(
            HomeStateEvent.HomeTopJobs(
                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!,
                1
            )
        )
        initUi()
        setObserver()
        return binding.root
    }

    private fun setObserver() {
        viewModel.singlePostResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SinglePostResponseBean> -> {
                    displayProgressBar(false)
                    singlePost = dataState.data.post
                    if (singlePost!!.applied == 1) {
                        binding.btnApplyForJob.text =
                            requireActivity().getString(R.string.job_applied)
                    }
                    setAllData(dataState.data.post)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.jobPostAppliedResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<JobPostApplyResponseBean> -> {
                    displayProgressBar(false)
//                    Toast.makeText(requireContext(), dataState.data.message, Toast.LENGTH_LONG)
//                        .show()
                    singlePost!!.applied = 1
                    binding.btnApplyForJob.text = requireActivity().getString(R.string.job_applied)
                    alreadyAppliedBottomSheet()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.topJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<TopJobsResponseBean> -> {
                    displayProgressBar(false)
                    linearLayoutManager =
                        LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
                    binding.recyclerViewJobs.layoutManager = linearLayoutManager
                    topJobListAdapter = TopJobsAdapter(
                        requireActivity(), this, dataState.data.posts?.data!!,
                        prefManager
                    )
                    /* binding.recyclerViewJobs.addItemDecoration(
                         DividerItemDecoration(
                             requireContext(), LinearLayoutManager.VERTICAL
                         ).apply {
                             setDrawable(resources.getDrawable(R.drawable.divider_bg))
                         }
                     )*/
                    binding.recyclerViewJobs.adapter = topJobListAdapter
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Home Banner Response */
        viewModel.favouriteJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<FavoriteJobResponseBean> -> {
                    displayProgressBar(false)
                    if (isPostLikeClicked) {
                        singlePost?.is_favourite = 1
                        binding.imgLike.setImageResource(R.drawable.img_fav)
                        binding.imgLike.setColorFilter(requireActivity().getColor(R.color.yellow));
                    } else {
                        topJobListAdapter.list[position].is_favourite = 1
                        topJobListAdapter.notifyItemChanged(position)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Home Banner Response */
        viewModel.removeFavouriteJobResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<RemoveFavoriteJobResponseBean> -> {
                    displayProgressBar(false)
                    if (isPostLikeClicked) {
                        singlePost?.is_favourite = 0
                        binding.imgLike.setImageResource(R.drawable.img_un_fav)
                        binding.imgLike.setColorFilter(requireActivity().getColor(R.color.dark_gray));

                    } else {
                        topJobListAdapter.list[position].is_favourite = 0
                        topJobListAdapter.notifyItemChanged(position)
                    }


                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.singlePostResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SinglePostResponseBean> -> {
                    displayProgressBar(false)
                    singlePost = dataState.data.post


                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }


    private fun setAllData(post: PostSinglePost?) {
        /** Update on 14th Apr 2022*/
        binding.txtTitleValue.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.title_en else post?.title
        binding.txtSpecializationDetail.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.specialization?.title_en else post?.specialization?.title
        binding.txtRoleDetail.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.job_role?.title_en else post?.job_role?.title
        binding.txtRequirementsDetail.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.job_city?.name_en else post?.job_city?.name_ar
        binding.txtExperienceDetail.text = post?.experienceFrom.toString() + " - " + post?.experienceTo.toString()
        binding.txtGenderDetail.text = post?.experienceFrom.toString() + " - " + post?.experienceTo.toString()
        binding.txtSalaryDetail.text = post?.salary
        binding.txtAgeDetail.text = if (post?.age == null) getString(R.string.unspecified) else post.age.toString()
        binding.txtQualificationDetail.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.qualification?.title_en else post?.qualification?.title_ar
        binding.txtMicroSoftOfficeDetail.text = if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.office_level?.title_en else post?.office_level?.title_ar
        binding.txtLicenceDetail.text =   if (post?.driving == "0") getString(R.string.lbl_not_required) else getString(R.string.lbl_required)
        binding.txtEnglishSkillDetail.text =   if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.language_level?.title_en else post?.language_level?.title_ar
        binding.txtEmploymentTypeDetail.text =   if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post?.job__type?.title_en else post?.job__type?.title_ar

        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${post!!.company_logo}")
            .placeholder(R.drawable.img_home)
            .into(binding.imgMain)
        binding.imgLike.apply {
            if (post.is_favourite == 1) {
                this.setImageResource(R.drawable.img_fav)
                this.setColorFilter(context.getColor(R.color.yellow));
            } else {
                this.setImageResource(R.drawable.img_un_fav)
            }
        }

        val aDate = Utils.parseDateToddMMyyyy(post.created_at, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss");
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
            binding.txtTitle.text = post.title_en ?: post.title
            if (post.job_city != null) {
                binding.txtJobCityMain.text = post.job_city!!.name_en
                binding.txtJobCityDetail.text = post.job_city!!.name_en
            }else {
                binding.txtJobCity.visibility = View.GONE
                binding.txtJobCityDetail.visibility = View.GONE
            }

            binding.txtTime.text = post.job__type?.title_en
            if (post.job__type?.title_en == null) binding.txtTime.visibility =
                View.GONE else binding.txtTime.visibility = View.VISIBLE
            Utils.covertTimeToText(aDate, requireContext())

            binding.txtAnnouncement.text = Utils.covertTimeToText(aDate, requireContext())
//            binding.txtAnnouncement.text = "${getString(R.string.announcement_date_)} $aDate"
            if (post.specialization == null)
            {

                binding.txtSpecialityOf.visibility = View.GONE
                binding.txtSpecialityOfDetail.visibility = View.GONE
                binding.txtSpecialityOfDetail.text =
                    ""
            } else binding.txtSpecialityOfDetail.text = post.specialization!!.title_en
            if (post.qualification == null) {
                binding.txtRequiredQuaDetail.visibility = View.GONE
                binding.txtRequiredQua.visibility = View.GONE
                binding.txtRequiredQuaDetail.text =
                    ""
            } else binding.txtRequiredQuaDetail.text = post.qualification!!.title_en
            if (post.computer_level == null) {
                binding.txtComputerDetail.visibility = View.GONE
                binding.txtComputer.visibility = View.GONE
                binding.txtComputerDetail.text =
                    ""
            } else binding.txtComputerDetail.text = post.computer_level!!.title_en
            if (post.office_level == null){
                binding.txtMicroSoftOff.visibility = View.GONE
                binding.txtMicroSoftOffDetail.visibility = View.GONE
                binding.txtMicroSoftOffDetail.text =
                    ""
            } else binding.txtMicroSoftOffDetail.text = post.office_level!!.title_en
            if (post.other_condition_en == null || post.other_condition_en == "") {
                binding.txtOtherCon.visibility = View.GONE
                binding.txtOtherConDetail.visibility = View.GONE
                binding.txtOtherConDetail.text =
                    ""
            } else binding.txtOtherConDetail.text = post.other_condition_en
            if(post.language_level == null){
                binding.txtEnglishDetail.visibility = View.GONE
                binding.txtEnglish.visibility = View.GONE
            }else {
                binding.txtEnglishDetail.text = post.language_level?.title_en
            }
        } else {
            binding.txtTitle.text = post.title ?: post.title_en
            if (post.job_city != null) {
                binding.txtJobCityMain.text = post.job_city!!.name_ar ?: post.job_city!!.name_en
                binding.txtJobCityDetail.text = post.job_city!!.name_ar
            }else {
                binding.txtJobCityDetail.visibility = View.GONE
                binding.txtJobCity.visibility = View.GONE
            }
            binding.txtTime.text = post.job__type?.title_ar
            if (post.job__type?.title_ar == null) binding.txtTime.visibility =
                View.GONE else binding.txtTime.visibility = View.VISIBLE
            binding.txtAnnouncement.text = Utils.covertTimeToText(aDate, requireContext())
            if (post.specialization == null) {
                binding.txtSpecialityOf.visibility = View.GONE
                binding.txtSpecialityOfDetail.visibility = View.GONE
                binding.txtSpecialityOfDetail.text =
                    ""
            } else binding.txtSpecialityOfDetail.text = post.specialization!!.title
            if (post.qualification == null){
                binding.txtRequiredQuaDetail.visibility = View.GONE
                binding.txtRequiredQua.visibility = View.GONE
                binding.txtRequiredQuaDetail.text =
                    ""
            } else binding.txtRequiredQuaDetail.text = post.qualification!!.title_ar
            if (post.computer_level == null) {
                binding.txtComputerDetail.visibility = View.GONE
                binding.txtComputer.visibility = View.GONE
                binding.txtComputerDetail.text =
                    ""
            } else binding.txtComputerDetail.text = post.computer_level!!.title_ar
            if (post.office_level == null) {
                binding.txtMicroSoftOff.visibility = View.GONE
                binding.txtMicroSoftOffDetail.visibility = View.GONE
                binding.txtMicroSoftOffDetail.text =
                    ""
            } else binding.txtMicroSoftOffDetail.text = post.office_level!!.title_ar
            if (post.other_condition == null || post.other_condition == "") {
                binding.txtOtherConDetail.visibility = View.GONE
                binding.txtOtherCon.visibility = View.GONE
                binding.txtOtherConDetail.text =
                    ""
            } else binding.txtOtherConDetail.text = post.other_condition

            if(post.language_level == null){
                binding.txtEnglishDetail.visibility = View.GONE
                binding.txtEnglish.visibility = View.GONE
            }else {
                binding.txtEnglishDetail.text = post.language_level?.title_ar
            }

        }
        if(post.job_role != null){
            binding.txtCareerRoleDetail.text =
                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post.job_role?.title_en else post.job_role?.title

        }else {
            binding.txtCareerRoleDetail.visibility = View.GONE
            binding.txtCareerRole.visibility = View.GONE
        }

        if (post.company == null) binding.txtLocation.visibility =
            View.GONE else binding.txtLocation.visibility = View.VISIBLE

        binding.txtLocation.text =
            post.company?.address


        if(post.experienceFrom == null){
            binding.txtYearsOfExperience.visibility = View.GONE
            binding.txtYearsOfExperienceDetail.visibility = View.GONE
        }else {
            binding.txtYearsOfExperienceDetail.text =
                post.experienceFrom.toString() + " - " + post.experienceTo.toString()
        }
        if(post.gender == null){
            binding.txtSexRequiredDetail.visibility = View.GONE
            binding.txtSexRequired.visibility = View.GONE
        }
        when (post.gender) {
            "1" -> {
                binding.txtSexRequiredDetail.text = getString(R.string.male)
                binding.txtGenderDetail.text = getString(R.string.male)
            }
            "2" -> {
                binding.txtSexRequiredDetail.text = getString(R.string.female)
                binding.txtGenderDetail.text = getString(R.string.female)
            }
            "3" -> {
                binding.txtSexRequiredDetail.text = getString(R.string.both_of_them)
                binding.txtGenderDetail.text = getString(R.string.both_of_them)
            }
            "4" -> {

            }
        }

        binding.txtAgeGroup.text =
            if (post.age == null) getString(R.string.unspecified) else post.age.toString()

        if(post.job__type == null){
            binding.txtTypeOfJobDetail.visibility = View.GONE
            binding.txtTypeOfJob.visibility = View.GONE
        }

        binding.txtTypeOfJobDetail.text =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") post.job__type?.title_en else post.job__type?.title_ar
        if(post.salary == null){
            binding.txtBaseSalaryDetail.visibility = View.GONE
            binding.txtBaseSalary.visibility = View.GONE
        }
        binding.txtBaseSalaryDetail.text = post.salary
        binding.txtDeliversLiceDetail.text =
            if (post.driving == "0") getString(R.string.lbl_not_required) else getString(R.string.lbl_required)





        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                if(post.description_en == null || post.description_en == ""){
                    binding.txtJobDetailsDetails.visibility = View.GONE
                    binding.txtJobDetails.visibility = View.GONE
                }
                binding.txtJobDetailsDetails.text =
                    Html.fromHtml(
                        "<p>${post.description_en ?: ""}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                binding.txtJobDescriptionDetails.text =
                    Html.fromHtml(
                        "<p>${post.description_en ?: ""}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                binding.txtOtherConditionDetail.text =
                    Html.fromHtml(
                        "<p>${post.other_condition_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            } else {
                if(post.description == null || post.description == ""){
                    binding.txtJobDetailsDetails.visibility = View.GONE
                    binding.txtJobDetails.visibility = View.GONE

                }
                binding.txtJobDetailsDetails.text =
                    Html.fromHtml(
                        "<p>${post.description}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                binding.txtJobDescriptionDetails.text =
                    Html.fromHtml(
                        "<p>${post.description}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                binding.txtOtherConditionDetail.text =
                    Html.fromHtml(
                        "<p>${post.other_condition}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )

            }
        } else {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                if(post.description_en == null || post.description_en == ""){
                    binding.txtJobDetailsDetails.visibility = View.GONE
                    binding.txtJobDetails.visibility = View.GONE
                }
                binding.txtJobDetailsDetails.text =
                    Html.fromHtml("<p>${post.description_en ?: ""}</p>")
            } else {
                if(post.description == null || post.description == ""){
                    binding.txtJobDetailsDetails.visibility = View.GONE
                    binding.txtJobDetails.visibility = View.GONE
                }
                binding.txtJobDetailsDetails.text =
                    Html.fromHtml("<p>${post.description}</p>")
            }
        }



        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
            binding.txtTitle.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtJobCityMain.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtLocation.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtTime.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtJobTitle.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtSpecialityOf.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtCareerRole.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtSpecialityOfDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtCareerRoleDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtJobRequirements.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtYearsOfExperience.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtJobCity.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtYearsOfExperienceDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtJobCityDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtSexRequired.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtDeliversLice.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtSexRequiredDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtDeliversLiceDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtRequiredQua.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtComputer.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtRequiredQuaDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtComputerDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtMicroSoftOff.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtOtherCon.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtMicroSoftOffDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtOtherConDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtSalAndType.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtTypeOfJob.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtBaseSalary.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtTypeOfJobDetail.textDirection = View.TEXT_DIRECTION_LTR
            binding.txtBaseSalaryDetail.textDirection = View.TEXT_DIRECTION_LTR

        } else {
            binding.txtTitle.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtJobCityMain.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtLocation.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtTime.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtJobTitle.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtSpecialityOf.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtCareerRole.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtSpecialityOfDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtCareerRoleDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtJobRequirements.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtYearsOfExperience.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtJobCity.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtYearsOfExperienceDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtJobCityDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtSexRequired.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtDeliversLice.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtSexRequiredDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtDeliversLiceDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtRequiredQua.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtComputer.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtRequiredQuaDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtComputerDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtMicroSoftOff.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtOtherCon.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtMicroSoftOffDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtOtherConDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtSalAndType.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtTypeOfJob.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtBaseSalary.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtTypeOfJobDetail.textDirection = View.TEXT_DIRECTION_RTL
            binding.txtBaseSalaryDetail.textDirection = View.TEXT_DIRECTION_RTL
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
    }

    private fun initUi() {


        /* linearLayoutManagerJobDetails = LinearLayoutManager(requireActivity())
         binding.recyclerView.layoutManager = linearLayoutManagerJobDetails
         jobDetailsListAdapter = JobDetailsAdapter(requireActivity(), this)
         binding.recyclerView.adapter = jobDetailsListAdapter


         linearLayoutManagerShare = LinearLayoutManager(requireActivity())
         binding.recyclerView.layoutManager = linearLayoutManagerShare
         sharedListAdapter = ShareListAdapter(requireActivity(), this)
         binding.recyclerView.adapter = sharedListAdapter*/

        binding.imgBack.setOnClickListener(this)
//        binding.viewMore.setOnClickListener(this)
        binding.txtViewAll.setOnClickListener(this)
        binding.imgShare.setOnClickListener(this)
        binding.imgLike.setOnClickListener(this)
        binding.btnApplyForJob.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgLike -> {
                isPostLikeClicked = true
                if (singlePost?.is_favourite == 1) {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeRemoveFavouriteJobList(
                            singlePost?.id!!,
                        )
                    )
                } else {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeFavouriteJobList(
                            singlePost?.id!!
                        )
                    )
                }
            }
            R.id.imgStar -> {
                position = v.tag.toString().toInt()
                isPostLikeClicked = false
                if (topJobListAdapter.list[position].is_favourite == 1) {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeRemoveFavouriteJobList(
                            topJobListAdapter.list[position].id!!
                        )
                    )
                    displayProgressBar(true)
                } else {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeFavouriteJobList(
                            topJobListAdapter.list[position].id!!
                        )
                    )
                    displayProgressBar(true)
                }
            }
            R.id.imgShareRecentSearch -> {
                val pos = v.tag.toString().toInt()
                viewModel.setStateEvent(
                    HomeStateEvent.HomeDetailSinglePost(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!, topJobListAdapter.list[pos].id!!
                    )
                )
            }
            R.id.imgBack -> {
                findNavController().popBackStack()
            }
            R.id.btnApplyForJob -> {
                if (singlePost!!.applied == 1) {
//                    binding.btnApplyForJob.visibility = View.GONE
                    alreadyAppliedBottomSheet()
                } else {
//                    binding.btnApplyForJob.visibility = View.VISIBLE
                    if (!prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                        loginDialog(requireContext())
                    } else {
                        viewModel.setStateEvent(HomeStateEvent.HomeJobPosted(post_id))
                    }
                }
            }
            R.id.viewMore -> {
                navController.navigate(R.id.action_homeDetailFragment_to_jobDetailsFragment)
            }

            R.id.txtViewAll -> {
                navController.navigate(R.id.action_homeDetailFragment_to_topJobsFragment)
            }

            R.id.imgShare -> {
                val icon = BitmapFactory.decodeResource(
                    resources,
                    R.drawable.img_back_white
                )
                val share = Intent(Intent.ACTION_SEND)
                share.type = "image/jpeg"
                val bytes = ByteArrayOutputStream()
                icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                val f = File(
                    (Environment.getExternalStorageDirectory()).toString() + "temporary_file.jpg"
                )
                try {
                    f.createNewFile()
                    val fo = FileOutputStream(f)
                    fo.write(bytes.toByteArray())
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                Utils.print("Share" + "${Constant.IMAGE_URL}${singlePost!!.com_logo}")
                share.putExtra(
                    Intent.EXTRA_STREAM,
                    Uri.parse("${Constant.IMAGE_URL}${singlePost!!.com_logo}")
                )
                if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                    share.putExtra(
                        Intent.EXTRA_TEXT,
//                        singlePost!!.title_en
                        "" + "\n"

//                                + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_en + "\n"
//                                + resources.getString(R.string.address) + " : " + singlePost?.company?.address + "\n"
//                                + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_en + "\n"
//                                + resources.getString(R.string.announcement_date) + " : " + singlePost?.created_at + "\n"
//                                + resources.getString(R.string.the_job_title) + " : " + singlePost?.job__type?.title_en + "\n"
                                + "Job title" + " : " + singlePost?.job__type?.title_en + "\n"
                                + "Description" + " : " + Utils.html2text(singlePost?.description_en) + "\n"
                                + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title_en + "\n"
//                                + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                    )
                    share.type = "text/plain"
//                    startActivity(share)
                } else {
                    share.putExtra(
                        Intent.EXTRA_TEXT,
//                        singlePost!!.title_en
                        "" + "\n"
//                                + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_ar + "\n"
//                                + resources.getString(R.string.address) + " : " + singlePost?.company?.address + "\n"
//                                + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_ar + "\n"
//                                + resources.getString(R.string.announcement_date) + " : " + singlePost?.created_at + "\n"
//                                + /*resources.getString(R.string.the_job_title) */+ " : " + singlePost?.job__type?.title_ar + "\n"
                                + "Job title" + " : " + singlePost?.job__type?.title_ar + "\n"
                                + "Description" + " : " + Utils.html2text(singlePost?.description) + "\n"
                                + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                + resources.getString(R.string.the_job_title) + "\n"
//                                + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title + "\n"
//                                + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                    )
                    share.type = "text/plain"
//                     startActivity(share)
                }
                startActivity(Intent.createChooser(share, "Share Image"))
            }
            R.id.clMain -> {
                val pos = v.tag.toString().toInt()
                if (Utils.isConnected(requireContext())) {
                    viewModel.setStateEvent(
                        HomeStateEvent.HomeDetailSinglePost(
                            prefManager.getString(
                                PrefConstant.PREF_CURRENT_LANGUAGE
                            )!!, topJobListAdapter.list[pos].id!!
                        )
                    )
                    binding.nsvJobDetails.fullScroll(View.FOCUS_UP)
                } else {
                    Utils.showSnackBar(v, requireContext())
                }

            }
        }
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }

    private fun loginDialog(context: Context) {
        var intent = Intent(requireContext(), LoginPopUpActivity::class.java)
        startActivity(intent)
        /*MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.title_login_dialog)

            .setPositiveButton(R.string.login_in) { dialog, _ ->
                context.startActivity(Intent(context, LoginActivity::class.java))
                dialog.dismiss()
            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()*/
    }

    private fun alreadyAppliedBottomSheet() {
        val bottomSheetDialogSocial = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        val bottomSheetViewSocial =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_sheet_already_applied,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )
        val txtOK: TextView = bottomSheetViewSocial.findViewById(R.id.txtOK)
        txtOK.setOnClickListener {
            bottomSheetDialogSocial.dismiss()
        }

        bottomSheetDialogSocial.setContentView(bottomSheetViewSocial)
        bottomSheetDialogSocial.show()
    }

}