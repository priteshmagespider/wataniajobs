package com.fnrco.wataniajobs.mvvm.model.response

data class JobTypeResponseBean(
    var jobsType: List<JobsType>? = null
)

data class JobsType(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)