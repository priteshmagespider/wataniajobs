package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentCallUsBinding
import com.fnrco.wataniajobs.mvvm.model.response.CallUsResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.CallUsEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.CallUsViewModel
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class CallUsFragment : Fragment(), View.OnClickListener {

    private val mTAG = CallUsFragment::class.java.simpleName
    private lateinit var binding: FragmentCallUsBinding

    @ExperimentalCoroutinesApi
    private val viewModel by viewModels<CallUsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCallUsBinding.inflate(layoutInflater)
        setObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
    }

    private fun isValid(): Boolean {
        /*binding.inputName.isErrorEnabled = false
        binding.inputEmail.isErrorEnabled = false
        binding.inputMessage.isErrorEnabled = false*/

        when {
            binding.edtName.text.toString().isEmpty() -> {
                binding.edtName.error =
                    getString(R.string.error_enter_full_name)
                binding.edtName.requestFocus()
                return false
            }
            binding.edtEmail.text.toString().isEmpty() -> {
                binding.edtEmail.error = getString(R.string.error_enter_email)
                binding.edtEmail.requestFocus()
                return false
            }
            !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches() -> {
                binding.edtEmail.error = getString(R.string.error_enter_valid_email)
                binding.edtEmail.requestFocus()
                return false
            }
            binding.edtMessage.text.toString().isEmpty() -> {
                binding.edtMessage.error =
                    getString(R.string.error_please_enter_message)
                binding.edtMessage.requestFocus()
                return false
            }
        }
        return true
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                findNavController().popBackStack()
            }
            R.id.btnSubmit -> {
                if (Utils.isConnected(requireContext())) {
                    if (isValid()) {
                        viewModel.setCallUsApiCall(
                            CallUsEvent.ContactUsEvent(
                                binding.edtName.text.toString(),
                                binding.edtEmail.text.toString(),
                                binding.edtMessage.text.toString(),
                                2
                            )
                        )
                    }
                } else {
                    Utils.showSnackBar(view, requireContext())
                }
            }
        }
    }

    private fun setObserver() {
        viewModel.callUsResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<CallUsResponseBean> -> {
                    displayProgressBar(false)
                    displayError(dataState.data.message)
                    findNavController().popBackStack()

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }
}