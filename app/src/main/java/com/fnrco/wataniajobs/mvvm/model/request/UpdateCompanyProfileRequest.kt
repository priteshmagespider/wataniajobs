package com.fnrco.wataniajobs.mvvm.model.request

class UpdateCompanyProfileRequest {
    var name: String? = null
    var user_job_title: String? = null
    var field: String? = null
    var name_en: String? = null

    /** Personal Info */


    var address: String? = null
    var city: String? = null
    var gender: String? = null
    var birthDate: String? = null
    var Dependents: String? = null
    var marital_status: String? = null
    var experience: String? = null
    var salary: String? = null



    /** Communication Data */
    var email: String? = null
    var phone: String? = null

}