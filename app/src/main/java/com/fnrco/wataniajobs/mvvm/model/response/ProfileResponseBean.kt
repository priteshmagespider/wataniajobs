package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class ProfileResponseBean(
    var user: User? = null,
    var jobPosts: List<JobPost>? = null,
    var jobsAppliedExpired: List<JobsAppliedExpired>? = null
) : Serializable

data class User(
    var id: Int? = null,
    var specialization_position: Int? = null,
    var name: String? = null,
    var name_en: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var token: String? = null,
    var is_admin: Int? = null,
    var provider: String? = null,
    var provider_id: String? = null,
    var mail: String? = null,
    var role: String? = null,
    var active: Int? = null,
    var code_win: String? = null,
    var enter_by: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var job_seeker: JobSeeker? = null,
    var langs: List<Lang>? = null,
    var experience: List<Experience>? = null,
    var skills: List<Skill>? = null,
    var courses: List<Course>? = null,
    var projects: List<Project>? = null,
    var certificates: List<Certificate>? = null,
    var interest: List<Interest>? = null
) : Serializable

data class JobPost(
    var id: Int? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var Description: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: String? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: String? = null,
    var computer_level: String? = null,
    var MsOffice_level: String? = null,
    var other_condition: String? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: String? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: String? = null,
    var job_id: String? = null,
    var website: String? = null,
    var com_email: String? = null,
    var com_name: String? = null,
    var com_logo: String? = null,
    var job_date: String? = null,
    var jobCategory: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var city: String? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: String? = null,
    var reqCreated: String? = null,
    var showed: Int? = null,
    var mainCategory: String? = null
) : Serializable

data class JobsAppliedExpired(
    var id: Int? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var Description: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: String? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: String? = null,
    var computer_level: String? = null,
    var MsOffice_level: String? = null,
    var other_condition: String? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: String? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: String? = null,
    var job_id: String? = null,
    var website: String? = null,
    var com_email: String? = null,
    var com_name: String? = null,
    var com_logo: String? = null,
    var job_date: String? = null,
    var jobCategory: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var city: String? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var photo_shared: String? = null,
    var reqCreated: String? = null,
    var showed: Int? = null,
    var mainCategory: String? = null
) : Serializable

data class JobSeeker(
    var id: Int? = null,
    var user_id: Int? = null,
    var currency: String? = null,
    var job_title: String? = null,
    var job_title_en: String? = null,
    var birthDate: String? = null,
    var salary: Int? = null,
    var marital_status: Int? = null,
    var military_status: String? = null,
    var job_status: String? = null,
    var idCard: String? = null,
    var phone: String? = null,
    var gender: String? = null,
    var city: String? = null,
    var carunt_location: String? = null,
    var aria: String? = null,
    var address: String? = null,
    var phone2: String? = null,
    var certificate: String? = null,
    var experience: Int? = null,
    var career_objective: String? = null,
    var special: Int? = null,
    var approve_special: String? = null,
    var ip: String? = null,
    var cv: String? = null,
    var photo: String? = null,
    var photo_approve: Int? = null,
    var cv2: String? = null,
    var cv2_enter_by: String? = null,
    var cv2_upload: String? = null,
    var enter_by: String? = null,
    var count_view: Int? = null,
    var fb: String? = null,
    var twitter: String? = null,
    var linkedIn: String? = null,
    var website: String? = null,
    var instagram: String? = null,
    var youtube: String? = null,
    var Dependents: Int? = null,
    var mainCat: String? = null,
    var subCat: String? = null,
    var phone_is_verified: Int? = null,
    var phone_verification_code: String? = null,
    var ratio: Int? = null,
    var confirm_training: Int? = null,
    var sms_try: Int? = null,
    var form_uni: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var city_seeker: CitySeeker? = null,
    var educations: List<Education>? = null,
    val sepecilization: Sepecilization? = null,
    val job_role: JobRoles? = null,
) : Serializable

data class JobRoles(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable


data class Sepecilization(
    val id: Int? = null,
    val title_en: String? = null,
    val title: String? = null
) : Serializable

data class Lang(
    var id: Int? = null,
    var language: LanguageO? = null,
    var level: LevelO? = null,
    var person_informateion_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class LanguageO(
    var id: Int? = null,
    var name: String? = null,
) : Serializable

data class LevelO(
    var id: Int? = null,
    var title: String? = null,
) : Serializable

data class Experience(
    var id: Int? = null,
    var start_date: String? = null,
    var end_date: String? = null,
    var total_experience_m: Int? = null,
    var company_address: String? = null,
    var company_name: String? = null,
    var job_title: String? = null,
    var job_description: String? = null,
    var person_informateion_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class Skill(
    var id: Int? = null,
    var skills_name: String? = null,
    var person_informateion_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class Course(
    var id: Int? = null,
    var address: String? = null,
    var center_name: String? = null,
    var action_date: String? = null,
    var count_times: String? = null,
    var description: String? = null,
    var image: String? = null,
    var user_id: Int? = null,
    var job_seeker_id: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class Project(
    var id: Int? = null,
    var job_seeker_id: Int? = null,
    var user_id: Int? = null,
    var title: String? = null,
    var month: String? = null,
    var year: Int? = null,
    var description: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class Certificate(
    var id: Int? = null,
    var title: String? = null,
    var from_by: String? = null,
    var month: String? = null,
    var year: Int? = null,
    var description: String? = null,
    var user_id: Int? = null,
    var job_seeker_id: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class Interest(
    var id: Int? = null,
    var job_seeker_id: Int? = null,
    var user_id: Int? = null,
    var interest: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable

data class CitySeeker(
    var id: Int? = null,
    var name_en: String? = null,
    var name_ar: String? = null
) : Serializable

data class Education(
    var id: Int? = null,
    var educational_institution: String? = null,
    var degree: String? = null,
    var general_appreciation: String? = null,
    var specialization: String? = null,
    var end_date_educational: String? = null,
    var description_education: String? = null,
    var uni_num: String? = null,
    var job_seeker_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
) : Serializable