package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.TopJobsAdapter
import com.fnrco.wataniajobs.adapter.TopJobsDetailsAdapter
import com.fnrco.wataniajobs.databinding.FragmentHomeBinding
import com.fnrco.wataniajobs.databinding.FragmentTopJobsBinding
import com.fnrco.wataniajobs.mvvm.model.response.SinglePostResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.TopJobsResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class TopJobsFragment : Fragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentTopJobsBinding
    lateinit var navController: NavController
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var topJobsDetailsAdapter: TopJobsDetailsAdapter
    var pastVisiblesItems: Int = -1
    var visibleItemCount: Int = -1
    var totalItemCount: Int = -1
    var page: Int = 1
    private var loading = true
    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTopJobsBinding.inflate(inflater, container, false)
binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) //check for scroll down
        {
            visibleItemCount = layoutManager.childCount
            totalItemCount = layoutManager.itemCount
            pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
            if (loading) {
                if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                    loading = false
                    if (Utils.isConnected(requireContext())) {
                        viewModel.setStateEvent(
                            HomeStateEvent.HomeTopJobs(
                                prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE
                                )!!, page))
                    } else {
                        Utils.showSnackBar(binding.mainLayout, requireContext())
                    }
                }
            }
        }
    }
})
        setObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        if (Utils.isConnected(requireContext())) {
            viewModel.setStateEvent(HomeStateEvent.HomeTopJobs(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!, page))
        } else {
            Utils.showSnackBar(view, requireContext())
        }
        initUi()
    }

    private fun setObserver() {
        viewModel.topJobResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<TopJobsResponseBean> -> {
                    displayProgressBar(false)
//                    linearLayoutManager = LinearLayoutManager(requireActivity())
//                    binding.recyclerView.layoutManager = linearLayoutManager
                    if (dataState.data.posts?.data!!.isNotEmpty()) {
                        if (page == 1) {
                            binding.recyclerView.visibility = View.VISIBLE
                            topJobsDetailsAdapter = TopJobsDetailsAdapter(
                                requireActivity(), this,
                                prefManager
                            )
                            topJobsDetailsAdapter.addData(
                                dataState.data.posts?.data!!,
                                true
                            )
                            binding.recyclerView.adapter = topJobsDetailsAdapter
                        } else {
                            if (page > 1) {
                                topJobsDetailsAdapter.addData(
                                    dataState.data.posts?.data!!,
                                    false
                                )
                                topJobsDetailsAdapter.notifyDataSetChanged()
                            }
                        }
                        page++
                        loading = true
                    }
                    else {
                        if (page == 1) {
                            binding.recyclerView.visibility = View.GONE
//                            binding.txtNoRecord.visibility = View.VISIBLE
                        }
                        loading = false
                    }
//                    topJobsDetailsAdapter = TopJobsDetailsAdapter(
//                        requireActivity(), this, dataState.data.posts?.data!!,
//                        prefManager
//                    )

//                    binding.recyclerView.adapter = topJobsDetailsAdapter
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        /*linearLayoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = linearLayoutManager
        topJobListAdapter = TopJobsAdapter(requireActivity(), this)
        binding.recyclerView.adapter = topJobListAdapter*/

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                navController.popBackStack()
            }
            R.id.clMain -> {
                val pos = v.tag.toString().toInt()
                val bundle = Bundle()
                bundle.putInt(Constant.POST_ID, topJobsDetailsAdapter.listData[pos].id!!)
                navController.navigate(
                    R.id.action_topJobsFragment_to_homeDetailFragment,
                    bundle
                )
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }

}