package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.FavouriteJobsListAdapter
import com.fnrco.wataniajobs.databinding.FragmentFavouriteJobsBinding
import com.fnrco.wataniajobs.mvvm.model.request.SearchRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginPopUpActivity
import com.fnrco.wataniajobs.mvvm.viewmodel.FindJobsStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.FindJobsViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class FavouriteJobsFragment(private val hideBack :  Boolean = false) : Fragment(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentFavouriteJobsBinding
    private lateinit var navController: NavController
    private lateinit var favouriteJobsListAdapter: FavouriteJobsListAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var job_title: String = ""
    private var city_id: Int = 0
    private var city_name: String = ""

    private var jobCat: Int = 0
    private var subCat: Int = 0
    private var from: Int = 0
    private var to: Int = 0
    private var jobType: Int = 0
    private var gender: Int = 0
    private var certificate: Int = 0
    private var created_at: Int = 0

    private var singlePost: PostSinglePost? = null
    val searchRequest: SearchRequest = SearchRequest()

    private var posFindAJobLike: Int = -1

    @ExperimentalCoroutinesApi
    private val viewModel: FindJobsViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavouriteJobsBinding.inflate(inflater, container, false)
        viewModel.setStateEvent(FindJobsStateEvent.FavouriteJobsList(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        setObserver()
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        initUi()

    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        binding.imgFilter.setOnClickListener(this)

        if(hideBack){
            binding.txtCandidateTitle.visibility = View.GONE
            binding.imgHeader.visibility = View.GONE
        }
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.clMain -> {
                if (!prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
                    loginDialog(requireContext())
                } else {
                    val pos = v.tag.toString().toInt()
                    val bundle = Bundle()
                    bundle.putInt(Constant.POST_ID, favouriteJobsListAdapter.list[pos].id!!)
                    navController.navigate(
                        R.id.homeDetailFragment,
                        bundle
                    )
                }

            }
            R.id.imgBack -> {
                findNavController().navigateUp()
            }
            R.id.imgShare -> {
                var pos = v.tag.toString().toInt()
                viewModel.setStateEvent(
                    FindJobsStateEvent.HomeDetailSinglePost(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!, favouriteJobsListAdapter.list[pos].id!!
                    )
                )
            }
            R.id.imgStarFindAJob -> {
                posFindAJobLike = v.tag.toString().toInt()
                if (favouriteJobsListAdapter.list[posFindAJobLike].is_favourite == 1) {
                    viewModel.setStateEvent(
                        FindJobsStateEvent.HomeRemoveFavouriteJobList(
                            favouriteJobsListAdapter.list[posFindAJobLike].id!!
                        )
                    )
                   //displayProgressBar(true)
                    favouriteJobsListAdapter.list.removeAt(posFindAJobLike)
                    favouriteJobsListAdapter.notifyDataSetChanged()
                } else {
                    viewModel.setStateEvent(
                        FindJobsStateEvent.HomeFavouriteJobList(
                            favouriteJobsListAdapter.list[posFindAJobLike].id!!
                        )
                    )
                    displayProgressBar(true)
                }


                /*var pos = v.tag.toString().toInt()
                viewModel.setStateEvent(
                    FindJobsStateEvent.HomeDetailSinglePost(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!, favouriteJobsListAdapter.list[pos].id!!
                    )
                )*/
            }
            R.id.imgFilter -> {
                val bundle = bundleOf(
                    Constant.JOB_TITLE_FILTER to searchRequest
                )
                navController.navigate(
                    R.id.action_findJobsFragment_to_filterJobListFragment,
                    bundle
                )
            }
        }
    }


    private fun setObserver() {
        viewModel.singlePostResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SinglePostResponseBean> -> {
                    displayProgressBar(false)
                    singlePost = dataState.data.post
                    Utils.print("HEHHEHEHEHRHEHRHEHR-------------->" + singlePost)


                    val icon = BitmapFactory.decodeResource(
                        resources,
                        R.drawable.img_back_white
                    )
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "image/jpeg"
                    val bytes = ByteArrayOutputStream()
                    icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    val f = File(
                        (Environment.getExternalStorageDirectory()).toString() + "temporary_file.jpg"
                    )
                    try {
                        f.createNewFile()
                        val fo = FileOutputStream(f)
                        fo.write(bytes.toByteArray())
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    share.putExtra(
                        Intent.EXTRA_STREAM,
                        Uri.parse("${Constant.IMAGE_URL}${singlePost!!.com_logo}")
                    )
                    if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                        share.putExtra(
                            Intent.EXTRA_TEXT,
//                            singlePost!!.title_en
                            "" + "\n"
//                                    + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_en + "\n"
//                                    + resources.getString(R.string.address) + " : " + singlePost?.company?.address  + "\n"
//                                    + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_en + "\n"
//                                    + resources.getString(R.string.announcement_date) + " : " +  singlePost?.created_at + "\n"
//                                    + resources.getString(R.string.the_job_title) + "\n"
                                    + "Job title" + " : " + singlePost?.job__type?.title_en + "\n"
                                    + "Description" + " : " + Utils.html2text(singlePost?.description_en) + "\n"
                                    + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                    + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                    + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                    + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title_en + "\n"
//                                    + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                        )
                        share.type = "text/plain"
//                    startActivity(share)
                    } else {
                        share.putExtra(
                            Intent.EXTRA_TEXT,
//                            singlePost!!.title_en
                            "" + "\n"
//                                    + resources.getString(R.string.city) + " : " + singlePost?.job_city?.name_ar + "\n"
//                                    + resources.getString(R.string.address) + " : " + singlePost?.company?.address  + "\n"
//                                    + resources.getString(R.string.job_type) + " : " + singlePost?.job__type?.title_ar + "\n"
//                                    + resources.getString(R.string.announcement_date) + " : " +  singlePost?.created_at + "\n"
//                                    + resources.getString(R.string.the_job_title) + "\n"
                                    + "Job title" + " : " + singlePost?.job__type?.title_ar + "\n"
                                    + "Description" + " : " + Utils.html2text(singlePost?.description) + "\n"
                                    + "Image" + " : " + "${Constant.IMAGE_URL}${singlePost!!.com_logo}" + "\n"
                                    + "Android Link" + " : " + "https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs" + "\n"
                                    + "IOS Link" + " : " + "https://apps.apple.com/sa/app/id1617017721" + "\n"
//                                    + resources.getString(R.string.speciality_of_the_job) + " : " + singlePost?.specialization?.title + "\n"
//                                    + resources.getString(R.string.career_role) + " : " + singlePost?.jobRole
                        )
                        share.type = "text/plain"
//                     startActivity(share)
                    }
                    startActivity(Intent.createChooser(share, "Share Image"))
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        /** Home Banner Response */
        viewModel.removeFavouriteJobResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<RemoveFavoriteJobResponseBean> -> {
                    displayProgressBar(false)
                    /*if(dataState.data.message == "Remove Job From Favourite Successfully")
                    {

                        favouriteJobsListAdapter.list[posFindAJobLike].is_favourite = 0
                        favouriteJobsListAdapter.notifyItemChanged(posFindAJobLike)
                    }*/
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Home Banner Response */
        viewModel.favouriteListResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<FavouriteJobResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.posts!!.data!!.isEmpty()) {
                        Utils.print("Data---------------------->::FindJobsFragment-----searchListResponseBean---->" + dataState.data.posts!!.data!!.size)
                        binding.idNoDataAvailable.visibility = View.VISIBLE
                        binding.recyclerView.visibility = View.GONE
                    } else {
                        binding.idNoDataAvailable.visibility = View.GONE
                        binding.recyclerView.visibility = View.VISIBLE
                        linearLayoutManager = LinearLayoutManager(requireActivity())
                        binding.recyclerView.layoutManager = linearLayoutManager
                        favouriteJobsListAdapter = FavouriteJobsListAdapter(
                            requireActivity(),
                            this,
                            dataState.data.posts!!.data!! as MutableList<DataFavouriteJob>,
                            prefManager
                        )
                        binding.recyclerView.adapter = favouriteJobsListAdapter
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        viewModel.favouriteJobResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<FavoriteJobResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.message == "Add Job to Favourite Successfully") {
                        favouriteJobsListAdapter.list[posFindAJobLike].is_favourite = 1
                        favouriteJobsListAdapter.notifyItemChanged(posFindAJobLike)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })


    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


    private fun loginDialog(context: Context) {
        var intent = Intent(requireContext(), LoginPopUpActivity::class.java)
        startActivity(intent)
        /*MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.title_login_dialog)

            .setPositiveButton(R.string.login_in) { dialog, _ ->
                context.startActivity(Intent(context, LoginActivity::class.java))
                dialog.dismiss()
            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()*/
    }

}