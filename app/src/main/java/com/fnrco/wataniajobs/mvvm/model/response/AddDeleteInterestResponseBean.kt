package com.fnrco.wataniajobs.mvvm.model.response

class AddDeleteInterestResponseBean : ArrayList<AddDeleteInterestResponseBeanItem>()

data class AddDeleteInterestResponseBeanItem(
    val created_at: String? = null,
    val id: Int? = null,
    val interest: String? = null,
    val job_seeker_id: Int? = null,
    val updated_at: String? = null,
    val user_id: Int? = null
)