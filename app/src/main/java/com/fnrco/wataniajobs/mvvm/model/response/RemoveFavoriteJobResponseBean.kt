package com.fnrco.wataniajobs.mvvm.model.response

data class RemoveFavoriteJobResponseBean(
    var message: String? = null
)