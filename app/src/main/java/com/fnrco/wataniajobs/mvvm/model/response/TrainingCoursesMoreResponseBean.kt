package com.fnrco.wataniajobs.mvvm.model.response

data class TrainingCoursesMoreResponseBean(
    val trainings: List<Training>? = null
)

data class Training(
    val id: Int? = null,
    val city: City? = null,
    val date: String? = null,
    val duration: String? = null,
    val duration_en: String? = null,
    val price: String? = null,
    val status: Int? = null,
    val title: String? = null,
    val title_en: String? = null,
    val type: Int? = null,
    val is_apply : Boolean
)
