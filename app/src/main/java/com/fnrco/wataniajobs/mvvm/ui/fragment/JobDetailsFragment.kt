package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentHomeBinding
import com.fnrco.wataniajobs.databinding.FragmentJobDetailsBinding


class JobDetailsFragment : Fragment(),View.OnClickListener {

    private val mTAG = HomeFragment::class.java.simpleName
    private lateinit var binding: FragmentJobDetailsBinding
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentJobDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        initUi()
    }

    private fun initUi() {
        binding.imgDrawer.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id)
        {
            R.id.imgDrawer -> {
                findNavController().popBackStack()
            }
            R.id.clMain -> {
                /*var intent = Intent(this,HomeDetailActivity::class.java)
                startActivity(intent)*/
                navController.navigate(R.id.homeDetailFragment)
            }
        }
    }


}