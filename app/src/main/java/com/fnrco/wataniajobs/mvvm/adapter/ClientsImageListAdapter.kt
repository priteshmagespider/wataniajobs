package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.ClientX
import com.fnrco.wataniajobs.utils.Constant

class ClientsImageListAdapter(
    var context: Context,
    val list: List<ClientX>
) : RecyclerView.Adapter<ClientsImageListAdapter.MyViewHolder>() {

    val listData = list as MutableList<ClientX>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgClients: ImageView = itemView.findViewById(R.id.imgClients)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_clients_image_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.d("TAG", "onBindViewHolder: ${Constant.IMAGE_URL_WITH_LOGOS}${listData[position].photo}")
        Glide.with(context)
            .load("${Constant.IMAGE_URL_WITH_LOGOS}${listData[position].photo}")
            .into(holder.imgClients)
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}