package com.fnrco.wataniajobs.mvvm.model.response

data class ClientsListResponseBean(
    val clients: List<Client>?
)

data class Client(
    val cat: String?,
    val cat_en: String?,
    val clients: List<ClientX>?,
    val id: Int?
)

data class ClientX(
    val cat_id: Int?,
    val id: Int?,
    val name_ar: Any?,
    val name: Any?,
    val photo: String?
)