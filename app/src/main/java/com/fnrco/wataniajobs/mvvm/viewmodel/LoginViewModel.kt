@file:Suppress("DEPRECATION")

package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.LoginResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.UpdatePlayerResponseBean
import com.fnrco.wataniajobs.repository.LoginRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class LoginViewModel
@ViewModelInject
constructor(
    private val loginRepository: LoginRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : ViewModel() {

    /** Login Response */
    private var _loginResponse: MutableLiveData<DataState<LoginResponseBean>> = MutableLiveData()

    val loginResponseBean: LiveData<DataState<LoginResponseBean>>
        get() = _loginResponse


    private val _updatePlayerResponse: MutableLiveData<DataState<UpdatePlayerResponseBean>> = MutableLiveData()

    val updatePlayerResponse: LiveData<DataState<UpdatePlayerResponseBean>>
        get() = _updatePlayerResponse

    fun callLoginEvent(loginEvent: LoginEvent) {
        viewModelScope.launch {
            when (loginEvent) {
                is LoginEvent.DoLogin -> {
                    loginRepository.loginApiCall(loginEvent.authRequest)
                        .onEach {
                            _loginResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is LoginEvent.UpdatePlayer -> {
                    loginRepository.updatePlayer(loginEvent.player_id)
                        .onEach {
                            _updatePlayerResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
            }
        }
    }
}


sealed class LoginEvent {

    data class DoLogin(val authRequest: AuthRequest) : LoginEvent()

    data class UpdatePlayer(val player_id:String) : LoginEvent()
}