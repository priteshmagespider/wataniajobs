package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentCurrentJobApplicationBinding
import com.fnrco.wataniajobs.mvvm.adapter.CurrentJobApplicationAdapter
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobPostListResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.AppliedJobResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.DeleteJobPostResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.DataState
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.internal.notify
import javax.inject.Inject


@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class CurrentJobApplicationFragment : Fragment(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentCurrentJobApplicationBinding
    private lateinit var navController: NavController

    private lateinit var currentJobApplicationAdapter: CurrentJobApplicationAdapter

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager
    lateinit var bottomSheetDialog: BottomSheetDialog
    lateinit var bottomSheetView: View

    lateinit var bottomSheetDeleteDialog: BottomSheetDialog
    lateinit var bottomSheetDeleteView: View
    var positionDelete : Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCurrentJobApplicationBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        initUi()


        viewModel.setStateEvent(EditProfileStateEvent.JobPostAppliedData(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        displayProgressBar(true)
        setObserver()
    }



    private fun setObserver() {
        /**Job Post Applied Response*/
        viewModel.jobPostAppliedDataResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<AppliedJobResponseBean> -> {
                    displayProgressBar(false)
                    if(dataState.data.posts!!.isEmpty())
                    {
                        binding.txtNoRecord.visibility = View.VISIBLE
                    }else{
                        binding.rvCurrentJobApplicationList.layoutManager = LinearLayoutManager(requireContext())
                        currentJobApplicationAdapter = CurrentJobApplicationAdapter(requireContext(), this,prefManager)
                        currentJobApplicationAdapter.addData(dataState.data.posts)
                        binding.rvCurrentJobApplicationList.adapter = currentJobApplicationAdapter
                    }

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        viewModel.deleteJobPostResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<DeleteJobPostResponseBean> -> {
                    displayProgressBar(false)
                    if(dataState.data.message == "jop Post Deleted Successfully")
                    {
                        currentJobApplicationAdapter.removeItem(positionDelete)
                        currentJobApplicationAdapter.notifyDataSetChanged()
                        bottomSheetDeleteDialog.dismiss()
                    }else{
                        Toast.makeText(requireContext(),dataState.data.message,Toast.LENGTH_LONG).show()
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun initUi() {

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgMoreOption -> {
                positionDelete= view.tag.toString().toInt()
                forWithdrawalOfOrder(positionDelete)
            }
            R.id.btnWithdraw ->{
                positionDelete= view.tag.toString().toInt()
                forAreYouSure(positionDelete)
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun forWithdrawalOfOrder(pos: Int) {
        bottomSheetDialog = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetView =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.fragment__withdrawal_of_order,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )
        bottomSheetView.findViewById<View>(R.id.btnWithdrawalOfOrder)
            .setOnClickListener {
                forAreYouSure(pos)
                bottomSheetDialog.dismiss()
            }




        bottomSheetView.findViewById<View>(R.id.btnCancel)
            .setOnClickListener(View.OnClickListener {
                bottomSheetDialog.dismiss()
            })

        bottomSheetDialog.setContentView(bottomSheetView)
        bottomSheetDialog.show()
    }

    private fun forAreYouSure(pos: Int) {
        bottomSheetDeleteDialog = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetDeleteView =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.fragment_are_you_sure,
                    requireActivity().findViewById<View>(R.id.constraintForAreYouSure) as ConstraintLayout?
                )
        bottomSheetDeleteView.findViewById<View>(R.id.btnDelete)
            .setOnClickListener {
                viewModel.setStateEvent(EditProfileStateEvent.DeleteJobPost(currentJobApplicationAdapter.getList()[pos].id!!))
            }


        bottomSheetDeleteView.findViewById<View>(R.id.btnCancel)
            .setOnClickListener(View.OnClickListener {
                bottomSheetDeleteDialog.dismiss()
            })

        bottomSheetDeleteDialog.setContentView(bottomSheetDeleteView)
        bottomSheetDeleteDialog.show()
    }

}