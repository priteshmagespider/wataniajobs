package com.fnrco.wataniajobs.mvvm.model.response

data class FavoriteJobResponseBean(
    var message: String? = null
)