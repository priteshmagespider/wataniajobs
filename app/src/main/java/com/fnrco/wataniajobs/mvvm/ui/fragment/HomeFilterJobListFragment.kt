package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.icu.text.NumberFormat
import android.icu.util.Currency
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.*
import com.fnrco.wataniajobs.databinding.FragmentJobListBinding
import com.fnrco.wataniajobs.databinding.FragmentJobListNewBinding
import com.fnrco.wataniajobs.mvvm.model.request.SearchRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.FindJobsStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.FindJobsViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.slider.RangeSlider
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class HomeFilterJobListFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentJobListNewBinding
    lateinit var jobsType: JobTypeResponseBean
    private var city_id: Int = 0
    private var major_id: Int = 0
    private var job_role_id: Int = 0
    private var jType : ArrayList<Int> = ArrayList()
    private var permanency_type_id: Int = 0
    private var qualification_id: Int = 0
    private lateinit var cityDropDownListAdapter: CityDropDownListAdapter
    private lateinit var majorListAdapter: MajorListAdapter
    private lateinit var jobRoleListAdapter: JobRoleListAdapter
    private lateinit var permanencyTypeListAdapter: PermanencyTypeListAdapter
    private lateinit var qualificationListAdapter: QualificationListAdapter
    private val listType = ArrayList<String>()
    private val listJobPostedSince = ArrayList<String>()

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: FindJobsViewModel by viewModels()
    private var job_title: String = ""
    private var seekbar_min: Int = 0
    private var seekbar_max: Int = 0
    private lateinit var navController: NavController

    private var city_name: String = ""

    private var jobCat: Int = 0
    private var subCat: Int = 0
    private var from: Int = 0
    private var to: Int = 0
    private var jobType: Int = 0
    private var gender: Int = 0
    private var certificate: Int = 0
    private var created_at: Int = 0
    lateinit var searchRequest: SearchRequest

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentJobListNewBinding.inflate(layoutInflater)
        if (requireArguments().containsKey("FROM_HOME_FILTER_BUTTON")) {
            searchRequest =
                requireArguments().getSerializable("FROM_HOME_FILTER_BUTTON") as SearchRequest
        }

        initUi()
        viewModel.setStateEvent(FindJobsStateEvent.FilterJobsCity(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        viewModel.setStateEvent(
            FindJobsStateEvent.FilterCategoriesList(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
        viewModel.setStateEvent(FindJobsStateEvent.JobTypeList(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        viewModel.setStateEvent(FindJobsStateEvent.JobRoleList(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        viewModel.setStateEvent(
            FindJobsStateEvent.QualificationList(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )

        setObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()

    }

    private fun setObserver() {
        viewModel.cityListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<CityListResponse> -> {
                    displayProgressBar(false)
                    if (dataState.data.cities != null) {
                        setDropDownCityData(dataState.data.cities)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.majorListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SpecializationResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.specializations != null) {
                        setMajorData(dataState.data.specializations)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }


        viewModel.jobTypeListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<JobTypeResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.jobsType != null) {
                        jobsType = dataState.data
                        jType.clear()
                        setPermanencyTypeData(dataState.data.jobsType)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }


        viewModel.jobRoleResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<JobRoleResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.categories != null) {
//                        setJobRoleData(dataState.data.categories)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }


        viewModel.qualificationResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<QualificationResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.qualifications != null) {
                        setQualificationData(dataState.data.qualifications)
                    }
                    setData(searchRequest)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.searchListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SearchListResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.posts != null) {
                        Utils.print("SearchListResponse------------>" + dataState.data.posts)
                        val bundle = Bundle()
                        bundle.putSerializable(Constant.FILTER_POST_DATA, dataState.data)
                        navController.navigate(
                            R.id.action_filterJobListFragment_to_findJobsFragment,
                            bundle
                        )
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun setData(searchRequest: SearchRequest) {
        binding.edtAutoCompleteSearch.setText(searchRequest.jobTitle)
    }


    private fun initUi() {
        binding.seekBarMinMax.values =
            listOf(
                searchRequest.from?.toFloat(),
                if (searchRequest.to == 0) 100000f else searchRequest.to?.toFloat()
            )

        binding.seekBarMinMax.setLabelFormatter { value: Float ->
            val format = NumberFormat.getCurrencyInstance()
            format.maximumFractionDigits = 0
            format.currency = Currency.getInstance("SAR")
            format.format(value.toDouble())
        }

        binding.textHeightOneStart.text = searchRequest.from.toString()
        binding.textHeightOneEnd.text =
            if (searchRequest.to == 0) "100000" else searchRequest.to.toString()
        binding.imgClose.setOnClickListener(this)
        binding.btnApply.setOnClickListener(this)
        binding.txtReset.setOnClickListener(this)


        binding.seekBarMinMax.addOnChangeListener(RangeSlider.OnChangeListener { slider, _, _ ->
            seekbar_min = slider.values[0].toInt()
            seekbar_max = slider.values[1].toInt()
            binding.textHeightOneStart.text = "$seekbar_min"
            binding.textHeightOneEnd.text = "$seekbar_max"
        })
        listType.add(resources.getString(R.string.male))
        listType.add(resources.getString(R.string.female))
        listType.add(resources.getString(R.string.both_of_them))
        val expAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                listType
            )
        (binding.edtType as? AutoCompleteTextView)?.setAdapter(expAdapter)


        listJobPostedSince.add(resources.getString(R.string.all_days))
        listJobPostedSince.add(resources.getString(R.string.two_days))
        listJobPostedSince.add(resources.getString(R.string.one_week))
        listJobPostedSince.add(resources.getString(R.string.two_weeks))
        listJobPostedSince.add(resources.getString(R.string.weeks_3))
        listJobPostedSince.add(resources.getString(R.string.weeks_4))
        val jobPostedSinceAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                listJobPostedSince
            )
        (binding.edtJobPostedSince as? AutoCompleteTextView)?.setAdapter(jobPostedSinceAdapter)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgClose -> {
                findNavController().popBackStack()
            }
            R.id.txtReset -> {
                clearSearch()
            }
            R.id.btnApply -> {
//                applyFilter()
                when {
                    binding.edtType.text.toString()
                        .trim() == resources.getString(R.string.male) -> {
                        gender = 1
                    }
                    binding.edtType.text.toString()
                        .trim() == resources.getString(R.string.female) -> {
                        gender = 2
                    }
                    binding.edtType.text.toString()
                        .trim() == resources.getString(R.string.both_of_them) -> {
                        gender = 3
                    }
                }

                when {
                    binding.edtJobPostedSince.text.toString()
                        .trim() == resources.getString(R.string.all_days) -> {
                        created_at = 0
                    }
                    binding.edtJobPostedSince.text.toString()
                        .trim() == resources.getString(R.string.two_days) -> {
                        created_at = 2
                    }
                    binding.edtJobPostedSince.text.toString()
                        .trim() == resources.getString(R.string.one_week) -> {
                        created_at = 7
                    }
                    binding.edtJobPostedSince.text.toString()
                        .trim() == resources.getString(R.string.two_weeks) -> {
                        created_at = 14
                    }
                    binding.edtJobPostedSince.text.toString()
                        .trim() == resources.getString(R.string.weeks_3) -> {
                        created_at = 21
                    }
                    binding.edtJobPostedSince.text.toString()
                        .trim() == resources.getString(R.string.weeks_4) -> {
                        created_at = 28
                    }
                }
                searchRequest.jobTitle = binding.edtAutoCompleteSearch.text.toString()
                searchRequest.gender = gender
                searchRequest.created_at = created_at
                searchRequest.city = city_id
                searchRequest.from = seekbar_min
                searchRequest.to = seekbar_max
                searchRequest.jobType.addAll(jType)
                searchRequest.certificate = qualification_id
                val bundle = bundleOf(
                    Constant.HOME_FILTER to searchRequest
                )
                navController.navigate(
                    R.id.action_homeFilterJobListFragment_to_findJobsFragment,
                    bundle
                )
            }
        }
    }

    private fun applyFilter() {
        when {
            binding.edtType.text.toString()
                .trim() == resources.getString(R.string.male) -> {
                gender = 1
            }
            binding.edtType.text.toString()
                .trim() == resources.getString(R.string.female) -> {
                gender = 2
            }
            binding.edtType.text.toString()
                .trim() == resources.getString(R.string.both_of_them) -> {
                gender = 3
            }
        }

        when {
            binding.edtJobPostedSince.text.toString()
                .trim() == resources.getString(R.string.all_days) -> {
                created_at = 0
            }
            binding.edtJobPostedSince.text.toString()
                .trim() == resources.getString(R.string.two_days) -> {
                created_at = 2
            }
            binding.edtJobPostedSince.text.toString()
                .trim() == resources.getString(R.string.one_week) -> {
                created_at = 7
            }
            binding.edtJobPostedSince.text.toString()
                .trim() == resources.getString(R.string.two_weeks) -> {
                created_at = 14
            }
            binding.edtJobPostedSince.text.toString()
                .trim() == resources.getString(R.string.weeks_3) -> {
                created_at = 21
            }
            binding.edtJobPostedSince.text.toString()
                .trim() == resources.getString(R.string.weeks_4) -> {
                created_at = 28
            }
        }
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "job_title", binding.edtAutoCompleteSearch.text.toString(),
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "city", city_id
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "jobCat", major_id
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "subCat", job_role_id
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "from", seekbar_min
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "to", seekbar_max
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "jobType", jType
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "gender", gender
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "certificate", qualification_id
        )
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "created_at", created_at
        )

        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            "HOME_API_CALL", "HOME_API_CALL"
        )
        navigateToFindJobs()



        findNavController().popBackStack()


    }

    private fun navigateToFindJobs() {
        searchRequest.jobTitle = job_title
        searchRequest.gender = gender
        searchRequest.created_at = created_at
        searchRequest.city = city_id
        searchRequest.from = seekbar_min
        searchRequest.to = seekbar_max
        searchRequest.jobType.add(permanency_type_id)
        searchRequest.certificate = qualification_id
        val bundle = bundleOf(
            "HOME_FILTER" to searchRequest
        )
        navController.navigate(
            R.id.action_homeFragment_to_findJobsFragment,
            bundle
        )
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


    private fun setDropDownCityData(city: List<CityList>?) {
        /**
         * Set City list into dropdown
         */
        cityDropDownListAdapter =
            CityDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                city!!
            )

        binding.edtChooseCity.setAdapter(cityDropDownListAdapter)
        binding.edtChooseCity.setOnClickListener {
            binding.edtChooseCity.showDropDown()
        }
        if (searchRequest.city != 0) {
            binding.edtChooseCity.setText(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") city.filter { it.id == searchRequest.city }
                .map { it.name_en }[0] else city.filter { it.id == searchRequest.city }
                .map { it.name_ar }[0])
        }

        binding.edtChooseCity.setOnItemClickListener { _, _, position, _ ->

            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                binding.edtChooseCity.setText(
                    cityDropDownListAdapter.getSpecialityList()[position].name_en
                        ?: cityDropDownListAdapter.getSpecialityList()[position].name_ar
                )
            } else {
                binding.edtChooseCity.setText(cityDropDownListAdapter.getSpecialityList()[position].name_ar)
            }
            city_id = cityDropDownListAdapter.getSpecialityList()[position].id!!
            cityDropDownListAdapter.notifyDataSetChanged()
        }
    }


    private fun setMajorData(specialization: List<SpecializationData>?) {
        /**
         * Set Major list into dropdown
         */
        majorListAdapter =
            MajorListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                specialization!!
            )

        binding.edtMajor.setAdapter(majorListAdapter)
        binding.edtMajor.setOnClickListener {
            binding.edtMajor.showDropDown()
        }
        if (searchRequest.jobCat != 0) {
            binding.edtMajor.setText(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") specialization.filter { it.id == searchRequest.jobCat }
                .map { it.title_en }[0] else specialization.filter { it.id == searchRequest.jobCat }
                .map { it.title }[0])
        }

        binding.edtMajor.setOnItemClickListener { _, _, position, _ ->
            setJobRoleData(majorListAdapter.getSpecialityList()[position].roles!!)
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                binding.edtMajor.setText(
                    majorListAdapter.getSpecialityList()[position].title_en
                        ?: majorListAdapter.getSpecialityList()[position].title
                )
            } else {
                binding.edtMajor.setText(majorListAdapter.getSpecialityList()[position].title)
            }
            major_id = majorListAdapter.getSpecialityList()[position].id!!
            majorListAdapter.notifyDataSetChanged()
        }
    }

    private fun setJobRoleData(category: List<Role>?) {
        /**
         * Set JobRole list into dropdown
         */
        jobRoleListAdapter =
            JobRoleListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                category!!
            )

        binding.edtJobRole.setAdapter(jobRoleListAdapter)
        binding.edtJobRole.setOnClickListener {
            binding.edtJobRole.showDropDown()
        }
        if (searchRequest.subCat != 0) {
            binding.edtJobRole.setText(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") category.filter { it.id == searchRequest.subCat }
                .map { it.title_en }[0] else category.filter { it.id == searchRequest.subCat }
                .map { it.title }[0])
        }

        binding.edtJobRole.setOnItemClickListener { _, _, position, _ ->

            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                binding.edtJobRole.setText(
                    jobRoleListAdapter.getSpecialityList()[position].title_en
                        ?: jobRoleListAdapter.getSpecialityList()[position].title
                )
            } else {
                binding.edtJobRole.setText(jobRoleListAdapter.getSpecialityList()[position].title)
            }
            job_role_id = jobRoleListAdapter.getSpecialityList()[position].id!!
            jobRoleListAdapter.notifyDataSetChanged()
        }
    }


    private fun setPermanencyTypeData(jobType: List<JobsType>?) {

        for (i in jobsType.jobsType!!.indices) {
            val chip: Chip =
                layoutInflater.inflate(R.layout.row_chip_view, binding.chipGroup, false) as Chip

            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                chip.text = jobsType.jobsType!![i].title_en
                chip.id = jobsType.jobsType!![i].id!!
            } else {
                chip.text = jobsType.jobsType!![i].title_ar
                chip.id = jobsType.jobsType!![i].id!!
            }
            chip.setOnClickListener {
                Log.d("TAG", "setPermanencyTypeData: CLICKED--> ${it.id}")
                if(jType.contains(it.id)){
                    jType.remove(it.id)
                    Log.d("TAG", "REMOVED --> $jType")
                    print("REMOVED--> $jType")
                }else{
                    jType.add(it.id)
                    Log.d("TAG", "ADDED --> $jType")
                    print("ADDED--> $jType")
                }
                Log.d("TAG", "ARRAY --> $jType")
            }
            binding.chipGroup.addView(chip)
            if (permanency_type_id != 0) {
                binding.chipGroup.check(permanency_type_id)
            }

        }
        binding.chipGroup.isSingleSelection = false
        binding.chipGroup.isSelectionRequired = false
        val checkedChipId = binding.chipGroup.checkedChipId

        binding.chipGroup.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == checkedChipId) {
                binding.chipGroup.check(checkedChipId)
            }

            jType.add(checkedId)
            permanency_type_id = checkedId
            Log.d("TAG", "setPermanencyTypeData: group --> $group")
            Log.d("TAG", "setPermanencyTypeData: ccc --> $checkedId")
            Log.d("TAG", "setPermanencyTypeData: Array --> $jType")
        }
        /**
         * Set JobRole list into dropdown
         */
        permanencyTypeListAdapter =
            PermanencyTypeListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                jobType!!
            )

        binding.edtPermanencyType.setAdapter(permanencyTypeListAdapter)
        binding.edtPermanencyType.setOnClickListener {
            binding.edtPermanencyType.showDropDown()
        }
        if (searchRequest.jobType.isNotEmpty()) {
            binding.edtPermanencyType.setText(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") jobType.filter {
                searchRequest.jobType.add(it.id!!)}
                .map { it.title_en }[0] else jobType.filter { searchRequest.jobType.add(it.id!!)  }
                .map { it.title_ar }[0])
        }

        binding.edtPermanencyType.setOnItemClickListener { _, _, position, _ ->
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                binding.edtPermanencyType.setText(
                    permanencyTypeListAdapter.getSpecialityList()[position].title_en
                        ?: permanencyTypeListAdapter.getSpecialityList()[position].title_ar
                )
            } else {
                binding.edtPermanencyType.setText(permanencyTypeListAdapter.getSpecialityList()[position].title_ar)
            }
            permanency_type_id = permanencyTypeListAdapter.getSpecialityList()[position].id!!
            permanencyTypeListAdapter.notifyDataSetChanged()
        }
    }


    private fun setQualificationData(qualificationList: List<QualificationList>?) {
        /**
         * Set JobRole list into dropdown
         */
        qualificationListAdapter =
            QualificationListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                qualificationList!!
            )

        binding.edtRequiredQualification.setAdapter(qualificationListAdapter)
        binding.edtRequiredQualification.setOnClickListener {
            binding.edtRequiredQualification.showDropDown()
        }
        if (searchRequest.certificate != 0) {
            binding.edtRequiredQualification.setText(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") qualificationList.filter { it.id == searchRequest.certificate }
                .map { it.title_en }[0] else qualificationList.filter { it.id == searchRequest.certificate }
                .map { it.title_ar }[0])
        }

        binding.edtRequiredQualification.setOnItemClickListener { _, _, position, _ ->
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                binding.edtRequiredQualification.setText(qualificationListAdapter.getSpecialityList()[position].title_en)
            } else {
                binding.edtRequiredQualification.setText(qualificationListAdapter.getSpecialityList()[position].title_ar)
            }
            qualification_id = qualificationListAdapter.getSpecialityList()[position].id!!
            qualificationListAdapter.notifyDataSetChanged()
        }
    }

    private fun clearSearch() {
        binding.edtAutoCompleteSearch.setText("")
        binding.edtChooseCity.setText("")
        binding.edtMajor.setText("")
        binding.edtJobRole.setText("")
        binding.edtPermanencyType.setText("")
        binding.edtType.setText("")
        binding.edtRequiredQualification.setText("")
        binding.edtJobPostedSince.setText("")

        binding.seekBarMinMax.values = listOf(0f, 100000f)
        binding.textHeightOneStart.text = "0"
        binding.textHeightOneEnd.text = "100000"

        job_title = ""
        city_id = 0
        jobCat = 0
        subCat = 0
        from = 0
        to = 0
        jobType = 0
        gender = 0
        certificate = 0
        created_at = 0
    }
}