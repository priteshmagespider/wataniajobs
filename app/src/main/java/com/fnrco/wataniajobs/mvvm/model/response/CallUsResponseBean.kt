package com.fnrco.wataniajobs.mvvm.model.response

data class CallUsResponseBean(
    val message: String? = null
)