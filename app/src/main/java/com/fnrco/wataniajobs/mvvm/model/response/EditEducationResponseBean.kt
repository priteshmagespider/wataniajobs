package com.fnrco.wataniajobs.mvvm.model.response

data class EditEducationResponseBean(
    val message: String? = null
)