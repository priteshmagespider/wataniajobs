package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentMoreScreenDetailsBinding
import com.fnrco.wataniajobs.mvvm.model.response.MoreScreenService
import com.fnrco.wataniajobs.mvvm.model.response.WhoWeAreResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class MoreScreenDetailsFragment : Fragment(), View.OnClickListener {

    private val mTAG = MoreScreenDetailsFragment::class.java.simpleName
    private lateinit var binding: FragmentMoreScreenDetailsBinding

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: MoreScreenViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMoreScreenDetailsBinding.inflate(layoutInflater, container, false)
        setObserver()
        when {
            requireArguments().containsKey(Constant.INTENT.ABOUT) -> {
                binding.txtCandidateTitle.text = requireActivity().getString(R.string.about_the_app)
                viewModel.setMoreScreenApiCall(
                    MoreScreenEvent.AboutEvent(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!
                    )
                )

            }
            requireArguments().containsKey(Constant.INTENT.SPEECH) -> {
                viewModel.setMoreScreenApiCall(
                    MoreScreenEvent.SpeechEvent(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!
                    )
                )
            }
            requireArguments().containsKey(Constant.INTENT.VISION_GOALS) -> {
                viewModel.setMoreScreenApiCall(
                    MoreScreenEvent.VisionGoalsEvent(
                        prefManager.getString(
                            PrefConstant.PREF_CURRENT_LANGUAGE
                        )!!
                    )
                )
            }
            requireArguments().containsKey(Constant.INTENT.SERVICE_TYPE) -> {
                val list: MoreScreenService =
                    requireArguments().getSerializable(Constant.INTENT.SERVICE_TYPE) as MoreScreenService
                setServiceData(list)

            }
        }
        Log.d(
            mTAG,
            "MoreScreenDetailsFragment_ CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
    }

    private fun setObserver() {
        /** About */
        viewModel.aboutResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<WhoWeAreResponseBean> -> {
                    displayProgressBar(false)
                    setAboutData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        /** Speech */
        viewModel.speechResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<WhoWeAreResponseBean> -> {
                    displayProgressBar(false)
                    speechData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /** Vision Goals */
        viewModel.visionGoalsResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<WhoWeAreResponseBean> -> {
                    displayProgressBar(false)
                    visionGoalsData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun setAboutData(whoWeAreResponseBean: WhoWeAreResponseBean) {
        Log.d(
            mTAG,
            "setObserver: ${Constant.IMAGE_URL}${whoWeAreResponseBean.setting!!.about_photo}"
        )
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.about_app_image}")
            .into(binding.imgWhoWeAre)

        binding.txtTitle.text = getString(R.string.lbl_about_the_company)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                val unEncodedHtml: String = whoWeAreResponseBean.setting.about_app_en!!.toString()
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.about_app_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            } else {
                val unEncodedHtml: String = "<html dir=\"rtl\" lang=\"\"><body>${whoWeAreResponseBean.setting.about_app_ar!!.toString()}</body></html>"
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.about_app_ar}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            }
        } else {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                val unEncodedHtml: String = whoWeAreResponseBean.setting.about_app_en!!.toString()
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.about_en}</p>")
            } else {
                val unEncodedHtml: String = "<html dir=\"rtl\" lang=\"\"><body>${whoWeAreResponseBean.setting.about_app_ar!!.toString()}</body></html>"
                val encodedHtml = Base64.encodeToString(
                    unEncodedHtml.toByteArray(),
                    Base64.NO_PADDING
                )
                binding.webViewCMSContent.loadData(encodedHtml, "text/html", "base64")
                binding.txtContent.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.about_ar}</p>")
            }
        }
    }

    private fun speechData(whoWeAreResponseBean: WhoWeAreResponseBean) {
        Log.d(
            mTAG,
            "setObserver: ${Constant.IMAGE_URL}${whoWeAreResponseBean.setting!!.speech_photo}"
        )
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.speech_photo}")
            .into(binding.imgWhoWeAre)

        binding.txtTitle.text = getString(R.string.lbl_director_speech)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.speech_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            } else {
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.speech_ar}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            }
        } else {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.txtContent.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.speech_en}</p>")
            } else {
                binding.txtContent.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.speech_ar}</p>")
            }
        }
    }

    private fun visionGoalsData(whoWeAreResponseBean: WhoWeAreResponseBean) {
        Log.d(
            mTAG,
            "setObserver: ${Constant.IMAGE_URL}${whoWeAreResponseBean.setting!!.vision_photo}"
        )
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        binding.txtVision.visibility = View.VISIBLE
        binding.cardWhoWeAreGoals.visibility = View.VISIBLE
        binding.txtGoals.visibility = View.VISIBLE
        binding.txtContentGoals.visibility = View.VISIBLE
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.vision_photo}")
            .into(binding.imgWhoWeAre)
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${whoWeAreResponseBean.setting.mission_photo}")
            .into(binding.imgWhoWeAreGoals)

        binding.txtTitle.text = getString(R.string.lbl_vision_goals)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.vision_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                binding.txtContentGoals.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.mission_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            } else {
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.vision_ar}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                binding.txtContentGoals.text =
                    Html.fromHtml(
                        "<p>${whoWeAreResponseBean.setting.mission_ar}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            }
        } else {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.txtContent.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.vision_en}</p>")
                binding.txtContentGoals.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.vision_en}</p>")
            } else {
                binding.txtContent.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.mission_en}</p>")
                binding.txtContentGoals.text =
                    Html.fromHtml("<p>${whoWeAreResponseBean.setting.mission_ar}</p>")
            }
        }
    }

    private fun setServiceData(list: MoreScreenService) {
        binding.nsvWhoWeAre.visibility = View.VISIBLE
        Log.d(mTAG, "setServiceData: ${Constant.IMAGE_URL_WITHOUT_PHOTOS}${list.image}")
        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITHOUT_PHOTOS}${list.image}")
            .into(binding.imgWhoWeAre)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            binding.txtTitle.text = list.name_en
        } else {
            binding.txtTitle.text = list.name_ar
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${list.content_en}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            } else {
                binding.txtContent.text =
                    Html.fromHtml(
                        "<p>${list.content_ar}</p>",
                        Html.FROM_HTML_MODE_COMPACT
                    )
            }
        } else {
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.txtContent.text =
                    Html.fromHtml("<p>${list.content_en}</p>")
            } else {
                binding.txtContent.text =
                    Html.fromHtml("<p>${list.content_ar}</p>")
            }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                when {
                    requireArguments().containsKey(Constant.INTENT.SERVICE_TYPE) -> {
                        findNavController().previousBackStackEntry?.savedStateHandle?.set(
                            "key1", "Service"
                        )
                        findNavController().popBackStack()
                    }
                    else -> {
                        findNavController().previousBackStackEntry?.savedStateHandle?.set(
                            "key1", "Who We Are"
                        )
                        findNavController().popBackStack()
                    }
                }

            }
        }
    }
}
