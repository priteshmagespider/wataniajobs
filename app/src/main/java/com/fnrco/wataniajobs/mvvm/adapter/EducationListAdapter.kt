package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Education
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager

class EducationListAdapter(
    var context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Education>,
    val prefManager: PrefManager
) : RecyclerView.Adapter<EducationListAdapter.MyViewHolder>() {

    val listData = list as MutableList<Education>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtCompleteYear: TextView = itemView.findViewById(R.id.txtCompleteYear)
        val txtSpecialisation: TextView = itemView.findViewById(R.id.txtSpecialisation)
        val txtCityAndCompany: TextView = itemView.findViewById(R.id.txtCityAndCompany)
        val imgEducationEdit: ImageView = itemView.findViewById(R.id.imgEducationEdit)
        val imgEducationDelete: ImageView = itemView.findViewById(R.id.imgEducationDelete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_education_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgEducationEdit.tag = position
        holder.imgEducationEdit.setOnClickListener(onClickListener)
        holder.imgEducationDelete.tag = position
        holder.imgEducationDelete.setOnClickListener(onClickListener)
        holder.txtCompleteYear.text = listData[position].end_date_educational
        holder.txtSpecialisation.text = listData[position].specialization
        holder.txtCityAndCompany.text = listData[position].educational_institution
        if(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en"){
            holder.txtCityAndCompany.textDirection = View.TEXT_DIRECTION_LTR
        }else{
            holder.txtCityAndCompany.textDirection = View.TEXT_DIRECTION_RTL
        }

    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}