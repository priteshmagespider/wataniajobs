package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.ActivityResetPasswordBinding
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.ResetPasswordResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.ForgotPasswordEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.ForgotPasswordViewModel
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class ResetPasswordActivity : AppCompatActivity(), View.OnClickListener {

    private val mTAG = ResetPasswordActivity::class.java.simpleName

    private lateinit var binding: ActivityResetPasswordBinding
    private var lastClickTime: Long = 0
    lateinit var email: String

    @ExperimentalCoroutinesApi
    private val viewModel: ForgotPasswordViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (intent.hasExtra(Constant.INTENT.INTENT_FROM_VERIFY_TO_RESET_PASSWORD)) {
            email = intent.extras!!.getString(Constant.INTENT.EMAIL)!!
        }
        initUi()
        setObserver()
    }


    private fun initUi() {

        binding.imgBack.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.btnSubmit -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                if (isValid()) {
                    val authRequest = AuthRequest()
                    authRequest.email = email
                    authRequest.password = binding.edtPasswordE.text.toString().trim()
                    authRequest.is_admin = Constant.IS_ADMIN
                    viewModel.setForgotPasswordApiCall(
                        ForgotPasswordEvent.ResetPasswordEvent(
                            authRequest
                        )
                    )
                }
            }
        }
    }

    private fun isValid(): Boolean {
//        binding.inputPassword.isErrorEnabled = false
//        binding.inputConfirmPassword.isErrorEnabled = false

        when {
            binding.edtPasswordE.text.toString().isEmpty() -> {
                binding.edtPasswordE.error = getString(R.string.error_enter_password)
                binding.edtPasswordE.requestFocus()
                return false
            }
            binding.edtPasswordE.text.toString().length < 6 -> {
                binding.edtPasswordE.error = getString(R.string.error_enter_password_long)
                binding.edtPasswordE.requestFocus()
                return false
            }
            binding.edtConfirmPasswordE.text.toString() != binding.edtPasswordE.text.toString() -> {
                binding.edtConfirmPasswordE.error =
                    getString(R.string.error_enter_confirm_password_not_match)
                binding.edtConfirmPasswordE.requestFocus()
                return false
            }
        }
        return true
    }

    private fun setObserver() {
        /** Reset Password Api call*/
        viewModel.resetPasswordResponse.observe(this, { dataState ->
            when (dataState) {
                is DataState.Success<ResetPasswordResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.message == "Password  Change successfully") {
                        startActivity(
                            Intent(this, LoginActivity::class.java)
                        )
                    }
                    Toast.makeText(this, dataState.data.message, Toast.LENGTH_SHORT).show()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }
}