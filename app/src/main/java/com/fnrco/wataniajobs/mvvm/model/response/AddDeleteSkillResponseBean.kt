package com.fnrco.wataniajobs.mvvm.model.response

class AddDeleteSkillResponseBean : ArrayList<AddDeleteSkillResponseBeanItem>()

data class AddDeleteSkillResponseBeanItem(
    val created_at: String? = null,
    val id: Int? = null,
    val person_informateion_id: Int? = null,
    val skills_name: String? = null,
    val updated_at: String? = null,
    val user_id: Int? = null
)