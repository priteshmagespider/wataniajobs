package com.fnrco.wataniajobs.mvvm.model.response

data class SinglePostResponseBean(
    var post: PostSinglePost? = null
)

data class PostSinglePost(
    var id: Int? = null,
    var com_id: Int? = null,
    var title: String? = null,
    var description: String,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: String? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: String? = null,
    var computer_level: ComputerLevelSingleResponse? = null,
    var MsOffice_level: String? = null,
    var other_condition: String? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: String? = null,
    var company_logo: String? = null,
    var is_favourite : Int? = null,

    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: String? = null,
    var job_id: String? = null,
    var website: String? = null,
    var com_email: String? = null,
    var com_name: String? = null,
    var com_logo: String? = null,
    var job_date: String? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var city: String? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var applied: Int? = null,
    var photo_shared: String? = null,
    var job_role: JobRoleSinglePost? = null,
    var specialization: SpecializationSinglePost? = null,
    var qualification: QualificationSinglePost? = null,
    var job__type: JobTypeSinglePost? = null,
    var language_level: LanguageLevelSinglePost? = null,
    var office_level: OfficeLevelSinglePost? = null,
    var job_city: JobCitySinglePost? = null,
    var company: CompanySinglePost? = null
)

data class OfficeLevelSinglePost(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null
)

data class LanguageLevelSinglePost(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)


data class SpecializationSinglePost(
    var id: Int? = null,
    var title_en: String? = null,
    var title: String? = null
)

data class JobRoleSinglePost(
    var id: Int? = null,
    var title_en: String? = null,
    var title: String? = null
)


data class CompanySinglePost(
    var id: Int? = null,
    var user_id: Int? = null,
    var comNum: String? = null,
    var sector: String? = null,
    var mobile: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var city: String? = null,
    var address: String?,
    var logo: String? = null,
    var photo_approve: String? = null,
    var size: String? = null,
    var `field`: String? = null,
    var user_job_title: String? = null,
    var status: Int? = null,
    var phone_is_verified: Int? = null,
    var sms_try: Int? = null,
    var phone_verification_code: String? = null,
    var career_objective: String? = null,
    var package_id: String? = null,
    var total_cv: String? = null,
    var expiration_date: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)


data class JobCitySinglePost(
    var id: Int? = null,
    var name_en: String? = null,
    var name_ar: String? = null
)


data class JobTypeSinglePost(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)

data class QualificationSinglePost(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)


data class ComputerLevelSingleResponse(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)