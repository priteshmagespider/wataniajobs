package com.fnrco.wataniajobs.mvvm.model.response

data class AppliedJobResponseBean(
    val posts: List<AppliedJobPostData>?,
    val type: String?
)

data class AppliedJobPostData(
    val description: String?,
    val description_en: String?,
    val MsOffice_level: String?,
    val age: Any?,
    val ageFrom: Int?,
    val ageTo: Int?,
    val certificate: Int?,
    val city: Any?,
    val com_email: Any?,
    val com_id: Int?,
    val com_logo: Any?,
    val com_name: Any?,
    val company: Company?,
    val computer_level: ComputerLevel?,
    val country: String?,
    val created_at: String?,
    val driving: String?,
    val end_at: String?,
    val english_level: String?,
    val experience: Any?,
    val experienceFrom: Int?,
    val experienceTo: Int?,
    val gender: String?,
    val id: Int?,
    val company_logo: String?,
    val is_favourite: Int?,
    val is_shared: Int?,
    val is_show: Int?,
    val jobCategory: Int?,
    val jobRole: String?,
    val jobSpecialization: Any?,
    val jobType: String?,
    val job__type: JobType?,
    val job_city: JobCity?,
    val job_date: Any?,
    val job_id: Any?,
    val job_role: JobRole?,
    val language_level: LanguageLevel?,
    val office_level: OfficeLevel?,
    val other_condition: String?,
    val other_condition_en: String?,
    val photo_shared: Any?,
    val positionNum: Int?,
    val qualification: Qualification?,
    val salary: String?,
    val specialization: Specialization?,
    val statue_job: Any?,
    val status: String?,
    val title: String?,
    val title_en: String?,
    val updated_at: String?,
    val website: Any?
)

data class AppliedJobCompany(
    val address: String?,
    val career_objective: String?,
    val city: String?,
    val comNum: Any?,
    val created_at: String?,
    val expiration_date: Any?,
    val field: String?,
    val id: Int?,
    val logo: String?,
    val mobile: String?,
    val package_id: Any?,
    val phone: String?,
    val phone_is_verified: Int?,
    val phone_verification_code: Int?,
    val photo_approve: Int?,
    val sector: Any?,
    val size: String?,
    val sms_try: Int?,
    val status: Int?,
    val total_cv: Any?,
    val updated_at: String?,
    val user_id: Int?,
    val user_job_title: String?,
    val website: String?
)

data class AppliedJobComputerLevel(
    val id: Int?,
    val title_ar: String?
)

data class AppliedJobJobType(
    val id: Int?,
    val title_ar: String?
)

data class AppliedJobJobCity(
    val id: Int?,
    val name_ar: String?
)

data class AppliedJobJobRole(
    val cat_id: Int?,
    val created_at: String?,
    val id: Int?,
    val title: String?,
    val title_en: Any?,
    val updated_at: String?
)

data class AppliedJobLanguageLevel(
    val id: Int?,
    val title_ar: String?
)

data class AppliedJobOfficeLevel(
    val id: Int?,
    val title_ar: String?
)

data class AppliedJobPostQualification(
    val id: Int?,
    val title_ar: String?
)

data class AppliedJobPostSpecialization(
    val created_at: String?,
    val id: Int?,
    val order: Int?,
    val title: String?,
    val title_en: String?,
    val updated_at: String?
)