package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Skill

class SkillListAdapter(
    val context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Skill>?
) : RecyclerView.Adapter<SkillListAdapter.MyViewHolder>() {

    val listData: MutableList<Skill> = list as MutableList<Skill>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtSkill: TextView = itemView.findViewById(R.id.txtSkill)
        val imgSkillDelete: ImageView = itemView.findViewById(R.id.imgSkillDelete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_skill_items, parent, false)
        )
    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgSkillDelete.tag = position
        holder.imgSkillDelete.setOnClickListener(onClickListener)
        holder.txtSkill.text = listData[position].skills_name
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}