package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.response.DeleteJobPostResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.NotificationListResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.SaveDeletePhoneEmailResponse
import com.fnrco.wataniajobs.repository.NotificationRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class NotificationViewModel
@ViewModelInject
constructor(
    private val notificationRepository: NotificationRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    /** Notification List */
    private val _notificationResponse: MutableLiveData<DataState<NotificationListResponseBean>> =
        MutableLiveData()

    val notificationResponse: LiveData<DataState<NotificationListResponseBean>>
        get() = _notificationResponse

    private val _deleteNotification: MutableLiveData<DataState<DeleteJobPostResponseBean>> =
        MutableLiveData()

    val deleteNotification: LiveData<DataState<DeleteJobPostResponseBean>>
        get() = _deleteNotification


    fun setNotificationApiCall(notificationEvent: NotificationEvent) {
        viewModelScope.launch {
            when (notificationEvent) {
                is NotificationEvent.NotificationListEvent -> {
                    notificationRepository.notificationListApiCall(notificationEvent.lang)
                        .onEach {
                            _notificationResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is NotificationEvent.DeleteNotification -> {
                    notificationRepository.deleteNotificationListApiCall(
                        notificationEvent.notificationId
                    )
                        .onEach {
                            _deleteNotification.value = it
                        }
                        .launchIn(viewModelScope)
                }
            }
        }
    }
}

sealed class NotificationEvent {

    data class NotificationListEvent(val lang: String) : NotificationEvent()

    data class DeleteNotification(val notificationId: String) : NotificationEvent()
}