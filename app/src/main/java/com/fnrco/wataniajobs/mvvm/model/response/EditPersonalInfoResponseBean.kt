package com.fnrco.wataniajobs.mvvm.model.response

data class EditPersonalInfoResponseBean(
    val status: Boolean? = null
)