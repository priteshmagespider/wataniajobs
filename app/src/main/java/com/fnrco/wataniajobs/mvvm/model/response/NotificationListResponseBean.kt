package com.fnrco.wataniajobs.mvvm.model.response

data class NotificationListResponseBean(
    val notifications: List<Notification>?
)

data class Notification(
    var id : Int? = null,
    var title_en: String? = null,
    var message_en: String? = null,
    var type: String? = null,
    var user_id: Int? = null,
    var post_id: Int? = null,
    val message_ar: String?,
    val title_ar: String?
)


