package com.fnrco.wataniajobs.mvvm.model.response

data class SaveDeletePhoneEmailResponse(
    val status: Boolean?
)