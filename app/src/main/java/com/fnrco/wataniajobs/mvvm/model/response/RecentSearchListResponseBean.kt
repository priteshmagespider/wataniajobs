package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class RecentSearchListResponseBean(
    var posts: PostsRecentSearch? = null
)

data class PostsRecentSearch(
    var current_page: Int? = null,
    var `data`: List<DataRecentSearch>? = null,
    var first_page_url: String? = null,
    var from: Int? = null,
    var last_page: Int? = null,
    var last_page_url: String? = null,
    var next_page_url: String? = null,
    var path: String? = null,
    var per_page: Int? = null,
    var prev_page_url: String? = null,
    var to: Int? = null,
    var total: Int? = null
)

data class DataRecentSearch(
    var id: Int? = null,
    var com_id: String? = null,
    var title: String? = null,
    var status: String? = null,
    var certificate: Int? = null,
    var end_at: String? = null,
    var positionNum: Int? = null,
    var experience: String? = null,
    var driving: String? = null,
    var gender: String? = null,
    var english_level: String? = null,
    var age: String? = null,
    var computer_level: ComputerLevelRecentSearch? = null,
    var MsOffice_level: String? = null,
    var other_condition: String? = null,
    var salary: String? = null,
    var jobType: String? = null,
    var statue_job: String? = null,
    var jobRole: String? = null,
    var country: String? = null,
    var jobSpecialization: String? = null,
    var job_id: Int? = null,
    var website: String? = null,
    var com_email: String? = null,
    var com_name: String? = null,
    var com_logo: String? = null,
    var job_date: String? = null,
    var jobCategory: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var other_condition_en: String? = null,
    var description_en: String? = null,
    var title_en: String? = null,
    var job_city: JobCityRecentSearch? = null,
    var city: String? = null,
    var experienceFrom: Int? = null,
    var experienceTo: Int? = null,
    var ageFrom: Int? = null,
    var ageTo: Int? = null,
    var is_shared: Int? = null,
    var is_favourite: Int? = null,
    var company_logo: String? = null,
    var photo_shared: String? = null,
    var description: String? = null,
    var job_role: JobRoleRecentSearch? = null,
    var specialization: SpecializationRecentSearch? = null,
    var job_applicants: List<JobApplicantRecentSearch>? = null,
    var qualification: QualificationRecentSearch? = null,
    var job__type: JobTypeRecentSearch? = null,
    var language_level: LanguageLevelRecentSearch? = null,
    var office_level: OfficeLevelRecentSearch? = null,
    var company: CompanyRecentSearch? = null
)



data class CompanyRecentSearch(
    var id: Int? = 0,
//    var user_id: UserId? = null,
    var user_id: Int? = null,
    var comNum: Int? = 0,
    var sector: String? = null,
    var mobile: String? = null,
    var phone: String? = null,
    var website: String? = null,
    var city: String? = null,
    var address: String? = null,
    var logo: String? = null,
    var photo_approve: Int? = 0,
    var size: String? = null,
    var `field`: String? = null,
    var user_job_title: String? = null,
    var status: Int? = 0,
    var phone_is_verified: Int? = 0,
    var sms_try: Int? = 0,
    var phone_verification_code: Int? = 0,
    var career_objective: String? = null,
    var package_id: Int? = 0,
    var total_cv: Int? = 0,
    var expiration_date: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
):Serializable

data class JobCityRecentSearch(
    var id: Int? = 0,
    var name_en: String? = null,
    var name_ar: String? = null
): Serializable

data class ComputerLevelRecentSearch(
    var id: Int? = null,
    var title_ar: String? = null
)

data class JobRoleRecentSearch(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class SpecializationRecentSearch(
    var id: Int? = null,
    var title: String? = null,
    var title_en: String? = null,
    var order: Int? = null,
    var created_at: String? = null,
    var updated_at: String? = null
)

data class JobApplicantRecentSearch(
    var job_seeker_id: Int? = null,
    var user_id: Int? = null,
    var created_at: String? = null,
    var pivot: PivotRecentSearch? = null
)

data class QualificationRecentSearch(
    var id: Int? = null,
    var title_ar: String? = null
)

data class JobTypeRecentSearch(
    var id: Int? = null,
    var title_ar: String? = null,
    var title_en: String? = null
)

data class LanguageLevelRecentSearch(
    var id: Int? = null,
    var title_ar: String? = null
)

data class OfficeLevelRecentSearch(
    var id: Int? = null,
    var title_ar: String? = null
)

data class PivotRecentSearch(
    var jobPost_id: Int? = null,
    var jobSeeker_id: Int? = null
)