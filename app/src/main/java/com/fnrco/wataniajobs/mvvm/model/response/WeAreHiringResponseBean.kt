package com.fnrco.wataniajobs.mvvm.model.response

data class WeAreHiringResponseBean(
    val specializations: List<WeAreHiringSpecialization>? = null
)

data class WeAreHiringSpecialization(
    val count: Int? = null,
    val created_at: String? = null,
    val id: Int? = null,
    val order: Int? = null,
    val title_en: String? = null,
    val title: String? = null,
    val updated_at: String? = null
)