package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentEditProfilePersonalInfoBinding
import com.fnrco.wataniajobs.mvvm.adapter.CityDropDownListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class EditProfilePersonalInfoFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentEditProfilePersonalInfoBinding
    private lateinit var navController: NavController
    private lateinit var profileResponseBean: ProfileResponseBean

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    private lateinit var cityDropDownListAdapter: CityDropDownListAdapter

    private var gender = ""
    private var maritalStatus = ""
    var city: String = ""
    private lateinit var specializationList: List<SpecializationBasicData>
    private lateinit var cityList: List<CityList>
    private var genderId = ""
    private var maritalStatusId = ""
    private var endDate = ""
    private val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    private val month = c.get(Calendar.MONTH)
    private val day = c.get(Calendar.DAY_OF_MONTH)

    private var arrayTypeSize = arrayOf(
        "Male",
        "Female"
    )

    private var arrayTypeArSize = arrayOf(
        "ذكر",
        "أنثى"
    )

    private var arraySocialStatusSize = arrayOf(
        "Married",
        "Unmarried",
        "Absolute",
        "Widower"
    )

    private var arraySocialStatusArSize = arrayOf(
        "متزوج",
        "اعزب",
        "مطلق",
        "أرمل"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditProfilePersonalInfoBinding.inflate(layoutInflater)



        if (requireArguments().containsKey(Constant.PROFILE_DATA)) {
            profileResponseBean =
                requireArguments().getSerializable(Constant.PROFILE_DATA) as ProfileResponseBean

        }


        viewModel.setStateEvent(
            EditProfileStateEvent.EditProfileCity(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )

        initUi()
        getProfileDataApiCall()
        setObserver()
        return binding.root
    }

    private fun setObserver() {
        /** Basic Data Update Interest  */
        viewModel.basicDataUpdate.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<BasicDataUpdateResponseBean> -> {
                    displayProgressBar(false)

                    /*findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "name", binding.edtName.text.toString(),
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "job_title", binding.edtTheJobTitle.text.toString()
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "specialization", binding.edtSpecialization.text.toString()
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "career_role", binding.edtCareerRole.text.toString()
                    )
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "functional_status", binding.edtFunctionalStatus.text.toString()
                    )
                    findNavController().popBackStack()*/

                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "API_CALL", "API_CALL"
                    )
                    findNavController().popBackStack()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })



        /** Personal info edit   */
        viewModel.personalInfoResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<EditPersonalInfoResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.status!!) {
                        getProfileDataApiCall()
                        findNavController().previousBackStackEntry?.savedStateHandle?.set(
                            "API_CALL", "API_CALL"
                        )
                        findNavController().popBackStack()
                    }

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
        /*** City list Response*/

        viewModel.cityListResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<CityListResponse> -> {
                    displayProgressBar(false)
                    setDropDownCities(dataState.data)
                    cityList = dataState.data.cities!!
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })



        /**User Profile data */
        viewModel.profileDataResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<ProfileResponseBean> -> {
                    displayProgressBar(false)
                    Log.d(mTAG, "setObserver: ${dataState.data}")
                    /*   listener?.setProfileScreenData(
                           dataState.data.user?.job_seeker?.cv!!, dataState.data.user?.email!!,
                           dataState.data.user?.job_seeker.phone!!
                       )*/
                    profileResponseBean = dataState.data
                    setData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

    }


    private fun setData(data: ProfileResponseBean) {
        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {

            gender = when (data.user?.job_seeker?.gender) {
                "1" -> {
                    resources.getString(R.string.male)
                }
                "2" -> {
                    resources.getString(R.string.female)
                }
                else -> {
                    resources.getString(R.string.undefined)
                }
            }


            maritalStatus = when (data.user?.job_seeker?.marital_status) {
                1 -> {
                    getString(R.string.married)
                }
                2 -> {
                    getString(R.string.unmarried)
                }
                3 -> {
                    getString(R.string.absolute)
                }
                else -> {
                    getString(R.string.widower)
                }
            }



            binding.edtAddress.setText(data.user?.job_seeker?.address)
            binding.edtCity.setText(data.user?.job_seeker?.city_seeker?.name_en)
            binding.edtType.setText(gender, false)
            binding.edtDateOfBirth.setText(data.user?.job_seeker?.birthDate)
            binding.edtFunctionalStatus.setText(data.user?.job_seeker?.Dependents.toString())
            binding.edtSocialStatus.setText(maritalStatus, false)
            binding.edtNumberOfYearsOfExperience.setText(data.user?.job_seeker?.experience.toString())
            binding.edtMinimumSalary.setText(data.user?.job_seeker?.salary.toString())


        } else {
            /**Basic Data Set*/


            gender = when (data.user?.job_seeker?.gender) {
                "1" -> {
                    resources.getString(R.string.male)
                }
                "2" -> {
                    resources.getString(R.string.female)
                }
                else -> {
                    resources.getString(R.string.undefined)
                }
            }
            maritalStatus = when (data.user?.job_seeker?.marital_status) {
                1 -> {
                    getString(R.string.married)
                }
                2 -> {
                    getString(R.string.unmarried)
                }
                3 -> {
                    getString(R.string.absolute)
                }
                else -> {
                    getString(R.string.widower)
                }
            }
            binding.edtAddress.setText(data.user?.job_seeker?.address)
            binding.edtCity.setText(data.user?.job_seeker?.city_seeker?.name_ar)
            binding.edtType.setText(gender, false)
            binding.edtDateOfBirth.setText(data.user?.job_seeker?.birthDate)
            binding.edtFunctionalStatus.setText(data.user?.job_seeker?.Dependents.toString())
            binding.edtSocialStatus.setText(maritalStatus, false)
            binding.edtNumberOfYearsOfExperience.setText(data.user?.job_seeker?.experience.toString())
            binding.edtMinimumSalary.setText(data.user?.job_seeker?.salary.toString())
        }
    }


    private fun setDropDownCities(cityListResponse: CityListResponse) {
        /**
         *  Set JobRole list into dropdown
         */
        cityDropDownListAdapter =
            CityDropDownListAdapter(
                requireContext(),
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                cityListResponse.cities!!,
                prefManager
            )

        binding.edtCity.setAdapter(cityDropDownListAdapter)
        binding.edtCity.setOnClickListener {
            binding.edtCity.showDropDown()
        }
        binding.edtCity.setOnItemClickListener { _, _, position, _ ->
//            speciality_id = specialityDropDownListAdapter.getSpecialityList()[position].id
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
                binding.edtCity.setText(cityDropDownListAdapter.getSpecialityList()[position].name_en)
            } else {
                binding.edtCity.setText(cityDropDownListAdapter.getSpecialityList()[position].name_ar)
            }
            city = cityDropDownListAdapter.getSpecialityList()[position].id!!.toString()
            cityDropDownListAdapter.notifyDataSetChanged()
        }
    }





    private fun initUi() {
        binding.txtSave.setOnClickListener(this)
        binding.txtCancel.setOnClickListener(this)
        binding.edtDateOfBirth.setOnClickListener(this)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayTypeSize
                )
            (binding.edtType as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.edtType.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        } else {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arrayTypeArSize
                )
            (binding.edtType as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.edtType.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        }





        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arraySocialStatusSize
                )
            (binding.edtSocialStatus as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.edtSocialStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        } else {
            val typeSizeAdapter =
                ArrayAdapter(
                    requireContext(),
                    R.layout.drop_down_list_item,
                    R.id.txtDropDown,
                    arraySocialStatusArSize
                )
            (binding.edtSocialStatus as? AutoCompleteTextView)?.setAdapter(
                typeSizeAdapter
            )
            binding.edtSocialStatus.dropDownWidth =
                WindowManager.LayoutParams.MATCH_PARENT
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
    }


    private fun getProfileDataApiCall() {
        viewModel.setStateEvent(
            EditProfileStateEvent.ProfileData(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCancel -> {
                findNavController().popBackStack()
            }

            R.id.edtDateOfBirth -> {
                birthDatePicker()
            }

            R.id.txtSave -> {
                if (Utils.isConnected(requireContext())) {
                    if (isPersonalInfoDataValid()) {
                        personalInfoEditApiCall()
                    }
                } else {
                    view?.let { Utils.showSnackBar(it, requireContext()) }
                }
            }
        }
    }

    private fun isPersonalInfoDataValid(): Boolean {
//        binding.inputAddress.isErrorEnabled = false
        binding.inputCity.isErrorEnabled = false
        binding.inputType.isErrorEnabled = false
//        binding.inputDateOfBirth.isErrorEnabled = false
//        binding.inputFunctionalStatus.isErrorEnabled = false
        binding.inputSocialStatus.isErrorEnabled = false
//        binding.inputNumberOfYearsOfExperience.isErrorEnabled = false
//        binding.inputMinimumSalary.isErrorEnabled = false

        when {
            binding.edtAddress.text.toString().isEmpty() -> {
                binding.edtAddress.error =
                    getString(R.string.error_enter_address)
                binding.edtAddress.requestFocus()
                return false
            }
            binding.edtCity.text.toString().isEmpty() -> {
                binding.inputCity.error =
                    getString(R.string.error_enter_city)
                binding.inputCity.requestFocus()
                return false
            }
            binding.edtType.text.toString().isEmpty() -> {
                binding.inputType.error =
                    getString(R.string.error_enter_type)
                binding.inputType.requestFocus()
                return false
            }

            binding.edtDateOfBirth.text.toString().isEmpty() -> {
                binding.edtDateOfBirth.error =
                    getString(R.string.error_enter_date_of_birth)
                binding.edtDateOfBirth.requestFocus()
                return false
            }
            binding.edtFunctionalStatus.text.toString().isEmpty() -> {
                binding.edtFunctionalStatus.error =
                    getString(R.string.error_enter_number_of_dependants)
                binding.edtFunctionalStatus.requestFocus()
                return false
            }
            binding.edtSocialStatus.text.toString().isEmpty() -> {
                binding.inputSocialStatus.error =
                    getString(R.string.error_enter_social_status)
                binding.inputSocialStatus.requestFocus()
                return false
            }
            binding.edtNumberOfYearsOfExperience.text.toString().isEmpty() -> {
                binding.edtNumberOfYearsOfExperience.error =
                    getString(R.string.error_enter_number_of_years_of_experience)
                binding.edtNumberOfYearsOfExperience.requestFocus()
                return false
            }
            binding.edtMinimumSalary.text.toString().isEmpty() -> {
                binding.edtMinimumSalary.error =
                    getString(R.string.error_enter_minimum_salary)
                binding.edtMinimumSalary.requestFocus()
                return false
            }
        }

        return true
    }

    private fun birthDatePicker() {
        val dpd = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->

                // Display Selected date in editText
                endDate = "$year-${monthOfYear + 1}-${dayOfMonth}"
                Log.d(mTAG, "DATE -> $endDate")
                binding.edtDateOfBirth.setText(endDate)
            },
            year,
            month,
            day
        )
        dpd.datePicker.maxDate = Date().time
        dpd.show()
    }

    private fun personalInfoEditApiCall() {
        genderId = when (binding.edtType.text.toString()) {
            "Male" -> {
                "1"
            }
            "Female" -> {
                "2"
            }
            else -> {
                "3"
            }
        }
        maritalStatusId =
            when (binding.edtSocialStatus.text.toString()) {
                "Married" -> {
                    "1"
                }
                "Unmarried" -> {
                    "2"
                }
                "Absolute" -> {
                    "3"
                }
                else -> {
                    "4"
                }
            }
        viewModel.setStateEvent(
            EditProfileStateEvent.PersonalInfoEdit(
                binding.edtAddress.text.toString(),
                city,
                genderId,
                binding.edtDateOfBirth.text.toString(),
                binding.edtFunctionalStatus.text.toString(),
                maritalStatusId,
                binding.edtNumberOfYearsOfExperience.text.toString(),
                binding.edtMinimumSalary.text.toString(),
            )
        )
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


}