package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class MoreScreenServiceResponseBean(
    val services: List<MoreScreenService>? = null
)

data class MoreScreenService(
    val content_en: String? = null,
    val content_ar: String? = null,
    val id: Int? = null,
    val image: String? = null,
    val name_en: String? = null,
    val name_ar: String? = null,
    var isSelected: Boolean = false
) : Serializable