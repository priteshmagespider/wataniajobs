package com.fnrco.wataniajobs.mvvm.model.response

data class WhoWeAreResponseBean(
    val setting: Setting?
)

data class Setting(
    val about_en: String?,
    val about_ar: String?,
    val about_app_en: String?,
    val about_app_ar: String?,
    val about_app_image: String?,
    val about_photo: String?,
    val speech_en: String?,
    val speech_ar: String?,
    val speech_photo: String?,
    val mission_en: String?,
    val mission_ar: String?,
    val mission_photo: String?,
    val vision_en: String?,
    val vision_ar: String?,
    val vision_photo: String?

)