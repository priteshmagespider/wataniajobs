package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.WeAreHiringSpecialization
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.google.android.material.card.MaterialCardView

class HomeBannerListAdapter(
    val context: Context,
    val onClickListener: View.OnClickListener,
    val list: List<WeAreHiringSpecialization>,
    val prefManager: PrefManager
) : RecyclerView.Adapter<HomeBannerListAdapter.MyViewHolder>() {

    val listData: MutableList<WeAreHiringSpecialization> =
        list as MutableList<WeAreHiringSpecialization>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardHomeBanner: MaterialCardView = itemView.findViewById(R.id.cardHomeBanner)
        val imgHomeBanner: ImageView = itemView.findViewById(R.id.imgHomeBanner)
        val txtWeAreHiring: TextView = itemView.findViewById(R.id.txtWeAreHiring)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_home_banner_items, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.cardHomeBanner.tag = position
        holder.cardHomeBanner.setOnClickListener(onClickListener)
        holder.txtTitle.text =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") listData[position].title_en else listData[position].title

        holder.imgHomeBanner.setImageResource(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") R.drawable.home_banner_ltr else R.drawable.home_banner_rtl)
    }

    override fun getItemCount(): Int = listData.size
}