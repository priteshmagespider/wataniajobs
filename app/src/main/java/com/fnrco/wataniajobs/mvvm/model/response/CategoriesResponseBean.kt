package com.fnrco.wataniajobs.mvvm.model.response

import java.io.Serializable

data class CategoriesResponseBean(
    var categories: List<CategoryList>? = null
):Serializable

data class CategoryList(
    var id: Int? = null,
    var cat_id: Int? = null,
    var title: String? = null,
    var title_en: Any? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var jop_posts_count: Int? = null
):Serializable