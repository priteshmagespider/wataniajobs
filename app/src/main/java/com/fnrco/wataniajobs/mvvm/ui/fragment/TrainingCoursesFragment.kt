package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentTrainingCoursesBinding
import com.fnrco.wataniajobs.mvvm.adapter.TrainingCoursesListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.DeleteJobPostResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.TrainingCoursesMoreResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.TrainingCoursesResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class TrainingCoursesFragment : Fragment(), View.OnClickListener {

    private val mTAG = TrainingCoursesFragment::class.java.simpleName
    private lateinit var binding: FragmentTrainingCoursesBinding

    private lateinit var trainingCoursesListAdapter: TrainingCoursesListAdapter

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager

    lateinit var bottomSheetDeleteDialog: BottomSheetDialog
    lateinit var bottomSheetDeleteView: View

    var positionDelete: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrainingCoursesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initUi()
        if (Utils.isConnected(requireContext())) {
            viewModel.setStateEvent(
                EditProfileStateEvent.TrainingCoursesData(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!
                )
            )
        } else {
            Utils.showSnackBar(view, requireContext())
        }

        setObserver()
    }

    private fun setObserver() {
        /** Change Password Response*/
        viewModel.trainingCoursesDataResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<TrainingCoursesMoreResponseBean> -> {
                    displayProgressBar(false)
                    Utils.print("RESPOSNNENNENENE---JOOFOSODFOSODFSKF------->" + dataState.data.trainings)
                    if (dataState.data.trainings!!.isNotEmpty()) {
                        binding.txtNoRecord.visibility = View.GONE
                        binding.rvTrainingCoursesList.layoutManager =
                            LinearLayoutManager(requireContext())
                        trainingCoursesListAdapter =
                            TrainingCoursesListAdapter(requireContext(), this, prefManager)
                        trainingCoursesListAdapter.addData(dataState.data.trainings)
                        binding.rvTrainingCoursesList.adapter = trainingCoursesListAdapter
                    } else {
                        binding.txtNoRecord.visibility = View.VISIBLE
                    }


                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })

        viewModel.deleteJobPostResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<DeleteJobPostResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.message == "deleted") {
                        trainingCoursesListAdapter.removeItem(positionDelete)
                        trainingCoursesListAdapter.notifyDataSetChanged()
                        bottomSheetDeleteDialog.dismiss()
                        viewModel.setStateEvent(
                            EditProfileStateEvent.TrainingCoursesData(
                                prefManager.getString(
                                    PrefConstant.PREF_CURRENT_LANGUAGE
                                )!!
                            )
                        )
                    } else {
                        Toast.makeText(requireContext(), dataState.data.message, Toast.LENGTH_LONG)
                            .show()
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun initUi() {

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgDeleteTrainingCourse -> {
                positionDelete = view.tag.toString().toInt()
                forAreYouSure(positionDelete)
            }
        }
    }


    private fun forAreYouSure(pos: Int) {
        bottomSheetDeleteDialog = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        bottomSheetDeleteView =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.fragment_are_you_sure,
                    requireActivity().findViewById<View>(R.id.constraintForAreYouSure) as ConstraintLayout?
                )
        bottomSheetDeleteView.findViewById<View>(R.id.btnDelete)
            .setOnClickListener {
                viewModel.setStateEvent(
                    EditProfileStateEvent.DeleteTrainingCoursesEvent(
                        trainingCoursesListAdapter.getList()[pos].id!!
                    )
                )


            }
        bottomSheetDeleteView.findViewById<View>(R.id.btnCancel)
            .setOnClickListener(View.OnClickListener {
                bottomSheetDeleteDialog.dismiss()
            })

        bottomSheetDeleteDialog.setContentView(bottomSheetDeleteView)
        bottomSheetDeleteDialog.show()
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }
}