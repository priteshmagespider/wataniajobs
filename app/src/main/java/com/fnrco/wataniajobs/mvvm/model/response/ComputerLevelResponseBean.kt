package com.fnrco.wataniajobs.mvvm.model.response

data class ComputerLevelResponseBean(
    val levels: List<ComputerLevel>?
)

data class ComputerLevelX(
    val id: Int?,
    val title_en: String?,
    val title_ar: String?
)