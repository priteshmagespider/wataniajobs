package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.JobRoleListAdapter
import com.fnrco.wataniajobs.adapter.JobTitleListAdapter
import com.fnrco.wataniajobs.adapter.MajorListAdapter
import com.fnrco.wataniajobs.databinding.ActivitySignupBinding
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.SignUpEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.SignUpViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.onesignal.OSSubscriptionObserver
import com.onesignal.OSSubscriptionStateChanges
import com.onesignal.OneSignal
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject
import kotlin.random.Random

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SignUpActivity : AppCompatActivity(), View.OnClickListener, OSSubscriptionObserver {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivitySignupBinding
    private val experienceItem = ArrayList<String>()
    private val experienceItems = ArrayList<String>()
    private lateinit var jobTitleListAdapter: JobRoleListAdapter
    private var jobId: Int = -1

    @ExperimentalCoroutinesApi
    private val viewModel: SignUpViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager
    private val authRequest = AuthRequest()
    private var lastClickTime: Long = 0
    private var fileCv: String = ""
    var code: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()

        OneSignal.addSubscriptionObserver(this)

        val oneSignalUserID = OneSignal.getDeviceState()!!.userId
        Utils.print("ONESIGNALID---login----------------->$oneSignalUserID")
        if (oneSignalUserID != null) {
            prefManager.setString(PrefConstant.PREF_ONE_SIGNAL_ID, oneSignalUserID)
        }
        if (Utils.isConnected(this)) {
            viewModel.setStateEvent(
                SignUpEvent.JobTitleList(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!
                )
            )
        } else {
            Utils.showSnackBar(binding.mainLayout, this)
        }
        setObserver()
    }

    private fun setObserver() {
        /** SignUp Api Call /  New User */
        viewModel.signUpResponse.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<SignUpResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.errors != null) {
                        displayError(dataState.data.errors.toString())
                    } else {
                        prefManager.setString(
                            PrefConstant.PREF_ACCESS_TOKEN,
                            dataState.data.user!!.token
                        )
                        prefManager.setString(
                            PrefConstant.PREF_USER_NAME,
                            dataState.data.user!!.name
                        )

                        Utils.print(
                            "ONE_SIGNAL_SIGNUP--------------->" + prefManager.getString(
                                PrefConstant.PREF_ONE_SIGNAL_ID
                            )!!
                        )
                        viewModel.setStateEvent(
                            SignUpEvent.UpdatePlayer(
                                prefManager.getString(
                                    PrefConstant.PREF_ONE_SIGNAL_ID
                                )!!
                            )
                        )
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Send Verify Otp*/
        /** SignUp Api Call /  New User */
        viewModel.updatePlayerResponse.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<UpdatePlayerResponseBean> -> {
                    displayProgressBar(false)
                    viewModel.setStateEvent(SignUpEvent.SendVerifyOTPPhone(authRequest))
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }


        /** Send Verify Otp*/
        /** SignUp Api Call /  New User */
        viewModel.sendVerifyOtpResponse.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<SendVerifyOtpResponseBean> -> {
                    displayProgressBar(false)
                    startActivity(
                        Intent(this, VerificationActivity::class.java)
                            .putExtra(
                                Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE,
                                Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE
                            )
                            .putExtra(Constant.INTENT.PHONE_NO, authRequest.phone)
                            .putExtra(Constant.INTENT.CODE, authRequest.code)
                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        viewModel.jobTitleListResponse.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<JobRoleResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.categories != null) {
                        setJobTitleData(dataState.data.categories)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun initUi() {
        binding.btnSignUp.setOnClickListener(this)
        binding.txtLogin.setOnClickListener(this)
        binding.clMainUpload.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)



        experienceItem.add(resources.getString(R.string.male))
        experienceItem.add(resources.getString(R.string.female))
        experienceItems.addAll(experienceItem)
        val expAdapter =
            ArrayAdapter(
                this,
                R.layout.experience_list_item,
                R.id.txtDropDown,
                experienceItems
            )
        (binding.edtSelectGender as? AutoCompleteTextView)?.setAdapter(expAdapter)
//        binding.edtSelectGender.dropDownWidth = 300
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.txtLogin -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
            R.id.btnSignUp -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                Utils.closeKeyBoard(this, binding.mainLayout)
                if (Utils.isConnected(this)) {
                    if (isValid()) {
                        if (fileCv == "") {
                            displayError(resources.getString(R.string.please_upload_your_cv))
                        } else {
                            val rnd = Random
                            code = rnd.nextInt(111111, 999999).toString()
                            Log.d(mTAG, "number: $code")
                            authRequest.name = binding.edtFullName.text.toString().trim()
                            authRequest.email = binding.edtEmailMobNo.text.toString().trim()
                            authRequest.password = binding.edtPasswordE.text.toString().trim()
                            if (binding.edtMobNo.text.toString().startsWith("0")) {
                                val result = binding.edtMobNo.text.toString().trim().drop(1)
                                authRequest.phone = "+966$result"
                                Log.d("WITH ZERO", "SIGNUP: ${authRequest.phone}")
                            } else {
                                authRequest.phone =
                                    "+966" + binding.edtMobNo.text.toString().trim()
                                Log.d("WITHOUT ZERO", "SIGNUP: ${authRequest.phone}")
                            }
//                            authRequest.phone = "+966" + binding.edtMobileNumber.text.toString().trim()
                            authRequest.job_title = binding.edtJobTitle.text.toString().trim()
                            authRequest.job_title_en = binding.edtJobTitle.text.toString().trim()
                            if (binding.edtSelectGender.text.toString() == "Male") {
                                authRequest.gender = "1"
                            } else {
                                authRequest.gender = "2"
                            }
                            authRequest.fileCv = fileCv
                            authRequest.code = code
                            authRequest.type = Constant.USE_TYPE
                            authRequest.is_admin = Constant.IS_ADMIN
                            viewModel.setStateEvent(SignUpEvent.SignUpApiCall(authRequest))
                        }

                    }
                } else {
                    Utils.showSnackBar(binding.mainLayout, this)
                }
            }
            R.id.clMainUpload -> {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                    type = "application/pdf"
                    addCategory(Intent.CATEGORY_OPENABLE)
                    flags = flags or Intent.FLAG_GRANT_READ_URI_PERMISSION
                }
                startActivityForResult(intent, 111)
            }
        }
    }

    private fun setJobTitleData(category: List<Role>?) {
        /**
         * Set JobRole list into dropdown
         */
        jobTitleListAdapter =
            JobRoleListAdapter(
                this,
                R.layout.drop_down_list_item,
                R.id.txtDropDown,
                category!!
            )

        binding.edtJobTitle.setAdapter(jobTitleListAdapter)
//        binding.edtJobTitle.setOnClickListener {
//            binding.edtJobTitle.showDropDown()
//        }
//        if (searchRequest.subCat != 0) {
//            binding.edtJobRole.setText(if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") category.filter { it.id == searchRequest.subCat }
//                .map { it.title_en }[0] else category.filter { it.id == searchRequest.subCat }
//                .map { it.title }[0])
//        }

        binding.edtJobTitle.setOnItemClickListener { _, _, position, _ ->

            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
                binding.edtJobTitle.setText(
                    jobTitleListAdapter.getSpecialityList()[position].title_en
                        ?: jobTitleListAdapter.getSpecialityList()[position].title
                )
            } else {
                binding.edtJobTitle.setText(jobTitleListAdapter.getSpecialityList()[position].title)
            }
            jobId = jobTitleListAdapter.getSpecialityList()[position].id!!
            jobTitleListAdapter.notifyDataSetChanged()
        }
    }


    private fun isValid(): Boolean {
        /* binding.inputFullName.isErrorEnabled = false
         binding.inputEmailMobNo.isErrorEnabled = false
         binding.inputPassword.isErrorEnabled = false
         binding.inputConfirmPassword.isErrorEnabled = false
         binding.inputJobTitle.isErrorEnabled = false
         binding.inputSelectGender.isErrorEnabled = false
         binding.inputMobileNumber.isErrorEnabled = false
 */
        when {
            binding.edtFullName.text.toString().isEmpty() -> {
                binding.edtFullName.error = getString(R.string.error_enter_email)
                binding.edtFullName.requestFocus()
                return false
            }
            binding.edtEmailMobNo.text.toString().isEmpty() -> {
                binding.edtEmailMobNo.error = getString(R.string.error_enter_email)
                binding.edtEmailMobNo.requestFocus()
                return false
            }
            !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmailMobNo.text.toString()).matches() -> {
                binding.edtEmailMobNo.error = getString(R.string.error_enter_valid_email)
                binding.edtEmailMobNo.requestFocus()
                return false
            }
            binding.edtPasswordE.text.toString().isEmpty() -> {
                binding.edtPasswordE.error = getString(R.string.error_enter_password)
                binding.edtPasswordE.requestFocus()
                return false
            }
            binding.edtPasswordE.text.toString().length < 6 -> {
                binding.edtPasswordE.error = getString(R.string.error_enter_password_long)
                binding.edtPasswordE.requestFocus()
                return false
            }
            binding.edtConfirmPasswordE.text.toString() != binding.edtPasswordE.text.toString() -> {
                binding.edtConfirmPasswordE.error =
                    getString(R.string.error_enter_confirm_password_not_match)
                binding.edtConfirmPasswordE.requestFocus()
                return false
            }
            binding.edtJobTitle.text.toString().isEmpty() -> {
                binding.edtJobTitle.error = getString(R.string.error_enter_job_title)
                binding.edtJobTitle.requestFocus()
                return false
            }
            binding.edtSelectGender.text.toString().isEmpty() -> {
                binding.inputSelectGender.error = getString(R.string.error_select_gender)
                binding.inputSelectGender.requestFocus()
                return false
            }
            binding.edtMobNo.text.toString().isEmpty() -> {
                binding.edtMobNo.error = getString(R.string.error_enter_phone)
                binding.edtMobNo.requestFocus()
                return false
            }

        }

        return true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            111 -> {
                data?.data?.also { documentUri ->
                    this.contentResolver?.takePersistableUriPermission(
                        documentUri,
                        Intent.FLAG_GRANT_READ_URI_PERMISSION
                    )
                    val file =
                        DocumentUtils.getFile(this, documentUri)//use pdf as file
                    fileCv = file.toString()
                    Utils.print("File->$file")
                    Glide.with(this)
                        .load(R.drawable.img_report_place_holder)
                        .centerCrop()
                        .into(binding.imageViewMain)
                }
            }

            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }


    object DocumentUtils {
        fun getFile(mContext: Context?, documentUri: Uri): File {
            val inputStream = mContext?.contentResolver?.openInputStream(documentUri)
            var file = File("")
            inputStream.use { input ->
                file =
                    File(mContext?.cacheDir, System.currentTimeMillis().toString() + ".pdf")
                FileOutputStream(file).use { output ->
                    val buffer =
                        ByteArray(4 * 1024) // or other buffer size
                    var read: Int = -1
                    while (input?.read(buffer).also {
                            if (it != null) {
                                read = it
                            }
                        } != -1) {
                        output.write(buffer, 0, read)
                    }
                    output.flush()
                }
            }
            return file
        }
    }


    override fun onOSSubscriptionChanged(stateChanges: OSSubscriptionStateChanges?) {
        /*Utils.print("PLAYER_ID--------isSubscribed--------------------->"+stateChanges!!.to.userId)
        if(stateChanges.to.userId!="")
        {
            prefManager.setString(PrefConstant.PREF_ONE_SIGNAL_ID, stateChanges.to.userId)
        }*/
        if (!stateChanges!!.from.isSubscribed &&
            stateChanges.to.isSubscribed
        ) {
            // The user is subscribed
            // Either the user subscribed for the first time
            // Or the user was subscribed -> unsubscribed -> subscribed
            stateChanges.to.userId


            // Make a POST call to your server with the user ID
        }
    }
}