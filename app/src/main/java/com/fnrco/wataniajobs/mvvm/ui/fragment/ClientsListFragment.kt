package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentClientsListBinding
import com.fnrco.wataniajobs.mvvm.adapter.ClientListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.ClientsListResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.DataState
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class ClientsListFragment : Fragment(), View.OnClickListener {

    private val mTAG = ClientsListFragment::class.java.simpleName
    private lateinit var binding: FragmentClientsListBinding

    private lateinit var clientListAdapter: ClientListAdapter

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: MoreScreenViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentClientsListBinding.inflate(layoutInflater, container, false)
        setObserver()
        viewModel.setMoreScreenApiCall(
            MoreScreenEvent.ClientListEvent(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
        Log.d(
            mTAG,
            "ClientsListFragment CURRENT_LANGUAGE--? ${prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)}"
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
    }

    private fun setObserver() {
        /** Clients */
        viewModel.clientsListResponse.observe(requireActivity(), { dataState ->
            when (dataState) {
                is DataState.Success<ClientsListResponseBean> -> {
                    displayProgressBar(false)
                    setData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun setData(data: ClientsListResponseBean) {
        binding.rvClientsList.layoutManager = LinearLayoutManager(requireContext())
        clientListAdapter = ClientListAdapter(requireContext(), data.clients!!, prefManager)
        binding.rvClientsList.adapter = clientListAdapter
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> {
                findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    "key1", "Who We Are"
                )
                findNavController().popBackStack()
            }
        }
    }
}