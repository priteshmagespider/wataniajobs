package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.Manifest
import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import com.bumptech.glide.Glide
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentProfileBinding
import com.fnrco.wataniajobs.mvvm.adapter.ProfileFragmentAdapter
import com.fnrco.wataniajobs.mvvm.model.response.BasicDataUpdateResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.ProfileResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.github.dhaval2404.imagepicker.ImagePicker
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class ProfileFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentProfileBinding
    lateinit var navController: NavController
    private val mTAG = this::class.java.simpleName
    private lateinit var editingTheProfileFragment: EditingTheProfileFragment
    private lateinit var favouriteJobsFragment : FavouriteJobsFragment
    private lateinit var currentJobApplicationFragment: CurrentJobApplicationFragment
    private lateinit var expiredJobApplicationsFragment: ExpiredJobApplicationsFragment
    private lateinit var cvExcellenceFragment: CvExcellenceFragment
    private lateinit var trainingCoursesFragment: TrainingCoursesFragment
    private lateinit var changePasswordFragment: ChangePasswordFragment
    private lateinit var profileFragmentAdapter: ProfileFragmentAdapter
    private var logo: String = ""

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()
    private var writeAccess = false
    private var downloadPage = ""

    @Inject
    lateinit var prefManager: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(mTAG, "onCreate: ")
        setObserver()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        viewModel.setStateEvent(
            EditProfileStateEvent.ProfileData(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }


    private

    fun initUi() {
        binding.profileImage.setOnClickListener(this)
        binding.imgProfileEdit.setOnClickListener(this)
        binding.candidateProgress.setOnClickListener(this)
        binding.txtDownloadCV.setOnClickListener(this)
        binding.clProfileImage.setOnClickListener(this)
        binding.candidateProgress.setOnClickListener(this)
        editingTheProfileFragment = EditingTheProfileFragment()
        favouriteJobsFragment = FavouriteJobsFragment(true)
        currentJobApplicationFragment = CurrentJobApplicationFragment()
        expiredJobApplicationsFragment = ExpiredJobApplicationsFragment()
        cvExcellenceFragment = CvExcellenceFragment()
        trainingCoursesFragment = TrainingCoursesFragment()
        changePasswordFragment = ChangePasswordFragment()
//        binding.nsvProfile.isFillViewport = true
//        binding.nsvProfile.isNestedScrollingEnabled = true
        binding.tabLayout.setupWithViewPager(binding.viewPager)
//        binding.viewPager.isSaveFromParentEnabled = false


        binding.txtDownloadCV.textDirection =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") View.TEXT_DIRECTION_LTR else View.TEXT_DIRECTION_RTL
        binding.txtEmail.textDirection =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") View.TEXT_DIRECTION_LTR else View.TEXT_DIRECTION_RTL
//        binding.txtPhoneNo.textDirection =
//            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") View.TEXT_DIRECTION_LTR else View.TEXT_DIRECTION_RTL

        profileFragmentAdapter =
            ProfileFragmentAdapter(childFragmentManager)
        profileFragmentAdapter.addFragment(
            editingTheProfileFragment,
            getString(R.string.editing_the_profile)
        )
        profileFragmentAdapter.addFragment(
            favouriteJobsFragment,
            getString(R.string.tab_fav_job)
        )

        profileFragmentAdapter.addFragment(
            currentJobApplicationFragment,
            getString(R.string.current_job_application)
        )
        profileFragmentAdapter.addFragment(
            expiredJobApplicationsFragment,
            getString(R.string.expired_job_application)
        )
        profileFragmentAdapter.addFragment(
            cvExcellenceFragment,
            getString(R.string.cv_excellence)
        )
        profileFragmentAdapter.addFragment(
            trainingCoursesFragment,
            getString(R.string.training_courses)
        )
        profileFragmentAdapter.addFragment(
            changePasswordFragment,
            getString(R.string.change_password)
        )
        binding.viewPager.adapter = profileFragmentAdapter
        profileFragmentAdapter.notifyDataSetChanged()

    }

    override fun onResume() {
        super.onResume()

    }

    private fun setObserver() {
        /**User Profile data */
        viewModel.profileDataResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<ProfileResponseBean> -> {
                    displayProgressBar(false)
                    Log.d(mTAG, "setObserver: ${dataState.data}")
                    setData(dataState.data)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Basic data Api response */
        viewModel.updateUserImage.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<BasicDataUpdateResponseBean> -> {
                    displayProgressBar(false)
//                    binding.mainView.fullScroll(View.FOCUS_UP)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

    }

    private fun setData(data: ProfileResponseBean) {

        if(data.user?.name != null)
        prefManager.setString(
            PrefConstant.PREF_USER_NAME,
            data.user?.name
        ) else prefManager.setString(
            PrefConstant.PREF_USER_NAME,
            data.user?.name_en
        )

        prefManager.setString(
            PrefConstant.PREF_USER_PHOTO,
            data.user?.job_seeker?.photo
        )


        Glide.with(requireContext())
            .load("${Constant.IMAGE_URL_WITH_PHOTOS}${data.user?.job_seeker?.photo}")
            .placeholder(R.drawable.userplaceholder)
            .into(binding.profileImage)
        binding.txtName.text =
            if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") if(data.user?.name_en != null) data.user?.name_en else data.user?.name else data.user?.name
        binding.txtEmail.text = data.user?.email
        binding.txtPhoneNo.text = data.user?.job_seeker?.phone
        binding.progressBarStatus.progress = data.user?.job_seeker?.ratio!!
        binding.profileCompletePercentage.text = "${data.user?.job_seeker?.ratio.toString()}%"
        binding.candidateProgress.max = 100
        binding.candidateProgress.progress = data.user?.job_seeker?.ratio!!
        downloadPage = "${Constant.CV_URL}${data.user?.job_seeker?.cv}"

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!! == "en") {
            binding.txtEmail.textDirection = View.TEXT_DIRECTION_LTR
//            binding.txtPhoneNo.textDirection = View.TEXT_DIRECTION_LTR
            when (data.user?.job_seeker!!.job_status) {
                "1" -> {
                    binding.txtNewOppertunity.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0)
                }
                "2" -> {
                    binding.txtNewOppertunity.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yellow_dot, 0, 0, 0)
                }
                "3" -> {
                    binding.txtNewOppertunity.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0)
                }
            }

        } else {
            binding.txtEmail.textDirection = View.TEXT_DIRECTION_RTL
//            binding.txtPhoneNo.textDirection = View.TEXT_DIRECTION_RTL
            when (data.user?.job_seeker!!.job_status) {
                "1" -> {
                    binding.txtNewOppertunity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_green_dot, 0)
                }
                "2" -> {
                    binding.txtNewOppertunity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_yellow_dot, 0)
                }
                "3" -> {
                    binding.txtNewOppertunity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_red_dot, 0)
                }
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()

        }
    }

    fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
//        if (isDisplayed) {
//            binding.viewProgress.visibility = View.VISIBLE
//            binding.viewProgress.isEnabled = false
//        } else {
//            binding.viewProgress.visibility = View.GONE
//        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(mTAG, "onAttach: ")
    }



    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        Log.d(mTAG, "onDestroyOptionsMenu: ")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(mTAG, "onDestroyView: ")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(mTAG, "onDetach: ")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(mTAG, "onDestroy: ")
    }

    override fun onPause() {
        super.onPause()
        Log.d(mTAG, "onPause: ")
    }


    override fun onStop() {
        super.onStop()
        Log.d(mTAG, "onStop: ")
    }

    override fun onStart() {
        super.onStart()
        Log.d(mTAG, "onStart: ")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(mTAG, "onSaveInstanceState: ")
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            when (resultCode) {
                Activity.RESULT_OK -> {
                    val uri: Uri = data?.data!!
                    logo = uri.path.toString()
                    Glide.with(requireContext())
                        .load(uri)
                        .into(binding.profileImage)
//                    binding.mainView.fullScroll(View.FOCUS_UP)
                    viewModel.setStateEvent(EditProfileStateEvent.UpdateUserProfileImage(logo))
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(
                        requireContext(),
                        ImagePicker.getError(data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                val uri: Uri = data?.data!!
                logo = uri.path.toString()
                Glide.with(requireContext())
                    .load(uri)
                    .into(binding.profileImage)
                viewModel.setStateEvent(EditProfileStateEvent.UpdateUserProfileImage(logo))
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(requireContext(), ImagePicker.getError(data), Toast.LENGTH_SHORT)
                    .show()
            }
            else -> {
                Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }*/

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.clProfileImage,
            R.id.imgProfileEdit,
            R.id.candidateProgress -> {
                ImagePicker.with(requireActivity())
                    .compress(512)
                    .crop()
                    .createIntent { intent ->
                        startForProfileImageResult.launch(intent)
                    }
            }
            R.id.txtDownloadCV -> {
                checkRunTimePermission()
            }
        }
    }

    private fun checkRunTimePermission() {
        Dexter.withContext(requireContext())
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(permission: PermissionGrantedResponse?) {
                    writeAccess = true
                    createDownloadListener()
                    onDownloadComplete()
                }

                override fun onPermissionDenied(permission: PermissionDeniedResponse?) {
                    Log.d(mTAG, "onPermissionDenied: ${permission.toString()}")
                    Toast.makeText(requireContext(), "PermissionDenied", Toast.LENGTH_SHORT).show()
                    writeAccess = false
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Toast.makeText(requireContext(), "Something went wrong.", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    private fun createDownloadListener() {
        Toast.makeText(requireContext(), "starting download... ", Toast.LENGTH_LONG).show()
        //downloadListener = DownloadListener { url, userAgent, contentDescription, mimetype, contentLength ->
        val request = DownloadManager.Request(Uri.parse(downloadPage))
        request.setAllowedOverMetered(true)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        val fileName = URLUtil.guessFileName(downloadPage, "wataniajobs", "employer")
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
        val dManager =
            requireContext().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        if (writeAccess) {
            dManager.enqueue(request)
        } else {
            Toast.makeText(context, "Unable to download", Toast.LENGTH_LONG).show()
            checkRunTimePermission()
        }
    }

    private fun onDownloadComplete() {

        val onComplete = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_LONG).show()
            }
        }
        requireContext().registerReceiver(
            onComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
    }

}