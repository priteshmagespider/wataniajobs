package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.facebook.LoginStatusCallback
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.ActivityDesignBinding
import com.fnrco.wataniajobs.databinding.ActivityLoginBinding
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.LoginResponseBean
import com.fnrco.wataniajobs.mvvm.model.response.UpdatePlayerResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.LoginEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.LoginViewModel
import com.fnrco.wataniajobs.mvvm.viewmodel.SignUpEvent
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.onesignal.OSSubscriptionObserver
import com.onesignal.OSSubscriptionStateChanges
import com.onesignal.OneSignal
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class DesignActivity : AppCompatActivity(), View.OnClickListener  {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivityDesignBinding

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: LoginViewModel by viewModels()


    private var lastClickTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDesignBinding.inflate(layoutInflater)

        setContentView(binding.root)
        initUi()


        binding.progressBarStatus.setProgress(70);
//        txtProgress.setText(pStatus + " %");



    }

    private fun initUi() {
//        binding.imgBack.setOnClickListener(this)
    }



    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }


}