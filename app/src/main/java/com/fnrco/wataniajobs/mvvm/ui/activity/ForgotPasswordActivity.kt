package com.fnrco.wataniajobs.mvvm.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.ActivityForgotPasswordBinding
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.SendVerifyOtpResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.ForgotPasswordEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.ForgotPasswordViewModel
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import kotlin.random.Random

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ForgotPasswordActivity : AppCompatActivity(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivityForgotPasswordBinding

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: ForgotPasswordViewModel by viewModels()
    val authRequest = AuthRequest()
    lateinit var code: String
    private var lastClickTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        when {
            intent.hasExtra(Constant.INTENT.LOGIN_TO_FORGOT_FOR_PHONE) -> {
                binding.txtLoginDetailForPhone.visibility = View.VISIBLE
                binding.edtPhone.visibility = View.VISIBLE
                Utils.print("Print-------ForgotPassword------------->"+"phone")
            }
            intent.hasExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_EMAIL) -> {
                binding.txtLoginDetailForEmail.visibility = View.VISIBLE
                binding.edtEmail.visibility = View.VISIBLE
                binding.txtPhoneNo.visibility = View.GONE
                binding.txtNotReceivedPhone.visibility = View.VISIBLE
                binding.txtNotReceivedPhone.setOnClickListener(this)
                Utils.print("Print-------ForgotPassword------------->"+"email")
            }
            intent.hasExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE) -> {
                binding.txtLoginDetailForPhone.visibility = View.VISIBLE
                binding.edtPhone.visibility = View.VISIBLE
                Utils.print("Print-------ForgotPassword------------->"+"phone")
            }
        }



        initUi()
        setObserver()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
        binding.btnSend.setOnClickListener(this)



    }

    @ExperimentalCoroutinesApi
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                onBackPressed()
            }

            R.id.txtNotReceivedPhone -> {
                /*binding.txtNotReceivedPhone.visibility = View.GONE
                binding.txtLoginDetailForPhone.visibility = View.VISIBLE
                binding.inputPhone.visibility = View.VISIBLE

                binding.txtLoginDetailForEmail.visibility = View.GONE
                binding.inputEmail.visibility = View.GONE*/

                val intent = Intent(
                    this,
                    ForgotPasswordActivity::class.java
                ).putExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE,Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE)
                startActivity(intent)
                finish()
            }
            R.id.btnSend -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                if (Utils.isConnected(this)) {
                    when {
                        intent.hasExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_EMAIL) -> {
                            if (isValidEmail()) {
                                val rnd = Random
                                code = rnd.nextInt(111111, 999999).toString()
                                Log.d(mTAG, "number: $code")
                                authRequest.phone =  binding.edtEmail.text.toString().trim()
                                authRequest.code = code
                                authRequest.type = Constant.USE_TYPE
                                /*authRequest.is_admin = Constant.IS_ADMIN*/
                                Utils.print("Print-------ForgotPassword-------button_email------>"+AuthRequest.toStringForgotPasswordEmail(authRequest))
                                viewModel.setForgotPasswordApiCall(
                                    ForgotPasswordEvent.SendVerifyForgotPasswordOTPForPhone(
                                        authRequest
                                    )
                                )
                            }
                        }
                        intent.hasExtra(Constant.INTENT.LOGIN_TO_FORGOT_FOR_PHONE) -> {
                            if (isValidPhoneNo()) {
                                val rnd = Random
                                 code = rnd.nextInt(111111, 999999).toString()
                                Log.d(mTAG, "number: $code")
                                authRequest.phone = "+966"+binding.edtPhone.text.toString().trim()
                                authRequest.code = code
                                authRequest.type = Constant.USE_TYPE
                                /*authRequest.is_admin = Constant.IS_ADMIN*/
                                Utils.print("Print-------ForgotPassword-------button_phone------>"+AuthRequest.toStringForgotPasswordPhone(authRequest))
                                viewModel.setForgotPasswordApiCall(
                                    ForgotPasswordEvent.SendVerifyForgotPasswordOTPForPhone(
                                        authRequest
                                    )
                                )
                            }
                        }
                        intent.hasExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE) -> {
                            if (isValidPhoneNo()) {
                                val rnd = Random
                                 code = rnd.nextInt(111111, 999999).toString()
                                Log.d(mTAG, "number: $code")
                                authRequest.phone = "+966"+ binding.edtPhone.text.toString().trim()
                                authRequest.code = code
                                authRequest.type = Constant.USE_TYPE
                                /*authRequest.is_admin = Constant.IS_ADMIN*/
                                Utils.print("Print-------ForgotPassword-------button_phone------>"+AuthRequest.toStringForgotPasswordPhone(authRequest))
                                viewModel.setForgotPasswordApiCall(
                                    ForgotPasswordEvent.SendVerifyForgotPasswordOTPForPhone(
                                        authRequest
                                    )
                                )
                            }
                        }
                        else -> {
                            if (isValidPhoneNo()) {
                                val rnd = Random
                                code = rnd.nextInt(111111, 999999).toString()
                                Log.d(mTAG, "number: $code")
                                authRequest.phone = "+966"+binding.edtPhone.text.toString().trim()
                                authRequest.code = code
                                authRequest.type = Constant.USE_TYPE
                                /*authRequest.is_admin = Constant.IS_ADMIN*/
                                Utils.print("Print-------ForgotPassword-------button_phone------>"+AuthRequest.toStringForgotPasswordPhone(authRequest))
                                viewModel.setForgotPasswordApiCall(
                                    ForgotPasswordEvent.SendVerifyForgotPasswordOTPForPhone(
                                        authRequest
                                    )
                                )
                            }
                        }
                    }


                } else {
                    Utils.showSnackBar(v, this)
                }

            }
        }
    }

    private fun isValidPhoneNo(): Boolean {
//        binding.edtPhone.isErrorEnabled = false
        when {
            binding.edtPhone.text.toString().isEmpty() -> {
                binding.edtPhone.error = getString(R.string.error_enter_phone)
                binding.edtPhone.requestFocus()
                return false
            }
        }
        return true
    }

    private fun isValidEmail(): Boolean {
//        binding.inputEmail.isErrorEnabled = false
        when {
            binding.edtEmail.text.toString().isEmpty() -> {
                binding.edtEmail.error = getString(R.string.error_enter_email)
                binding.edtEmail.requestFocus()
                return false
            }
            !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches() -> {
                binding.edtEmail.error = getString(R.string.error_enter_valid_email)
                binding.edtEmail.requestFocus()
                return false
            }
        }
        return true
    }

    private fun setObserver() {
        /** Send Verify Otp*/
        viewModel.sendVerifyOtpForgotPasswordResponse.observe(this, { dataState ->
            when (dataState) {
                is DataState.Success<SendVerifyOtpResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.message == "send Code Successfully" || dataState.data.message == "Success") {
                        when {
                            intent.hasExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_EMAIL) -> {
                                startActivity(
                                    Intent(this, VerificationActivity::class.java)
                                        .putExtra(
                                            Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_EMAIL,
                                            Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_EMAIL
                                        )
                                        .putExtra(Constant.INTENT.EMAIL, authRequest.phone)
                                        .putExtra(Constant.INTENT.CODE, authRequest.code)
                                )

                            }
                            intent.hasExtra(Constant.INTENT.LOGIN_TO_FORGOT_FOR_PHONE) -> {
                                startActivity(
                                    Intent(this, VerificationActivity::class.java)
                                        .putExtra(
                                            Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE,
                                            Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE
                                        )
                                        .putExtra(Constant.INTENT.PHONE_NO, authRequest.phone)
                                        .putExtra(Constant.INTENT.CODE, authRequest.code)
                                )
                            }
                            intent.hasExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE) -> {
                                startActivity(
                                    Intent(this, VerificationActivity::class.java)
                                        .putExtra(
                                            Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE,
                                            Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE
                                        )
                                        .putExtra(Constant.INTENT.PHONE_NO, authRequest.phone)
                                        .putExtra(Constant.INTENT.CODE, authRequest.code)
                                )
                            }
                        }
                    }else{
                        displayError(dataState.data.message)
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }
}