package com.fnrco.wataniajobs.mvvm.model.request

class AuthRequest {
    var name: String? = null
    var email: String? = null
    var password: String? = null
    var city: String? = null
    var job_title: String? = null
    var job_title_en: String? = null
    var gender: String? = null
    var phone: String? = null
    var mobile: String? = null
    var address: String? = null
    var website: String? = null
    var fileCv: String? = null
    var code: String? = null
    var is_admin: String? = null
    var type: String? = null

    // Login //
    var grant_type: String? = null
    var client_id: String? = null
    var client_secret: String? = null
    var username: String? = null

    companion object {
        fun toStringForgotPasswordPhone(authRequest: AuthRequest): String {
            return "AuthRequest(phone=${authRequest.phone},code=${authRequest.code}, is_admin=${authRequest.is_admin}, type=${authRequest.type})"
        }
        fun toStringForgotPasswordEmail(authRequest: AuthRequest): String {
            return "AuthRequest(phone=${authRequest.phone},code=${authRequest.code}, is_admin=${authRequest.is_admin}, type=${authRequest.type})"
        }
        fun toStringVerificationPhone(authRequest: AuthRequest): String {
            return "AuthRequest(phone=${authRequest.phone},code=${authRequest.code}, is_admin=${authRequest.is_admin}, type=${authRequest.type})"
        }
        fun toStringVerificationEmail(authRequest: AuthRequest): String {
            return "AuthRequest(phone=${authRequest.phone},code=${authRequest.code}, is_admin=${authRequest.is_admin}, type=${authRequest.type})"
        }

    }


}