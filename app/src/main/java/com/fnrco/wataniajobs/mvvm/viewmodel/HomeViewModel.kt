@file:Suppress("DEPRECATION")

package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.repository.HomeRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class HomeViewModel
@ViewModelInject
constructor(
    private val homeRepository: HomeRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    /** Home Players List Response */
    private val _homeListCityResponse: MutableLiveData<DataState<SearchListResponseBean>> =
        MutableLiveData()

    val homeListCityResponse: LiveData<DataState<SearchListResponseBean>>
        get() = _homeListCityResponse


    private val _homeCategoriesListResponse: MutableLiveData<DataState<SpecializationResponseBean>> =
        MutableLiveData()

    val homeCategoriesListResponse: LiveData<DataState<SpecializationResponseBean>>
        get() = _homeCategoriesListResponse



    private val _recentSearchListResponse: MutableLiveData<DataState<RecentSearchListResponseBean>> =
        MutableLiveData()

    val recentSearchListResponse: LiveData<DataState<RecentSearchListResponseBean>>
        get() = _recentSearchListResponse


    private val _homeJobTitleListResponse: MutableLiveData<DataState<SearchListResponseBean>> =
        MutableLiveData()

    val homeJobTitleListResponse: LiveData<DataState<SearchListResponseBean>>
        get() = _homeJobTitleListResponse


    private val _cityListResponse: MutableLiveData<DataState<CityListResponse>> =
        MutableLiveData()

    val cityListResponse: LiveData<DataState<CityListResponse>>
        get() = _cityListResponse


    private val _autoBioGraphyListResponse: MutableLiveData<DataState<AutoBioGraphyResponseBean>> =
        MutableLiveData()

    val autoBioGraphyListResponse: LiveData<DataState<AutoBioGraphyResponseBean>>
        get() = _autoBioGraphyListResponse


    private val _singlePostResponse: MutableLiveData<DataState<SinglePostResponseBean>> =
        MutableLiveData()

    val singlePostResponse: LiveData<DataState<SinglePostResponseBean>>
        get() = _singlePostResponse


    private val _topJobResponse: MutableLiveData<DataState<TopJobsResponseBean>> =
        MutableLiveData()

    val topJobResponse: LiveData<DataState<TopJobsResponseBean>>
        get() = _topJobResponse



    private val _favouriteJobResponse: MutableLiveData<DataState<FavoriteJobResponseBean>> =
        MutableLiveData()

    val favouriteJobResponse: LiveData<DataState<FavoriteJobResponseBean>>
        get() = _favouriteJobResponse

    private val _removeFavouriteJobResponse: MutableLiveData<DataState<RemoveFavoriteJobResponseBean>> =
        MutableLiveData()

    val removeFavouriteJobResponse: LiveData<DataState<RemoveFavoriteJobResponseBean>>
        get() = _removeFavouriteJobResponse


    private val _jobPostAppliedResponse: MutableLiveData<DataState<JobPostApplyResponseBean>> =
        MutableLiveData()

    val jobPostAppliedResponse: LiveData<DataState<JobPostApplyResponseBean>>
        get() = _jobPostAppliedResponse


    private val _jobRoleResponse: MutableLiveData<DataState<JobRoleResponseBean>> =
        MutableLiveData()

    val jobRoleResponse: LiveData<DataState<JobRoleResponseBean>>
        get() = _jobRoleResponse

    /** Home Banner */
    private val _homeBannerResponse: MutableLiveData<DataState<WeAreHiringResponseBean>> =
        MutableLiveData()

    val homeBannerResponse: LiveData<DataState<WeAreHiringResponseBean>>
        get() = _homeBannerResponse


    fun setStateEvent(homeStateEvent: HomeStateEvent) {
        viewModelScope.launch {
            when (homeStateEvent) {
                is HomeStateEvent.HomeCityList -> {
                    homeRepository.cityList(
                        homeStateEvent.language,
                        homeStateEvent.city,
                        homeStateEvent.jobTitle
                    )
                        .onEach {
                            _homeListCityResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeCategoriesList -> {
                    homeRepository.categoriesList(homeStateEvent.language)
                        .onEach {
                            _homeCategoriesListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeRecentSearchList -> {
                    homeRepository.recentSearchList(homeStateEvent.language)
                        .onEach {
                            _recentSearchListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeCity -> {
                    homeRepository.cityList(homeStateEvent.language)
                        .onEach {
                            _cityListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeAutoBioGraphyList -> {
                    homeRepository.autoBiographyList(homeStateEvent.language)
                        .onEach {
                            _autoBioGraphyListResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeDetailSinglePost -> {
                    homeRepository.singlePost(homeStateEvent.language, homeStateEvent.post_id)
                        .onEach {
                            _singlePostResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeTopJobs -> {
                    homeRepository.topJobs(homeStateEvent.language, homeStateEvent.page)
                        .onEach {
                            _topJobResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeFavouriteJobList -> {
                    homeRepository.addFavoriteJob(homeStateEvent.job_id)
                        .onEach {
                            _favouriteJobResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeRemoveFavouriteJobList -> {
                    homeRepository.removeFavoriteJob(homeStateEvent.job_id)
                        .onEach {
                            _removeFavouriteJobResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeJobPosted -> {
                    homeRepository.jobPostApplied(homeStateEvent.post_id)
                        .onEach {
                            _jobPostAppliedResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.JobRoleList -> {
                    homeRepository.roleList(homeStateEvent.language)
                        .onEach {
                            _jobRoleResponse.value = it
                        }.launchIn(viewModelScope)
                }
                is HomeStateEvent.HomeBannerEvent -> {
                    homeRepository.homeBannerApiCall(homeStateEvent.language)
                        .onEach {
                            _homeBannerResponse.value = it
                        }.launchIn(viewModelScope)
                }

                else -> {

                }
            }
        }
    }
}


sealed class HomeStateEvent {
    data class HomeCityList(val language: String, val city: Int, val jobTitle: String) :
        HomeStateEvent()

    data class HomeCategoriesList(val language: String) : HomeStateEvent()
    data class HomeFavouriteJobList(val job_id: Int) : HomeStateEvent()
    data class HomeRemoveFavouriteJobList(val job_id: Int) : HomeStateEvent()
    data class HomeRecentSearchList(val language: String) : HomeStateEvent()
    data class HomeAutoBioGraphyList(val language: String) : HomeStateEvent()
    data class HomeCity(val language: String) : HomeStateEvent()
    data class HomeTopJobs(val language: String, val page : Int) : HomeStateEvent()
    data class HomeDetailSinglePost(val language: String, val post_id: Int) : HomeStateEvent()
    data class HomeJobPosted(val post_id: Int) : HomeStateEvent()
    data class JobRoleList(val language: String) : HomeStateEvent()
    data class HomeBannerEvent(val language: String) : HomeStateEvent()
}