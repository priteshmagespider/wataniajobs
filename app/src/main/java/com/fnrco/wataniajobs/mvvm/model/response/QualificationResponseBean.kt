package com.fnrco.wataniajobs.mvvm.model.response

data class QualificationResponseBean(
    var qualifications: List<QualificationList>? = null
)


data class QualificationList(
    var id: Int? = null,
    var title_en: String? = null,
    var title_ar: String? = null
)