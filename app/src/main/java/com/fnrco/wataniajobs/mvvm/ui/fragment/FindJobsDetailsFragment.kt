package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.FindJobsDetailsListAdapter
import com.fnrco.wataniajobs.databinding.FragmentFindJobsDetailsBinding
import com.fnrco.wataniajobs.mvvm.model.response.SpecializationResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.HomeViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class FindJobsDetailsFragment : Fragment(), View.OnClickListener {
    private val mTAG = HomeFragment::class.java.simpleName
    private lateinit var binding: FragmentFindJobsDetailsBinding
    lateinit var navController: NavController
    lateinit var findJobsDetailsListAdapter: FindJobsDetailsListAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var browseCities: String = ""

    @ExperimentalCoroutinesApi
    private val viewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var prefManager: PrefManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFindJobsDetailsBinding.inflate(inflater, container, false)
        browseCities = arguments?.getString(Constant.TITLE) as String
        // binding.txtSignUpTitle.text = browseCities
        binding.tvSearch.text = browseCities
        setObserver()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        viewModel.setStateEvent(HomeStateEvent.HomeCategoriesList(prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE)!!))
        initUi()
    }

    private fun initUi() {
        binding.imgBack.setOnClickListener(this)
    }


    private fun setObserver() {
        viewModel.homeCategoriesListResponse.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<SpecializationResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.specializations != null) {
                        linearLayoutManager = LinearLayoutManager(requireActivity())
                        binding.recyclerView.layoutManager = linearLayoutManager
                        findJobsDetailsListAdapter = FindJobsDetailsListAdapter(
                            requireActivity(),
                            this,
                            dataState.data.specializations!!
                        )
                        binding.recyclerView.adapter = findJobsDetailsListAdapter
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.clMain -> {
                val pos = view.tag.toString().toInt()
                val bundle = bundleOf(
                    Constant.SPECIALITY_DATA_TITLE to if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "en") findJobsDetailsListAdapter.list[pos].title_en else findJobsDetailsListAdapter.list[pos].title,
                    Constant.SPECIALITY_CITY_ID to 0,
                    "JOB_CAT_ID" to findJobsDetailsListAdapter.list[pos].id
                )
                navController.navigate(
                    R.id.action_findJobsDetailsFragment_to_findJobsFragment,
                    bundle
                )
            }
            R.id.imgBack -> {
                findNavController().popBackStack()
            }
        }
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }

}