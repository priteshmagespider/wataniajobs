package com.fnrco.wataniajobs.mvvm.model.response

data class UploadCvResponseBean(
    var message: String? = null
)