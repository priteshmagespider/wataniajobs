package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.databinding.FragmentMoreBinding
import com.fnrco.wataniajobs.mvvm.adapter.ServicesListAdapter
import com.fnrco.wataniajobs.mvvm.model.response.MoreScreenServiceResponseBean
import com.fnrco.wataniajobs.mvvm.ui.activity.LoginPopUpActivity
import com.fnrco.wataniajobs.mvvm.ui.activity.SplashActivity
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.MoreScreenViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class MoreFragment : Fragment(), View.OnClickListener {

    private val mTAG = MoreFragment::class.java.simpleName
    private lateinit var binding: FragmentMoreBinding
    private lateinit var navController: NavController
    private lateinit var servicesListAdapter: ServicesListAdapter

    private var isWhoWeAreOpen: Boolean = false
    private var isServicesOpen: Boolean = false
    private var isTrainingCoursesOpen: Boolean = false


    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: MoreScreenViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMoreBinding.inflate(layoutInflater, container, false)
        setObserver()
        if (Utils.isConnected(requireContext())) {
            viewModel.setMoreScreenApiCall(
                MoreScreenEvent.ServiceListEvent(
                    prefManager.getString(
                        PrefConstant.PREF_CURRENT_LANGUAGE
                    )!!
                )
            )
        } else {
            Utils.showSnackBar(binding.mainView, requireContext())
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("key1")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "More-> $data")
//                if (data == "Who We Are") {
//                    whoWeAreOpenClose(false)
//                } else {
//                    servicesOpenClose(false)
//                }
//            }
//        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("ISO_COURSES")
//            ?.observe(viewLifecycleOwner) { data ->
//                Log.d(mTAG, "ISO_COURSES-> $data")
//                trainingCoursesOpenClose(false)
//            }
        initUi()

    }

    private fun initUi() {
        binding.viewWhoWeAre.setOnClickListener(this)
        binding.viewServices.setOnClickListener(this)
        binding.viewTrainingCourses.setOnClickListener(this)
        binding.viewCallUs.setOnClickListener(this)
        binding.viewFavoutirteJobs.setOnClickListener(this)
        binding.viewPrivacyPolicy.setOnClickListener(this)
        binding.viewTermsOfService.setOnClickListener(this)
        binding.viewLogOut.setOnClickListener(this)

        binding.txtAboutCompany.setOnClickListener(this)
        binding.txtDirectorSpeech.setOnClickListener(this)
        binding.txtVisionAndGoals.setOnClickListener(this)
        binding.txtClients.setOnClickListener(this)
        binding.clTrainingCourses.setOnClickListener(this)

        binding.clLanguage.setOnClickListener(this)
        binding.clWhoweare.setOnClickListener(this)
        binding.clOurService.setOnClickListener(this)
        binding.clConnectWithUs.setOnClickListener(this)
        binding.clShareTheApp.setOnClickListener(this)
        binding.clAboutTheApp.setOnClickListener(this)
        binding.clPrivacyPolicy.setOnClickListener(this)
        binding.clLogOff.setOnClickListener(this)

        if (!prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
            binding.viewLogOut.visibility = View.GONE
            binding.viewLine.visibility = View.GONE
            binding.txtLogOut.visibility = View.GONE

            binding.clLogOff.visibility = View.GONE

        }
    }


    private fun setObserver() {
        /** Services Goals */
        viewModel.serviceListResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<MoreScreenServiceResponseBean> -> {
                    displayProgressBar(false)
                    setData(dataState.data)

                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "LOGIN CUSTOM ERROR  ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message)
                    Log.d(mTAG, "LOGIN ERROR ERROR  ${dataState.exception.message}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun setData(data: MoreScreenServiceResponseBean) {

        binding.rvServices.layoutManager = LinearLayoutManager(requireContext())
        servicesListAdapter =
            ServicesListAdapter(requireContext(), this, data.services!!, prefManager)
        binding.rvServices.adapter = servicesListAdapter
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun logoutDialog(context: Context) {

        MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.logout_message)

            .setPositiveButton(R.string.lbl_ok) { dialog, _ ->
                prefManager.clearAll()
                prefManager.setBoolean(PrefConstant.PREF_IS_LOGIN, false)
                prefManager.setString(PrefConstant.PREF_CURRENT_LANGUAGE, "ar")
                dialog.dismiss()
                startActivity(Intent(requireActivity(), SplashActivity::class.java))
                requireActivity().finish()

            }
//            .setNeutralButton(R.string.lbl_cancel) { dialog, which ->
//                dialog.dismiss()
//            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
//            .setSingleChoiceItems(singleItems, checkedItem) { dialog, which ->
//                // Respond to item chosen
//            }
            .show()
    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.clLanguage -> {
                languageBottomSheet()
            }
            R.id.clLogOff -> {
                logoutDialog(requireContext())
            }

            R.id.clWhoweare -> {
                navController.navigate(
                    R.id.action_moreFragment_to_whoWeAreDetailsFragment
                )
            }

            R.id.clOurService -> {
                navController.navigate(R.id.action_moreFragment_to_servicesDetailsFragment)
            }

            R.id.clConnectWithUs -> {
                navController.navigate(
                    R.id.action_moreFragment_to_callUsFragment
                )
            }
            R.id.clShareTheApp -> {
                shareIt()
            }
            R.id.clAboutTheApp -> {
                val bundle = bundleOf(
                    Constant.INTENT.ABOUT to Constant.INTENT.ABOUT
                )
                navController.navigate(
                    R.id.action_moreFragment_to_moreScreenDetailsFragment,
                    bundle
                )
            }
            R.id.clPrivacyPolicy -> {
                val bundle = bundleOf(
                    Constant.INTENT.PRIVACY_POLICY to Constant.INTENT.PRIVACY_POLICY
                )
                navController.navigate(
                    R.id.action_moreFragment_to_CMSPageFragment,
                    bundle
                )
            }

            R.id.viewWhoWeAre -> {
                whoWeAreOpenClose(isWhoWeAreOpen)
                servicesOpenClose(true)
                trainingCoursesOpenClose(true)
            }
            R.id.viewServices -> {
                whoWeAreOpenClose(true)
                servicesOpenClose(isServicesOpen)
                trainingCoursesOpenClose(true)
            }
            R.id.viewTrainingCourses -> {
                trainingCoursesOpenClose(isTrainingCoursesOpen)
            }
            R.id.viewCallUs -> {
                navController.navigate(
                    R.id.action_moreFragment_to_callUsFragment
                )
            }
            R.id.viewFavoutirteJobs -> {
                navController.navigate(
                    R.id.action_moreFragment_to_favouriteJobsFragment
                )
            }
            R.id.viewPrivacyPolicy -> {
                val bundle = bundleOf(
                    Constant.INTENT.PRIVACY_POLICY to Constant.INTENT.PRIVACY_POLICY
                )
                navController.navigate(
                    R.id.action_moreFragment_to_CMSPageFragment,
                    bundle
                )
            }
            R.id.viewTermsOfService -> {
                val bundle = bundleOf(
                    Constant.INTENT.TERMS_CONDITION to Constant.INTENT.TERMS_CONDITION
                )
                navController.navigate(
                    R.id.action_moreFragment_to_CMSPageFragment,
                    bundle
                )
            }
            R.id.viewLogOut -> {
                logoutDialog(requireContext())
            }
            R.id.txtAboutCompany -> {
                val bundle = bundleOf(
                    Constant.INTENT.ABOUT to Constant.INTENT.ABOUT
                )
                navController.navigate(
                    R.id.action_moreFragment_to_moreScreenDetailsFragment,
                    bundle
                )
            }
            R.id.txtDirectorSpeech -> {
                val bundle = bundleOf(
                    Constant.INTENT.SPEECH to Constant.INTENT.SPEECH
                )
                navController.navigate(
                    R.id.action_moreFragment_to_moreScreenDetailsFragment,
                    bundle
                )
            }
            R.id.txtVisionAndGoals -> {
                val bundle = bundleOf(
                    Constant.INTENT.VISION_GOALS to Constant.INTENT.VISION_GOALS
                )
                navController.navigate(
                    R.id.action_moreFragment_to_moreScreenDetailsFragment,
                    bundle
                )
            }
            R.id.txtClients -> {
                navController.navigate(R.id.action_moreFragment_to_clientsListFragment)
            }

            R.id.txtServiceLbl -> {
                val pos = view.tag.toString().toInt()
                val bundle = bundleOf(
                    Constant.INTENT.SERVICE_TYPE to servicesListAdapter.listData[pos],
                )
                navController.navigate(
                    R.id.action_moreFragment_to_moreScreenDetailsFragment,
                    bundle
                )
            }
            R.id.clTrainingCourses -> {
                if (prefManager.getBoolean(PrefConstant.PREF_IS_LOGIN)) {
//                    navController.navigate(R.id.action_moreFragment_to_moreTrainingCoursesFragment)
                } else {
                    loginDialog(requireContext())
                }

            }
        }
    }

    private fun shareIt() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_SUBJECT, "Watania JobSeeker App")
        intent.putExtra(
            Intent.EXTRA_TEXT,
            "Apply on https://play.google.com/store/apps/details?id=com.fnrco.wataniajobs"
        )
        intent.type = "text/plain"
        startActivity(intent)
    }

    private fun whoWeAreOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.clWhoWeAre.visibility = View.GONE
            binding.imgWhoWeAreDropUp.setImageResource(R.drawable.ic_arrow_drop_down)
            isWhoWeAreOpen = false
        } else {
            binding.clWhoWeAre.visibility = View.VISIBLE
            binding.imgWhoWeAreDropUp.setImageResource(R.drawable.ic_arrow_drop_up)
            isWhoWeAreOpen = true
        }
    }

    private fun servicesOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.rvServices.visibility = View.GONE
            binding.imgServicesDropUp.setImageResource(R.drawable.ic_arrow_drop_down)
            isServicesOpen = false
        } else {
            binding.rvServices.visibility = View.VISIBLE
            binding.imgServicesDropUp.setImageResource(R.drawable.ic_arrow_drop_up)
            isServicesOpen = true
        }
    }

    private fun trainingCoursesOpenClose(isOpen: Boolean) {
        if (isOpen) {
            binding.clTrainingCourses.visibility = View.GONE
            binding.imgTrainingCoursesDropUp.setImageResource(R.drawable.ic_arrow_drop_down)
            isTrainingCoursesOpen = false
        } else {
            binding.clTrainingCourses.visibility = View.VISIBLE
            binding.imgTrainingCoursesDropUp.setImageResource(R.drawable.ic_arrow_drop_up)
            isTrainingCoursesOpen = true
        }
    }

    private fun loginDialog(context: Context) {
        val intent = Intent(requireContext(), LoginPopUpActivity::class.java)
        startActivity(intent)
        /*MaterialAlertDialogBuilder(context, R.style.MyAlertDialogTheme)
            .setTitle(R.string.app_name)
            .setMessage(R.string.title_login_dialog)

            .setPositiveButton(R.string.login_in) { dialog, _ ->
                context.startActivity(Intent(context, LoginActivity::class.java))
                dialog.dismiss()
            }
            .setNegativeButton(R.string.lbl_cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()*/
    }


    private fun languageBottomSheet() {
        val bottomSheetDialogSkill = BottomSheetDialog(
            requireActivity(), R.style.BottomsheetDialogThem
        )
        val bottomSheetViewSkill =
            LayoutInflater.from(requireActivity())
                .inflate(
                    R.layout.bottom_language_change,
                    requireActivity().findViewById<View>(R.id.constraintForWithDarawal) as ConstraintLayout?
                )

        val rbArabic: RadioButton = bottomSheetViewSkill.findViewById(R.id.rbArabic)
        val rbEnglish: RadioButton = bottomSheetViewSkill.findViewById(R.id.rbEnglish)
        val txtSave: TextView = bottomSheetViewSkill.findViewById(R.id.txtSave)
        val txtCancel: TextView = bottomSheetViewSkill.findViewById(R.id.txtCancel)

        if (prefManager.getString(PrefConstant.PREF_CURRENT_LANGUAGE) == "ar") {
            rbArabic.isChecked = true
            rbEnglish.isChecked = false
        } else {
            rbArabic.isChecked = false
            rbEnglish.isChecked = true
        }

        txtCancel.setOnClickListener {
            bottomSheetDialogSkill.dismiss()
        }
        txtSave.setOnClickListener {

            Utils.setLocale(requireActivity(), if (rbArabic.isChecked) "ar" else "en", "")
            bottomSheetDialogSkill.dismiss()
        }

        bottomSheetDialogSkill.setContentView(bottomSheetViewSkill)
        bottomSheetDialogSkill.show()
    }
}