package com.fnrco.wataniajobs.mvvm.ui.fragment

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.adapter.EmailListAdapter
import com.fnrco.wataniajobs.adapter.PhoneListAdapter
import com.fnrco.wataniajobs.common.SmsBroadcastReceiver
import com.fnrco.wataniajobs.databinding.FragmentEditProfileCommunicationBinding
import com.fnrco.wataniajobs.mvvm.adapter.CityDropDownListAdapter
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileStateEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.EditProfileViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.regex.Pattern
import javax.inject.Inject
import kotlin.random.Random

@ExperimentalCoroutinesApi
@AndroidEntryPoint
@WithFragmentBindings
class EditProfileCommunicationFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private val mTAG = this::class.java.simpleName
    private lateinit var binding: FragmentEditProfileCommunicationBinding
    private lateinit var navController: NavController
    private lateinit var profileResponseBean: ProfileResponseBean
    private lateinit var phoneListAdapter: PhoneListAdapter
    private lateinit var emailListAdapter: EmailListAdapter
    private lateinit var phoneListResponseBean: PhoneListResponseBean
    private lateinit var emailListResponseBean: PhoneListResponseBean
    private lateinit var phoneList: MutableList<Phone>
    private lateinit var emailList: MutableList<Phone>
    var code: String = ""
    var enterCode: String = ""
    private val REQ_USER_CONSENT = 200
    var smsBroadcastReceiver: SmsBroadcastReceiver? = null
    var isAddPhoneOpen = false
    var isAddEmailOpen = false
    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: EditProfileViewModel by viewModels()

    private lateinit var cityDropDownListAdapter: CityDropDownListAdapter

    private var gender = ""
    private var maritalStatus = ""
    private var addMobileNumber = ""
    private var type = ""
    var city: String = ""
    var existingPhoneNumber: String = ""
    var existingEmail: String = ""
    lateinit var bottomSheet: BottomSheetDialog
    lateinit var bottomConfirmedSheet: BottomSheetDialog

    lateinit var otpView: PinEntryEditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditProfileCommunicationBinding.inflate(layoutInflater)
        if (requireArguments().containsKey(Constant.PROFILE_DATA)) {
            profileResponseBean =
                requireArguments().getSerializable(Constant.PROFILE_DATA) as ProfileResponseBean
            existingPhoneNumber = profileResponseBean.user?.phone!!
            existingEmail = profileResponseBean.user?.email!!
        }
//        viewModel.setStateEvent(
//            EditProfileStateEvent.EditProfileCity(
//                prefManager.getString(
//                    PrefConstant.PREF_CURRENT_LANGUAGE
//                )!!
//            )
//        )
        initUi()
//        getProfileDataApiCall()
        setObserver()
        viewModel.setStateEvent(EditProfileStateEvent.ShowPhoneList)
        viewModel.setStateEvent(EditProfileStateEvent.ShowEmailList)

        return binding.root
    }

    private fun initRecyclerView(data: List<Phone>) {
        binding.apply {
            rvPhoneList.layoutManager = LinearLayoutManager(requireActivity())
            phoneListAdapter = PhoneListAdapter(requireContext(), data, ::onEditTextChanged, ::deletePhone)
            rvPhoneList.adapter = phoneListAdapter
        }
    }

    private fun initEmailRecyclerView(data: List<Phone>) {
        binding.apply {
            rvEmailList.layoutManager = LinearLayoutManager(requireActivity())
            emailListAdapter = EmailListAdapter(requireContext(), data, ::onEditTextChangedEmail, ::deleteEmail)
            rvEmailList.adapter = emailListAdapter
        }
    }

    private fun addPhone(){
        val phone = Phone("", 0, "", "phone", "", 0, true)
        phoneList.add(phone)
        initRecyclerView(phoneList)
        isAddPhoneOpen = true
        phoneAddSubmitButtonStatus(isAddPhoneOpen)
    }


    private fun addEmail(){
        val phone = Phone("", 0, "", "email", "", 0, true)
        emailList.add(phone)
        initEmailRecyclerView(emailList)
        isAddEmailOpen = true
        emailAddSubmitButtonStatus(isAddEmailOpen)
    }

    private fun emailAddSubmitButtonStatus(isAddOpen : Boolean) {
        binding.apply {
            clAddEmail.visibility = if(isAddOpen) View.INVISIBLE else View.VISIBLE
            txtSubmitEmail.visibility = if(isAddOpen) View.VISIBLE else View.INVISIBLE
        }
    }

    private fun phoneAddSubmitButtonStatus(isAddOpen : Boolean) {
        binding.apply {
            clAddPhone.visibility = if(isAddOpen) View.INVISIBLE else View.VISIBLE
            txtSubmitPhone.visibility = if(isAddOpen) View.VISIBLE else View.INVISIBLE
        }
    }


    private fun deletePhone(id : Int){
        Log.d("TAG", "deletePhone: $id")
        viewModel.setStateEvent(EditProfileStateEvent.DeletePhoneEmail(id.toString()))
    }

    private fun deleteEmail(id : Int){
        Log.d("TAG", "deleteEmail: $id")
        viewModel.setStateEvent(EditProfileStateEvent.DeletePhoneEmail(id.toString()))
    }

  private fun onEditTextChanged(number : String) {
      addMobileNumber = number
      type = "phone"
    }

    private fun onEditTextChangedEmail(email : String) {
        addMobileNumber = email
        type = "email"
    }

    private fun setObserver() {


        /** Delete Phone/email list   */
        viewModel.deletePhoneEmail.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SaveDeletePhoneEmailResponse> -> {
                    displayProgressBar(false)
                    viewModel.setStateEvent(EditProfileStateEvent.ShowPhoneList)
                    viewModel.setStateEvent(EditProfileStateEvent.ShowEmailList)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
        /** Show Phone list   */
        viewModel.showPhoneList.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<PhoneListResponseBean> -> {
                    displayProgressBar(false)
                    phoneListResponseBean = dataState.data
                    phoneList = phoneListResponseBean.phones!!.toMutableList()
                    val phone = Phone("", -1, existingPhoneNumber, "phone", "", 0, false)
                    phoneList.add(0, phone)
                    initRecyclerView(phoneList)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Show Phone list   */
        viewModel.showEmailList.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<PhoneListResponseBean> -> {
                    displayProgressBar(false)
                    emailListResponseBean = dataState.data
                    val phone = Phone("", -1, existingEmail, "email", "", 0, false)
                    emailList = emailListResponseBean.phones!!.toMutableList()
                    emailList.add(0, phone)
                    initEmailRecyclerView(emailList)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Save Delete Phone email list   */
        viewModel.saveDeletePhoneEmail.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<SaveDeletePhoneEmailResponse> -> {
                    displayProgressBar(false)
                    bottomSheet.dismiss()
                    showConfirmedBottomSheet(type)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Communication data info edit   */
        viewModel.communicationDataResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<EditCommunicationDataResponseBean> -> {
                    displayProgressBar(false)
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        "API_CALL", "API_CALL"
                    )
                    findNavController().popBackStack()
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /**User Profile data */
        viewModel.profileDataResponse.observe(requireActivity()) { dataState ->
            when (dataState) {
                is DataState.Success<ProfileResponseBean> -> {
                    displayProgressBar(false)
                    Log.d(mTAG, "setObserver: ${dataState.data}")
                    /*   listener?.setProfileScreenData(
                           dataState.data.user?.job_seeker?.cv!!, dataState.data.user?.email!!,
                           dataState.data.user?.job_seeker.phone!!
                       )*/
                    /**Communication Data Set*/
//                    binding.edtEmail.setText(dataState.data.user?.email)
//                    binding.edtMobileNumber.setText(dataState.data.user?.job_seeker?.phone)


                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }

        /** Send Verify Otp*/
        /** SignUp Api Call /  New User */
        viewModel.sendVerifyOtpResponse.observe(this) { dataState ->
            when (dataState) {
                is DataState.Success<SendVerifyOtpResponseBean> -> {
                    displayProgressBar(false)
                    showBottomSheet()
//                    startActivity(
//                        Intent(this, VerificationActivity::class.java)
//                            .putExtra(
//                                Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE,
//                                Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE
//                            )
//                            .putExtra(Constant.INTENT.PHONE_NO, authRequest.phone)
//                            .putExtra(Constant.INTENT.CODE, authRequest.code)
//                    )
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        }
    }

    private fun initUi() {
        binding.txtSave.setOnClickListener(this)
        binding.txtCancel.setOnClickListener(this)
        binding.clAddPhone.setOnClickListener(this)
        binding.clAddEmail.setOnClickListener(this)
        binding.txtSubmitPhone.setOnClickListener(this)
        binding.txtSubmitEmail.setOnClickListener(this)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
    }

    private fun getProfileDataApiCall() {
        viewModel.setStateEvent(
            EditProfileStateEvent.ProfileData(
                prefManager.getString(
                    PrefConstant.PREF_CURRENT_LANGUAGE
                )!!
            )
        )
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCancel -> {
                findNavController().popBackStack()
            }

            R.id.txtSave -> {
                if(type == "phone"){
                    if(addMobileNumber.isEmpty()){
                        Toast.makeText(requireActivity(), requireActivity().getString(R.string.error_enter_mobile_number), Toast.LENGTH_SHORT).show()
                        return
                    }
                    val rnd = Random
                    code = rnd.nextInt(111111, 999999).toString()
                    Log.d(mTAG, "number: $code")
                    val authRequest = AuthRequest()
                    authRequest.phone = addMobileNumber
                    authRequest.code = code
                    authRequest.type = Constant.USE_TYPE
                    viewModel.setStateEvent(EditProfileStateEvent.SendVerifyOTPPhone(authRequest))
                    isAddPhoneOpen = false
                    phoneAddSubmitButtonStatus(isAddPhoneOpen)
                }else {
                    if(addMobileNumber.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(addMobileNumber).matches()){
                        Toast.makeText(requireActivity(), requireActivity().getString(R.string.error_enter_valid_email), Toast.LENGTH_SHORT).show()
                        return
                    }
                    val rnd = Random
                    code = rnd.nextInt(111111, 999999).toString()
                    Log.d(mTAG, "number: $code")
                    val authRequest = AuthRequest()
                    authRequest.phone = addMobileNumber
                    authRequest.code = code
                    authRequest.type = Constant.USE_TYPE
                    viewModel.setStateEvent(EditProfileStateEvent.SendVerifyOTPPhone(authRequest))
                    isAddEmailOpen = false
                    emailAddSubmitButtonStatus(isAddEmailOpen)
                }

//                if (Utils.isConnected(requireContext())) {
//                    if (isCommunicationDataValid()) {
//                        viewModel.setStateEvent(
//                            EditProfileStateEvent.CommunicationDataEdit(
//                                binding.edtEmail.text.toString(),
//                                binding.edtMobileNumber.text.toString()
//                            )
//                        )
//                    }
//                } else {
//                    view?.let { Utils.showSnackBar(it, requireContext()) }
//                }
            }
            R.id. txtSubmitEmail ->{
                if(addMobileNumber.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(addMobileNumber).matches()){
                    Toast.makeText(requireActivity(), requireActivity().getString(R.string.error_enter_valid_email), Toast.LENGTH_SHORT).show()
                    return
                }
                val rnd = Random
                code = rnd.nextInt(111111, 999999).toString()
                Log.d(mTAG, "number: $code")
                val authRequest = AuthRequest()
                authRequest.phone = addMobileNumber
                authRequest.code = code
                authRequest.type = Constant.USE_TYPE
                viewModel.setStateEvent(EditProfileStateEvent.SendVerifyOTPPhone(authRequest))
                isAddEmailOpen = false
                emailAddSubmitButtonStatus(isAddEmailOpen)
            }
            R.id.clAddPhone ->{
                addPhone()
            }

            R.id.clAddEmail ->{
                addEmail()
            }
            R.id.txtSubmitPhone ->{
                if(addMobileNumber.isEmpty()){
                    Toast.makeText(requireActivity(), requireActivity().getString(R.string.error_enter_mobile_number), Toast.LENGTH_SHORT).show()
                    return
                }
                val rnd = Random
                code = rnd.nextInt(111111, 999999).toString()
                Log.d(mTAG, "number: $code")
                val authRequest = AuthRequest()
                authRequest.phone = addMobileNumber
                authRequest.code = code
                authRequest.type = Constant.USE_TYPE
                viewModel.setStateEvent(EditProfileStateEvent.SendVerifyOTPPhone(authRequest))
                isAddPhoneOpen = false
                phoneAddSubmitButtonStatus(isAddPhoneOpen)
            }
        }
    }


    private fun isCommunicationDataValid(): Boolean {
//        binding.inputMobileNumber.isErrorEnabled = false
//        binding.inputEmail.isErrorEnabled = false

        when {
//            binding.edtMobileNumber.text.toString().isEmpty() -> {
//                binding.edtMobileNumber.error =
//                    getString(R.string.error_enter_mobile_number)
//                binding.edtMobileNumber.requestFocus()
//                return false
//            }
//            binding.edtEmail.text.toString().isEmpty() -> {
//                binding.edtEmail.error =
//                    getString(R.string.error_enter_e_mail)
//                binding.edtEmail.requestFocus()
//                return false
//            }
        }

        return true
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed)
            View.VISIBLE
        else
            View.GONE
    }


    private fun startSmartUserConsent() {
        val client = SmsRetriever.getClient(requireActivity())
        client.startSmsUserConsent(null)

    }

    private fun registerBroadcastReceiver() {
        smsBroadcastReceiver = SmsBroadcastReceiver()
        smsBroadcastReceiver!!.smsBroadcastReceiverListener =
            object : SmsBroadcastReceiver.SmsBroadcastReceiverListener {
                override fun onSuccess(intent: Intent?) {
                    startActivityForResult(intent, REQ_USER_CONSENT)
                }

                override fun onFailure() {
                }
            }
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        requireActivity().registerReceiver(smsBroadcastReceiver, intentFilter)
    }

    override fun onStart() {
        super.onStart()
        registerBroadcastReceiver()
    }

    override fun onStop() {
        super.onStop()
        requireActivity().unregisterReceiver(smsBroadcastReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                val message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)
                getOtpFromMessage(message)
            }
        }
    }

    private fun getOtpFromMessage(message: String?) {
        val otpPatter = Pattern.compile("(|^)\\d{6}")
        val matcher = otpPatter.matcher(message)
        if (matcher.find()) {
            otpView.setText(matcher.group(0))
        }

    }

    private fun showBottomSheet() {
        bottomSheet = BottomSheetDialog(requireActivity())
        bottomSheet.setContentView(R.layout.bottom_sheet_ot_layout)

        val submit: Button? = bottomSheet.findViewById(R.id.btnSubmit)
        otpView = bottomSheet.findViewById(R.id.otp_view)!!
        val save: TextView? = bottomSheet.findViewById(R.id.txtSave)

        val txtCancel: TextView? = bottomSheet.findViewById(R.id.txtCancel)
        val txtTitle: TextView? = bottomSheet.findViewById(R.id.txtTitle)
        val txtSubTitle: TextView? = bottomSheet.findViewById(R.id.txtSubTitle)

        if(type == "phone"){
            txtTitle?.text = requireActivity().getString(R.string.please_confirm_your_mobile_number)
            txtSubTitle?.text = requireActivity().getString(R.string.the_confirmation_code_has_been_sent_to_the_newly_added_mobile_number_)
        }else {

            txtTitle?.text = requireActivity().getString(R.string.please_confirm_your_email_number)
            txtSubTitle?.text = requireActivity().getString(R.string.the_confirmation_code_has_been_sent_to_the_newly_added_email_)
        }
        txtCancel?.setOnClickListener {
            bottomSheet.dismiss()
        }
        submit!!.setOnClickListener {
            if (enterCode.isNotEmpty()) {
                verifyOtp()
            } else {
                displayError(getString(R.string.error_please_enter_6_digit_code))
            }
        }
        save!!.setOnClickListener {
            if (enterCode.isNotEmpty()) {
                verifyOtp()
            } else {
                displayError(getString(R.string.error_please_enter_6_digit_code))
            }
        }
        otpView.setOnPinEnteredListener(PinEntryEditText.OnPinEnteredListener { str ->
            if (str.length == 6) {
                enterCode = str.toString()
                verifyOtp()
            } else {
                Toast.makeText(requireActivity(), "FAIL", Toast.LENGTH_SHORT)
                    .show()
                otpView.text = null
            }
        })
        bottomSheet.show()
    }

    private fun verifyOtp() {
        Utils.print("Print-------Verification--------code----->$code------------->$enterCode")
        if (code == enterCode) {
//            Toast.makeText(requireActivity(), "SUCCESS", Toast.LENGTH_SHORT)
//                .show()
            bottomSheet.dismiss()

            viewModel.setStateEvent(EditProfileStateEvent.SaveDeletePhoneEmail(addMobileNumber, type))
        } else {
            Toast.makeText(
                requireActivity(),
                getString(R.string.error_code_not_match),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun showConfirmedBottomSheet(type : String) {
        bottomConfirmedSheet = BottomSheetDialog(requireActivity())
        bottomConfirmedSheet.setContentView(R.layout.bottom_succes_phone)

        val txtBackBottom : TextView = bottomConfirmedSheet.findViewById(R.id.txtBackBottom)!!
        val txtCancel : TextView = bottomConfirmedSheet.findViewById(R.id.txtCancel)!!
        val txtMobileNumberHasConfirmed : TextView = bottomConfirmedSheet.findViewById(R.id.txtMobileNumberHasConfirmed)!!
        val imgConfirmedImage : ImageView = bottomConfirmedSheet.findViewById(R.id.imgConfirmedImage)!!

        if(type == "phone"){
            imgConfirmedImage.setImageResource(R.drawable.img_confirmed_phone)
            txtMobileNumberHasConfirmed.text = requireActivity().getString(R.string.mobile_number_hase_been_save)
        }else {
            imgConfirmedImage.setImageResource(R.drawable.img_email_confirmed)
            txtMobileNumberHasConfirmed.text = requireActivity().getString(R.string.email_hase_been_save)
        }
        txtBackBottom.setOnClickListener {
            bottomConfirmedSheet.dismiss()
            viewModel.setStateEvent(EditProfileStateEvent.ShowPhoneList)
            viewModel.setStateEvent(EditProfileStateEvent.ShowEmailList)
        }
        txtCancel.setOnClickListener {
            bottomConfirmedSheet.dismiss()
            viewModel.setStateEvent(EditProfileStateEvent.ShowPhoneList)
            viewModel.setStateEvent(EditProfileStateEvent.ShowEmailList)
        }
        bottomConfirmedSheet.show()
    }
}