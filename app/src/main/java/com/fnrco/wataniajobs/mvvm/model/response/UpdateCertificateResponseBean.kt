package com.fnrco.wataniajobs.mvvm.model.response

data class UpdateCertificateResponseBean(
    val message: String? = null
)