package com.fnrco.wataniajobs.mvvm.model.response

data class JobPostApplyResponseBean(
    var message: String? = null
)