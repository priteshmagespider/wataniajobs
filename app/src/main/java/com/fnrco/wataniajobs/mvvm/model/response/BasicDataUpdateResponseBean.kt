package com.fnrco.wataniajobs.mvvm.model.response

data class BasicDataUpdateResponseBean(
    val message: String
)