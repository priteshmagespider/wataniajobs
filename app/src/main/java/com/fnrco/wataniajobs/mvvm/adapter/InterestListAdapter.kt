package com.fnrco.wataniajobs.mvvm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.mvvm.model.response.Interest
import com.fnrco.wataniajobs.mvvm.model.response.Skill

class InterestListAdapter(
    val context: Context,
    var onClickListener: View.OnClickListener,
    val list: List<Interest>?
) : RecyclerView.Adapter<InterestListAdapter.MyViewHolder>() {

    val listData: MutableList<Interest> = list as MutableList<Interest>

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtInterest: TextView = itemView.findViewById(R.id.txtInterest)
        val imgInterestDelete: ImageView = itemView.findViewById(R.id.imgInterestDelete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_interest_items, parent, false)
        )
    }

    fun deleteItem(pos: Int) {
        listData.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.imgInterestDelete.tag = position
        holder.imgInterestDelete.setOnClickListener(onClickListener)
        holder.txtInterest.text = listData[position].interest
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}