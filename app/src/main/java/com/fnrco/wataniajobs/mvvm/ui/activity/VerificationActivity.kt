package com.fnrco.wataniajobs.mvvm.ui.activity

import `in`.aabhasjindal.otptextview.OTPListener
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.fnrco.wataniajobs.R
import com.fnrco.wataniajobs.common.SmsBroadcastReceiver
import com.fnrco.wataniajobs.databinding.ActivityVerificationBinding
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.SendVerifyOtpResponseBean
import com.fnrco.wataniajobs.mvvm.viewmodel.SignUpEvent
import com.fnrco.wataniajobs.mvvm.viewmodel.SignUpViewModel
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Constant
import com.fnrco.wataniajobs.utils.DataState
import com.fnrco.wataniajobs.utils.Utils
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.phone.SmsRetriever
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import java.util.regex.Pattern
import javax.inject.Inject
import kotlin.random.Random
import com.alimuzaffar.lib.pin.PinEntryEditText
import com.alimuzaffar.lib.pin.PinEntryEditText.OnPinEnteredListener


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class VerificationActivity : AppCompatActivity(), View.OnClickListener {

    private val mTAG = this::class.java.simpleName
    private lateinit var binding: ActivityVerificationBinding

    private var lastClickTime: Long = 0
    lateinit var phone: String
    lateinit var email: String
    lateinit var code: String
    var enterCode: String = ""
    val authRequest = AuthRequest()

    private val REQ_USER_CONSENT =200
    var smsBroadcastReceiver:SmsBroadcastReceiver? = null

    @Inject
    lateinit var prefManager: PrefManager

    @ExperimentalCoroutinesApi
    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        when {
            intent.hasExtra(Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE) -> {
                phone = intent.extras!!.getString(Constant.INTENT.PHONE_NO)!!
                code = intent.extras!!.getString(Constant.INTENT.CODE)!!
                binding.txtNumber.text = phone
                binding.txtNotReceivedPhone.visibility = View.GONE
                Utils.print("Print-------Verification--------from_sign_up----->$phone------->$code")
            }
            intent.hasExtra(Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE) -> {
                phone = intent.extras!!.getString(Constant.INTENT.PHONE_NO)!!
                code = intent.extras!!.getString(Constant.INTENT.CODE)!!
                binding.txtNumber.text = phone
                binding.txtNotReceivedEmail.visibility = View.VISIBLE
                Utils.print("Print-------Verification--------from_forgot_for_phone----->$phone------->$code")
            }
            intent.hasExtra(Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_EMAIL) -> {
                email = intent.extras!!.getString(Constant.INTENT.EMAIL)!!
                code = intent.extras!!.getString(Constant.INTENT.CODE)!!
                binding.txtNumber.text = email
                binding.txtNotReceivedPhone.visibility = View.VISIBLE
                Utils.print("Print-------Verification--------from_forgot_for_email----->$email------->$code")
            }
        }

        setObserver()
        initUi()
        startSmartUserConsent()

    }

    private fun startSmartUserConsent() {
        val client = SmsRetriever.getClient(this)
        client.startSmsUserConsent(null)

    }

    private fun registerBroadcastReceiver(){
        smsBroadcastReceiver = SmsBroadcastReceiver()
        smsBroadcastReceiver!!.smsBroadcastReceiverListener = object : SmsBroadcastReceiver.SmsBroadcastReceiverListener{
            override fun onSuccess(intent: Intent?) {
                startActivityForResult(intent,REQ_USER_CONSENT)
            }
            override fun onFailure() {
            }
        }
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(smsBroadcastReceiver,intentFilter)
    }

    override fun onStart() {
        super.onStart()
        registerBroadcastReceiver()
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(smsBroadcastReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == REQ_USER_CONSENT){
            if(resultCode == RESULT_OK && data != null){
                val message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)
                getOtpFromMessage(message)
            }
        }
    }

    private fun getOtpFromMessage(message: String?) {
        val otpPatter = Pattern.compile("(|^)\\d{6}")
        val matcher  = otpPatter.matcher(message)
        if(matcher.find()){
            binding.otpView.setText(matcher.group(0))
        }

    }


    private fun initUi() {
        binding.btnVerify.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
        binding.txtResend.setOnClickListener(this)
        binding.txtNotReceivedEmail.setOnClickListener(this)
        binding.txtNotReceivedPhone.setOnClickListener(this)
        enterCode = binding.otpView.text.toString()
        binding.otpView.setOnPinEnteredListener(OnPinEnteredListener { str ->
            if (str.length == 6) {
                Utils.closeKeyBoard(this@VerificationActivity, binding.mainLayout)
                enterCode = str.toString()
                verifyOtp()
                Toast.makeText(this@VerificationActivity, "SUCCESS", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this@VerificationActivity, "FAIL", Toast.LENGTH_SHORT)
                    .show()
                binding.otpView.setText(null)
            }
        })



        /*enterCode = binding.otpView.otp!!
        binding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {

            }

            override fun onOTPComplete(otp: String) {
                if (otp.length == 6) {
                    Utils.closeKeyBoard(this@VerificationActivity, binding.mainLayout)
                    enterCode = otp
                    verifyOtp()
                }

            }
        }*/

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.txtNotReceivedEmail -> {
                val intent = Intent(
                    this,
                    ForgotPasswordActivity::class.java
                ).putExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_EMAIL,Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_EMAIL)
                startActivity(intent)
            }
            R.id.txtNotReceivedPhone -> {
                val intent = Intent(
                    this,
                    ForgotPasswordActivity::class.java
                ).putExtra(Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE,Constant.INTENT.VERIFICATION_TO_FORGOT_FOR_PHONE)
                startActivity(intent)
            }
            R.id.btnVerify -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                if (enterCode.isNotEmpty()) {
                    verifyOtp()
                } else {
                    displayError(getString(R.string.error_please_enter_6_digit_code))
                }
            }
            R.id.txtResend -> {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return
                }
                lastClickTime = SystemClock.elapsedRealtime()
                if (Utils.isConnected(this)) {

                    when {
                        intent.hasExtra(Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE) -> {
                            val rnd = Random
                            code = rnd.nextInt(111111, 999999).toString()
                            Log.d(mTAG, "number: $code")
                               authRequest.phone = phone
                            authRequest.code = code
                            authRequest.type = Constant.USE_TYPE
                            viewModel.setStateEvent(SignUpEvent.SendVerifyOTPPhone(authRequest))
                            Utils.print("Print-------Verification--------from_forgot_for_phone----->"+AuthRequest.toStringVerificationEmail(authRequest))
                        }
                        intent.hasExtra(Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE) -> {
                            val rnd = Random
                            code = rnd.nextInt(111111, 999999).toString()
                            Log.d(mTAG, "number: $code")
                            authRequest.phone = phone
                            authRequest.code = code
                            authRequest.type = Constant.USE_TYPE
                            viewModel.setStateEvent(SignUpEvent.SendVerifyOTPPhone(authRequest))
                            Utils.print("Print-------Verification--------from_forgot_for_phone----->"+AuthRequest.toStringVerificationPhone(authRequest))
                        }
                        intent.hasExtra(Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_EMAIL) -> {
                            val rnd = Random
                            code = rnd.nextInt(111111, 999999).toString()
                            Log.d(mTAG, "number: $code")
                            authRequest.phone = email
                            authRequest.code = code
                            authRequest.type = Constant.USE_TYPE
                            viewModel.setStateEvent(SignUpEvent.SendVerifyOTPPhone(authRequest))
                            Utils.print("Print-------Verification--------from_forgot_for_phone----->"+AuthRequest.toStringVerificationEmail(authRequest))
                        }
                    }
                } else {
                    Utils.showSnackBar(v, this)
                }

            }
        }
    }

    private fun setObserver() {
        /** Send Verify Otp*/
        /** SignUp Api Call /  New User */
        viewModel.sendVerifyOtpResponse.observe(this, { dataState ->
            when (dataState) {
                is DataState.Success<SendVerifyOtpResponseBean> -> {
                    displayProgressBar(false)
//                    displayError(dataState)
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }
                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }


    private fun verifyOtp() {
        Utils.print("Print-------Verification--------code----->$code------------->$enterCode")
        if (code == enterCode) {
            when {
                intent.hasExtra(Constant.INTENT.SIGN_UP_TO_VERIFICATION_FOR_PHONE) -> {
                    prefManager.setBoolean(PrefConstant.PREF_IS_LOGIN, true)
                    startActivity(Intent(this@VerificationActivity, HomeActivity::class.java))
                }
                intent.hasExtra(Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_PHONE) -> {
                    startActivity(
                        Intent(
                            this@VerificationActivity,
                            ResetPasswordActivity::class.java
                        )
                            .putExtra(
                                Constant.INTENT.INTENT_FROM_VERIFY_TO_RESET_PASSWORD,
                                Constant.INTENT.INTENT_FROM_VERIFY_TO_RESET_PASSWORD
                            )
                            .putExtra(Constant.INTENT.EMAIL, phone)
                    )
                }
                intent.hasExtra(Constant.INTENT.FORGOT_TO_VERIFICATION_FOR_EMAIL) -> {
                    startActivity(
                        Intent(
                            this@VerificationActivity,
                            ResetPasswordActivity::class.java
                        )
                            .putExtra(
                                Constant.INTENT.INTENT_FROM_VERIFY_TO_RESET_PASSWORD,
                                Constant.INTENT.INTENT_FROM_VERIFY_TO_RESET_PASSWORD
                            )
                            .putExtra(Constant.INTENT.EMAIL, email)
                    )
                }
            }
        } else {
            Toast.makeText(
                this,
                getString(R.string.error_code_not_match),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}