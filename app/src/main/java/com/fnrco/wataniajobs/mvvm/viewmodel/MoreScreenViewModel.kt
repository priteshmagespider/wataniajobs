package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.repository.MoreScreenRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class MoreScreenViewModel
@ViewModelInject
constructor(
    private val moreScreenRepository: MoreScreenRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    /** About */
    private val _aboutResponse: MutableLiveData<DataState<WhoWeAreResponseBean>> = MutableLiveData()

    val aboutResponse: LiveData<DataState<WhoWeAreResponseBean>>
        get() = _aboutResponse


    /** Speech */
    private val _speechResponse: MutableLiveData<DataState<WhoWeAreResponseBean>> =
        MutableLiveData()

    val speechResponse: LiveData<DataState<WhoWeAreResponseBean>>
        get() = _speechResponse


    /** Vision Goals */
    private val _visionGoalsResponse: MutableLiveData<DataState<WhoWeAreResponseBean>> =
        MutableLiveData()

    val visionGoalsResponse: LiveData<DataState<WhoWeAreResponseBean>>
        get() = _visionGoalsResponse

    /** Clients List */
    private val _clientsListResponse: MutableLiveData<DataState<ClientsListResponseBean>> =
        MutableLiveData()

    val clientsListResponse: LiveData<DataState<ClientsListResponseBean>>
        get() = _clientsListResponse

    /** Service List */
    private val _serviceListResponse: MutableLiveData<DataState<MoreScreenServiceResponseBean>> =
        MutableLiveData()

    val serviceListResponse: LiveData<DataState<MoreScreenServiceResponseBean>>
        get() = _serviceListResponse


    /** PrivacyPolicy */
    private val _privacyPolicyResponse: MutableLiveData<DataState<CMSPageResponseBean>> =
        MutableLiveData()

    val privacyPolicyResponse: LiveData<DataState<CMSPageResponseBean>>
        get() = _privacyPolicyResponse

    /** Terms Condition*/
    private val _termsConditionResponse: MutableLiveData<DataState<CMSPageResponseBean>> =
        MutableLiveData()

    val termsConditionResponse: LiveData<DataState<CMSPageResponseBean>>
        get() = _termsConditionResponse

    /** More ISO training Courses */
    private val _moreTrainingCourseResponse: MutableLiveData<DataState<TrainingCoursesMoreResponseBean>> =
        MutableLiveData()

    val moreTrainingCourseResponse: LiveData<DataState<TrainingCoursesMoreResponseBean>>
        get() = _moreTrainingCourseResponse

    /** Register training course */
    private val _registerCourseResponse: MutableLiveData<DataState<RegisterTrainingCourse>> =
        MutableLiveData()

    val registerCourseResponse: LiveData<DataState<RegisterTrainingCourse>>
        get() = _registerCourseResponse


    fun setMoreScreenApiCall(moreScreenEvent: MoreScreenEvent) {
        viewModelScope.launch {
            when (moreScreenEvent) {
                is MoreScreenEvent.AboutEvent -> {
                    moreScreenRepository.aboutCompanyApiCall(moreScreenEvent.lang)
                        .onEach {
                            _aboutResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.SpeechEvent -> {
                    moreScreenRepository.speechApiCall(moreScreenEvent.lang)
                        .onEach {
                            _speechResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }

                is MoreScreenEvent.VisionGoalsEvent -> {
                    moreScreenRepository.visionGoalsApiCall(moreScreenEvent.lang)
                        .onEach {
                            _visionGoalsResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.ClientListEvent -> {
                    moreScreenRepository.clientListApiCall(moreScreenEvent.lang)
                        .onEach {
                            _clientsListResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.ServiceListEvent -> {
                    moreScreenRepository.moreServiceList(moreScreenEvent.lang)
                        .onEach {
                            _serviceListResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.PrivacyPolicyEvent -> {
                    moreScreenRepository.privacyPolicyApiCall(moreScreenEvent.lang)
                        .onEach {
                            _privacyPolicyResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.TermsConditionEvent -> {
                    moreScreenRepository.termsConditionApiCall(moreScreenEvent.lang)
                        .onEach {
                            _termsConditionResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.MoreTrainingCoursesEvent -> {
                    moreScreenRepository.moreTrainingCoursesApiCall(moreScreenEvent.lang)
                        .onEach {
                            _moreTrainingCourseResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is MoreScreenEvent.RegisterTrainingCourseEvent -> {
                    moreScreenRepository.registerTrainingCourseApiCall(moreScreenEvent.trainingId)
                        .onEach {
                            _registerCourseResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
            }
        }
    }
}

sealed class MoreScreenEvent {

    data class AboutEvent(val lang: String) : MoreScreenEvent()

    data class SpeechEvent(val lang: String) : MoreScreenEvent()

    data class VisionGoalsEvent(val lang: String) : MoreScreenEvent()

    data class ClientListEvent(val lang: String) : MoreScreenEvent()

    data class ServiceListEvent(val lang: String) : MoreScreenEvent()

    data class PrivacyPolicyEvent(val lang: String) : MoreScreenEvent()

    data class TermsConditionEvent(val lang: String) : MoreScreenEvent()

    data class MoreTrainingCoursesEvent(val lang: String) : MoreScreenEvent()

    data class RegisterTrainingCourseEvent(val trainingId: Int) : MoreScreenEvent()
}