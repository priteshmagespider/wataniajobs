package com.fnrco.wataniajobs.mvvm.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fnrco.wataniajobs.mvvm.model.request.AuthRequest
import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.repository.SignUpRepository
import com.fnrco.wataniajobs.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class SignUpViewModel
@ViewModelInject
constructor(
    private val signUpRepository: SignUpRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {



    /** Signup Api Call */
    private val _signUpResponse: MutableLiveData<DataState<SignUpResponseBean>> = MutableLiveData()

    val signUpResponse: LiveData<DataState<SignUpResponseBean>>
        get() = _signUpResponse


    /** Signup Api Call */
    private val _updatePlayerResponse: MutableLiveData<DataState<UpdatePlayerResponseBean>> = MutableLiveData()

    val updatePlayerResponse: LiveData<DataState<UpdatePlayerResponseBean>>
        get() = _updatePlayerResponse

    /** Send Verify Otp  */
    private val _sendVerifyOtpResponse: MutableLiveData<DataState<SendVerifyOtpResponseBean>> =
        MutableLiveData()

    val sendVerifyOtpResponse: LiveData<DataState<SendVerifyOtpResponseBean>>
        get() = _sendVerifyOtpResponse

    private val _jobTitleList: MutableLiveData<DataState<JobRoleResponseBean>> =
        MutableLiveData()

    val jobTitleListResponse: LiveData<DataState<JobRoleResponseBean>>
        get() = _jobTitleList

    fun setStateEvent(signUpEvent: SignUpEvent) {
        viewModelScope.launch {
            when (signUpEvent) {
                is SignUpEvent.SignUpApiCall -> {
                    signUpRepository.signUpApiCall(signUpEvent.authRequest)
                        .onEach {
                            _signUpResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is SignUpEvent.UpdatePlayer -> {
                    signUpRepository.updatePlayer(signUpEvent.player_id)
                        .onEach {
                            _updatePlayerResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is SignUpEvent.SendVerifyOTPPhone -> {
                    signUpRepository.sendVerifyOtpForPhoneApiCall(signUpEvent.authRequest)
                        .onEach {
                            _sendVerifyOtpResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is SignUpEvent.SendVerifyOTPEmail -> {
                    signUpRepository.sendVerifyOtpForEmail(signUpEvent.authRequest)
                        .onEach {
                            _sendVerifyOtpResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }
                is SignUpEvent.JobTitleList -> {
                    signUpRepository.jobTitleList(signUpEvent.language)
                        .onEach {
                            _jobTitleList.value = it
                        }.launchIn(viewModelScope)
                }
                else -> {

                }
            }
        }
    }


}

sealed class SignUpEvent {

    object FetchCityList : SignUpEvent()
    data class JobTitleList(val language: String) : SignUpEvent()
    data class SignUpApiCall(val authRequest: AuthRequest) : SignUpEvent()

    data class SendVerifyOTPPhone(val authRequest: AuthRequest) : SignUpEvent()
    data class SendVerifyOTPEmail(val authRequest: AuthRequest) : SignUpEvent()
    data class UpdatePlayer(val player_id:String) : SignUpEvent()
}