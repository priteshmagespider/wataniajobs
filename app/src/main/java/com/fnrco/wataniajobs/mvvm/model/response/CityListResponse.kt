package com.fnrco.wataniajobs.mvvm.model.response

data class CityListResponse(
    var cities: List<CityList>? = null
)

data class CityList(
    var id: Int? = null,
    var name_ar: String? = null,
    var name_en: String? = null
)