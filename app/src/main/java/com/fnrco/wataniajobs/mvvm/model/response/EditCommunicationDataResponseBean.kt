package com.fnrco.wataniajobs.mvvm.model.response

data class EditCommunicationDataResponseBean(
    val email: String? = null,
    val phone: String? = null,
    val verified: String? = null
)