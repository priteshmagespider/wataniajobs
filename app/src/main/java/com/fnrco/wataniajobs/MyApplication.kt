package com.fnrco.wataniajobs

import android.app.Application
import android.content.Context
import com.fnrco.wataniajobs.pref.PrefConstant
import com.fnrco.wataniajobs.pref.PrefManager
import com.fnrco.wataniajobs.utils.Utils
import com.onesignal.OneSignal
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class MyApplication  : Application() {
//    private val ONESIGNAL_APP_ID = "96410bab-8685-4b8c-93bf-e2052b87a88a"
    private val ONESIGNAL_APP_ID = "61a7a430-c640-4795-b860-9df9412aaeef"

    @Inject
    lateinit var prefManager: PrefManager
    init {
        instance = this
    }

    companion object {
        private var instance: MyApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        // initialize for any

        // Use ApplicationContext.
        // example: SharedPreferences etc...
        val context: Context = MyApplication.applicationContext()

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);

        /*val oneSignalUserID = OneSignal.getDeviceState()!!.userId
        Utils.print("ONESIGNALID-------------------->$oneSignalUserID")
        if(oneSignalUserID!=null)
        {
            prefManager.setString(PrefConstant.PREF_ONE_SIGNAL_ID, oneSignalUserID)
        }*/


    }

}