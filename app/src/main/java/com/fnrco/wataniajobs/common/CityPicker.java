package com.fnrco.wataniajobs.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import com.fnrco.wataniajobs.R;
import com.fnrco.wataniajobs.mvvm.model.response.CityList;

import java.util.ArrayList;
import java.util.List;


public class CityPicker {
    Dialog d;
    Context context;
    SelectCategory selectCategory;
    private AlertDialog categoryDialog;
    List<CityList> categorieResults;
    List<String> CategoryName = new ArrayList<>();
    public CityPicker(Context context, SelectCategory selectCategory) {
        this.context = context;
        this.selectCategory = selectCategory;
    }

    public List<CityList> getSelectCategory() {
        return categorieResults;
    }

    public void setCategory(List<CityList> categorieResults, Boolean isArabic) {
        this.categorieResults = categorieResults;
        for (int i = 0; i < categorieResults.size(); i++) {
            CategoryName.add(isArabic ? categorieResults.get(i).getName_ar() : categorieResults.get(i).getName_en());
        }
    }


    public void showCategoryList() {
        /*List<String> mGenderlist = new ArrayList<String>();
        mGenderlist.add("Male");
        mGenderlist.add("Female");
        mGenderlist.add("Other");
*/
        //Create sequence of items
        final String[] mCategory = CategoryName.toArray(new String[CategoryName.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getString(R.string.select_location));
        dialogBuilder.setItems(mCategory, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                /*profselectedCountry = Animals[item].toString();  //Selected item in listview
                edtPCountry.setText(profselectedCountry);*/
                // selectBusType.(mBusType[item]);
                selectCategory.onSelectCategory(item);
            }
        });
        //Create alert dialog object via builder
        categoryDialog = dialogBuilder.create();
        //Show the dialog
        categoryDialog.show();

    }

    public interface SelectCategory {
        void onSelectCategory(int pos);
    }
}
