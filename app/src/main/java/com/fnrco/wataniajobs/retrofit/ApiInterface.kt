package com.fnrco.wataniajobs.retrofit


import com.fnrco.wataniajobs.mvvm.model.response.*
import com.fnrco.wataniajobs.utils.ApiConstant
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface {

    @Headers(
        "Accept: application/json",
        "Content-type:application/json"
    )

    @GET(ApiConstant.SEARCH_LIST + "/{language}")
    suspend fun cityList(
        @Path("language") language: String,
        @Query("city") city: Int,
        @Query("jobTitle") jobTitle: String
    ): SearchListResponseBean


//    @GET(ApiConstant.RECENT_SEARCH + "/{language}")
//    suspend fun recentSearchList(
//        @Path("language") language: String
//    ): RecentSearchListResponseBean

    @GET(ApiConstant.RECENT_JOBS + "/{language}")
    suspend fun recentSearchList(
        @Path("language") language: String
    ): RecentSearchListResponseBean

    @GET(ApiConstant.AUTOBIOGRAPHY + "/{language}")
    suspend fun autoBiographyList(
        @Path("language") language: String
    ): AutoBioGraphyResponseBean

    @POST(ApiConstant.JOB_POST_APPLIED)
    suspend fun jobPostApplied(@Query("post_id") post_id: Int): JobPostApplyResponseBean


    @POST(ApiConstant.ADD_FAVORITE_JOB)
    suspend fun addFavoriteJob(@Query("job_id") post_id: Int): FavoriteJobResponseBean

    @POST(ApiConstant.REMOVE_FAVORITE_JOB)
    suspend fun removeFavoriteJob(@Query("job_id") post_id: Int): RemoveFavoriteJobResponseBean


    @GET(ApiConstant.SINGLE_POST + "/{language}")
    suspend fun singlePost(
        @Path("language") language: String,
        @Query("post_id") post_id: Int
    ): SinglePostResponseBean


    @GET(ApiConstant.TOP_JOBS + "/{language}")
    suspend fun topJobs(
        @Path("language") language: String,
        @Query("page") page: Int
    ): TopJobsResponseBean


    @GET(ApiConstant.SEARCH_LIST + "/{language}")
    suspend fun searchList(
        @Path("language") language: String,
        @Query("jobTitle") jobTitle: String?,
        @Query("city") city: Int?,
        @Query("jobCat") jobCat: Int?,
        @Query("subCat") subCat: Int?,
        @Query("from") from: Int?,
        @Query("to") to: Int?,
        @Query("jobType[]") jobType: List<Int>,
        @Query("gender") gender: Int?,
        @Query("certificate") certificate: Int?,
        @Query("created_at") created_at: Int?,
        @Query("page") page: Int?,
    ): SearchListResponseBean


    @FormUrlEncoded
    @POST(ApiConstant.DELETE_JOB_POSTS)
    suspend fun deleteJobPost(
        @Field("post_id") post_id: Int
    ): DeleteJobPostResponseBean


    @FormUrlEncoded
    @POST(ApiConstant.DELETE_TRAINING_COURSES)
    suspend fun deleteTrainingCourses(
        @Field("training_id") training_id: Int
    ): DeleteJobPostResponseBean


    @GET(ApiConstant.SPECIALIZATIONS + "/{language}")
    suspend fun specializationsList(@Path("language") language: String): SpecializationResponseBean

    @GET(ApiConstant.FAVOURITE_JOBS + "/{language}")
    suspend fun favouriteJobsList(@Path("language") language: String): FavouriteJobResponseBean


    @GET(ApiConstant.CV_EXCELLENCE)
    suspend fun cvExcellence(): CvExcellenceResponseBean


    @GET(ApiConstant.CATEGORIES_API + "/{language}")
    suspend fun categoriesList(@Path("language") language: String): CategoriesListResponseBean

    @GET(ApiConstant.SPECIALIZATIONS + "/{language}")
    suspend fun basicDataCategoriesList(@Path("language") language: String): BasicDataSpecializationResponse


    @GET(ApiConstant.JOB_POSTS_APPLIED + "/{language}")
    suspend fun jobPostAppliedList(@Path("language") language: String): AppliedJobResponseBean


    @GET(ApiConstant.USER_TRAINING_COURSE + "{language}")
    suspend fun trainingCoursesList(@Path("language") language: String): TrainingCoursesMoreResponseBean


    @GET(ApiConstant.EXPIRED_JOB_APPLICATION + "/{language}")
    suspend fun expiredJobApplicationList(@Path("language") language: String): ExpiredJobResponseBean


    @GET(ApiConstant.ROLE + "/{language}")
    suspend fun roleList(@Path("language") language: String): JobRoleResponseBean


    @GET(ApiConstant.QUALIFICATION + "/{language}")
    suspend fun qualificationList(@Path("language") language: String): QualificationResponseBean


    @GET(ApiConstant.CITY_LIST + "/{language}")
    suspend fun cityList(@Path("language") language: String): CityListResponse


    @GET(ApiConstant.JOB_TYPE_LIST + "/{language}")
    suspend fun jobTypeList(@Path("language") language: String): JobTypeResponseBean


    /** Login Api Call */
    @FormUrlEncoded
    @POST(ApiConstant.LOG_IN)
    suspend fun logIn(
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("username") username: String,
        @Field("password") password: String
    ): LoginResponseBean


    /** SignUp Api Call */
    @Multipart
    @POST(ApiConstant.SIGN_UP)
    suspend fun signUp(
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("job_title") job_title: RequestBody,
        @Part("job_title_en") job_title_en: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part fileCv: MultipartBody.Part
    ): SignUpResponseBean


    /** SignUp Api Call */
    @Multipart
    @POST(ApiConstant.UPDATE_PLAYER)
    suspend fun updatePlayer(
        @Part("player_id") player_id: RequestBody,
    ): UpdatePlayerResponseBean


    /** SignUp Api Call */
    @Multipart
    @POST(ApiConstant.UPLOAD_CV)
    suspend fun uploadCV(
        @Part file: MultipartBody.Part
    ): UploadCvResponseBean


    /** Send Verify Otp */

    @FormUrlEncoded
    @POST(ApiConstant.SEND_VERIFY_OTP)
    suspend fun sendVerifyOtpForPhone(
        @Field("phone") phone: String,
        @Field("code") code: String,
        @Field("type") type: String
    ): SendVerifyOtpResponseBean


    @FormUrlEncoded
    @POST(ApiConstant.SEND_VERIFY_OTP)
    suspend fun sendVerifyOtpForEmail(
        @Field("email") email: String,
        @Field("code") code: String,
        @Field("type") type: String
    ): SendVerifyOtpResponseBean


    /** Reset password Api */
    @FormUrlEncoded
    @POST(ApiConstant.FORGOT_PASSWORD)
    suspend fun resetPassword(
        @Field("password") password: String,
        @Field("email") email: String,
        @Field("is_admin") is_admin: String
    ): ResetPasswordResponseBean


    /*@GET("clinics/{id}/doctors")
    suspend fun doctorList(
        @Path("id") id: Int?,
        @Query("page") page: Int,
        @Query("date") date: String,
        @Query("services") services: String,
        @Query("consultation") consultation: Int,
        @Query("search") search: String

    ): ChooseDoctorResponseBean*/


    /** Who We Are API Call */
    /** About Company */
    @GET(ApiConstant.ABOUT_COMPANY + "{lang}")
    suspend fun aboutCompany(@Path("lang") lang: String): WhoWeAreResponseBean

    /** Speech Company */
    @GET(ApiConstant.SPEECH + "{lang}")
    suspend fun speech(@Path("lang") lang: String): WhoWeAreResponseBean

    /** Vision goals Company */
    @GET(ApiConstant.VISION_MISSION + "{lang}")
    suspend fun visionGoals(@Path("lang") lang: String): WhoWeAreResponseBean

    /** Clients  */
    @GET(ApiConstant.CLIENTS + "{lang}")
    suspend fun clientList(@Path("lang") lang: String): ClientsListResponseBean


    /** More screen Service Api Call */
    @GET(ApiConstant.MORE_SCREEN_SERVICES + "{lang}")
    suspend fun moreServices(
        @Path("lang") language: String
    ): MoreScreenServiceResponseBean

    /** PrivacyPolicy */
    @GET(ApiConstant.PRIVACY_POLICY + "{lang}")
    suspend fun privacyPolicy(@Path("lang") lang: String): CMSPageResponseBean

    /** Terms Condition */
    @GET(ApiConstant.TERM_CONDITION + "{lang}")
    suspend fun termsCondition(@Path("lang") lang: String): CMSPageResponseBean

    /** Call us /Contact Us Api Call*/
    @FormUrlEncoded
    @POST(ApiConstant.CONTACT_US)
    suspend fun callUsApiCall(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("message") message: String,
        @Field("type") type: Int
    ): CallUsResponseBean


    /** Notification */
    @GET(ApiConstant.NOTIFICATION + "{lang}")
    suspend fun notificationList(@Path("lang") lang: String): NotificationListResponseBean


    @GET(ApiConstant.PROFILE + "{lang}")
    suspend fun profileDataList(@Path("lang") lang: String): ProfileResponseBean


    /**Change Password */
    @FormUrlEncoded
    @POST(ApiConstant.CHANGE_PASSWORD)
    suspend fun changePassword(
        @Field("curPassword") curPassword: String,
        @Field("newPassword") newPassword: String
    ): ChangePasswordResponseBean

    /**  Personal Info Edit Api call */

    @FormUrlEncoded
    @POST(ApiConstant.EDIT_PERSONAL_INFO)
    suspend fun personalInfoEdit(
        @Field("address") address: String,
        @Field("city") city: String,
        @Field("gender") gender: String,
        @Field("birthDate") birthDate: String,
        @Field("Dependents") Dependents: String,
        @Field("marital_status") marital_status: String,
        @Field("experience") experience: String,
        @Field("salary") salary: String
    ): EditPersonalInfoResponseBean

    /** Communication Data Edit Api Call*/
    @FormUrlEncoded
    @POST(ApiConstant.EDIT_COMMUNICATION_DATA)
    suspend fun communicationDataEdit(
        @Field("email") email: String,
        @Field("phone") phone: String,
    ): EditCommunicationDataResponseBean

    /** Edit Education Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_EDUCATION)
    suspend fun addEducation(
        @Field("educational_institution") educational_institution: String,
        @Field("general_appreciation") general_appreciation: String,
        @Field("specialization") specialization: String,
        @Field("degree") degree: String,
        @Field("end_date_educational") end_date_educational: String,
        @Field("description_education") description_education: String,
        @Field("id") id: Int
    ): EditEducationResponseBean

    /** Edit Education Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.EDIT_EDUCATION)
    suspend fun editEducation(
        @Field("educational_institution") educational_institution: String,
        @Field("general_appreciation") general_appreciation: String,
        @Field("specialization") specialization: String,
        @Field("degree") degree: String,
        @Field("end_date_educational") end_date_educational: String,
        @Field("description_education") description_education: String,
        @Field("id") id: String,
    ): EditEducationResponseBean

    /** Delete Education Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_EDUCATION)
    suspend fun deleteEducation(
        @Field("edu_id") edu_id: String
    ): DeleteEducationResponseBean


    /** Add Experience Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_EXPERIENCE)
    suspend fun addExperience(
        @Field("position_in_company") position_in_company: String,
        @Field("company_name") company_name: String,
        @Field("location") location: String,
        @Field("start_jop_M") start_jop_M: String,
        @Field("start_jop_Y") start_jop_Y: String,
        @Field("end_jop_M") end_jop_M: String,
        @Field("end_jop_Y") end_jop_Y: String,
        @Field("to_present") to_present: String,
        @Field("description_job") description_job: String,
    ): AddEditDeleteExperienceResponseBean


    /** Add Experience Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_EXPERIENCE)
    suspend fun deleteExperience(
        @Field("exp_id") exp_id: String
    ): AddEditDeleteExperienceResponseBean

    /** Edit Experience Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.EDIT_EXPERIENCE)
    suspend fun editExperience(
        @Field("position_in_company") position_in_company: String,
        @Field("company_name") company_name: String,
        @Field("location") location: String,
        @Field("start_jop_M") start_jop_M: String,
        @Field("start_jop_Y") start_jop_Y: String,
        @Field("end_jop_M") end_jop_M: String,
        @Field("end_jop_Y") end_jop_Y: String,
        @Field("to_present") to_present: String,
        @Field("description_job") description_job: String,
        @Field("id") id: Int,
    ): AddEditDeleteExperienceResponseBean


    /** Edit Experience Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_EDIT_SHORT_DESCRIPTION)
    suspend fun addEditShortDescription(
        @Field("career_objective") career_objective: String
    ): EditEducationResponseBean


    /** Edit Experience Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_SKILL)
    suspend fun addSkill(
        @Field("skills_name") skills_name: String
    ): AddDeleteSkillResponseBean

    /** Edit Experience Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_SKILL)
    suspend fun deleteSkill(
        @Field("skill_id") skill_id: String
    ): AddDeleteSkillResponseBean


    /** Add Interest Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_INTEREST)
    suspend fun addInterest(
        @Field("interest") interest: String
    ): AddDeleteInterestResponseBean

    /** Delete Interest Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_INTEREST)
    suspend fun deleteInterest(
        @Field("interest_id") interest_id: String
    ): AddDeleteInterestResponseBean


    /** Add Courses Api call*/
    @Multipart
    @POST(ApiConstant.ADD_COURSES)
    suspend fun addCourses(
        @Part("address") address: RequestBody,
        @Part("center_name") center_name: RequestBody,
        @Part("action_date") action_date: RequestBody,
        @Part("count_times") count_times: RequestBody,
        @Part("description") description: RequestBody,
        @Part image: MultipartBody.Part
//        @Field("address") address: String,
//        @Field("center_name") center_name: String,
//        @Field("action_date") action_date: String,
//        @Field("count_times") count_times: String,
//        @Field("description") description: String,
//        @Field("id") id: Int
    ): AddEditDeleteCoursesResponseBean

    /** Edit Courses with logo Api call*/
    @Multipart
    @POST(ApiConstant.EDIT_COURSES)
    suspend fun editCourses(
        @Part("address") address: RequestBody,
        @Part("center_name") center_name: RequestBody,
        @Part("action_date") action_date: RequestBody,
        @Part("count_times") count_times: RequestBody,
        @Part("description") description: RequestBody,
        @Part("course_id") course_id: RequestBody,
        @Part image: MultipartBody.Part
    ): AddEditDeleteCoursesResponseBean

    /** Edit Courses without logo Api call*/
    @Multipart
    @POST(ApiConstant.EDIT_COURSES)
    suspend fun editCoursesWithoutLogo(
        @Part("address") address: RequestBody,
        @Part("center_name") center_name: RequestBody,
        @Part("action_date") action_date: RequestBody,
        @Part("count_times") count_times: RequestBody,
        @Part("description") description: RequestBody,
        @Part("course_id") course_id: RequestBody,
    ): AddEditDeleteCoursesResponseBean


    /** Delete Course Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_COURSE)
    suspend fun deleteCourse(
        @Field("course_id") course_id: Int
    ): AddEditDeleteCoursesResponseBean


    /** Add Certificate Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_CERTIFICATE)
    suspend fun addCertificate(
        @Field("title") title: String,
        @Field("from_by") from_by: String,
        @Field("month") month: String,
        @Field("year") year: String,
        @Field("description") description: String,
    ): AddEditDeleteCertificateResponseBean

    /** Edit Certificate Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.EDIT_CERTIFICATE)
    suspend fun editCertificate(
        @Field("title") title: String,
        @Field("from_by") from_by: String,
        @Field("month") month: String,
        @Field("year") year: String,
        @Field("description") description: String,
        @Field("certificate_id") certificate_id: Int
    ): UpdateCertificateResponseBean

    @FormUrlEncoded
    @POST(ApiConstant.DELETE_CERTIFICATE)
    suspend fun deleteCertificate(
        @Field("certificate_id") certificate_id: Int
    ): AddEditDeleteCertificateResponseBean

    /** Add Project Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.ADD_PROJECT)
    suspend fun addProject(
        @Field("title") title: String,
        @Field("month") month: String,
        @Field("year") year: String,
        @Field("description") description: String
    ): AddEditDeleteProjectResponseBean

    /** Edit Project Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.EDIT_PROJECT)
    suspend fun editProject(
        @Field("title") title: String,
        @Field("month") month: String,
        @Field("year") year: String,
        @Field("description") description: String,
        @Field("project_id") certificate_id: Int
    ): AddEditDeleteProjectResponseBean

    /** Delete Project Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_PROJECT)
    suspend fun deleteProject(
        @Field("project_id") certificate_id: Int
    ): AddEditDeleteProjectResponseBean

    /** Computer level  */
    @GET(ApiConstant.COMPUTER_LEVEL + "{lang}")
    suspend fun computerLevelList(@Path("lang") lang: String): ComputerLevelResponseBean

    /** Add Language Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.SAVE_LANGUAGE)
    suspend fun saveLanguage(
        @Field("language") title: String,
        @Field("level") month: String
    ): LanguageAddDeleteResponseBean

    /** Delete Language Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.DELETE_LANGUAGE)
    suspend fun deleteLanguage(
        @Field("id") title: Int
    ): LanguageAddDeleteResponseBean

    /** Social Updatet Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.UPDATE_SOCIAL)
    suspend fun updateSocial(
        @Field("fb") fb: String,
        @Field("twitter") twitter: String,
        @Field("linkedIn") linkedIn: String,
        @Field("website") website: String,
        @Field("youtube") youtube: String,
        @Field("instagram") instagram: String,
    ): SocialResponseBean

    /** Basic Data Update Api call*/
    @FormUrlEncoded
    @POST(ApiConstant.UPDATE_BASIC_INFO)
    suspend fun updateBasicData(
        @Field("name") name: String,
        @Field("job_title_en") job_title_en: String,
        @Field("job_title") job_title: String,
        @Field("specilization") specilization: Int,
        @Field("jobRole") jobRole: Int,
        @Field("job_status") job_status: Int
    ): BasicDataUpdateResponseBean

    /** Update User Profile */
    @Multipart
    @POST(ApiConstant.UPDATE_BASIC_INFO)
    suspend fun updateUserImage(
        @Part photo: MultipartBody.Part
    ): BasicDataUpdateResponseBean

    /** More ISO training Courses */
    @GET(ApiConstant.MORE_TRAINING_COURSES + "{lang}")
    suspend fun moreTrainingCourses(@Path("lang") lang: String): TrainingCoursesMoreResponseBean

    /** Register training course */
    @FormUrlEncoded
    @POST(ApiConstant.REGISTER_TRAINING_COURSE)
    suspend fun registerCourse(
        @Field("training_id") training_id: Int
    ): RegisterTrainingCourse

    /** Home Banner Response */
    @GET(ApiConstant.HOME_BANNER + "{lang}")
    suspend fun homeBanner(
        @Path("lang") lang: String
    ): WeAreHiringResponseBean


    @GET(ApiConstant.SHOW_PHONES)
    suspend fun phoneList(
        @Query("type") type : String
    ): PhoneListResponseBean

    @GET(ApiConstant.SHOW_PHONES)
    suspend fun emailList(
        @Query("type") type : String
    ): PhoneListResponseBean

    @FormUrlEncoded
    @POST(ApiConstant.SAVE_DELETE_PHONE_EMAIL)
    suspend fun phoneEmailSaveDelete(
        @Field("phone") phone : String,
        @Field("type") type : String
    ) : SaveDeletePhoneEmailResponse

    @FormUrlEncoded
    @POST(ApiConstant.DELETE_PHONE_EMAIL)
    suspend fun deletePhoneEmail(
        @Field("id") id : String,
    ) : SaveDeletePhoneEmailResponse


    @GET(ApiConstant.LANGUAGE_LIST + "{language}")
    suspend fun languageList(@Path("language") language: String): LanguageListResponseBean


    @FormUrlEncoded
    @POST(ApiConstant.DELETE_NOTIFICATION)
    suspend fun deleteNotification(
        @Field("notification_id") notification_id : String,
    ) : DeleteJobPostResponseBean


}
