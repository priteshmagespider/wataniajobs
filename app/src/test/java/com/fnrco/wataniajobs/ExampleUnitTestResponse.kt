package com.fnrco.wataniajobs

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTestResponse {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}